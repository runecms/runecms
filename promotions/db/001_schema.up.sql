CREATE TABLE IF NOT EXISTS campaigns_campaign_types (
  id INTEGER NOT NULL PRIMARY KEY,
  name STRING NOT NULL,
  description STRING
);

INSERT INTO 
  campaigns_campaign_types (
    id,
    name,
    description
  )
  VALUES (
    0,
    'subscription',
    'Subscription campaigns are campaigns which offer a subscription to a publication as the product'
  ), (
    1,
    'multi',
    'A multi-campaign is a campaign that has multiple products'
  );

CREATE TABLE IF NOT EXISTS campaigns (
  id UUID PRIMARY KEY default gen_random_uuid(),
  -- market is a the corporate market running this campaign (see rune.subscriptions.markets)?
  market_id UUID NOT NULL,

  -- created_by is the user who created this campaign
  created_by UUID NOT NULL,

  -- name is something readable and unique to this campaign to display in the interface. it should be generated (by Campaign.Create()), not manually entered
  name STRING NOT NULL,

  -- slug is what is shown in the URL for this campaign. it should have a sensible default, but be overridable
  slug STRING NOT NULL UNIQUE,

  -- allowed_domains is a comma delimited list of domains which are allowed to access this campaign
  -- Not exactly compatible with OAuth2 but we'll figure it out...
  allowed_domains STRING,

  is_preview BOOLEAN NOT NULL DEFAULT TRUE,
  start_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  end_at TIMESTAMP WITH TIME ZONE,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  deleted_at TIMESTAMP WITH TIME ZONE,
  campaign_type INTEGER REFERENCES campaigns_campaign_types(id) NOT NULL
);

-- This is not built into the campaigns table under the impression that
-- in the future we may offer campaigns that are not subscription-based at all
-- We could offer products, or even pair up with other services and offer unique campaigns
-- Spotify example: we would create `campaigns_spotify_campaigns`, maybe
CREATE TABLE IF NOT EXISTS campaigns_subscription_campaigns (
  -- campaign_id refers to a single campgin, designating that this subscription_campaign is
  -- essentially an extension of that campaign campaign_id UUID REFERENCES campaigns(id) NOT NULL UNIQUE,
  id UUID REFERENCES campaigns(id) NOT NULL UNIQUE,
  -- Mostly a metadata field
  is_promotional BOOLEAN DEFAULT TRUE,

  -- Should we bill the user immediately?
  bill_immediately BOOLEAN DEFAULT TRUE,

  -- collect_token defines whether or not we collect the billing token.  Collect token nullifies `bill_immediately 
  collect_token BOOLEAN DEFAULT TRUE,

  -- Comma delimited list of zip codes that are allowed to use this promotion
  allowed_zipcodes STRING,

  -- Page data
  header STRING,
  subheader STRING,
  fine_print STRING,
  extra_js STRING,
  extra_css STRING,

  logo_url STRING,
  background_image_url STRING,

  -- Success page data
  success_header STRING,
  success_subheader STRING,
  success_body STRING,
  success_js STRING,
  success_css STRING,

  -- the publication contains the billing information to bill this campaign
  publication_id UUID NOT NULL,

  -- the product_id will be generated upon the creation of a new subscription campaign
  product_id UUID NOT NULL
);


-- Whenever are managing the billing, this will
-- become part of the product or subscription_type or something
-- For now this is just informational
CREATE TABLE IF NOT EXISTS campaigns_subscription_frequencies (
  descriptor STRING UNIQUE,
  singular STRING,
  plural STRING
);
INSERT INTO
  campaigns_subscription_frequencies (
    descriptor,
    singular,
    plural
  )
VALUES (
  'daily',
  'day',
  'days'
),(
  'weekly',
  'week',
  'weeks'
),(
  'monthly',
  'month',
  'months'
),(
  'yearly',
  'year',
  'years'
);

-- an individual rate on a single campaign
-- this might refer to:
--  $10 for the first month
--  $10 for the first 3 weeks
--  $200 for the next 2 years
--  $10 forever
CREATE TABLE IF NOT EXISTS campaigns_subscription_campaign_rates (
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  -- Weight determines the order relative to the other rates associated with this campaign
  weight INTEGER NOT NULL,
  subscription_campaign_id UUID REFERENCES campaigns_subscription_campaigns(id) NOT NULL,
  price INTEGER NOT NULL,
  -- example: Every 2 weeks
  -- 2
  billing_term INTEGER NOT NULL,
  -- weeks
  billing_frequency STRING REFERENCES campaigns_subscription_frequencies(descriptor) NOT NULL,

  -- example: for 3 months
  -- 3
  campaign_term INTEGER,
  -- months
  campaign_frequency STRING REFERENCES campaigns_subscription_frequencies(descriptor)
);

-- multi_campaigns are campaigns with multiple campaign options
-- like "Print", "Online", "Daypass"

-- Essentially this replaces "voluntary" in old promobuilder
CREATE TABLE IF NOT EXISTS campaigns_multi_campaigns (
  -- This is the specific campaign for rendering this multicampaign
  id                    UUID REFERENCES campaigns(id) NOT NULL UNIQUE,
  fine_print            STRING,
  extra_js              STRING,
  extra_css             STRING,
  logo_url              STRING,
  background_image_url  STRING
);

-- maps multi_campaigns to campaigns
CREATE TABLE IF NOT EXISTS campaigns_multi_campaigns_campaigns (
  id UUID PRIMARY KEY default gen_random_uuid(),
  multi_campaign_id UUID REFERENCES campaigns_multi_campaigns(id) NOT NULL,
  campaign_id UUID REFERENCES campaigns(id) NOT NULL,
  content STRING NOT NULL
);
