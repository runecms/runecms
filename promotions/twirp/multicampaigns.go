// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package promotionstwirp

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/promotions/internal/campaigns"
	"git.runecms.io/rune/runecms/promotions/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreateMultiCampaign(c context.Context, req *promotionspb.CreateMultiCampaignRequest) (*promotionspb.CreateMultiCampaignResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	campaign, err := campaigns.CreateMultiCampaignRequest(
		c,
		s.DB,
		s.Warden,
		user,
		s.Markets,
		s.Products,
		s.Publications,
		s.Webhooks,
		req,
	)
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &promotionspb.CreateMultiCampaignResponse{
		MultiCampaign: campaign.GetMultiCampaign(),
	}, nil
}

func (s *Server) UpdateMultiCampaign(c context.Context, req *promotionspb.UpdateMultiCampaignRequest) (*promotionspb.UpdateMultiCampaignResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	campaign, err := campaigns.UpdateMultiCampaign(
		c,
		s.DB,
		s.Warden,
		user,
		s.Webhooks,
		req,
	)
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &promotionspb.UpdateMultiCampaignResponse{
		Campaign: campaign.GetMultiCampaign(),
	}, nil
}
