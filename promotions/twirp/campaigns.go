// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package promotionstwirp

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/promotions/internal/campaigns"
	"git.runecms.io/rune/runecms/promotions/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) GetCampaignById(c context.Context, req *promotionspb.GetCampaignByIdRequest) (*promotionspb.GetCampaignByIdResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	campaign, err := campaigns.GetCampaignById(c, s.DB, s.Warden, user, req.GetId())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &promotionspb.GetCampaignByIdResponse{
		Campaign: campaign,
	}, nil
}

func (s *Server) GetCampaignBySlug(c context.Context, req *promotionspb.GetCampaignBySlugRequest) (*promotionspb.GetCampaignBySlugResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	campaign, err := campaigns.GetCampaignBySlug(c, s.DB, s.Warden, user, req.GetSlug())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &promotionspb.GetCampaignBySlugResponse{
		Campaign: campaign,
	}, nil
}

func (s *Server) DeleteCampaign(c context.Context, req *promotionspb.DeleteCampaignRequest) (*promotionspb.DeleteCampaignResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	campaign, err := campaigns.DeleteCampaign(c, s.DB, s.Warden, user, req.GetId())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &promotionspb.DeleteCampaignResponse{
		Campaign: campaign,
	}, nil
}
