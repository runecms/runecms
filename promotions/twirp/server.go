// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package promotionstwirp

import (
	"database/sql"

	"git.runecms.io/rune/runecms/internal/twirputil"
	"git.runecms.io/rune/runecms/promotions/rpc"
	"git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/gorilla/mux"
	"github.com/ory/ladon"
)

type Server struct {
	DB            *sql.DB
	Warden        *ladon.Ladon
	Webhooks      webhookspb.Webhooks
	Subscriptions subscriberspb.Subscriptions
	Markets       subscriberspb.Markets
	Products      subscriberspb.Products
	Publications  subscriberspb.Publications
}

func NewServer(db *sql.DB,
	w *ladon.Ladon,
	marketsClient subscriberspb.Markets,
	productsClient subscriberspb.Products,
	publicationsClient subscriberspb.Publications,
	subscriptionsClient subscriberspb.Subscriptions,
	webhooksClient webhookspb.Webhooks,
) *Server {
	return &Server{
		DB:            db,
		Warden:        w,
		Webhooks:      webhooksClient,
		Subscriptions: subscriptionsClient,
		Markets:       marketsClient,
		Products:      productsClient,
		Publications:  publicationsClient,
	}
}

func (s *Server) AddRoutes(r *mux.Router) {
	var (
		promotionsHandler = promotionspb.NewPromotionsServer(s, nil)
	)
	r.Handle(promotionspb.PromotionsPathPrefix, twirputil.AddMiddleware(promotionsHandler))
}
