if(typeof rune === "undefined") {
  var rune = {
    // The base API URL. Typically this is just a domain + a protocol, but some users might opt to prefix the API URL with `/api` on the same domain
    apiUrl: 'https://api.runecms.io',
    // The API version is usually appended to the apiUrl path + the service. Using the above configuration, we'll find it will evaluate to https://api.runecms.io/billing/v1/
    apiVersion: 'v1',
    // The JWT is typically used with OAuth2 validation but we also use them as API keys. Create a JWT with limited billing access (create only) in the admin panel or the API
    jwt: 'asdf-jwt',
  };
}

rune.getServiceUrl = function(service) {
  // https://api.runecms.io/billing/v1/
  return rune.apiUrl.concat('/', service, '/', rune.apiVersion, '/');
};
/**
 * @param {Object}  options
 * @param {Boolean} [options.isPreview=false]       - If this is set, then no billing information will be processed at all. A token will be collected using the sandbox account.
 * @param {Boolean} [options.billImmediately=true]  - If this is not true, then billing information will be collected, but the token will not be immediately charged.
 * @param {String}  options.token                   - This is the token that is returned by the Braintree form submission
 * @param {String}  options.productId               - This is the product ID that has been purchased
 * @param {Number}  {options.price=}                - This is the initial price. If this is not set, then the initial price is assumed to be the price of the product
 * @param {String}  {options.billingAccountId=}     - The BillingAccountId should be provided if this purchase is for a billing account that already exists in Rune
 * @param {String}  {options.subscriberId=}         - Required if BillingAccountId is not present. If set, a new BillingAccount will be created for this Subscriber using the provided token.
 */
rune.submitPurchase = function(options) {
  options = options || {};

  // Can't use || syntax with Booleans :(
  if(typeof options.isPreview !== 'boolean') {
    options.isPreview = false;
  }
  if(typeof options.billImmediately !== 'boolean') {
    options.billImmediately = true;
  }

  options.token = options.token || '';
  options.productId = options.productId || '';
};



