// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package server

import (
	"database/sql"
	"net/http"

	"git.runecms.io/rune/runecms/promotions/rpc"

	"github.com/gorilla/mux"
	"github.com/ory/ladon"
)

type Server struct {
	DB         *sql.DB
	Warden     *ladon.Ladon
	Promotions promotionspb.Promotions
}

func NewServer(db *sql.DB, w *ladon.Ladon, promotions promotionspb.Promotions) *Server {
	return &Server{
		DB:         db,
		Warden:     w,
		Promotions: promotions,
	}
}

func (s *Server) AddRoutes(r *mux.Router) {
	r.HandleFunc("/", HandleIndex)
	r.HandleFunc("/campaigns/{campaign_id}", HandleCampaignIndex)
	r.HandleFunc("/campaigns/{campaign_id}/{step}", HandleCampaignStep)
}

func HandleIndex(w http.ResponseWriter, r *http.Request) {
}

func HandleCampaignIndex(w http.ResponseWriter, r *http.Request) {
}

func HandleCampaignStep(w http.ResponseWriter, r *http.Request) {
}
