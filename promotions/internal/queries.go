// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package promotions

var (
	SqlDeleteCampaign = `
	UPDATE
		campaigns
	SET
		deleted_at = current_timestamp
	WHERE
		id = $1
	RETURNING
		market_id,
		created_by,
		name,
		slug,
		allowed_domains,
		is_preview,
		start_at,
		end_at,
		created_at,
		deleted_at,
		campaign_type
`

	SqlGetCampaignById = `
	SELECT
		market_id,
		created_by,
		name,
		slug,
		allowed_domains,
		is_preview,
		start_at,
		end_at,
		created_at,
		deleted_at,
		campaign_type
	FROM
		campaigns
	WHERE
		id = $1
`

	SqlGetCampaignBySlug = `
	SELECT
		id,
		market_id,
		created_by,
		name,
		allowed_domains,
		is_preview,
		start_at,
		end_at,
		created_at,
		deleted_at,
		campaign_type
	FROM
		campaigns
	WHERE
		slug = $1
`

	SqlGetMultiCampaignById = `
	SELECT
		fine_print,
		extra_js,
		extra_css,
		logo_url,
		background_image_url
	FROM
		campaigns_multi_campaigns
	WHERE
		id = $1
	`

	SqlGetMultiCampaignCampaigns = `
	SELECT
		a.campaign_id,
		b.market_id,
		b.created_by,
		b.name,
		b.slug,
		b.allowed_domains,
		b.is_preview,
		b.start_at,
		b.end_at,
		b.created_at,
		b.updated_at,
		b.deleted_at,
		b.campaign_type
	FROM
		campaigns_multi_campaigns_campaigns as a,
		campaigns AS b
	WHERE
		a.multi_campaign_id = $1
	AND
		b.campaign_id = a.campaign_id
		`

	SqlGetSubscriptionCampaignById = `
	SELECT
		is_promotional,
		bill_immediately,
		collect_token,
		allowed_zipcodes,
		header,
		subheader,
		fine_print,
		extra_js,
		extra_css,
		logo_url,
		background_image_url,
		success_header,
		success_subheader,
		success_body,
		success_js,
		success_css,
		publication_id,
		product_id
	FROM
		campaigns_subscription_campaigns
	WHERE
		id = $1
	`

	SqlGetSubscriptionCampaignRates = `
	SELECT
		id,
		weight,
		price,
		billing_term,
		b.descriptor,
		b.plural,
		b.singular,
		campaign_term,
		c.descriptor,
		c.plural,
		c.singular
	FROM
		campaigns_subscription_campaign_rates campaign
	LEFT JOIN campaigns_subscription_frequencies b ON campaign.billing_frequency = b.descriptor
	LEFT JOIN campaigns_subscription_frequencies c ON campaign.campaign_frequency = c.descriptor
	WHERE
		subscription_campaign_id = $1
	ORDER BY weight ASC;
	`

	SqlInsertBaseCampaign = `
	INSERT INTO campaigns (
		market_id,
		created_by,
		name,
		slug,
		allowed_domains,
		is_preview,
		start_at,
		end_at,
		campaign_type
	)
	VALUES (
		$1,
		$2,
		$3,
		$4,
		$5,
		$6,
		$7,
		$8,
		$9
	)
	RETURNING id, created_at, updated_at
	`

	SqlCampaignUpdatedAt = `
	UPDATE
		campaigns
	SET
		updated_at = current_timestamp
	WHERE
		id = $1;
	`

	SqlUpdateSubscriptionCampaign = `
	UPDATE
		campaigns_subscription_campaigns
	SET
		is_promotional = $2,
		bill_immediately = $3,
		collect_token = $4,
		allowed_zipcodes = $5,
		header = $6,
		subheader = $7,
		fine_print = $8,
		extra_js = $9,
		extra_css = $10,
		logo_url = $11,
		background_image_url = $12,
		success_header = $13,
		success_subheader = $14,
		success_body = $15,
		success_js = $16,
		success_css = $17,
		publication_id = $18,
		product_id = $19
	WHERE
	  id = $1;
	`

	SqlInsertSubscriptionCampaign = `
	INSERT INTO
		campaigns_subscription_campaigns (
			id,
			is_promotional,
			bill_immediately,
			collect_token,
			allowed_zipcodes,
			header,
			subheader,
			fine_print,
			extra_js,
			extra_css,
			logo_url,
			background_image_url,
			success_header,
			success_subheader,
			success_body,
			success_js,
			success_css,
			publication_id,
			product_id
		)
	VALUES (
		$1,
		$2,
		$3,
		$4,
		$5,
		$6,
		$7,
		$8,
		$9,
		$10,
		$11,
		$12,
		$13,
		$14,
		$15,
		$16,
		$17,
		$18,
		$19
	);
	`
	SqlInsertSubscriptionCampaignRate = `
	INSERT INTO campaigns_subscription_campaign_rates (
		weight,
		subscription_campaign_id,
		price,
		billing_term,
		billing_frequency,
		campaign_term,
		campaign_frequency
	)
	VALUES (
		$1,
		$2,
		$3,
		$4,
		$5,
		$6,
		$7
	)
	RETURNING id
	`

	SqlInsertMultiCampaign = `
	INSERT INTO
		campaigns_multi_campaigns (
			id
		)
	VALUES (
		$1
	)
	`

	SqlAddCampaignToMulti = `
	INSERT INTO
		campaigns_multi_campaigns_campaigns (
			multi_campaign_id,
			campaign_id,
			content
		)
	VALUES (
		$1,
		$2,
		$3
	)
	`
)
