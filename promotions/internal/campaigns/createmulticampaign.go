// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package campaigns

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"strings"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/promotions/internal"
	"git.runecms.io/rune/runecms/promotions/rpc"
	"git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/google/uuid"
	"github.com/gosimple/slug"
	"github.com/opentracing/opentracing-go"
	tags "github.com/opentracing/opentracing-go/ext"
	"github.com/ory/ladon"
)

var (
	ErrUnknownProduct = errors.New("Product unknown")
)

func CreateMultiCampaign(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	marketsClient subscriberspb.Markets,
	productsClient subscriberspb.Products,
	publicationsClient subscriberspb.Publications,
	webhooksClient webhookspb.Webhooks,
	req *promotionspb.CreateMultiCampaignRequest,
) (*promotionspb.Campaign, error) {
	var (
		campaign = &promotionspb.MultiCampaign{
			Campaigns:      nil,
			Name:           req.GetName(),
			Slug:           req.GetSlug(),
			AllowedDomains: req.GetAllowedDomains(),
			IsPreview:      req.GetIsPreview(),
			StartAt:        req.GetStartAt(),
			EndAt:          req.GetEndAt(),
			CreatedBy:      ident.User,
		}
		span  = opentracing.SpanFromContext(ctx)
		child opentracing.Span
		endAt *string
	)

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("Ladon permission check", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "ladon")
		child.SetTag("ladon.action", "create")
		child.SetTag("ladon.resource", "promotions:campaigns")
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	if err := runecms.Access(ident, w, "create", "promotions:campaigns"); err != nil {
		finishError(child, err)
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	finishSpan(child)

	c, err := promotions.Identity.ToRemoteTwirpContext()
	if err != nil {
		return nil, err
	}

	if campaign.GetStartAt() == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "A Start date (start_at) must be provided")
	}
	if campaign.GetEndAt() != "" {
		endAt = &campaign.EndAt
	}
	if strings.TrimSpace(req.GetName()) == "" {
		// TODO: get actual firstname / lastname from users service
		user := ident.User
		if len(user) > 6 {
			user = user[:5]
		}
		req.Name = fmt.Sprintf("multi-%s-%s", user, uuid.New().String()[:10])
	}

	if strings.TrimSpace(req.Slug) == "" {
		campaign.Slug = slug.Make(campaign.GetName())
	}

	cmps := make([]*promotionspb.Campaign, len(req.GetCampaigns()))
	for i, v := range req.GetCampaigns() {
		switch cmp := v.Campaign.(type) {
		case *promotionspb.CreateMultiCampaignRequest_Campaigns_CampaignId:
			if span != nil {
				child = opentracing.GlobalTracer().StartSpan("Get Campaign By ID", opentracing.ChildOf(span.Context()))
				child.SetTag("id", cmp)
				// tags.PeerService.Set(child, "subscribers")
				ctx = opentracing.ContextWithSpan(ctx, child)
			}
			c, err := GetCampaignById(ctx, db, w, ident, cmp.CampaignId)
			if err != nil {
				finishError(child, err)
				return nil, err
			}
			cmps[i] = c
		case *promotionspb.CreateMultiCampaignRequest_Campaigns_CreateSubscriptionCampaignRequest:
			if span != nil {
				child = opentracing.GlobalTracer().StartSpan("Create Subscription Campaign", opentracing.ChildOf(span.Context()))
				// child.SetTag("id", cmp)
				// tags.PeerService.Set(child, "subscribers")
				ctx = opentracing.ContextWithSpan(ctx, child)
			}
			c, err := CreateSubscriptionCampaign(
				ctx,
				db,
				w,
				ident,
				marketsClient,
				productsClient,
				publicationsClient,
				webhooksClient,
				cmp.CreateSubscriptionCampaignRequest,
			)
			if err != nil {
				finishError(child, err)
				return nil, err
			}
			cmps[i] = c
		default:
			return nil, ErrUnknownProduct
		}
	}
	finishSpan(child)

	var txChild opentracing.Span
	if span != nil {
		txChild = opentracing.GlobalTracer().StartSpan("SQL BEGIN", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "cockroachdb")
		// child.SetTag("sql.query", promotions.SqlInsertBaseCampaign)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	tx, err := db.Begin()
	if err != nil {
		finishError(child, err)
		return nil, runecms.Error(runecms.Internal, err.Error())
	}
	if txChild != nil {
		child = opentracing.GlobalTracer().StartSpan("SQL INSERT", opentracing.ChildOf(txChild.Context()))
		tags.PeerService.Set(child, "cockroachdb")
		child.SetTag("sql.query", promotions.SqlInsertBaseCampaign)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	if err := tx.QueryRow(promotions.SqlInsertBaseCampaign,
		campaign.GetMarketId(),
		ident.User,
		campaign.GetName(),
		campaign.GetSlug(),
		campaign.GetAllowedDomains(),
		campaign.GetIsPreview(),
		campaign.GetStartAt(),
		endAt,
		promotionspb.CAMPAIGN_TYPE_MULTI,
	).Scan(
		&campaign.Id,
		&campaign.CreatedAt,
		&campaign.UpdatedAt,
	); err != nil {
		finishError(child, err)
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	finishSpan(child)

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("SQL INSERT", opentracing.ChildOf(txChild.Context()))
		tags.PeerService.Set(child, "cockroachdb")
		child.SetTag("sql.query", promotions.SqlInsertMultiCampaign)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}

	_, err = tx.Exec(promotions.SqlInsertMultiCampaign,
		campaign.GetId(),
	)
	if err != nil {
		finishError(child, err)
		return nil, runecms.Errorf(runecms.InvalidArgument, "Error inserting the multi-campaign into the database. Error: %s", err.Error())
	}
	finishSpan(child)

	for i, v := range cmps {
		var id string
		switch cmp := v.Campaign.(type) {
		case *promotionspb.Campaign_SubscriptionCampaign:
			id = cmp.SubscriptionCampaign.GetId()
		default:
			return nil, ErrUnknownProduct
		}
		_, err := tx.Exec(
			promotions.SqlAddCampaignToMulti,
			campaign.GetId(),
			id,
			req.GetCampaigns()[i].GetContent(),
		)
		if err != nil {
			return nil, runecms.Error(runecms.InvalidArgument, err.Error())
		}
	}

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("SQL COMMIT", opentracing.ChildOf(txChild.Context()))
		tags.PeerService.Set(child, "cockroachdb")
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	if err := tx.Commit(); err != nil {
		finishError(child, err)
		if err := tx.Rollback(); err != nil {
			finishError(txChild, err)
			return nil, runecms.Error(runecms.Internal, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}
	finishSpan(child)
	finishSpan(txChild)
	return GetMultiCampaign(ctx, db, ident, &promotionspb.CampaignBase{
		Id:             campaign.GetId(),
		MarketId:       campaign.GetMarketId(),
		CreatedBy:      campaign.GetCreatedBy(),
		Name:           campaign.GetName(),
		Slug:           campaign.GetSlug(),
		AllowedDomains: campaign.GetAllowedDomains(),
		IsPreview:      campaign.GetIsPreview(),
		StartAt:        campaign.GetStartAt(),
		EndAt:          campaign.GetEndAt(),
		CreatedAt:      campaign.GetCreatedAt(),
		UpdatedAt:      campaign.GetUpdatedAt(),
		DeletedAt:      campaign.GetDeletedAt(),
		CampaignType:   promotionspb.CAMPAIGN_TYPE_MULTI,
	})
}
