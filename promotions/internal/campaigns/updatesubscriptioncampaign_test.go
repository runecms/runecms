// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package campaigns

import (
	"context"
	"testing"
	"time"

	"git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/promotions/rpc"
	"git.runecms.io/rune/runecms/subscribers/rpc"
	"github.com/google/uuid"
)

func TestUpdate(t *testing.T) {
	db := testutil.GetTestDb(t)
	t.Run("Create a SubscriptionCampaign with a product id", func(t *testing.T) {
		var (
			name          = uuid.New().String()
			slug          = uuid.New().String()
			marketID      = uuid.New().String()
			publicationID = uuid.New().String()
		)
		product, _ := productsClient.CreateProduct(context.Background(), &subscriberspb.CreateProductRequest{
			Name:          uuid.New().String(),
			PublicationId: uuid.New().String(),
		})
		productID := product.GetProduct().GetId()

		req := &promotionspb.CreateSubscriptionCampaignRequest{
			Name:          name,
			StartAt:       time.Now().Format(time.RFC3339),
			Slug:          slug,
			MarketId:      marketID,
			PublicationId: publicationID,
			ProductInfo: &promotionspb.CreateSubscriptionCampaignRequest_ProductId{
				ProductId: productID,
			},
		}

		campaign, err := CreateSubscriptionCampaign(
			context.Background(),
			db,
			warden,
			validID,
			marketsClient,
			productsClient,
			publicationsClient,
			webhooksClient,
			req,
		)
		if err != nil {
			t.Fatal(err)
		}

		if campaign == nil {
			t.Fatal("Returned campaign is nil")
		}

		t.Run("Update the subscription campaign", func(t *testing.T) {
			s := campaign.GetSubscriptionCampaign()
			zips := "00000,00001,00002"
			s.AllowedZipcodes = zips
			updated, err := UpdateSubscriptionCampaign(context.Background(), db, warden, validID, webhooksClient, &promotionspb.UpdateSubscriptionCampaignRequest{
				Campaign: s,
			})
			if err != nil {
				t.Fatal(err)
			}
			if updated.GetSubscriptionCampaign().AllowedZipcodes != zips {
				t.Fatal("The subscriptioncampaign did not get updated")
			}
		})
	})
}
