// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package campaigns

import (
	"context"
	"database/sql"
	"fmt"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/promotions/internal"
	"git.runecms.io/rune/runecms/promotions/rpc"

	"github.com/opentracing/opentracing-go"
	tags "github.com/opentracing/opentracing-go/ext"
)

func GetMultiCampaign(ctx context.Context, db *sql.DB, ident runecms.Identity, b *promotionspb.CampaignBase) (*promotionspb.Campaign, error) {
	var (
		span  = opentracing.SpanFromContext(ctx)
		child opentracing.Span
		c     = &promotionspb.MultiCampaign{
			Id:             b.Id,
			MarketId:       b.MarketId,
			Name:           b.Name,
			Slug:           b.Slug,
			AllowedDomains: b.AllowedDomains,
			IsPreview:      b.IsPreview,
			StartAt:        b.StartAt,
			EndAt:          b.EndAt,
			CreatedAt:      b.CreatedAt,
			DeletedAt:      b.DeletedAt,
		}
	)
	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("SQL SELECT", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "cockroachdb")
		child.SetTag("sql.query", promotions.SqlGetMultiCampaignById)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}

	err := db.QueryRow(promotions.SqlGetMultiCampaignById, c.Id).Scan(
		&c.FinePrint,
		&c.ExtraJs,
		&c.ExtraCss,
		&c.LogoUrl,
		&c.BackgroundImageUrl,
	)

	if err != nil {
		finishError(child, err)
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, fmt.Sprintf("MultiCampiagn with 'campaign_id' '%s' not found", b.Id))
		}
		return nil, runecms.Errorf(runecms.Internal, "Error querying database for a MultiCampaign with this id. Error: %s", err.Error())
	}
	finishSpan(child)

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("SQL SELECT", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "cockroachdb")
		child.SetTag("sql.query", promotions.SqlGetMultiCampaignCampaigns)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	campaigns := []*promotionspb.CampaignBase{}
	rows, err := db.Query(promotions.SqlGetMultiCampaignCampaigns, c.Id)
	if err != nil {
		finishError(child, err)
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, fmt.Sprintf("Multi Campiagn with 'campaign_id' '%s' not found", b.Id))
		}
		return nil, runecms.Errorf(runecms.Internal, "Error querying database for a MultiCampaign with this id. Error: %s", err.Error())
	}
	defer rows.Close()
	for rows.Next() {
		var (
			endAt     sql.NullString
			deletedAt sql.NullString
			b         = &promotionspb.CampaignBase{}
		)
		if err := rows.Scan(
			&b.Id,
			&b.MarketId,
			&b.CreatedBy,
			&b.Name,
			&b.Slug,
			&b.AllowedDomains,
			&b.IsPreview,
			&b.StartAt,
			&endAt,
			&b.CreatedAt,
			&b.DeletedAt,
			&b.CampaignType,
		); err != nil {
			finishError(child, err)
			return nil, runecms.Errorf(runecms.Internal, "Error scanning SQL results for MultiCampaign. Error: %s", err.Error())
		}
		campaigns = append(campaigns, b)
	}
	finishSpan(child)

	cmps := make([]*promotionspb.Campaign, len(campaigns))

	for _, v := range campaigns {
		if val, ok := CampaignTypesMap[v.CampaignType]; ok {
			cmp, err := val(ctx, db, ident, c)
			if err != nil {
				return nil, err
			}
			cmps[i] = cmp
		}
	}
	c.Campaigns = cmps
	return c, nil
}
