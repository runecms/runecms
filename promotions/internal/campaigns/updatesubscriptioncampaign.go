// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package campaigns

import (
	"context"
	"database/sql"
	"fmt"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/promotions/internal"
	"git.runecms.io/rune/runecms/promotions/rpc"
	// "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/gosimple/slug"
	"github.com/opentracing/opentracing-go"
	tags "github.com/opentracing/opentracing-go/ext"
	"github.com/ory/ladon"
)

func UpdateSubscriptionCampaign(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	webhooksClient webhookspb.Webhooks,
	req *promotionspb.UpdateSubscriptionCampaignRequest,
) (*promotionspb.Campaign, error) {
	var (
		span  = opentracing.SpanFromContext(ctx)
		child opentracing.Span
	)

	campaign := req.GetCampaign()
	resource := fmt.Sprintf("promotions:campaigns:%s", campaign.GetId())
	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("Ladon permission check", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "ladon")
		child.SetTag("ladon.action", "update")
		child.SetTag("ladon.resource", resource)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}

	if err := runecms.Access(ident, w, "update", resource); err != nil {
		finishError(child, err)
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	finishSpan(child)
	if slug.IsSlug(campaign.GetSlug()) != true {
		campaign.Slug = slug.Make(campaign.GetSlug())
	}

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("SQL Transaction", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "cockroachdb")
		child.SetTag("sql.query1", promotions.SqlUpdateSubscriptionCampaign)
		child.SetTag("sql.query2", promotions.SqlCampaignUpdatedAt)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	if tx, err := db.Begin(); err == nil {
		_, err := tx.Exec(promotions.SqlUpdateSubscriptionCampaign,
			campaign.GetId(),
			campaign.GetIsPromotional(),
			campaign.GetBillImmediately(),
			campaign.GetCollectToken(),
			campaign.GetAllowedZipcodes(),
			campaign.GetHeader(),
			campaign.GetSubheader(),
			campaign.GetFinePrint(),
			campaign.GetExtraJs(),
			campaign.GetExtraCss(),
			campaign.GetLogoUrl(),
			campaign.GetBackgroundImageUrl(),
			campaign.GetSuccessHeader(),
			campaign.GetSuccessSubheader(),
			campaign.GetSuccessBody(),
			campaign.GetSuccessJs(),
			campaign.GetSuccessCss(),
			campaign.GetPublicationId(),
			campaign.GetProductId(),
		)

		if err != nil {
			if err := tx.Rollback(); err != nil {
				finishError(child, err)
				return nil, runecms.Error(runecms.Internal, "Unable to rollback SQL transaction")
			}
			finishError(child, err)
			if err == sql.ErrNoRows {
				return nil, runecms.Errorf(runecms.NotFound, "Campaign with id '%s' not found", campaign.GetId())
			}
			return nil, runecms.Errorf(runecms.Internal, "Error updating campaign with id '%s'. Error: %s", campaign.GetId(), err.Error())
		}
		_, err = tx.Exec(promotions.SqlCampaignUpdatedAt, campaign.GetId())

		if err != nil {
			if err := tx.Rollback(); err != nil {
				finishError(child, err)
				return nil, runecms.Error(runecms.Internal, "Unable to rollback SQL transaction")
			}
			finishError(child, err)
			if err == sql.ErrNoRows {
				return nil, runecms.Errorf(runecms.NotFound, "Campaign with id '%s' not found", campaign.GetId())
			}
			return nil, runecms.Errorf(runecms.Internal, "Error updating campaign with id '%s'. Error: %s", campaign.GetId(), err.Error())
		}

		if err := tx.Commit(); err != nil {
			if err := tx.Rollback(); err != nil {
				finishError(child, err)
				return nil, runecms.Error(runecms.Internal, "Unable to rollback SQL transaction")
			}
			finishError(child, err)
			return nil, runecms.Error(runecms.Internal, "Unable to commit SQL transaction")
		}
	}
	// GetSubscriptionCampaign
	return GetSubscriptionCampaign(ctx, db, ident, &promotionspb.CampaignBase{
		Id:             campaign.GetId(),
		MarketId:       campaign.GetMarketId(),
		CreatedBy:      campaign.GetCreatedBy(),
		Name:           campaign.GetName(),
		Slug:           campaign.GetSlug(),
		AllowedDomains: campaign.GetAllowedDomains(),
		IsPreview:      campaign.GetIsPreview(),
		StartAt:        campaign.GetStartAt(),
		EndAt:          campaign.GetEndAt(),
		CreatedAt:      campaign.GetCreatedAt(),
		UpdatedAt:      campaign.GetUpdatedAt(),
		DeletedAt:      campaign.GetDeletedAt(),
		CampaignType:   campaign.GetCampaignType(),
	})
}
