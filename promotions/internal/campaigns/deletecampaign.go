// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package campaigns

import (
	"context"
	"database/sql"
	"fmt"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/promotions/internal"
	"git.runecms.io/rune/runecms/promotions/rpc"

	"github.com/opentracing/opentracing-go"
	tags "github.com/opentracing/opentracing-go/ext"
	"github.com/ory/ladon"
)

func DeleteCampaign(ctx context.Context, db *sql.DB, w *ladon.Ladon, ident runecms.Identity, id string) (*promotionspb.Campaign, error) {
	var (
		span  = opentracing.SpanFromContext(ctx)
		child opentracing.Span

		c = &promotionspb.CampaignBase{
			Id: id,
		}
		endAt sql.NullString
	)

	resource := fmt.Sprintf("promotions:campaigns:%s", id)

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("Ladon permission check", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "ladon")
		child.SetTag("ladon.action", "delete")
		child.SetTag("ladon.resource", resource)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	if err := runecms.Access(ident, w, "delete", resource); err != nil {
		finishError(child, err)
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	finishSpan(child)

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("SQL UPDATE", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "cockroachdb")
		child.SetTag("sql.query", promotions.SqlDeleteCampaign)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}

	err := db.QueryRow(promotions.SqlDeleteCampaign, id).Scan(
		&c.MarketId,
		&c.CreatedBy,
		&c.Name,
		&c.Slug,
		&c.AllowedDomains,
		&c.IsPreview,
		&c.StartAt,
		&endAt,
		&c.CreatedAt,
		&c.DeletedAt,
		&c.CampaignType,
	)

	if err != nil {
		finishError(child, err)
		return nil, fmt.Errorf("Error querying database for campaign %s. Error: %s", id, err.Error())
	}
	finishSpan(child)

	if endAt.Valid {
		c.EndAt = endAt.String
	}
	var campaignType promotionspb.CAMPAIGN_TYPE

	if val, ok := CampaignTypesMap[campaignType]; ok {
		return val(ctx, db, ident, c)
	}

	return nil, ErrBadCampaignType
}
