// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package campaigns

import (
	"context"
	"database/sql"
	"fmt"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/promotions/internal"
	"git.runecms.io/rune/runecms/promotions/rpc"
	"git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/opentracing/opentracing-go"
	tags "github.com/opentracing/opentracing-go/ext"
)

var (
	MarketsClient      subscriberspb.Markets
	ProductsClient     subscriberspb.Products
	PublicationsClient subscriberspb.Publications
	SubscribersClient  subscriberspb.Subscribers
)

// GetSubscriptionCampaign will retrieve a SubscriptionCampaign based on the BaseCampaign information provided
func GetSubscriptionCampaign(
	ctx context.Context,
	db *sql.DB,
	ident runecms.Identity,
	b *promotionspb.CampaignBase,
) (*promotionspb.Campaign, error) {
	var (
		span  = opentracing.SpanFromContext(ctx)
		child opentracing.Span
		c     = &promotionspb.SubscriptionCampaign{
			Id:             b.Id,
			MarketId:       b.MarketId,
			Name:           b.Name,
			Slug:           b.Slug,
			AllowedDomains: b.AllowedDomains,
			IsPreview:      b.IsPreview,
			StartAt:        b.StartAt,
			EndAt:          b.EndAt,
			CreatedAt:      b.CreatedAt,
			DeletedAt:      b.DeletedAt,
			CampaignType:   b.CampaignType,
		}
	)

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("SQL Select", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "cockroachdb")
		child.SetTag("sql.query", promotions.SqlGetSubscriptionCampaignById)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	err := db.QueryRow(promotions.SqlGetSubscriptionCampaignById, c.Id).Scan(
		&c.IsPromotional,
		&c.BillImmediately,
		&c.CollectToken,
		&c.AllowedZipcodes,
		&c.Header,
		&c.Subheader,
		&c.FinePrint,
		&c.ExtraJs,
		&c.ExtraCss,
		&c.LogoUrl,
		&c.BackgroundImageUrl,
		&c.SuccessHeader,
		&c.SuccessSubheader,
		&c.SuccessBody,
		&c.SuccessJs,
		&c.SuccessCss,
		&c.PublicationId,
		&c.ProductId,
	)
	if err != nil {
		finishError(child, err)
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, fmt.Sprintf("Subscription Campiagn with 'campaign_id' '%s' not found", b.Id))
		}
		return nil, runecms.Errorf(runecms.Internal, "Error querying database for a SubscriptionCampaign with this id. Error: %s", err.Error())
	}
	finishSpan(child)

	tctx, err := promotions.Identity.ToRemoteTwirpContext()
	if err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("Products GET", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "subscribers")
		ctx = opentracing.ContextWithSpan(ctx, child)
	}

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("Products GET", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "subscribers")
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	res, err := ProductsClient.GetProduct(tctx, &subscriberspb.GetProductRequest{
		Id: c.GetProductId(),
	})
	if err != nil {
		finishError(child, err)
		return nil, err
	}
	c.Product = res.GetProduct()
	finishSpan(child)
	return &promotionspb.Campaign{
		Campaign: &promotionspb.Campaign_SubscriptionCampaign{
			SubscriptionCampaign: c,
		},
	}, nil
}
