// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package campaigns

import (
	"github.com/opentracing/opentracing-go"
)

func finishError(s opentracing.Span, err error) {
	if s != nil {
		s.SetTag("error", true)
		s.SetTag("error.description", err.Error())
		s.Finish()
	}
}

func finishSpan(s opentracing.Span) {
	if s != nil {
		s.Finish()
	}
}
