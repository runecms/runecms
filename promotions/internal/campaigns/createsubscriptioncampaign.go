// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package campaigns

import (
	"context"
	"database/sql"
	"fmt"
	"strings"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/promotions/internal"
	"git.runecms.io/rune/runecms/promotions/rpc"
	"git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/google/uuid"
	"github.com/gosimple/slug"
	"github.com/opentracing/opentracing-go"
	tags "github.com/opentracing/opentracing-go/ext"
	"github.com/ory/ladon"
)

func CreateSubscriptionCampaign(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	marketsClient subscriberspb.Markets,
	productsClient subscriberspb.Products,
	publicationsClient subscriberspb.Publications,
	webhooksClient webhookspb.Webhooks,
	req *promotionspb.CreateSubscriptionCampaignRequest,
) (*promotionspb.Campaign, error) {
	var (
		campaign = &promotionspb.SubscriptionCampaign{
			MarketId:           req.GetMarketId(),
			Name:               req.GetName(),
			Slug:               req.GetSlug(),
			AllowedDomains:     req.GetAllowedDomains(),
			AllowedZipcodes:    req.GetAllowedZipcodes(),
			IsPreview:          req.GetIsPreview(),
			IsPromotional:      req.GetIsPromotional(),
			BillImmediately:    req.GetBillImmediately(),
			CollectToken:       req.GetCollectToken(),
			Header:             req.GetHeader(),
			Subheader:          req.GetSubheader(),
			FinePrint:          req.GetFinePrint(),
			ExtraJs:            req.GetExtraJs(),
			ExtraCss:           req.GetExtraCss(),
			LogoUrl:            req.GetLogoUrl(),
			BackgroundImageUrl: req.GetBackgroundImageUrl(),
			SuccessHeader:      req.GetSuccessHeader(),
			SuccessSubheader:   req.GetSuccessSubheader(),
			SuccessBody:        req.GetSuccessBody(),
			SuccessJs:          req.GetSuccessJs(),
			SuccessCss:         req.GetSuccessCss(),
			PublicationId:      req.GetPublicationId(),
			ProductId:          req.GetProductId(),
			StartAt:            req.GetStartAt(),
			EndAt:              req.GetEndAt(),
			CampaignType:       promotionspb.CAMPAIGN_TYPE_SUBSCRIPTION,
		}
		span  = opentracing.SpanFromContext(ctx)
		child opentracing.Span
		endAt *string
	)

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("Ladon permission check", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "ladon")
		child.SetTag("ladon.action", "create")
		child.SetTag("ladon.resource", "promotions:campaigns")
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	if err := runecms.Access(ident, w, "create", "promotions:campaigns"); err != nil {
		finishError(child, err)
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	finishSpan(child)

	c, err := promotions.Identity.ToRemoteTwirpContext()
	if err != nil {
		return nil, err
	}

	// Validate data
	if strings.TrimSpace(req.GetMarketId()) == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "A Market ID (market_id) must be provided")
	}

	mRes, err := marketsClient.GetMarket(c, &subscriberspb.GetMarketRequest{
		Id: campaign.GetMarketId(),
	})
	if err != nil {
		return nil, err
	}

	campaign.Market = mRes.GetMarket()
	if strings.TrimSpace(req.GetPublicationId()) == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "A Publciation ID (publication_id) must be provided")
	}
	pRes, err := publicationsClient.GetPublication(c, &subscriberspb.GetPublicationRequest{
		Id: campaign.GetPublicationId(),
	})
	if err != nil {
		return nil, err
	}

	campaign.Publication = pRes.GetPublication()

	if campaign.GetStartAt() == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "A Start date (start_at) must be provided")
	}
	if campaign.GetEndAt() != "" {
		endAt = &campaign.EndAt
	}
	if strings.TrimSpace(req.GetName()) == "" {
		// TODO: get actual firstname / lastname from users service
		// entry.Name = fmt.Sprintf("subscription-%s-%s-%s", entry.PublicationId[:3], entry.CreatedBy[:6], uuid.NewV1().String()[:10])
		user := ident.User
		if len(user) > 6 {
			user = user[:5]
		}
		req.Name = fmt.Sprintf("subscription-%s-%s-%s", req.PublicationId[:3], user, uuid.New().String()[:10])
	}

	if strings.TrimSpace(req.Slug) == "" {
		campaign.Slug = slug.Make(campaign.GetName())
	}

	// 1. Create a product in Rune
	switch pinfo := req.GetProductInfo().(type) {
	case *promotionspb.CreateSubscriptionCampaignRequest_ProductId:
		if span != nil {
			child = opentracing.GlobalTracer().StartSpan("Get product", opentracing.ChildOf(span.Context()))
			tags.PeerService.Set(child, "subscribers")
			child.SetTag("product_id", pinfo.ProductId)
			ctx = opentracing.ContextWithSpan(ctx, child)
		}
		campaign.ProductId = pinfo.ProductId
		res, err := productsClient.GetProduct(c, &subscriberspb.GetProductRequest{
			Id: pinfo.ProductId,
		})
		if err != nil {
			finishError(child, err)
			return nil, err
		}
		campaign.Product = res.GetProduct()
	case *promotionspb.CreateSubscriptionCampaignRequest_Product:
		if span != nil {
			child = opentracing.GlobalTracer().StartSpan("Create Product", opentracing.ChildOf(span.Context()))
			tags.PeerService.Set(child, "subscribers")
			ctx = opentracing.ContextWithSpan(ctx, child)
		}
		pinfo.Product.Name = fmt.Sprintf("subscription_campaign_%s", campaign.GetName())
		pinfo.Product.PublicationId = campaign.GetPublicationId()
		res, err := productsClient.CreateProduct(c, pinfo.Product)
		if err != nil {
			finishError(child, err)
			return nil, err
		}
		campaign.ProductId = res.GetProduct().GetId()
		campaign.Product = res.GetProduct()
	default:
		return nil, ErrUnknownProduct
	}
	finishSpan(child)

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("SQL Insert", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "cockroachdb")
		child.SetTag("sql.query", promotions.SqlInsertBaseCampaign)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}

	// 2. Insert a new row into the campaigns table, with the "subscription" campaign type
	if err := db.QueryRow(promotions.SqlInsertBaseCampaign,
		campaign.GetMarketId(),
		ident.User,
		campaign.GetName(),
		campaign.GetSlug(),
		campaign.GetAllowedDomains(),
		campaign.GetIsPreview(),
		campaign.GetStartAt(),
		endAt,
		promotionspb.CAMPAIGN_TYPE_SUBSCRIPTION,
	).Scan(
		&campaign.Id,
		&campaign.CreatedAt,
		&campaign.UpdatedAt,
	); err != nil {
		finishError(child, err)
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	finishSpan(child)

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("SQL Insert", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "cockroachdb")
		child.SetTag("sql.query", promotions.SqlInsertSubscriptionCampaign)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}

	// 3. Insert a new row into the campaigns_subscription_campaigns table
	_, err = db.Exec(promotions.SqlInsertSubscriptionCampaign,
		campaign.GetId(),
		campaign.GetIsPromotional(),
		campaign.GetBillImmediately(),
		campaign.GetCollectToken(),
		campaign.GetAllowedZipcodes(),
		campaign.GetHeader(),
		campaign.GetSubheader(),
		campaign.GetFinePrint(),
		campaign.GetExtraJs(),
		campaign.GetExtraCss(),
		campaign.GetLogoUrl(),
		campaign.GetBackgroundImageUrl(),
		campaign.GetSuccessHeader(),
		campaign.GetSuccessSubheader(),
		campaign.GetSuccessBody(),
		campaign.GetSuccessJs(),
		campaign.GetSuccessCss(),
		campaign.GetPublicationId(),
		campaign.GetProductId(),
	)

	if err != nil {
		return nil, fmt.Errorf("Error inserting the subscription campaign into the database. Error: %s", err.Error())
	}

	// GetSubscriptionCampaign
	return GetSubscriptionCampaign(ctx, db, ident, &promotionspb.CampaignBase{
		Id:             campaign.GetId(),
		MarketId:       campaign.GetMarketId(),
		CreatedBy:      campaign.GetCreatedBy(),
		Name:           campaign.GetName(),
		Slug:           campaign.GetSlug(),
		AllowedDomains: campaign.GetAllowedDomains(),
		IsPreview:      campaign.GetIsPreview(),
		StartAt:        campaign.GetStartAt(),
		EndAt:          campaign.GetEndAt(),
		CreatedAt:      campaign.GetCreatedAt(),
		UpdatedAt:      campaign.GetUpdatedAt(),
		DeletedAt:      campaign.GetDeletedAt(),
		CampaignType:   campaign.GetCampaignType(),
	})
}
