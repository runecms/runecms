// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package campaigns

import (
	"context"
	"database/sql"
	"fmt"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/promotions/internal"
	"git.runecms.io/rune/runecms/promotions/rpc"

	"github.com/ory/ladon"
)

func GetCampaignBySlug(ctx context.Context, db *sql.DB, w *ladon.Ladon, ident runecms.Identity, slug string) (*promotionspb.Campaign, error) {
	var (
		c = &promotionspb.CampaignBase{
			Slug: slug,
		}
		endAt     sql.NullString
		deletedAt sql.NullString
	)

	// Step 1: Get the `campaign` from the `campaigns` table
	err := db.QueryRow(promotions.SqlGetCampaignBySlug, slug).Scan(
		&c.Id,
		&c.MarketId,
		&c.CreatedBy,
		&c.Name,
		&c.AllowedDomains,
		&c.IsPreview,
		&c.StartAt,
		&endAt,
		&c.CreatedAt,
		&deletedAt,
		&c.CampaignType,
	)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, fmt.Sprintf("Error querying database for campaign with slug '%s'. Error: %s", slug, err.Error()))
	}
	if endAt.Valid {
		c.EndAt = endAt.String
	}

	if deletedAt.Valid {
		c.DeletedAt = deletedAt.String
	}

	var campaignType promotionspb.CAMPAIGN_TYPE

	// Step 2: get the struct for the subscriptioncampaign, multi-campaign, or whatever, based on the campaigns_campaign_types.name provided
	if val, ok := CampaignTypesMap[campaignType]; ok {
		return val(ctx, db, ident, c)
	}

	return nil, ErrBadCampaignType
}
