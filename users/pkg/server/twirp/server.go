// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package servertwirp

import (
	"database/sql"
	"fmt"
	"net/http"

	"git.runecms.io/rune/runecms/internal/twirputil"
	userstwirp "git.runecms.io/rune/runecms/users/pkg/users/twirp"
	pb "git.runecms.io/rune/runecms/users/rpc"
	"git.runecms.io/rune/runecms/webhooks/rpc"
	"github.com/ory/ladon"
)

// Server wraps the Twirp server
type Server struct {
	Db             *sql.DB
	Warden         ladon.Warden
	WebhooksClient webhookspb.Webhooks
}

// Start starts the server on the specified port
func (s *Server) Start(port int) error {
	usersHandler := pb.NewUsersServer(userstwirp.NewServer(s.Db, s.Warden, s.WebhooksClient), nil)
	mux := http.NewServeMux()
	mux.Handle(pb.UsersPathPrefix, twirputil.AddMiddleware(usersHandler))
	return http.ListenAndServe(fmt.Sprintf(":%d", port), mux)
}

// NewServer instantiates a new server with the provided database and warden
func NewServer(db *sql.DB, w ladon.Warden, wc webhookspb.Webhooks) *Server {
	return &Server{
		Db:             db,
		Warden:         w,
		WebhooksClient: wc,
	}
}
