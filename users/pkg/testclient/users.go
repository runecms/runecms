// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package testclient

import (
	"context"
	"strings"
	"testing"
	"time"

	"git.runecms.io/rune/runecms/internal/testutil"
	pb "git.runecms.io/rune/runecms/users/rpc"
)

type TestClient struct {
	T *testing.T
}

var (
	token = strings.ToUpper(testutil.GetUUID())
)

func (c *TestClient) Create(ctx context.Context, req *pb.CreateUserRequest) (*pb.CreateUserResponse, error) {
	return &pb.CreateUserResponse{
		User: &pb.User{
			Id:        strings.ToUpper(testutil.GetUUID()),
			Username:  "testuser",
			Email:     "testuser@example.com",
			Password:  "abc123",
			FirstName: "John",
			LastName:  "Doe",
			CreatedAt: time.Now().Format(time.RFC3339),
			UpdatedAt: time.Now().Format(time.RFC3339),
			DeletedAt: "",
		},
	}, nil
}

func (c *TestClient) Get(ctx context.Context, req *pb.GetUserRequest) (*pb.GetUserResponse, error) {
	return &pb.GetUserResponse{
		User: &pb.User{
			Id:        req.GetId(),
			Username:  "testuser",
			Email:     "testuser@example.com",
			Password:  "abc123",
			FirstName: "John",
			LastName:  "Doe",
			CreatedAt: time.Now().Format(time.RFC3339),
			UpdatedAt: time.Now().Format(time.RFC3339),
			DeletedAt: "",
		},
	}, nil
}

func (c *TestClient) Update(ctx context.Context, req *pb.UpdateUserRequest) (*pb.UpdateUserResponse, error) {
	return &pb.UpdateUserResponse{
		User: req.GetUser(),
	}, nil
}

func (c *TestClient) Delete(ctx context.Context, req *pb.DeleteUserRequest) (*pb.DeleteUserResponse, error) {
	return &pb.DeleteUserResponse{}, nil
}

func (c *TestClient) FindByEmail(ctx context.Context, req *pb.FindByEmailRequest) (*pb.FindByEmailResponse, error) {
	return &pb.FindByEmailResponse{
		User: &pb.User{
			Id:        strings.ToUpper(testutil.GetUUID()),
			Username:  "testuser",
			Email:     req.GetEmail(),
			Password:  "abc123",
			FirstName: "John",
			LastName:  "Doe",
			CreatedAt: time.Now().Format(time.RFC3339),
			UpdatedAt: time.Now().Format(time.RFC3339),
			DeletedAt: "",
		},
	}, nil
}
