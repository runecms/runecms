// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package users

import (
	"crypto/md5"
	"database/sql"
	"fmt"
	"log"
	"strings"

	runecms "git.runecms.io/rune/runecms"
	pb "git.runecms.io/rune/runecms/users/rpc"
	"git.runecms.io/rune/runecms/webhooks"
	"git.runecms.io/rune/runecms/webhooks/rpc"
	"github.com/ory/ladon"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

// Create a user
func Create(db *sql.DB, w ladon.Warden, wc webhookspb.Webhooks, ident runecms.Identity, user *pb.User) (*pb.User, error) {
	if err := runecms.Access(ident, w, "create", "users:users"); err != nil {
		log.Printf("User %s tried (and failed) to create a user", ident.User)
		return nil, errors.New(runecms.PermissionDenied)
	}

	// Input validation
	if user == nil {
		return nil, errors.Wrap(errors.New(runecms.InvalidArgument), "User not provided")
	}
	if user.Username == "" {
		return nil, errors.Wrap(errors.New(runecms.InvalidArgument), "Username not provided")
	}
	if user.Password == "" {
		return nil, errors.Wrap(errors.New(runecms.InvalidArgument), "Password not provided")
	}
	if user.Email == "" {
		return nil, errors.Wrap(errors.New(runecms.InvalidArgument), "Email not provided")
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, errors.New(runecms.Internal)
	}

	row := db.QueryRow(
		sqlCreate,
		user.Username,
		user.Email,
		"bcrypt:"+string(hashedPassword),
		user.FirstName,
		user.LastName,
	)

	model := &pb.User{}
	if err := row.Scan(
		&model.Id,
		&model.Username,
		&model.Email,
		&model.Password,
		&model.FirstName,
		&model.LastName,
		&model.CreatedAt,
	); err != nil {
		msg := fmt.Sprintf("Error scanning SQL result when creating new User: %v", err)
		log.Print(msg)
		return nil, errors.Wrap(errors.New(runecms.Internal), msg)
	}

	log.Printf("User %s successfully created User %s", ident.User, model.GetId())

	go webhooks.Distribute(wc, ident, "users:users", "create", model)

	return model, nil
}

// Get a user by ID
func Get(db *sql.DB, w ladon.Warden, ident runecms.Identity, id string) (*pb.User, error) {
	if err := runecms.Access(ident, w, "read", fmt.Sprintf("users:%s", id)); err != nil {
		log.Printf("User %s tried (and failed) to access user %s", ident.User, id)
		return nil, errors.New(runecms.PermissionDenied)
	}

	if id == "" {
		return nil, errors.New(runecms.InvalidArgument)
	}

	model := &pb.User{}

	row := db.QueryRow(sqlRead, id)

	var deletedAt sql.NullString

	if err := row.Scan(
		&model.Id,
		&model.Username,
		&model.Email,
		&model.Password,
		&model.FirstName,
		&model.LastName,
		&model.CreatedAt,
		&model.UpdatedAt,
		&deletedAt,
	); err != nil {
		msg := fmt.Sprintf("Error scanning SQL result for %s: %v", id, err)
		log.Print(msg)
		if err == sql.ErrNoRows {
			return nil, errors.New(runecms.NotFound)
		}
		return nil, errors.Wrap(errors.New(runecms.Internal), msg)
	}

	if deletedAt.Valid {
		model.DeletedAt = deletedAt.String
	}

	return model, nil
}

// Update an existing user
func Update(db *sql.DB, w ladon.Warden, wc webhookspb.Webhooks, ident runecms.Identity, user *pb.User) (*pb.User, error) {
	if user == nil {
		return nil, errors.New(runecms.InvalidArgument)
	}
	if user.Id == "" {
		return nil, errors.New(runecms.InvalidArgument)
	}
	if user.Username == "" {
		return nil, errors.New(runecms.InvalidArgument)
	}
	if user.Email == "" {
		return nil, errors.New(runecms.InvalidArgument)
	}
	if user.Password == "" {
		return nil, errors.New(runecms.InvalidArgument)
	}
	if user.FirstName == "" {
		return nil, errors.New(runecms.InvalidArgument)
	}
	if user.LastName == "" {
		return nil, errors.New(runecms.InvalidArgument)
	}

	if err := runecms.Access(ident, w, "update", fmt.Sprintf("users:%s", user.Id)); err != nil {
		log.Printf("User %s tried (and failed) to update user %s", ident.User, user.Id)
		return nil, errors.New(runecms.PermissionDenied)
	}

	res, err := db.Exec(
		sqlUpdate,
		user.Username,
		user.Email,
		user.Password,
		user.FirstName,
		user.LastName,
		user.Id,
	)
	if err != nil {
		return nil, errors.Wrap(errors.New(runecms.Internal), err.Error())
	}

	if rows, err := res.RowsAffected(); rows == 0 || err != nil {
		msg := fmt.Sprintf("Failed to update user %s", user.Id)
		log.Print(msg)
		if rows == 0 {
			return nil, errors.Wrap(errors.New(runecms.NotFound), msg)
		}
		if err != nil {
			return nil, errors.Wrap(err, msg)
		}
	}

	go webhooks.Distribute(wc, ident, "users:users", "update", user)

	return Get(db, w, ident, user.Id)
}

// Delete a user
func Delete(db *sql.DB, w ladon.Warden, wc webhookspb.Webhooks, ident runecms.Identity, user *pb.User) error {
	if user == nil {
		return errors.New(runecms.InvalidArgument)
	}
	if user.Id == "" {
		return errors.New(runecms.InvalidArgument)
	}

	if err := runecms.Access(ident, w, "delete", fmt.Sprintf("users:%s", user.Id)); err != nil {
		log.Printf("User %s tried (and failed) to delete user %s", ident.User, user.Id)
		return errors.New(runecms.PermissionDenied)
	}

	res, err := db.Exec(sqlDelete, user.Id)
	if err != nil {
		return errors.Wrap(errors.New(runecms.Internal), err.Error())
	}

	if rows, err := res.RowsAffected(); rows == 0 || err != nil {
		msg := fmt.Sprintf("Failed to update user %s", user.Id)
		log.Print(msg)
		if rows == 0 {
			return errors.Wrap(errors.New(runecms.NotFound), msg)
		}
		if err != nil {
			return errors.Wrap(err, msg)
		}
	}

	go webhooks.Distribute(wc, ident, "users:users", "delete", user)

	return nil
}

// Authenticate a user
func Authenticate(db *sql.DB, w ladon.Warden, ident runecms.Identity, email string, password string) (*pb.User, error) {
	if email == "" {
		return nil, errors.New(runecms.InvalidArgument)
	}
	if password == "" {
		return nil, errors.New(runecms.InvalidArgument)
	}

	user, err := FindByEmail(db, w, ident, email)
	if err != nil {
		return nil, err
	}
	parts := strings.SplitN(user.Password, ":", 2)
	if len(parts) < 2 {
		parts = append([]string{"bcrypt"}, parts...)
	}
	switch parts[0] {
	case "bcrypt":
		if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
			return nil, err
		}
	case "md5":
		if fmt.Sprintf("%x", md5.Sum([]byte(password))) != parts[1] {
			return nil, errors.New("hashed password is not the hash of the given password")
		}
		newHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		if err != nil {
			return nil, errors.New(runecms.Internal)
		}
		res, err := db.Exec(
			sqlUpdate,
			user.Username,
			user.Email,
			"bcrypt:"+string(newHash),
			user.FirstName,
			user.LastName,
			user.Id,
		)
		if err != nil {
			return nil, errors.Wrap(errors.New(runecms.Internal), err.Error())
		}
		if rows, err := res.RowsAffected(); rows == 0 || err != nil {
			msg := fmt.Sprintf("Failed to update user %s", user.Id)
			log.Print(msg)
			if rows == 0 {
				return nil, errors.Wrap(errors.New(runecms.NotFound), msg)
			}
			if err != nil {
				return nil, errors.Wrap(err, msg)
			}
		}
	default:
		return nil, errors.New(fmt.Sprintf("Unknown password hash type \"%s\"", parts[0]))

	}

	return user, nil
}

// FindByEmail finds a user by email
func FindByEmail(db *sql.DB, w ladon.Warden, ident runecms.Identity, email string) (*pb.User, error) {
	if email == "" {
		return nil, errors.New(runecms.InvalidArgument)
	}

	var id string

	row := db.QueryRow(sqlFindByEmail, email)

	if err := row.Scan(
		&id,
	); err != nil {
		msg := fmt.Sprintf("Error scanning SQL result for %s: %v", email, err)
		log.Print(msg)
		if err == sql.ErrNoRows {
			return nil, errors.New(runecms.NotFound)
		}
		return nil, errors.Wrap(errors.New(runecms.Internal), msg)
	}

	return Get(db, w, ident, id)
}
