// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package userstwirp

import (
	"context"

	runecms "git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/users/pkg/users"
	pb "git.runecms.io/rune/runecms/users/rpc"
	"github.com/twitchtv/twirp"
)

// Create a new user
func (s *Server) Create(c context.Context, req *pb.CreateUserRequest) (*pb.CreateUserResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := users.Create(s.Db, s.Warden, s.WebhooksClient, user, req.GetUser())
	if err != nil {
		return nil, runecms.Error(err.Error()).GetTwirpError()
	}

	return &pb.CreateUserResponse{
		User: model,
	}, nil
}

// Get user by ID
func (s *Server) Get(c context.Context, req *pb.GetUserRequest) (*pb.GetUserResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := users.Get(s.Db, s.Warden, user, req.GetId())
	if err != nil {
		return nil, runecms.Error(err.Error()).GetTwirpError()
	}

	return &pb.GetUserResponse{
		User: model,
	}, nil
}

// Update an existing user
func (s *Server) Update(c context.Context, req *pb.UpdateUserRequest) (*pb.UpdateUserResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := users.Update(s.Db, s.Warden, s.WebhooksClient, user, req.GetUser())

	if err != nil {
		return nil, runecms.Error(err.Error()).GetTwirpError()
	}

	return &pb.UpdateUserResponse{
		User: model,
	}, nil
}

// Delete a user
func (s *Server) Delete(c context.Context, req *pb.DeleteUserRequest) (*pb.DeleteUserResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	err = users.Delete(s.Db, s.Warden, s.WebhooksClient, user, req.GetUser())
	if err != nil {
		return nil, runecms.Error(err.Error()).GetTwirpError()
	}

	return &pb.DeleteUserResponse{}, nil
}

// Authenticate a user via email/password
func (s *Server) Authenticate(c context.Context, req *pb.AuthenticateUserRequest) (*pb.AuthenticateUserResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := users.Authenticate(s.Db, s.Warden, user, req.GetEmail(), req.GetPassword())
	if err != nil {
		return nil, runecms.Error(err.Error()).GetTwirpError()
	}
	return &pb.AuthenticateUserResponse{
		User: model,
	}, nil
}

// FindByEmail finds a user by email address
func (s *Server) FindByEmail(c context.Context, req *pb.FindByEmailRequest) (*pb.FindByEmailResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := users.FindByEmail(s.Db, s.Warden, user, req.GetEmail())
	if err != nil {
		return nil, runecms.Error(err.Error()).GetTwirpError()
	}

	return &pb.FindByEmailResponse{
		User: model,
	}, nil
}
