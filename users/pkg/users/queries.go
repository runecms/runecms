// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package users

const (
	sqlCreate = `
		INSERT INTO users(
			username,
			email,
			password,
			first_name,
			last_name
		)
		VALUES ($1,$2,$3,$4,$5)
		RETURNING
			id,
			username,
			email,
			password,
			first_name,
			last_name,
			created_at
	`

	sqlRead = `
		SELECT
			id,
			username,
			email,
			password,
			first_name,
			last_name,
			created_at,
			updated_at,
			deleted_at
		FROM
			users
		WHERE
			id = $1
	`

	sqlUpdate = `
		UPDATE users SET
			username = $1,
			email = $2,
			password = $3,
			first_name = $4,
			last_name = $5,
			updated_at = now()
		WHERE id = $6
	`

	sqlDelete = `
		UPDATE users SET deleted_at = now() WHERE id = $1
	`

	sqlFindByEmail = `
		SELECT
			id,
		WHERE
			email = $1
	`
)
