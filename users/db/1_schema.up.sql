CREATE TABLE users(
  id            UUID NOT NULL DEFAULT gen_random_uuid(),
  username      TEXT NOT NULL,
  email         TEXT NOT NULL,
  password      TEXT NOT NULL,
  first_name    TEXT NOT NULL,
  last_name     TEXT NOT NULL,
  created_at    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  updated_at    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at    TIMESTAMP WITH TIME ZONE,

  PRIMARY KEY (id)
);

CREATE TABLE users_oauth(
  id              UUID NOT NULL DEFAULT gen_random_uuid(),
  user_id         UUID NOT NULL,
  oauth2_uid      TEXT NOT NULL,
  oauth2_provider TEXT NOT NULL,
  created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at      TIMESTAMP WITH TIME ZONE,

  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users (id)
);
