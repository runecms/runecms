CREATE TABLE stories(
  id              UUID PRIMARY KEY default gen_random_uuid(),
  created_by      UUID NOT NULL,
  last_updated_by UUID,
  created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at      TIMESTAMP WITH TIME ZONE,
  updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp
);

CREATE TABLE stories_elements(
  id              UUID PRIMARY KEY default gen_random_uuid(),
  story_id        UUID NOT NULL REFERENCES stories(id),
  -- Doubly linked list, basically.
  -- after_element is the element that comes before this one
  after_element   UUID          REFERENCES stories_elements(id),
  -- before_element is the element that comes after this one
  before_element  UUID          REFERENCES stories_elements(id)

  -- content
);

