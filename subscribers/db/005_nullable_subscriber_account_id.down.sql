-- Copyright (C) 2018 WEHCO Media, Inc.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

BEGIN;

ALTER TABLE subscribers RENAME COLUMN account_id TO account_id; 
ALTER TABLE subscribers ADD COLUMN account_id UUID NOT NULL;

UPDATE
    subscribers
SET
    account_id = account_id_old
WHERE
    account_id_old IS NOT NULL;

UPDATE
    subscribers
SET
    account_id = gen_random_uuid()
WHERE
    account_id_old IS NULL;

ALTER TABLE subscribers DROP COLUMN account_id_old;

COMMIT;
