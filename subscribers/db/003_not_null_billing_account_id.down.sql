-- Copyright (C) 2018 WEHCO Media, Inc.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

BEGIN;

ALTER TABLE subscribers_billing_accounts RENAME COLUMN billing_account_id TO billing_account_id_old; 
ALTER TABLE subscribers_billing_accounts ADD COLUMN billing_account_id UUID NOT NULL;

UPDATE
    subscribers_billing_accounts
SET
    billing_account_id = billing_account_id_old
WHERE
    billing_account_id_old IS NOT NULL;

UPDATE
    subscribers_billing_accounts
SET
    billing_account_id = gen_random_uuid(),
    is_external = TRUE
WHERE
    billing_account_id_old IS NULL;

ALTER TABLE subscribers_billing_accounts DROP COLUMN billing_account_id_old;

COMMIT;
