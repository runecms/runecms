-- Copyright (C) 2018 WEHCO Media, Inc.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.


CREATE TABLE subscribers(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  account_id UUID NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp
);

CREATE TABLE subscribers_addresses(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE,
  address         TEXT,
  city            TEXT,
  state           TEXT,
  zip             TEXT,
  country         TEXT
);

CREATE TABLE subscribers_subscription_hold_types(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  name TEXT NOT NULL UNIQUE,
  description TEXT,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE subscribers_complaint_types(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  name TEXT NOT NULL UNIQUE,
  description TEXT,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE subscribers_complaint_statuses(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  name TEXT NOT NULL UNIQUE,
  description TEXT,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE subscribers_markets(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  name TEXT NOT NULL UNIQUE,
  description TEXT,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE subscribers_publications(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  name TEXT NOT NULL UNIQUE,
  description TEXT,
  market_id UUID NOT NULL REFERENCES subscribers_markets(id),
  billing_processor_data BYTEA NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE subscribers_billing_accounts(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  name TEXT NOT NULL,
  subscriber_id UUID REFERENCES subscribers(id),
  billing_account_id UUID NOT NULL,
  is_default boolean NOT NULL DEFAULT false,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE subscribers_subscription_statuses(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  name TEXT NOT NULL UNIQUE,
  description TEXT,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

-- weekly, monthly, yearly
CREATE TABLE subscribers_product_frequencies (
  id TEXT PRIMARY KEY,
  singular TEXT,
  plural TEXT
);
INSERT INTO
  subscribers_product_frequencies (
    id,
    singular,
    plural
  )
VALUES (
  'daily',
  'day',
  'days'
),(
  'weekly',
  'week',
  'weeks'
),(
  'monthly',
  'month',
  'months'
),(
  'yearly',
  'year',
  'years'
);

CREATE TABLE subscribers_product_types(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  name TEXT NOT NULL UNIQUE,
  description TEXT,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

-- In the Subscribers service, a product is essentially a subscription that a subscriber can purchase.
CREATE TABLE subscribers_products(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  name TEXT NOT NULL UNIQUE,
  description TEXT,
  price integer NOT NULL,
  product_type_id UUID NOT NULL REFERENCES subscribers_product_types(id),
  publication_id UUID NOT NULL REFERENCES subscribers_publications(id),
  billing_processor_data TEXT NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

-- an individual rate on a single product
-- this might refer to:
--  $10 for the first month
--  $10 for the first 3 weeks
--  $200 for the next 2 years
--  $10 forever
-- Products can have multiple rates. The order is determined by the "weight"
CREATE TABLE subscribers_product_rates(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  -- Weight determines the order relative to the other rates associated with this campaign
  weight INTEGER NOT NULL,
  product_id UUID REFERENCES subscribers_products(id) NOT NULL,
  price INTEGER NOT NULL,

  -- how often and for how much the rate is billed
  -- example: Every 2 weeks
  -- 2
  billing_term INTEGER NOT NULL,
  -- weeks
  billing_frequency_id TEXT REFERENCES subscribers_product_frequencies(id) NOT NULL,

  -- how long the rate lasts
  -- example: for 3 months
  -- 3
  rate_term INTEGER,
  -- months
  rate_frequency_id TEXT REFERENCES subscribers_product_frequencies(id),

  -- These are generated by the products serice based on the billing_term / frequency and the rate term / frequency
  start_at DATE NOT NULL,
  end_at DATE,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE subscribers_subscription_types(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  name TEXT NOT NULL UNIQUE,
  description TEXT,
  product_id UUID NOT NULL REFERENCES subscribers_products(id),
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE subscribers_delivery_addresses(
  id            UUID PRIMARY KEY  DEFAULT gen_random_uuid(),
  subscriber_id UUID                      NOT NULL REFERENCES subscribers(id),
  address_id    UUID                      NOT NULL REFERENCES subscribers_addresses(id),
  is_default    boolean                   NOT NULL DEFAULT false,
  created_at    TIMESTAMP WITH TIME ZONE  NOT NULL DEFAULT current_timestamp,
  deleted_at    TIMESTAMP WITH TIME ZONE
);

CREATE TABLE subscribers_subscriptions(
  id                      UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  rate                    integer         NOT NULL,
  active                  boolean         NOT NULL DEFAULT true,
  start_at                date            NOT NULL,
  end_at                  date            NOT NULL,
  subscription_status_id  UUID            NOT NULL REFERENCES subscribers_subscription_statuses(id),
  subscription_type_id    UUID            NOT NULL REFERENCES subscribers_subscription_types(id),
  subscriber_id           UUID            NOT NULL REFERENCES subscribers(id),
  billing_account_id      UUID            NOT NULL REFERENCES subscribers_billing_accounts(id),
  created_at TIMESTAMP    WITH TIME ZONE  NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP    WITH TIME ZONE,
  updated_at TIMESTAMP    WITH TIME ZONE  NOT NULL DEFAULT current_timestamp,
  last_bill_at            DATE            NOT NULL DEFAULT current_date,
  next_bill_at            DATE,
  balance                 INTEGER         NOT NULL DEFAULT 0,
  last_bill_failed        BOOLEAN
);

CREATE TABLE subscribers_physical_subscriptions(
  subscription_id     UUID  PRIMARY KEY REFERENCES subscribers_subscriptions(id),
  delivery_address_id UUID  NOT NULL    REFERENCES subscribers_delivery_addresses(id)
);

CREATE TABLE subscribers_authorized_users(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  subscriber_id UUID NOT NULL REFERENCES subscribers(id),
  subscription_id UUID NOT NULL REFERENCES subscribers_subscriptions(id),
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE subscribers_subscriptions_discounts(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  subscription_id UUID NOT NULL REFERENCES subscribers_subscriptions(id),
  discount_id integer NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE subscribers_subscriptions_addons(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  subscription_id UUID NOT NULL REFERENCES subscribers_subscriptions(id),
  addon_id integer NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE subscribers_subscription_holds(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  start_at date NOT NULL,
  end_at date NOT NULL,
  subscription_id UUID NOT NULL REFERENCES subscribers_subscriptions(id),
  subscription_hold_type_id UUID NOT NULL references subscribers_subscription_hold_types(id),
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp
);

CREATE TABLE subscribers_complaints(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  complaint TEXT,
  responded boolean NOT NULL DEFAULT false,
  complaint_type_id UUID NOT NULL REFERENCES subscribers_complaint_types(id),
  complaint_status_id UUID NOT NULL REFERENCES subscribers_complaint_statuses(id),
  subscription_id UUID NOT NULL REFERENCES subscribers_subscriptions(id),
  acknowledged_at TIMESTAMP WITH TIME ZONE,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp
);

CREATE TABLE subscribers_notes(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  body text NOT NULL,
  submitted_by integer NOT NULL,
  complaint_id UUID NOT NULL REFERENCES subscribers_complaints(id),
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

