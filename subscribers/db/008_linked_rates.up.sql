-- Copyright (C) 2018 WEHCO Media, Inc.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.


-- 1. Create the new column
-- 2. Create the foreign key constraints
-- 3. Update the tables to match the new layout
  ALTER TABLE
    subscribers_product_rates
  ADD COLUMN
    next_rate_id UUID;

  CREATE INDEX ON subscribers_product_rates(next_rate_id);

  ALTER TABLE
    subscribers_product_rates
  ADD CONSTRAINT next_rate_fk
  FOREIGN KEY (next_rate_id) REFERENCES subscribers_product_rates(id);

BEGIN;
  -- Update every rate
  UPDATE
    subscribers_product_rates
  SET
    next_rate_id = (
      SELECT
        id
      FROM
        subscribers_product_rates AS b
      WHERE
        b.weight > weight
      AND
        b.product_id = product_id
      ORDER BY b.weight ASC
      LIMIT 1
    );

  ALTER TABLE
    subscribers_product_rates
  RENAME COLUMN weight to weight_old;
  ALTER TABLE
    subscribers_product_rates
  ALTER COLUMN weight_old SET DEFAULT 0;
COMMIT;
