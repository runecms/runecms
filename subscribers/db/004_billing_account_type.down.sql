BEGIN;

ALTER TABLE
  subscribers_billing_accounts
DROP COLUMN
  billing_account_type;

DROP TABLE subscribers_billing_account_types;

COMMIT;
