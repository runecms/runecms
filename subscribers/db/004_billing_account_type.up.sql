BEGIN;

-- This table uses an int primary key because it corresponds to an enum in the protobuf
CREATE TABLE subscribers_billing_account_types(
  id INTEGER UNIQUE NOT NULL,
  name TEXT UNIQUE NOT NULL
);

INSERT INTO
  subscribers_billing_account_types
VALUES
  (0, 'credit card'),
  (1, 'bank draft'),
  (2, 'paypal'),
  (3, 'google pay'),
  (4, 'apply pay');

ALTER TABLE
  subscribers_billing_accounts
ADD COLUMN
  billing_account_type INTEGER NOT NULL DEFAULT 0;

CREATE INDEX subscribers_billing_accounts_account_types ON subscribers_billing_accounts (billing_account_type);
COMMIT;

BEGIN;

ALTER TABLE
  subscribers_billing_accounts
ADD CONSTRAINT billing_account_type_fk FOREIGN KEY (billing_account_type) REFERENCES subscribers_billing_account_types (id);

COMMIT;
