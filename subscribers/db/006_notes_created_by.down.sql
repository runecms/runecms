BEGIN;


ALTER TABLE
  subscribers_notes
RENAME COLUMN
  submitted_by
to
  submitted_by_old;


ALTER TABLE
  subscribers_notes
ADD COLUMN
  submitted_by integer;
 
ALTER TABLE
  subscribers_notes
DROP COLUMN
  submitted_by_old;


COMMIT;
