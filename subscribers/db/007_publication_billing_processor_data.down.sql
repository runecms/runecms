-- Copyright (C) 2018 WEHCO Media, Inc.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

BEGIN;

ALTER TABLE
  subscribers_publications
RENAME COLUMN billing_processor_data to billing_processor_data_text;


ALTER TABLE
  subscribers_publications
ADD COLUMN
  billing_processor_data BYTEA;

UPDATE
  subscribers_publications
SET
  billing_processor_data = billing_processor_data_bytes::BYTEA;

UPDATE
  subscribers_publications
SET
  billing_processor_data = '{}'::BYTEA
WHERE
  billing_processor_data IS NULL;

ALTER TABLE
  subscribers_publications
DROP COLUMN
  billing_processor_data_text;

COMMIT;
