-- Copyright (C) 2018 WEHCO Media, Inc.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.


DROP TABLE subscribers_notes;
DROP TABLE subscribers_complaints;
DROP TABLE subscribers_complaint_statuses;
DROP TABLE subscribers_subscriptions_discounts;
DROP TABLE subscribers_subscriptions_addons;
DROP TABLE subscribers_subscription_holds;
DROP TABLE subscribers_subscription_hold_types;
DROP TABLE subscribers_complaint_types;
DROP TABLE subscribers_authorized_users;
DROP TABLE subscribers_physical_subscriptions;
DROP TABLE subscribers_subscriptions;
DROP TABLE subscribers_subscription_statuses;
DROP TABLE subscribers_subscription_types;
DROP TABLE subscribers_products;
DROP TABLE subscribers_product_types;
DROP TABLE subscribers_product_rates;
DROP TABLE subscribers_product_frequencies;
DROP TABLE subscribers_delivery_addresses;
DROP TABLE subscribers_billing_accounts;
DROP TABLE subscribers_addresses;
DROP TABLE subscribers_publications;
DROP TABLE subscribers_markets;
DROP TABLE subscribers;
