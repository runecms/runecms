// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscribers

import (
	"context"
	"fmt"
	"time"

	"git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/google/uuid"
)

type MockProducts struct {
	products map[string]*subscriberspb.Product
}

func (p *MockProducts) CreateProduct(ctx context.Context, req *subscriberspb.CreateProductRequest) (*subscriberspb.CreateProductResponse, error) {
	if p.products == nil {
		p.products = make(map[string]*subscriberspb.Product)
	}
	id := uuid.New().String()
	p.products[id] = &subscriberspb.Product{
		Id:            id,
		Name:          req.GetName(),
		ProductTypeId: req.GetProductTypeId(),
		PublicationId: req.GetPublicationId(),
	}
	return &subscriberspb.CreateProductResponse{
		Product: p.products[id],
	}, nil
}

func (p *MockProducts) DeleteProduct(ctx context.Context, req *subscriberspb.DeleteProductRequest) (*subscriberspb.DeleteProductResponse, error) {
	return &subscriberspb.DeleteProductResponse{
		Product: &subscriberspb.Product{
			Id:        req.GetId(),
			DeletedAt: time.Now().Format(time.RFC3339),
		},
	}, nil
}

func (p *MockProducts) GetProduct(ctx context.Context, req *subscriberspb.GetProductRequest) (*subscriberspb.GetProductResponse, error) {
	if val, ok := p.products[req.GetId()]; ok {
		return &subscriberspb.GetProductResponse{
			Product: val,
		}, nil
	}
	return nil, fmt.Errorf("Not found")
}

func (p *MockProducts) GetProductByName(ctx context.Context, req *subscriberspb.GetProductByNameRequest) (*subscriberspb.GetProductByNameResponse, error) {
	return &subscriberspb.GetProductByNameResponse{
		Product: &subscriberspb.Product{
			Name: req.GetName(),
		},
	}, nil
}
