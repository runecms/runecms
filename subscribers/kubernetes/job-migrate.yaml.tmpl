apiVersion: batch/v1
kind: Job
metadata:
  name: migrate-${SERVICE}-${VERSION_SED}
spec:
  template:
    metadata:
      annotations:
        sidecar.istio.io/inject: "false"
    spec:
      serviceAccountName: cockroachdb
      initContainers:
      # The init-certs container sends a certificate signing request to the
      # kubernetes cluster.
      # You can see pending requests using: kubectl get csr
      # CSRs can be approved using:         kubectl certificate approve <csr name>
      #
      # In addition to the client certificate and key, the init-certs entrypoint will symlink
      # the cluster CA to the certs directory.
      - name: init-certs
        image: cockroachdb/cockroach-k8s-request-cert:0.3
        imagePullPolicy: IfNotPresent
        command:
        - "/bin/ash"
        - "-ecx"
        - "/request-cert -namespace=$${POD_NAMESPACE} -certs-dir=/cockroach-certs -type=client -user=rune -symlink-ca-from=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
        env:
        - name: POD_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        volumeMounts:
        - name: client-certs
          mountPath: /cockroach-certs
      volumes:
      - name: client-certs
        emptyDir: {}
      containers:
      - name: migrate-${SERVICE}-${VERSION_SED}
        image: ${DOCKER_IMAGE}
        imagePullPolicy: Always
        volumeMounts:
        - mountPath: /tmp/pod
          name: tmp-pod
        command: 
        - "/bin/sh"
        - "-c"
        - '/app/migrate -database "$(RUNE_MIGRATE_DATABASE_URL)&x-migrations-table=$(DATABASE_MIGRATIONS_TABLE)&x-lock-table=$(DATABASE_MIGRATIONS_LOCK_TABLE)" -path /db up'
        envFrom:
        - configMapRef:
            name: rune-config
        env:
        - name: DATABASE_MIGRATIONS_TABLE
          value: schema_${SERVICE}_migrations
        - name: DATABASE_MIGRATIONS_LOCK_TABLE
          value: schema_${SERVICE}_lock
        volumeMounts:
        - name: client-certs
          mountPath: /cockroach-certs
      restartPolicy: Never
  backoffLimit: 3
