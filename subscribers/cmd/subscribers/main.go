// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"database/sql"
	"flag"
	"net/http"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/billing/rpc"
	server "git.runecms.io/rune/runecms/subscribers/twirp"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/go-redis/redis"
	"github.com/golang/glog"
	_ "github.com/lib/pq"
	"github.com/ory/ladon"
	manager "github.com/wehco/ladon-community/manager/redis"
)

func init() {
	flag.Parse()
}

func main() {
	config, err := runecms.GetConfig()
	if err != nil {
		glog.Fatalln("Unable to get config. Error:", err.Error())
	}
	glog.Infoln("Connecting to database...")
	db, err := sql.Open("postgres", config.RuneDBURL)
	if err != nil {
		glog.Fatalln(err.Error())
	}
	defer db.Close()

	opt, err := redis.ParseURL(config.LadonRedisURL)
	if err != nil {
		glog.Fatalln("Error parsing redis URL. Error:", err.Error())
	}
	glog.Infoln("Connecting to ladon (redis) database...")
	redisClient := redis.NewClient(opt)

	err = db.Ping()
	if err != nil {
		glog.Fatalln("Could not verify database connection")
	}

	glog.Infoln("Connection to database established")

	db.SetMaxOpenConns(config.DatabaseConnectionLimit)

	billingClient := billingpb.NewBillingAccountsProtobufClient(config.RuneBillingAddress, &http.Client{})
	webhooksClient := webhookspb.NewWebhooksProtobufClient(config.RuneWebhooksAddress, &http.Client{})

	warden := &ladon.Ladon{
		Manager: manager.NewRedisManager(redisClient, "ladon"),
	}

	if err != nil {
		glog.Fatalf(err.Error())
	}

	s := server.NewServer(db, warden, billingClient, webhooksClient)
	err = s.Start(config.ListenPort)

	if err != nil {
		glog.Fatalf(err.Error())
	}
}
