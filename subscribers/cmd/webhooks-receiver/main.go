// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/internal/webhookutil"
	subscriberspb "git.runecms.io/rune/runecms/subscribers/rpc"
	userspb "git.runecms.io/rune/runecms/users/rpc"
	pb "git.runecms.io/rune/runecms/webhooks/rpc"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/ory/ladon"
	manager "github.com/ory/ladon/manager/sql"
)

var (
	webhooks    pb.Webhooks
	subscribers subscriberspb.Subscribers
	identity    runecms.Identity
	ctx         context.Context
)

const service = "subscribers"

func init() {
	config, err := runecms.GetConfig()
	if err != nil {
		log.Panicln("Unable to get config. Error:", err.Error())
	}

	identity = runecms.Be(fmt.Sprintf("%s-webhooks-receiver", service), "rune-internal")
	ctx, err = identity.ToRemoteTwirpContext()
	if err != nil {
		log.Panic(err)
	}

	webhooks = pb.NewWebhooksProtobufClient(config.RuneWebhooksAddress, &http.Client{})
	subscribers = subscriberspb.NewSubscribersProtobufClient(config.RuneSubscribersAddress, &http.Client{})

	name := fmt.Sprintf("rune-internal-webhooks-%s", service)
	_, err = webhooks.Get(ctx, &pb.GetWebhookRequest{
		Name: name,
	})
	if err != nil {
		// Attempt to retrieve the webhook endpoint...
		_, err = webhooks.Create(ctx, &pb.CreateWebhookRequest{
			Name:        name,
			Description: fmt.Sprintf("Internal webhook for provisioning initial use rdata in the %s service", service),
			Url:         fmt.Sprintf("rune-%s-webhook-receiver.default.svc.cluster.local:3000", service),
		})
		log.Panicln("Unable to get or create webhook endpoint. Error:", err.Error())
	}

	log.SetPrefix(fmt.Sprintf("[%s-webhooks-receiver] ", service))
}

type UsersReceiver struct {
	Warden *ladon.Ladon
}

func (s *UsersReceiver) Create(u *userspb.User) error {
	if u.GetId() == "" {
		return runecms.Error(runecms.InvalidArgument, "The user ID provided is empty")
	}
	return nil
	// Create the subscriber object
	res, err := subscribers.CreateSubscriber(ctx, &subscriberspb.CreateSubscriberRequest{
		AccountId: u.GetId(),
	})
	if err != nil {
		return err
	}
	model := res.GetSubscriber()

	// Give them access to their resources
	if err := s.Warden.Manager.Create(&ladon.DefaultPolicy{
		ID:          fmt.Sprintf("subscribers-%s-introspection", model.GetId()),
		Description: "Give subscribers access to their resources",
		Subjects: []string{
			runecms.UserSubject(model.GetAccountId()),
		},
		Effect: ladon.AllowAccess,
		Resources: []string{
			fmt.Sprintf("subscribers:%s:<.*>", model.GetAccountId()),
		},
		Actions: []string{"create", "read", "update", "delete"},
	}); err != nil {
		return runecms.Error(runecms.Internal, err.Error())
	}

	if err := s.Warden.Manager.Create(&ladon.DefaultPolicy{
		ID:          fmt.Sprintf("subscribers-%s-default-introspection", model.GetId()),
		Description: "Give subscribers access to create subscriptions, holds, delivery addresses, complaints, and billing accounts",
		Subjects: []string{
			runecms.UserSubject(model.GetAccountId()),
		},
		Effect: ladon.AllowAccess,
		Resources: []string{
			"subscribers:billing_accounts",
			"subscribers:complaints",
			"subscribers:delivery_addresses",
			"subscribers:subscriptions",
			"subscribers:subscription_holds",
		},
		Actions: []string{"create"},
	}); err != nil {
		return runecms.Error(runecms.Internal, err.Error())
	}

	if err := s.Warden.Manager.Create(&ladon.DefaultPolicy{
		ID:          fmt.Sprintf("subscribers-%s-default-global-usage", model.GetId()),
		Description: "Give subscribers access to read meta types",
		Subjects: []string{
			runecms.UserSubject(model.GetAccountId()),
		},
		Effect: ladon.AllowAccess,
		Resources: []string{
			"subscribers:addresses",
			"subscribers:complaint_statuses",
			"subscribers:complaint_types",
			"subscribers:markets",
			"subscribers:publications",
			"subscribers:products",
			"subscribers:product_types",
			"subscribers:subscription_hold_types",
			"subscribers:subscription_statuses",
			"subscribers:subscription_types",
		},
		Actions: []string{"read"},
	}); err != nil {
		return runecms.Error(runecms.Internal, err.Error())
	}
	return nil
}

func (s *UsersReceiver) Update(u *userspb.User) error {
	return nil
}

func (s *UsersReceiver) Delete(u *userspb.User) error {
	return nil
}

func main() {
	config, err := runecms.GetConfig()
	if err != nil {
		log.Panicln("Unable to get config. Error:", err.Error())
	}

	log.Println("Connecting to ladon database...")
	ladonDB, err := sqlx.Open("postgres", config.LadonDBURL)
	if err != nil {
		log.Panicln(err.Error())
	}
	defer ladonDB.Close()

	if err := ladonDB.Ping(); err != nil {
		log.Panicln(err.Error())
	}
	log.Println("Connected to ladon database")

	ladonDB.SetMaxOpenConns(config.DatabaseConnectionLimit)
	warden := &ladon.Ladon{
		Manager: manager.NewSQLManager(ladonDB, nil),
	}
	server := webhookutil.UsersWebhookReceiver{
		Receiver: &UsersReceiver{
			Warden: warden,
		},
	}

	log.Println(server.ListenAndServe(":3000"))
}
