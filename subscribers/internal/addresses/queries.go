package addresses

var (
	SqlFindAddress = `
	SELECT
		id,
		created_at
	FROM
		subscribers_addresses
	WHERE
		address = $1
	AND
		city		= $2
	AND
		state		= $3
	AND
		zip			= $4
	AND
		country = $5
	AND
		deleted_at IS NULL
	`

	SqlGetAddress = `
	SELECT
		address,
		city,
		state,
		zip,
		country,
		created_at
	FROM
		subscribers_addresses
	WHERE
		id = $1
	AND
		deleted_at IS NULL
	`

	SqlCreateAddress = `
		INSERT INTO
			subscribers_addresses (
				address,
				city,
				state,
				zip,
				country,
				created_at
			)
		VALUES (
			$1,
			$2,
			$3,
			$4,
			$5,
			current_timestamp
		)
		RETURNING id, created_at
	`

	SqlDeleteAddress = `
		UPDATE
			subscribers_addresses
		SET
			deleted_at = current_timestamp
		WHERE
			id = $1
		RETURNING
			address,
			city,
			state,
			zip,
			country,
			created_at,
			deleted_at
	`
)
