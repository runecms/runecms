// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package addresses

import (
	"context"
	"database/sql"
	"strings"

	"git.runecms.io/rune/runecms"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/ory/ladon"
)

func CreateAddress(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	address string,
	city string,
	state string,
	zip string,
	country string,
) (*models.Address, error) {
	if err := runecms.Access(ident, w, "create", "subscribers:addresses"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if address == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "address can not be empty")
	}
	if city == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "city can not be empty")
	}
	if state == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "state can not be empty")
	}
	if zip == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "zip can not be empty")
	}
	if country == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "country can not be empty")
	}

	address = strings.ToLower(address)
	state = strings.ToLower(state)
	city = strings.ToLower(city)
	zip = strings.ToLower(zip)
	country = strings.ToLower(country)

	model := &models.Address{
		Address: address,
		City:    city,
		State:   state,
		Zip:     zip,
		Country: country,
	}

	row := db.QueryRowContext(ctx, SqlCreateAddress, address, city, state, zip, country)
	if err := row.Scan(&model.Id, &model.CreatedAt); err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:addresses", "create", model)
	return model, nil
}

func DeleteAddress(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	id string,
) (*models.Address, error) {
	if err := runecms.Access(ident, w, "delete", "subscribers:addresses"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.Address{
		Id: id,
	}

	row := db.QueryRowContext(ctx, SqlDeleteAddress, id)
	if err := row.Scan(
		&model.Address,
		&model.City,
		&model.State,
		&model.Zip,
		&model.Country,
		&model.CreatedAt,
		&model.DeletedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:addresses", "delete", model)
	return model, nil
}

func GetAddressById(ctx context.Context, db *sql.DB, w *ladon.Ladon, ident runecms.Identity, id string) (*models.Address, error) {
	if err := runecms.Access(ident, w, "read", "subscribers:addresses"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.Address{
		Id: id,
	}
	row := db.QueryRowContext(ctx, SqlGetAddress, id)
	if err := row.Scan(
		&model.Address,
		&model.City,
		&model.State,
		&model.Zip,
		&model.Country,
		&model.CreatedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	return model, nil
}

func FindAddress(ctx context.Context, db *sql.DB, w *ladon.Ladon, ident runecms.Identity,
	addr string,
	city string,
	state string,
	zip string,
	country string,
) (*models.Address, error) {
	if err := runecms.Access(ident, w, "read", "subscribers:addresses"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	addr = strings.ToLower(addr)
	city = strings.ToLower(city)
	state = strings.ToLower(state)
	zip = strings.ToLower(zip)
	country = strings.ToLower(country)

	model := &models.Address{
		Address: addr,
		City:    city,
		State:   state,
		Zip:     zip,
		Country: country,
	}

	row := db.QueryRowContext(ctx, SqlFindAddress, addr, city, state, zip, country)
	if err := row.Scan(
		&model.Address,
		&model.City,
		&model.State,
		&model.Zip,
		&model.Country,
		&model.CreatedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	return model, nil
}
