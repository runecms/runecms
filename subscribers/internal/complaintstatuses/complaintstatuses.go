// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package complaintstatuses

import (
	"context"
	"database/sql"

	"git.runecms.io/rune/runecms"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/ory/ladon"
)

func CreateComplaintStatus(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	name string,
	description string,
) (*models.ComplaintStatus, error) {
	if err := runecms.Access(ident, w, "create", "subscribers:complaint_statuses"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	model := &models.ComplaintStatus{
		Name:        name,
		Description: description,
	}

	if err := db.QueryRowContext(
		ctx,
		SqlCreateComplaintStatus,
		name,
		description,
	).Scan(
		&model.Id,
		&model.CreatedAt,
	); err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:complaint_statuses", "create", model)
	return model, nil
}

func DeleteComplaintStatus(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	id string,
) (*models.ComplaintStatus, error) {
	if err := runecms.Access(ident, w, "delete", "subscribers:complaint_statuses"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if _, err := db.ExecContext(
		ctx,
		SqlDeleteComplaintStatus,
		id,
	); err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}
	model, err := GetComplaintStatus(ctx, db, w, ident, id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:complaint_statuses", "delete", model)
	return model, nil
}

func GetComplaintStatus(ctx context.Context, db *sql.DB, w *ladon.Ladon, ident runecms.Identity, id string) (*models.ComplaintStatus, error) {
	if err := runecms.Access(ident, w, "read", "subscribers:complaint_statuses"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.ComplaintStatus{
		Id: id,
	}
	var (
		deletedAt sql.NullString
	)
	if err := db.QueryRowContext(
		ctx,
		SqlGetComplaintStatus,
		id,
	).Scan(
		&model.Name,
		&model.Description,
		&model.CreatedAt,
		&deletedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}
	model.DeletedAt = deletedAt.String
	return model, nil
}

func GetComplaintStatusByName(ctx context.Context, db *sql.DB, w *ladon.Ladon, ident runecms.Identity, name string) (*models.ComplaintStatus, error) {
	if err := runecms.Access(ident, w, "read", "subscribers:complaint_statuses"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.ComplaintStatus{
		Name: name,
	}
	var (
		deletedAt sql.NullString
	)
	if err := db.QueryRowContext(
		ctx,
		SqlGetComplaintStatus,
		name,
	).Scan(
		&model.Id,
		&model.Description,
		&model.CreatedAt,
		&deletedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}
	model.DeletedAt = deletedAt.String
	return model, nil
}
