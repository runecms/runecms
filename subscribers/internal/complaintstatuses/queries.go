// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package complaintstatuses

var (
	SqlCreateComplaintStatus = `
	INSERT INTO
		subscribers_complaint_statuses (
			name,
			description
		)
	VALUES (
		$1,
		$2
	)
	RETURNING id, created_at
	`

	SqlGetComplaintStatus = `
	SELECT
		name,
		description,
		created_at,
		deleted_at
	FROM
		subscribers_complaint_statuses
	WHERE
		id = $1
	`

	SqlGetComplaintStatusByName = `
	SELECT
		id,
		description,
		created_at,
		deleted_at
	FROM
		subscribers_complaint_statuses
	WHERE
		name = $1
	`

	SqlDeleteComplaintStatus = `
	UPDATE
		subscribers_complaint_statuses
	SET
		deleted_at = current_timestamp
	WHERE
		id = $1
	RETURNING
		deleted_at
	`
)
