// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package complaints

var (
	SqlCreateComplaint = `
	INSERT INTO
		subscribers_complaints (
			complaint,
			complaint_type_id,
			complaint_status_id,
			subscription_id
		)
	VALUES (
		$1,
		$2,
		$3,
		$4
	)
	RETURNING
		id,
		created_at,
		updated_at
	`

	SqlDeleteComplaint = `
	UPDATE
		subscribers_complaints
	SET
		deleted_at = current_timestamp
	WHERE
		id = $1
	AND
		deleted_at IS NULL
	`

	SqlGetComplaint = `
	SELECT
		a.complaint,
		a.responded,
		a.complaint_type_id,
		a.complaint_status_id,
		a.subscription_id,
		a.acknowledged_at,
		a.created_at,
		a.updated_at,
		a.deleted_at,
		b.id,
		b.name,
		b.description,
		b.created_at,
		b.deleted_at,
		c.id,
		c.name,
		c.description,
		c.created_at,
		c.deleted_at
	FROM
		subscribers_complaints			   AS a,
		subscribers_complaint_types    AS b,
		subscribers_complaint_statuses AS c
	WHERE
		a.id = $1
	AND
		b.id = a.complaint_type_id
	AND
		c.id = a.complaint_status_id
	`

	SqlGetNotes = `
	SELECT
		body,
		submitted_by,
		created_at,
		deleted_at
	FROM
		subscribers_notes
	WHERE
		complaint_id = $1
	AND
		deleted_at IS NULL
	ORDER BY
		created_at ASC
	`
	SqlUpdateComplaint = `
	UPDATE
		subscribers_complaints
	SET
		complaint							= $1,
		responded							= $2,
		complaint_type_id 		= $3,
		complaint_status_id		= $4,
		acknowledged_at				= $5
	WHERE
		id = $6
	AND
		deleted_at IS NULL
	`

	SqlDeleteNotes = `
	UPDATE
		subscribers_notes
	SET
		deleted_at = current_timestamp
	WHERE
		complaint_id = $1
	AND
		deleted_at IS NULL
	`

	SqlAddNote = `
	INSERT INTO
		subscribers_notes (
			body,
			submitted_by,
			complaint_id
		)
	VALUES (
		$1,
		$2,
		$3
	)
	RETURNING id, created_at
	`
)
