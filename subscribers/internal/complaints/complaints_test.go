// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package complaints

import (
	"context"
	"database/sql"
	"os"
	"testing"
	"time"

	"git.runecms.io/rune/runecms"
	billingtest "git.runecms.io/rune/runecms/billing"
	billingpb "git.runecms.io/rune/runecms/billing/rpc"
	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/billingaccounts"
	"git.runecms.io/rune/runecms/subscribers/internal/complaintstatuses"
	"git.runecms.io/rune/runecms/subscribers/internal/complainttypes"
	"git.runecms.io/rune/runecms/subscribers/internal/markets"
	"git.runecms.io/rune/runecms/subscribers/internal/products"
	"git.runecms.io/rune/runecms/subscribers/internal/producttypes"
	"git.runecms.io/rune/runecms/subscribers/internal/publications"
	"git.runecms.io/rune/runecms/subscribers/internal/subscribers"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptions"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptionstatuses"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptiontypes"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"

	"github.com/google/go-cmp/cmp"
	"github.com/ory/ladon"
)

var (
	ctx            = context.Background()
	w              *ladon.Ladon
	db             *sql.DB
	valid_id       runecms.Identity
	invalid_id     runecms.Identity
	billing        *models.BillingAccount
	ctype          *models.ComplaintType
	cstatus        *models.ComplaintStatus
	publication    *models.Publication
	subscriber     *models.Subscriber
	sstatus        *models.SubscriptionStatus
	market         *models.Market
	producttype    *models.ProductType
	product        *models.Product
	stype          *models.SubscriptionType
	subscription   *models.Subscription
	billingClient  billingpb.BillingAccounts
	webhooksClient = &webhooks.TestClient{}
	exampleUuid    = "63616665-6630-3064-6465-616462656562"
)

func GetTestDb(t *testing.T) {
	var err error
	if db != nil {
		return
	}
	w = runetestutil.GetTestLadon()
	db = runetestutil.GetTestDb(t)
	if err != nil {
		t.Fatal(err)
	}

	valid_id = runecms.Be("valid", "admin")
	invalid_id = runecms.Be("invalid", "invalid")

	billingClient = billingtest.NewTestClient(t)
	market, err = markets.CreateMarket(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint")
	if err != nil {
		t.Fatal(err.Error())
	}
	publication, err = publications.CreatePublication(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint", market.Id, "{}")
	if err != nil {
		t.Fatal(err.Error())
	}

	sstatus, err = subscriptionstatuses.CreateSubscriptionStatus(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint")
	if err != nil {
		t.Fatal(err.Error())
	}

	producttype, err = producttypes.CreateProductType(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint")
	if err != nil {
		t.Fatal(err.Error())
	}

	product, err = products.CreateProduct(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test product", 1000, producttype.Id, publication.Id)
	if err != nil {
		t.Fatal(err.Error())
	}

	stype, err = subscriptiontypes.CreateSubscriptionType(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint", product.Id)
	if err != nil {
		t.Fatal(err.Error())
	}

	ctype, err = complainttypes.CreateComplaintType(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint")
	if err != nil {
		t.Fatal(err.Error())
	}

	cstatus, err = complaintstatuses.CreateComplaintStatus(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint")
	if err != nil {
		t.Fatal(err.Error())
	}

	subscriber, err = subscribers.CreateSubscriber(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID())
	if err != nil {
		t.Fatal(err.Error())
	}

	billing, err = billingaccounts.CreateBillingAccount(ctx, db, w, webhooksClient, billingClient, valid_id, "new complaint", false, false, subscriber.Id, exampleUuid, "4321", "Test User", time.Now().AddDate(1, 1, 0), models.BillingAccountType_GOOGLE_PAY)
	if err != nil {
		t.Fatal(err.Error())
	}

	subscription, err = subscriptions.CreateSubscription(ctx, db, w, webhooksClient, valid_id,
		true,
		1000,
		time.Now(),
		time.Now().AddDate(1, 1, 0),
		sstatus.Id,
		stype.Id,
		subscriber.Id,
		billing.Id,
		nil,
	)
	if err != nil {
		t.Fatal(err.Error())
	}

}

func TestMain(m *testing.M) {
	i := m.Run()
	runetestutil.KillAll()
	os.Exit(i)
}

func TestCreate(t *testing.T) {
	GetTestDb(t)
	t.Run("Create subscription", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			model, err := CreateComplaint(
				ctx,
				db,
				w,
				webhooksClient,
				valid_id,
				"Test new complaint",
				ctype.Id,
				cstatus.Id,
				subscription.Id,
			)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Fatal("Returned model is nil")
			}

			// Make sure we were given an id
			if model.Id == "" {
				t.Error("Returned model does not have an id")
			}

			if cmp.Equal(model.CreatedAt, time.Time{}) {
				t.Error("Returned model's CreatedAt was not set")
			}
		})
		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := CreateComplaint(
				ctx,
				db,
				w,
				webhooksClient,
				invalid_id,
				"Test new complaint",
				ctype.Id,
				cstatus.Id,
				subscription.Id,
			)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})
	})
}

func TestGet(t *testing.T) {
	GetTestDb(t)
	model, err := CreateComplaint(
		ctx,
		db,
		w,
		webhooksClient,
		valid_id,
		"Test new complaint",
		ctype.Id,
		cstatus.Id,
		subscription.Id,
	)
	if err != nil {
		t.Fatalf("Unable to set up test for Get: %s", err.Error())
	}

	t.Run("Get complaint", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := GetComplaint(ctx, db, w, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Error("Returned model is nil")
			}

			if cmp.Equal(model, m) != true {
				t.Errorf("models do not match\n%s\n", cmp.Diff(model, m))
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := GetComplaint(ctx, db, w, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := GetComplaint(ctx, db, w, invalid_id, "invaluduuid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})

}

func TestDelete(t *testing.T) {
	GetTestDb(t)
	model, err := CreateComplaint(
		ctx,
		db,
		w,
		webhooksClient,
		valid_id,
		"Test delete complaint",
		ctype.Id,
		cstatus.Id,
		subscription.Id,
	)
	if err != nil {
		t.Fatalf("Unable to set up test for Delete: %s", err.Error())
	}

	t.Run("Delete complaint", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := DeleteComplaint(ctx, db, w, webhooksClient, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if m == nil {
				t.Error("Returned model is nil")
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := DeleteComplaint(ctx, db, w, webhooksClient, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := DeleteComplaint(ctx, db, w, webhooksClient, invalid_id, "invaliduuid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})
}

func TestPoll(t *testing.T) {
	GetTestDb(t)
}
