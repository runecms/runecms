// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package complaints

import (
	"context"
	"database/sql"
	"fmt"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptions"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/ory/ladon"
)

func getAccountId(db *sql.DB, id string) (string, error) {
	const unassigned = "unassigned"
	var accountId sql.NullString

	if err := db.QueryRow(`
        SELECT
            subscribers.account_id
        FROM
            subscribers,
            subscribers_subscriptions   as subscriptions,
            subscribers_complaints		as complaints
        WHERE
            complaints.id = $1
        AND
            subscriptions.id = complaints.subscription_id
        AND
            subscribers.id = subscriptions.subscriber_id
    `, id).Scan(&accountId); err != nil {
		return "", runecms.Error(runecms.Internal, err.Error())
	}

	if accountId.Valid {
		return accountId.String, nil
	}
	return unassigned, nil
}

func CreateComplaint(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	complaint string,
	complaintTypeId string,
	complaintStatusId string,
	subscriptionId string,
) (*models.Complaint, error) {
	if err := runecms.Access(ident, w, "create", "subscribers:complaints"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	var (
		id        string
		createdAt string
		updatedAt string
	)
	row := db.QueryRowContext(
		ctx,
		SqlCreateComplaint,
		complaint,
		complaintTypeId,
		complaintStatusId,
		subscriptionId,
	)

	if err := row.Scan(&id, &createdAt, &updatedAt); err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	model, err := GetComplaint(ctx, db, w, ident, id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:complaints", "create", model)
	return model, nil
}

// DeleteComplaint will delete a complaint given a database connection
func DeleteComplaint(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	id string,
) (*models.Complaint, error) {
	accountId, err := getAccountId(db, id)
	if err != nil {
		return nil, err
	}
	resource := fmt.Sprintf("subscribers:%s:complaints:%s", accountId, id)

	if err := runecms.Access(ident, w, "delete", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	_, err = db.ExecContext(
		ctx,
		SqlDeleteComplaint,
		id,
	)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	model, err := GetComplaint(ctx, db, w, ident, id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:complaints", "delete", model)
	return model, nil
}

// GetComplaint retrieves a single complaint given a database connection
func GetComplaint(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	id string,
) (*models.Complaint, error) {
	accountId, err := getAccountId(db, id)
	if err != nil {
		return nil, err
	}

	resource := fmt.Sprintf("subscribers:%s:complaints:%s", accountId, id)
	if err := runecms.Access(ident, w, "read", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	var (
		acknowledgedAt           sql.NullString
		deletedAt                sql.NullString
		complaintStatusDeletedAt sql.NullString
		complaintTypeDeletedAt   sql.NullString
	)

	model := &models.Complaint{
		Id:              id,
		ComplaintStatus: &models.ComplaintStatus{},
		ComplaintType:   &models.ComplaintType{},
	}

	if err := db.QueryRowContext(
		ctx,
		SqlGetComplaint,
		id,
	).Scan(
		&model.Complaint,
		&model.Responded,
		&model.ComplaintTypeId,
		&model.ComplaintStatusId,
		&model.SubscriptionId,
		&acknowledgedAt,
		&model.CreatedAt,
		&model.UpdatedAt,
		&deletedAt,
		&model.ComplaintType.Id,
		&model.ComplaintType.Name,
		&model.ComplaintType.Description,
		&model.ComplaintType.CreatedAt,
		&complaintTypeDeletedAt,
		&model.ComplaintStatus.Id,
		&model.ComplaintStatus.Name,
		&model.ComplaintStatus.Description,
		&model.ComplaintStatus.CreatedAt,
		&complaintStatusDeletedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	model.DeletedAt = deletedAt.String
	model.AcknowledgedAt = acknowledgedAt.String
	model.ComplaintType.DeletedAt = complaintTypeDeletedAt.String
	model.ComplaintStatus.DeletedAt = complaintStatusDeletedAt.String

	// Step 2: Get notes for this complaint
	notes := make([]*models.Note, 0)

	rows, err := db.QueryContext(ctx, SqlGetNotes, id)
	if err == nil {
		note := &models.Note{
			ComplaintId: id,
		}
		var deletedAt sql.NullString
		if err := rows.Scan(
			&note.Body,
			&note.SubmittedBy,
			&note.CreatedAt,
			&deletedAt,
		); err == nil {
			if deletedAt.Valid {
				note.DeletedAt = deletedAt.String
			}
			notes = append(notes, note)
		}
	}
	model.Notes = notes
	subscription, err := subscriptions.GetSubscription(ctx, db, w, ident, model.SubscriptionId)
	if err != nil {
		return nil, err
	}

	model.Subscription = subscription
	return model, nil
}

// UpdateComplaintFromModel will accept a model and a database connection to update a single complaint
func UpdateComplaintFromModel(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	model *models.Complaint,
) (*models.Complaint, error) {
	accountId, err := getAccountId(db, model.GetId())
	if err != nil {
		return nil, err
	}
	resource := fmt.Sprintf("subscribers:%s:complaints:%s", accountId, model.GetId())

	if err := runecms.Access(ident, w, "update", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if len(model.Notes) != 0 {
		tx, err := db.BeginTx(ctx, nil)
		if err != nil {
			return nil, runecms.Error(runecms.Internal, err.Error())
		}

		if _, err := tx.ExecContext(
			ctx,
			SqlDeleteNotes,
			model.Id,
		); err != nil {
			if err != sql.ErrNoRows {
				return nil, runecms.Error(runecms.Internal, err.Error())
			}
		}

		for _, v := range model.Notes {
			if err := db.QueryRowContext(
				ctx,
				SqlAddNote,
				v.Body,
				accountId,
				model.Id,
			).Scan(
				&v.Id,
				&v.CreatedAt,
			); err != nil {
				return nil, runecms.Error(runecms.Internal, "Error adding note for complaint", model.Id, err.Error())
			}
		}

		if err := tx.Commit(); err != nil {
			return nil, runecms.Error(runecms.Internal, "Error committing transaction", err.Error())
		}
	}

	subscription, err := subscriptions.GetSubscription(ctx, db, w, ident, model.SubscriptionId)
	if err != nil {
		return nil, err
	}

	model.Subscription = subscription
	go webhooks.Distribute(webhooksClient, ident, "subscribers:complaints", "update", model)
	return model, nil
}
