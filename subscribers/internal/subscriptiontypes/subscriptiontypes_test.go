// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscriptiontypes

import (
	"context"
	"database/sql"
	"os"
	"testing"
	"time"

	"git.runecms.io/rune/runecms"
	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/markets"
	"git.runecms.io/rune/runecms/subscribers/internal/products"
	"git.runecms.io/rune/runecms/subscribers/internal/producttypes"
	"git.runecms.io/rune/runecms/subscribers/internal/publications"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"

	"github.com/google/go-cmp/cmp"
	"github.com/ory/ladon"
)

var (
	ctx            = context.Background()
	w              *ladon.Ladon
	db             *sql.DB
	valid_id       runecms.Identity
	invalid_id     runecms.Identity
	market         *models.Market
	publication    *models.Publication
	producttype    *models.ProductType
	product        *models.Product
	webhooksClient = &webhooks.TestClient{}
)

func GetTestDb(t *testing.T) {
	var err error
	w = runetestutil.GetTestLadon()
	db = runetestutil.GetTestDb(t)

	valid_id = runecms.Be("valid", "admin")
	invalid_id = runecms.Be("invalid", "invalid")

	market, err = markets.CreateMarket(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test subscription type")
	if err != nil {
		t.Fatal(err.Error())
	}

	publication, err = publications.CreatePublication(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test subscription type", market.Id, "{}")
	if err != nil {
		t.Fatal(err.Error())
	}

	producttype, err = producttypes.CreateProductType(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test subscription type")
	if err != nil {
		t.Fatal(err.Error())
	}

	product, err = products.CreateProduct(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test subscription type", 1000, producttype.Id, publication.Id)
	if err != nil {
		t.Fatal(err.Error())
	}
}

func getsubscriptiontype() (*models.SubscriptionType, error) {
	return CreateSubscriptionType(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "Test Subscription Type", product.Id)
}

func TestMain(m *testing.M) {
	i := m.Run()
	runetestutil.KillAll()
	os.Exit(i)
}

func TestCreate(t *testing.T) {
	GetTestDb(t)
	t.Run("Create subscriptiontype", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			model, err := CreateSubscriptionType(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "Create subscriptiontype", product.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Fatal("Returned model is nil")
			}

			// Make sure we were given an id
			if model.Id == "" {
				t.Error("Returned model does not have an id")
			}

			if cmp.Equal(model.CreatedAt, time.Time{}) {
				t.Error("Returned model's CreatedAt was not set")
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := CreateSubscriptionType(ctx, db, w, webhooksClient, invalid_id, runetestutil.GetUUID(), "Unauthorized subscription type", product.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid input", func(t *testing.T) {
			_, err := CreateSubscriptionType(ctx, db, w, webhooksClient, invalid_id, runetestutil.GetUUID(), "Invalid subscription type input", product.Id)
			if err == nil {
				t.Error("Providing an invalid input should return an error")
			}
		})
	})
}

func TestGet(t *testing.T) {
	GetTestDb(t)
	model, err := CreateSubscriptionType(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "get subscriptiontype", product.Id)
	if err != nil {
		t.Fatal("Unable to set up test for Get")
	}

	if model == nil {
		t.Fatal("Unable to set up test for Get; model is nil")
	}

	t.Run("Get subscriptiontype", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := GetSubscriptionType(ctx, db, w, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if m == nil {
				t.Fatal("Returned model is nil")
			}

			if cmp.Equal(model, m) != true {
				t.Fatalf("models do not match\n%s\n", cmp.Diff(model, m))
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := GetSubscriptionType(ctx, db, w, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := GetSubscriptionType(ctx, db, w, invalid_id, "invalid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})

}

func TestDelete(t *testing.T) {
	GetTestDb(t)
	model, err := CreateSubscriptionType(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "delete subscriptiontype", product.Id)
	if err != nil {
		t.Fatal("Unable to set up test for Delete")
	}

	t.Run("Delete subscriptiontype", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := DeleteSubscriptionType(ctx, db, w, webhooksClient, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if m == nil {
				t.Error("Returned model is nil")
			}
			if m.DeletedAt == "" {
				t.Error("Returned model's DeletedAt is nil")
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := DeleteSubscriptionType(ctx, db, w, webhooksClient, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := DeleteSubscriptionType(ctx, db, w, webhooksClient, invalid_id, "invalid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})
}
