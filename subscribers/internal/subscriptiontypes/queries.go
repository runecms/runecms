// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscriptiontypes

var (
	SqlGetSubscriptionType = `
	SELECT
		a.name,
		a.description,
		a.product_id,
		a.created_at,
		a.deleted_at,
		p.id,
		p.name,
		p.description,
		p.price,
		p.product_type_id,
		p.publication_id,
		p.billing_processor_data,
		p.created_at,
		p.updated_at,
		p.deleted_at
	FROM
		subscribers_subscription_types AS a,
		subscribers_products AS p
	WHERE
		a.id = $1
	AND
		p.id = a.product_id
	`

	SqlGetSubscriptionTypeByName = `
	SELECT
		a.id,
		a.description,
		a.product_id,
		a.created_at,
		a.deleted_at,
		p.id,
		p.name,
		p.description,
		p.price,
		p.product_type_id,
		p.publication_id,
		p.billing_processor_data,
		p.created_at,
		p.updated_at,
		p.deleted_at
	FROM
		subscribers_subscription_types AS a,
		subscribers_products AS p
	WHERE
		a.name = $1
	AND
		p.id = a.product_id
	`

	SqlCreateSubscriptionType = `
	INSERT INTO
		subscribers_subscription_types (
			name,
			description,
			product_id
		)
	VALUES (
		$1,
		$2,
		$3
	)
	RETURNING id, created_at
	`

	SqlDeleteSubscriptionType = `
	UPDATE
		subscribers_subscription_types
	SET
		deleted_at = current_timestamp
	WHERE
		id = $1
	AND
		deleted_at IS NULL
	RETURNING
		deleted_at
	`
)
