// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscriptiontypes

import (
	"context"
	"database/sql"

	"git.runecms.io/rune/runecms"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/ory/ladon"
)

func CreateSubscriptionType(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	name string,
	description string,
	productId string,
) (*models.SubscriptionType, error) {
	if err := runecms.Access(ident, w, "create", "subscribers:subscription_types"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	var (
		err   error
		model = &models.SubscriptionType{
			Name:        name,
			Description: description,
			ProductId:   productId,
		}
	)

	if err := db.QueryRowContext(
		ctx,
		SqlCreateSubscriptionType,
		model.Name,
		model.Description,
		model.ProductId,
	).Scan(
		&model.Id,
		&model.CreatedAt,
	); err != nil {
		return nil, runecms.Error(runecms.Internal, "Error creating Subscription Type", err.Error())
	}

	model, err = GetSubscriptionType(ctx, db, w, ident, model.Id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:subscription_types", "create", model)
	return model, nil
}

func DeleteSubscriptionType(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	id string,
) (*models.SubscriptionType, error) {
	if err := runecms.Access(ident, w, "delete", "subscribers:subscription_types"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if _, err := db.ExecContext(
		ctx,
		SqlDeleteSubscriptionType,
		id,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, "Error deleting subscription type", id, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, "Error deleting subscription type", id, err.Error())
	}

	model, err := GetSubscriptionType(ctx, db, w, ident, id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:subscription_types", "delete", model)
	return model, nil
}

func GetSubscriptionType(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	id string,
) (*models.SubscriptionType, error) {
	if err := runecms.Access(ident, w, "read", "subscribers:subscription_types"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.SubscriptionType{
		Id:      id,
		Product: &models.Product{},
	}

	var (
		deletedAt        sql.NullString
		productDeletedAt sql.NullString
	)
	if err := db.QueryRowContext(
		ctx,
		SqlGetSubscriptionType,
		id,
	).Scan(
		&model.Name,
		&model.Description,
		&model.ProductId,
		&model.CreatedAt,
		&deletedAt,
		&model.Product.Id,
		&model.Product.Name,
		&model.Product.Description,
		&model.Product.Price,
		&model.Product.ProductTypeId,
		&model.Product.PublicationId,
		&model.Product.BillingProcessorData,
		&model.Product.CreatedAt,
		&model.Product.UpdatedAt,
		&productDeletedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, "Error searching for Subscription Type", id, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, "Error searching for Subscription Type", id, err.Error())
	}

	model.DeletedAt = deletedAt.String
	model.Product.DeletedAt = productDeletedAt.String

	return model, nil
}

func GetSubscriptionTypeByName(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	name string,
) (*models.SubscriptionType, error) {
	if err := runecms.Access(ident, w, "read", "subscribers:subscription_types"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.SubscriptionType{
		Name:    name,
		Product: &models.Product{},
	}

	var (
		deletedAt        sql.NullString
		productDeletedAt sql.NullString
	)
	if err := db.QueryRowContext(
		ctx,
		SqlGetSubscriptionTypeByName,
		name,
	).Scan(
		&model.Id,
		&model.Description,
		&model.ProductId,
		&model.CreatedAt,
		&deletedAt,
		&model.Product.Id,
		&model.Product.Name,
		&model.Product.Description,
		&model.Product.Price,
		&model.Product.ProductTypeId,
		&model.Product.PublicationId,
		&model.Product.BillingProcessorData,
		&model.Product.CreatedAt,
		&model.Product.UpdatedAt,
		&productDeletedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, "Error searching for Subscription Type", name, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, "Error searching for Subscription Type", name, err.Error())
	}

	model.DeletedAt = deletedAt.String
	model.Product.DeletedAt = productDeletedAt.String

	return model, nil
}
