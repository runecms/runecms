// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package publications

import (
	"context"
	"database/sql"

	"git.runecms.io/rune/runecms"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/ory/ladon"
)

func CreatePublication(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	name string,
	description string,
	marketId string,
	billingProcessorData string,
) (*models.Publication, error) {
	if err := runecms.Access(ident, w, "create", "subscribers:publications"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.Publication{
		Name:                 name,
		Description:          description,
		MarketId:             marketId,
		BillingProcessorData: billingProcessorData,
	}
	if err := db.QueryRowContext(
		ctx,
		SqlCreatePublication,
		name,
		description,
		marketId,
		billingProcessorData,
	).Scan(
		&model.Id,
		&model.CreatedAt,
	); err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	model, err := GetPublication(ctx, db, w, ident, model.GetId())
	if err != nil {
		return nil, err
	}
	go webhooks.Distribute(webhooksClient, ident, "subscribers:publications", "create", model)
	return model, nil
}

func DeletePublication(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	id string,
) (*models.Publication, error) {
	if err := runecms.Access(ident, w, "delete", "subscribers:publications"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.Publication{
		Id: id,
	}

	if _, err := db.ExecContext(
		ctx,
		SqlDeletePublication,
		id,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	model, err := GetPublication(ctx, db, w, ident, model.GetId())
	if err != nil {
		return nil, err
	}
	go webhooks.Distribute(webhooksClient, ident, "subscribers:publications", "delete", model)
	return model, nil
}

func GetPublicationByName(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	name string,
) (*models.Publication, error) {
	if err := runecms.Access(ident, w, "read", "subscribers:publications"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	var (
		model = &models.Publication{
			Name:   name,
			Market: &models.Market{},
		}

		deletedAt       sql.NullString
		marketDeletedAt sql.NullString
	)

	if err := db.QueryRowContext(
		ctx,
		SqlGetPublicationByName,
		model.Name,
	).Scan(
		&model.Id,
		&model.Description,
		&model.MarketId,
		&model.BillingProcessorData,
		&model.CreatedAt,
		&deletedAt,
		&model.Market.Id,
		&model.Market.Name,
		&model.Market.Description,
		&model.Market.CreatedAt,
		&marketDeletedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	model.DeletedAt = deletedAt.String
	model.Market.DeletedAt = marketDeletedAt.String
	return model, nil
}

func GetPublication(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	id string,
) (*models.Publication, error) {
	if err := runecms.Access(ident, w, "read", "subscribers:publications"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	var (
		model = &models.Publication{
			Id:     id,
			Market: &models.Market{},
		}

		deletedAt       sql.NullString
		marketDeletedAt sql.NullString
	)
	if err := db.QueryRowContext(
		ctx,
		SqlGetPublication,
		model.Id,
	).Scan(
		&model.Name,
		&model.Description,
		&model.MarketId,
		&model.BillingProcessorData,
		&model.CreatedAt,
		&deletedAt,
		&model.Market.Id,
		&model.Market.Name,
		&model.Market.Description,
		&model.Market.CreatedAt,
		&marketDeletedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}
	return model, nil
}
