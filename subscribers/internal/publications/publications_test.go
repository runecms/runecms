// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package publications

import (
	"context"
	"database/sql"
	"os"
	"testing"
	"time"

	"git.runecms.io/rune/runecms"
	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/markets"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"

	"github.com/google/go-cmp/cmp"
	"github.com/ory/ladon"
)

var (
	ctx            = context.Background()
	w              *ladon.Ladon
	db             *sql.DB
	valid_id       runecms.Identity
	invalid_id     runecms.Identity
	market         *models.Market
	webhooksClient = &webhooks.TestClient{}
)

func GetTestDb(t *testing.T) {
	var err error
	if db != nil {
		return
	}
	w = runetestutil.GetTestLadon()
	db = runetestutil.GetTestDb(t)
	if err != nil {
		panic(err.Error())
	}

	valid_id = runecms.Be("valid", "admin")
	invalid_id = runecms.Be("invalid", "invalid")

	market, err = markets.CreateMarket(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test publication")
	if err != nil {
		t.Fatal("Unable to create market for publication...", err.Error())
	}
}

func getPublication() (*models.Publication, error) {
	return CreatePublication(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "Test publication", market.Id, "{}")
}

func TestMain(m *testing.M) {
	i := m.Run()
	runetestutil.KillAll()
	os.Exit(i)
}

func TestCreate(t *testing.T) {
	GetTestDb(t)
	t.Run("Create publication", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			model, err := CreatePublication(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "Create publication", market.Id, "{}")
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Fatal("Returned model is nil")
			}

			// Make sure we were given an id
			if model.Id == "" {
				t.Error("Returned model does not have an id")
			}

			if cmp.Equal(model.CreatedAt, time.Time{}) {
				t.Error("Returned model's CreatedAt was not set")
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := CreatePublication(ctx, db, w, webhooksClient, invalid_id, runetestutil.GetUUID(), "Unauthorized complaint type", market.Id, "{}")
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid input", func(t *testing.T) {
			_, err := CreatePublication(ctx, db, w, webhooksClient, invalid_id, runetestutil.GetUUID(), "Invalid complaint type input", market.Id, "{}")
			if err == nil {
				t.Error("Providing an invalid input should return an error")
			}
		})
	})
}

func TestGet(t *testing.T) {
	GetTestDb(t)
	name := runetestutil.GetUUID()
	model, err := CreatePublication(ctx, db, w, webhooksClient, valid_id, name, "get publication", market.Id, "{}")
	if err != nil {
		t.Fatal("Unable to set up test for Get")
	}

	t.Run("Get publication", func(t *testing.T) {
		t.Run("By name", func(t *testing.T) {
			m, err := GetPublicationByName(ctx, db, w, valid_id, name)
			if err != nil {
				t.Errorf(err.Error())
			}

			if m == nil {
				t.Fatal("Returned model is nil")
			}
			if m.Market == nil {
				t.Fatal("Returned model's market is nil")
			}
			if m.Market.Id == "" {
				t.Error("Returned model's market is id 0")
			}
			if cmp.Equal(m.Market, market) != true {
				t.Errorf("related markets do not match\n%s\n", cmp.Diff(m.Market, market))
			}
			if cmp.Equal(model, m) != true {
				t.Errorf("models do not match\n%s\n", cmp.Diff(model, m))
			}
		})
		t.Run("Successful request", func(t *testing.T) {
			m, err := GetPublication(ctx, db, w, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if m == nil {
				t.Error("Returned model is nil")
			}
			if m.Market == nil {
				t.Error("Returned model's market is nil")
			}
			if m.Market.Id == "" {
				t.Error("Returned model's market is id 0")
			}
			if cmp.Equal(m.Market, market) != true {
				t.Errorf("related markets do not match\n%s\n", cmp.Diff(m.Market, market))
			}
			if cmp.Equal(model, m) != true {
				t.Errorf("models do not match\n%s\n", cmp.Diff(model, m))
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := GetPublication(ctx, db, w, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := GetPublication(ctx, db, w, invalid_id, "invalid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})

}

func TestDelete(t *testing.T) {
	GetTestDb(t)
	model, err := CreatePublication(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "delete publication", market.Id, "{}")
	if err != nil {
		t.Fatal("Unable to set up test for Delete")
	}

	t.Run("Delete publication", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := DeletePublication(ctx, db, w, webhooksClient, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if m == nil {
				t.Error("Returned model is nil")
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := DeletePublication(ctx, db, w, webhooksClient, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := DeletePublication(ctx, db, w, webhooksClient, invalid_id, "invalid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})
}
