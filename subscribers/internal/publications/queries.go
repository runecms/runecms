// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package publications

var (
	SqlCreatePublication = `
	INSERT INTO
		subscribers_publications (
			name,
			description,
			market_id,
			billing_processor_data
		)
	VALUES (
		$1,
		$2,
		$3,
		$4
	)
	RETURNING id, created_at
	`

	SqlDeletePublication = `
	UPDATE
		subscribers_publications
	SET
		deleted_at = current_timestamp
	WHERE
		id = $1
	AND
		deleted_at IS NULL
	`

	SqlGetPublication = `
	SELECT
		p.name,
		p.description,
		p.market_id,
		p.billing_processor_data,
		p.created_at,
		p.deleted_at,
		m.id,
		m.name,
		m.description,
		m.created_at,
		m.deleted_at
	FROM
		subscribers_publications AS p,
		subscribers_markets AS m
	WHERE
		p.id = $1
	AND
		m.id = p.market_id
	`

	SqlGetPublicationByName = `
	SELECT
		p.id,
		p.description,
		p.market_id,
		p.billing_processor_data,
		p.created_at,
		p.deleted_at,
		m.id,
		m.name,
		m.description,
		m.created_at,
		m.deleted_at
	FROM
		subscribers_publications AS p,
		subscribers_markets AS m
	WHERE
		p.name = $1
	AND
		m.id = p.market_id
	`
)
