// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package products

var (
	SqlCreateProductWithProcessingData = `
	INSERT INTO
		subscribers_products (
			name,
			description,
			price,
			product_type_id,
			publication_id,
			billing_processor_data
		)
	VALUES (
		$1,
		$2,
		$3,
		$4,
		$5,
		$6
	)
	RETURNING id, created_at
	`

	SqlCreateProduct = `
	INSERT INTO
		subscribers_products(
			name,
			description,
			price,
			product_type_id,
			publication_id,
			billing_processor_data
		)
	SELECT
		$1,
		$2,
		$3,
		$4,
		$5,
		billing_processor_data
	FROM
		subscribers_publications
	WHERE
		id=$5
	RETURNING id, created_at, billing_processor_data
	`

	SqlDeleteProduct = `
	UPDATE
		subscribers_products
	SET
		deleted_at = current_timestamp
	WHERE
		id = $1
	AND
		deleted_at IS NULL
	RETURNING deleted_at
	`

	SqlUpdateProduct = `
	UPDATE
		subscribers_products
	SET
		name											= $1,
		description								= $2,
		price											= $3,
		billing_processor_data		= $4,
		publication_id						= $5,
		product_type_id						= $6,
		updated_at								= current_timestamp
	WHERE
		id = $7
	RETURNING
		updated_at
	`

	SqlGetProduct = `
	SELECT
		p.name,
		p.description,
		p.price,
		p.billing_processor_data,
		p.publication_id,
		p.product_type_id,
		p.created_at,
		p.updated_at,
		p.deleted_at,
		pt.name,
		pt.description,
		pt.created_at,
		pt.deleted_at,
		pu.name,
		pu.description,
		pu.market_id,
		pu.created_at,
		pu.deleted_at
	FROM
		subscribers_products			AS p,
		subscribers_product_types	AS pt,
		subscribers_publications  AS pu
	WHERE
		p.id = $1
	AND
		pt.id = p.product_type_id
	AND
		pu.id = p.publication_id
	`

	SqlGetProductByName = `
	SELECT
		p.id,
		p.description,
		p.price,
		p.billing_processor_data,
		p.publication_id,
		p.product_type_id,
		p.created_at,
		p.updated_at,
		p.deleted_at,
		pt.name,
		pt.description,
		pt.created_at,
		pt.deleted_at,
		pu.name,
		pu.description,
		pu.market_id,
		pu.created_at,
		pu.deleted_at
	FROM
		subscribers_products			AS p,
		subscribers_product_types	AS pt,
		subscribers_publications  AS pu
	WHERE
		p.name = $1
	AND
		pt.id = p.product_type_id
	AND
		pu.id = p.publication_id
	`
)

var (
	SqlGetProductRates = `
	SELECT
		r.id,
		r.price,
		r.billing_term,
		r.billing_frequency_id,
		r.rate_term,
		r.rate_frequency_id,
		r.start_at,
		r.end_at,
		r.next_rate_id,
		r.created_at,
		r.deleted_at,
		f.id,
		f.singular,
		f.plural,
		f2.id,
		f2.singular,
		f2.plural
	FROM
		subscribers_product_rates	AS r
	LEFT JOIN subscribers_product_frequencies AS f
		ON f.id = r.billing_frequency_id
	LEFT JOIN subscribers_product_frequencies AS f2
		ON f2.id = r.rate_frequency_id
	WHERE
		r.product_id = $1
	`

	// SqlCreateProductRate creates a single product rate. This will not set the "next_rate_id"
	SqlCreateProductRate = `
	INSERT INTO
		subscribers_product_rates (
			product_id,
			price,
			billing_term,
			billing_frequency_id,
			rate_term,
			rate_frequency_id,
			start_at,
			end_at
		)
	VALUES (
		$1,
		$2,
		$3,
		$4,
		$5,
		$6,
		$7,
		$8
	) RETURNING id, created_at
	`

	SqlDeleteProductRates = `
	DELETE FROM
		subscribers_product_rates
	WHERE
		product_id = $1
	`
	SqlUpdateProductRateNextId = `
	UPDATE
		subscribers_product_rates
	SET
		next_rate_id=$1
	WHERE
		id = $2
	`
)
