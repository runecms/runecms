// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package products

import (
	"context"
	"database/sql"
	"os"
	"testing"
	"time"

	"git.runecms.io/rune/runecms"
	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/markets"
	"git.runecms.io/rune/runecms/subscribers/internal/producttypes"
	"git.runecms.io/rune/runecms/subscribers/internal/publications"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"

	"github.com/google/go-cmp/cmp"
	"github.com/ory/ladon"
)

var (
	ctx            = context.Background()
	w              *ladon.Ladon
	db             *sql.DB
	valid_id       runecms.Identity
	invalid_id     runecms.Identity
	market         *models.Market
	publication    *models.Publication
	producttype    *models.ProductType
	webhooksClient = &webhooks.TestClient{}
)

func GetTestDb(t *testing.T) {
	if db != nil {
		return
	}
	var err error
	w = runetestutil.GetTestLadon()
	db = runetestutil.GetTestDb(t)

	valid_id = runecms.Be("valid", "admin")
	invalid_id = runecms.Be("invalid", "invalid")

	market, err = markets.CreateMarket(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new subscription")
	if err != nil {
		t.Fatal("Error creating market for product...", err.Error())
	}

	publication, err = publications.CreatePublication(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new subscription", market.Id, "{}")
	if err != nil {
		t.Fatal("Error creating publication for product...", err.Error())
	}

	producttype, err = producttypes.CreateProductType(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new subscription")
	if err != nil {
		t.Fatal("Error creating Product Type for product...", err.Error())
	}
}

func TestMain(m *testing.M) {
	i := m.Run()
	runetestutil.KillAll()
	os.Exit(i)
}

func TestCreate(t *testing.T) {
	GetTestDb(t)
	t.Run("Create product", func(t *testing.T) {
		t.Run("Successful request (inherit billing processor data)", func(t *testing.T) {
			model, err := CreateProduct(
				ctx,
				db,
				w, webhooksClient,
				valid_id,
				runetestutil.GetUUID(),
				"Test creating new product",
				1000,
				producttype.Id,
				publication.Id,
			)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Fatal("Returned model is nil")
			}

			// Make sure we were given an id
			if model.Id == "" {
				t.Error("Returned model does not have an id")
			}

			if cmp.Equal(model.CreatedAt, time.Time{}) {
				t.Error("Returned model's CreatedAt was not set")
			}
		})
		t.Run("Successful request (with deliveryaddress)", func(t *testing.T) {
			model, err := CreateProductWithProcessorData(
				ctx,
				db,
				w, webhooksClient,
				valid_id,
				runetestutil.GetUUID(),
				"Test creating new product",
				1000,
				producttype.Id,
				publication.Id,
				`{"what":"even"}`,
			)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Fatal("Returned model is nil")
			}

			// Make sure we were given an id
			if model.Id == "" {
				t.Error("Returned model does not have an id")
			}

			if cmp.Equal(model.CreatedAt, time.Time{}) {
				t.Error("Returned model's CreatedAt was not set")
			}
		})
		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := CreateProduct(
				ctx,
				db,
				w, webhooksClient,
				invalid_id,
				runetestutil.GetUUID(),
				"Test creating new product",
				1000,
				producttype.Id,
				publication.Id,
			)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})
	})
}

func TestGet(t *testing.T) {
	GetTestDb(t)
	model, err := CreateProduct(
		ctx,
		db,
		w, webhooksClient,
		valid_id,
		runetestutil.GetUUID(),
		"Test getting products",
		1000,
		producttype.Id,
		publication.Id,
	)
	if err != nil {
		t.Fatalf("Unable to set up test for Get: %s", err.Error())
	}

	t.Run("Get product", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := GetProduct(ctx, db, w, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if m == nil {
				t.Error("Returned model is nil")
			}

			if cmp.Equal(model, m) != true {
				t.Errorf("models do not match\n%s\n", cmp.Diff(model, m))
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := GetProduct(ctx, db, w, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := GetProduct(ctx, db, w, invalid_id, "invalid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})

}

func TestDelete(t *testing.T) {
	model, err := CreateProduct(
		ctx,
		db,
		w, webhooksClient,
		valid_id,
		runetestutil.GetUUID(),
		"Test getting products",
		1000,
		producttype.Id,
		publication.Id,
	)
	if err != nil {
		t.Fatalf("Unable to set up test for Delete: %s", err.Error())
	}

	t.Run("Delete product", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := DeleteProduct(ctx, db, w, webhooksClient, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Error("Returned model is nil")
			}
			if m.DeletedAt == "" {
				t.Error("Returned model's DeletedAt is nil")
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := DeleteProduct(ctx, db, w, webhooksClient, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := DeleteProduct(ctx, db, w, webhooksClient, invalid_id, "invalid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})
}

func TestUpdate(t *testing.T) {
	model := &models.Product{
		Name:          runetestutil.GetUUID(),
		Description:   runetestutil.GetUUID(),
		Price:         1032,
		ProductTypeId: producttype.GetId(),
		PublicationId: publication.GetId(),
		Rates: []*models.ProductRate{
			&models.ProductRate{
				Price:              100,
				BillingTerm:        1,
				BillingFrequencyId: "monthly",
				RateTerm:           1,
				RateFrequencyId:    "monthly",
			},
			&models.ProductRate{
				Price:              200,
				BillingTerm:        3,
				BillingFrequencyId: "monthly",
			},
		},
	}

	updated := &models.Product{
		Name:          runetestutil.GetUUID(),
		Description:   runetestutil.GetUUID(),
		Price:         9999,
		ProductTypeId: producttype.GetId(),
		PublicationId: publication.GetId(),
		Rates: []*models.ProductRate{
			&models.ProductRate{
				Price:              400,
				BillingTerm:        3,
				BillingFrequencyId: "monthly",
			},
		},
	}

	m, err := CreateProductFromModel(
		ctx,
		db,
		w,
		webhooksClient,
		valid_id,
		model,
	)

	if err != nil {
		t.Fatal(err)
	}

	updated.Id = m.Id

	result, err := UpdateProduct(
		ctx,
		db,
		w,
		webhooksClient,
		valid_id,
		updated,
	)

	if err != nil {
		t.Fatal(err.Error())
	}
	if result == nil {
		t.Fatal("Returned result is nil")
	}

	updated.UpdatedAt = result.UpdatedAt
	if cmp.Equal(result, updated) != true {
		t.Fatalf("Unexpected result from update\n%s", cmp.Diff(result, updated))
	}
}

func TestCreateFromModel(t *testing.T) {
	model := &models.Product{
		Name:          runetestutil.GetUUID(),
		Description:   runetestutil.GetUUID(),
		Price:         1032,
		ProductTypeId: producttype.GetId(),
		PublicationId: publication.GetId(),
		Rates: []*models.ProductRate{
			&models.ProductRate{
				Price:              100,
				BillingTerm:        1,
				BillingFrequencyId: "monthly",
			},
		},
	}

	m, err := CreateProductFromModel(
		ctx,
		db,
		w, webhooksClient,
		valid_id,
		model,
	)

	if err != nil {
		t.Fatal(err)
	}

	for i, v := range m.Rates {
		if v.GetBillingFrequency().GetId() == "" {
			t.Logf("%+v", v)
			t.Error("Foreign key of BillingFrequency is not evaluated (No Id) on element", i)
		}
		if v.GetBillingFrequency().GetPlural() == "" {
			t.Error("Foreign key of BillingFrequency is not evaluated (No plural) on element", i)
		}
		if v.GetBillingFrequency().GetSingular() == "" {
			t.Error("Foreign key of BillingFrequency is not evaluated (No singular) on element", i)
		}
	}
}

func TestSortRates(t *testing.T) {
	rates := []*models.ProductRate{
		&models.ProductRate{
			Id:         "product_3",
			NextRateId: "product_4",
		},
		&models.ProductRate{
			Id:         "product_2",
			NextRateId: "product_3",
		},
		&models.ProductRate{
			Id:         "product_1",
			NextRateId: "product_2",
		},
		&models.ProductRate{
			Id:         "product_4",
			NextRateId: "product_5",
		},
		&models.ProductRate{
			Id:         "product_6",
			NextRateId: "",
		},
		&models.ProductRate{
			Id:         "product_5",
			NextRateId: "product_6",
		},
	}

	expected := []*models.ProductRate{
		&models.ProductRate{
			Id:         "product_1",
			NextRateId: "product_2",
		},
		&models.ProductRate{
			Id:         "product_2",
			NextRateId: "product_3",
		},
		&models.ProductRate{
			Id:         "product_3",
			NextRateId: "product_4",
		},
		&models.ProductRate{
			Id:         "product_4",
			NextRateId: "product_5",
		},
		&models.ProductRate{
			Id:         "product_5",
			NextRateId: "product_6",
		},
		&models.ProductRate{
			Id:         "product_6",
			NextRateId: "",
		},
	}

	result := SortRates(rates)
	if cmp.Equal(result, expected) != true {
		t.Logf("%+v", expected)
		t.Logf("%+v", result)
		t.Fatalf("Unexpected response.\n%s", cmp.Diff(result, expected))
	}
}
