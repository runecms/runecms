// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package products

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"git.runecms.io/rune/runecms"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/ory/ladon"
)

const (
	RateDaily   = "daily"
	RateWeeky   = "weekly"
	RateMonthly = "monthly"
	RateYearly  = "yearly"
)

func CreateProductWithProcessorData(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	name string,
	description string,
	price int64,
	productTypeId string,
	publicationId string,
	billingProcessorData string,
) (*models.Product, error) {
	if err := runecms.Access(ident, w, "create", "subscribers:products"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.Product{
		Name:                 name,
		Description:          description,
		Price:                price,
		BillingProcessorData: billingProcessorData,
		PublicationId:        publicationId,
		ProductTypeId:        productTypeId,
	}

	if err := db.QueryRowContext(
		ctx,
		SqlCreateProductWithProcessingData,
		model.Name,
		model.Description,
		model.Price,
		model.ProductTypeId,
		model.PublicationId,
		model.BillingProcessorData,
	).Scan(
		&model.Id,
		&model.CreatedAt,
	); err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	model, err := GetProduct(ctx, db, w, ident, model.Id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:products", "create", model)
	return model, nil
}

func CreateProduct(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	name string,
	description string,
	price int64,
	productTypeId string,
	publicationId string,
) (*models.Product, error) {
	if err := runecms.Access(ident, w, "create", "subscribers:products"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.Product{
		Name:          name,
		Description:   description,
		Price:         price,
		PublicationId: publicationId,
		ProductTypeId: productTypeId,
	}

	if err := db.QueryRowContext(
		ctx,
		SqlCreateProduct,
		model.Name,
		model.Description,
		model.Price,
		model.ProductTypeId,
		model.PublicationId,
	).Scan(
		&model.Id,
		&model.CreatedAt,
		&model.BillingProcessorData,
	); err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	model, err := GetProduct(ctx, db, w, ident, model.Id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:products", "create", model)
	return model, nil
}

func DeleteProduct(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	id string,
) (*models.Product, error) {
	if err := runecms.Access(ident, w, "delete", "subscribers:products"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.Product{
		Id: id,
	}

	var deletedAt sql.NullString
	if err := db.QueryRowContext(
		ctx,
		SqlDeleteProduct,
		id,
	).Scan(
		&deletedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	model.DeletedAt = deletedAt.String

	model, err := GetProduct(ctx, db, w, ident, id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:products", "delete", model)
	return model, nil
}

func GetProductByName(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	name string,
) (*models.Product, error) {
	if err := runecms.Access(ident, w, "read", "subscribers:products"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	var (
		model = &models.Product{
			Name:        name,
			Rates:       []*models.ProductRate{},
			ProductType: &models.ProductType{},
			Publication: &models.Publication{},
		}
		err error
	)

	var (
		productDeletedAt     sql.NullString
		productTypeDeletedAt sql.NullString
		publicationDeletedAt sql.NullString
	)
	if err := db.QueryRowContext(
		ctx,
		SqlGetProductByName,
		name,
	).Scan(
		&model.Id,
		&model.Description,
		&model.Price,
		&model.BillingProcessorData,
		&model.PublicationId,
		&model.ProductTypeId,
		&model.CreatedAt,
		&model.UpdatedAt,
		&productDeletedAt,
		&model.ProductType.Name,
		&model.ProductType.Description,
		&model.ProductType.CreatedAt,
		&productTypeDeletedAt,
		&model.Publication.Name,
		&model.Publication.Description,
		&model.Publication.MarketId,
		&model.Publication.CreatedAt,
		&publicationDeletedAt,
	); err != nil {
		return nil, err
	}

	model.DeletedAt = productDeletedAt.String
	model.ProductType.DeletedAt = productTypeDeletedAt.String
	model.Publication.DeletedAt = publicationDeletedAt.String

	model.Rates, err = GetProductRates(ctx, db, w, ident, model.Id)
	if err != nil {
		return nil, err
	}

	return model, nil
}

func GetProduct(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	id string,
) (*models.Product, error) {
	if err := runecms.Access(ident, w, "read", "subscribers:products"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	var err error
	model := &models.Product{
		Id:          id,
		Rates:       []*models.ProductRate{},
		ProductType: &models.ProductType{},
		Publication: &models.Publication{},
	}

	var (
		productDeletedAt     sql.NullString
		productTypeDeletedAt sql.NullString
		publicationDeletedAt sql.NullString
	)

	if err := db.QueryRowContext(
		ctx,
		SqlGetProduct,
		id,
	).Scan(
		&model.Name,
		&model.Description,
		&model.Price,
		&model.BillingProcessorData,
		&model.PublicationId,
		&model.ProductTypeId,
		&model.CreatedAt,
		&model.UpdatedAt,
		&productDeletedAt,
		&model.ProductType.Name,
		&model.ProductType.Description,
		&model.ProductType.CreatedAt,
		&productTypeDeletedAt,
		&model.Publication.Name,
		&model.Publication.Description,
		&model.Publication.MarketId,
		&model.Publication.CreatedAt,
		&publicationDeletedAt,
	); err != nil {
		return nil, err
	}

	model.DeletedAt = productDeletedAt.String
	model.ProductType.DeletedAt = productTypeDeletedAt.String
	model.Publication.DeletedAt = publicationDeletedAt.String

	model.Rates, err = GetProductRates(ctx, db, w, ident, model.Id)
	if err != nil {
		return nil, err
	}

	return model, nil
}

func UpdateProduct(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	model *models.Product,
) (*models.Product, error) {
	if err := runecms.Access(ident, w, "update", "subscribers:products"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if err := db.QueryRowContext(
		ctx,
		SqlUpdateProduct,
		model.Name,
		model.Description,
		model.Price,
		model.BillingProcessorData,
		model.PublicationId,
		model.ProductTypeId,
		model.Id,
	).Scan(
		&model.UpdatedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, "Error updating product", err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	if _, err := tx.ExecContext(
		ctx,
		SqlDeleteProductRates,
		model.Id,
	); err != nil {
		if err != sql.ErrNoRows {
			return nil, runecms.Error(runecms.Internal, err.Error())
		}
	}

	if model.Rates != nil {
		if err := fillRateDates(model.Rates); err != nil {
			return nil, runecms.Error(runecms.Internal, err.Error())
		}

		for _, v := range model.Rates {
			var (
				endAt           *string
				rateTerm        *int64
				rateFrequencyId *string
			)
			if v.EndAt != "" {
				endAt = &v.EndAt
			}
			if v.RateTerm != 0 {
				rateTerm = &v.RateTerm
			}
			if v.RateFrequencyId != "" {
				rateFrequencyId = &v.RateFrequencyId
			}
			if err := tx.QueryRowContext(
				ctx,
				SqlCreateProductRate,
				model.Id,
				v.Price,
				v.BillingTerm,
				v.BillingFrequencyId,
				rateTerm,
				rateFrequencyId,
				v.StartAt,
				endAt,
			).Scan(
				&v.Id,
				&v.CreatedAt,
			); err != nil {
				return nil, runecms.Error(runecms.Internal, err.Error())
			}
		}
	}

	if err := tx.Commit(); err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	return model, nil
}

func GetProductRates(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	productId string,
) ([]*models.ProductRate, error) {
	if err := runecms.Access(ident, w, "read", "subscribers:products"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	rates := []*models.ProductRate{}

	rows, err := db.QueryContext(
		ctx,
		SqlGetProductRates,
		productId,
	)
	if err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	for rows.Next() {
		var (
			rateTerm         sql.NullInt64
			endAt            sql.NullString
			nextRateId       sql.NullString
			deletedAt        sql.NullString
			pRateFrequencyId sql.NullString

			rateFrequencyId       sql.NullString
			rateFrequencySingular sql.NullString
			rateFrequencyPlural   sql.NullString
		)
		rate := &models.ProductRate{
			RateFrequency:    &models.ProductFrequency{},
			BillingFrequency: &models.ProductFrequency{},
		}
		if err := rows.Scan(
			&rate.Id,
			&rate.Price,
			&rate.BillingTerm,
			&rate.BillingFrequencyId,
			&rateTerm,
			&pRateFrequencyId,
			&rate.StartAt,
			&endAt,
			&nextRateId,
			&rate.CreatedAt,
			&deletedAt,
			&rate.BillingFrequency.Id,
			&rate.BillingFrequency.Singular,
			&rate.BillingFrequency.Plural,
			&rateFrequencyId,
			&rateFrequencySingular,
			&rateFrequencyPlural,
		); err != nil {
			if err == sql.ErrNoRows {
				break
			}
			return nil, runecms.Error(runecms.Internal, err.Error())
		}

		rate.RateTerm = rateTerm.Int64
		rate.EndAt = endAt.String
		rate.NextRateId = nextRateId.String
		rate.DeletedAt = deletedAt.String
		rate.RateFrequencyId = pRateFrequencyId.String

		rate.RateFrequency.Id = rateFrequencyId.String
		rate.RateFrequency.Singular = rateFrequencySingular.String
		rate.RateFrequency.Plural = rateFrequencyPlural.String
		rates = append(rates, rate)
	}
	if len(rates) != 0 {
		rates = SortRates(rates)
	}

	return rates, nil
}

func SortRates(rates []*models.ProductRate) []*models.ProductRate {
	res := []*models.ProductRate{rates[0]}
	i := 0

	// end is the element without a "NextRateId"
	var end *models.ProductRate

	for i <= len(rates) {
		first := res[0]
		last := res[len(res)-1]

		for j, v := range rates {
			if end == nil {
				if v.NextRateId == "" {
					end = rates[j]
				}
			}

			// if v is next in line
			if v.Id == last.NextRateId {
				res = append(res, rates[j])
				// or if v was supposed to be before in the beginning
			} else if v.NextRateId == first.Id {
				res = append(res, nil)
				copy(res[1:], res)
				res[0] = rates[j]
			}
			i++
		}
	}
	res = append(res, end)

	return res
}

// fillRateDates will fill in the StartDate and EndDate based on the terms and frequencies provided
func fillRateDates(rates []*models.ProductRate) error {
	if rates == nil {
		return nil
	}

	now := time.Now().UTC()
	for i, v := range rates {
		v.StartAt = now.Format(time.RFC3339)
		if v.RateFrequencyId == "" {
			// This is supposed to be the last "rate" but there's another one right after. Return an error in this case
			if i < (len(rates) - 1) {
				return fmt.Errorf("The rates provided are poorly ordered; a rate (%s, product: %s) is does not have a term frequency, but it's followed by another rate (%s)", v.Id, v.ProductId, rates[i+1].Id)
			}
			break
		}
		if v.RateTerm == 0 {
			return fmt.Errorf("The RateFrequency for rate %s (product: %s) is not 0, but the term is; they both need to be set", v.Id, v.ProductId)
		}
		end := &now
		switch v.RateFrequencyId {
		case RateDaily:
			end.AddDate(0, 0, int(v.RateTerm))
		case RateWeeky:
			end.AddDate(0, 0, int(v.RateTerm)*7)
		case RateMonthly:
			end.AddDate(0, int(v.RateTerm), 0)
		case RateYearly:
			end.AddDate(int(v.RateTerm), 0, 0)
		}
		v.EndAt = end.Format(time.RFC3339)
	}

	return nil
}

func CreateProductFromModel(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	model *models.Product,
) (*models.Product, error) {
	if err := runecms.Access(ident, w, "create", "subscribers:products"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	var err error
	res := &models.Product{}

	if model.BillingProcessorData != "" {
		res, err = CreateProductWithProcessorData(
			ctx,
			db,
			w,
			webhooksClient,
			ident,
			model.Name,
			model.Description,
			model.Price,
			model.ProductTypeId,
			model.PublicationId,
			model.BillingProcessorData,
		)
		if err != nil {
			return nil, err
		}
	} else {
		res, err = CreateProduct(
			ctx,
			db,
			w,
			webhooksClient,
			ident,
			model.Name,
			model.Description,
			model.Price,
			model.ProductTypeId,
			model.PublicationId,
		)
		if err != nil {
			return nil, err
		}
	}

	if model.Rates != nil {
		if err := fillRateDates(model.Rates); err != nil {
			return nil, runecms.Error(runecms.Internal, err.Error())
		}

		for _, v := range model.Rates {
			var (
				endAt           *string
				rateTerm        *int64
				rateFrequencyId *string
			)
			if v.EndAt != "" {
				endAt = &v.EndAt
			}
			if v.RateTerm != 0 {
				rateTerm = &v.RateTerm
			}
			if v.RateFrequencyId != "" {
				rateFrequencyId = &v.RateFrequencyId
			}
			if err := db.QueryRowContext(
				ctx,
				SqlCreateProductRate,
				res.Id,
				v.Price,
				v.BillingTerm,
				v.BillingFrequencyId,
				rateTerm,
				rateFrequencyId,
				v.StartAt,
				endAt,
			).Scan(
				&v.Id,
				&v.CreatedAt,
			); err != nil {
				return nil, runecms.Error(runecms.Internal, err.Error())
			}
		}
	}

	res, err = GetProduct(ctx, db, w, ident, res.Id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:products", "create", res)
	return res, nil
}

func CreateProductRate(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	model *models.ProductRate,
) (*models.ProductRate, error) {
	return nil, nil
}
