// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscribers

var (
	SqlCreateSubscriber = `
	INSERT INTO
		subscribers (
			account_id
		)
	VALUES (
		$1
	)
	RETURNING id, created_at, updated_at
	`

	SqlDeleteSubscriber = `
	UPDATE
		subscribers
	SET
		deleted_at = current_timestamp
	WHERE
		id = $1
	AND
		deleted_at IS NULL
	RETURNING
		account_id,
		created_at,
		updated_at,
		deleted_at
	`

	SqlGetSubscriber = `
	SELECT
		account_id,
		created_at,
		deleted_at,
		updated_at
	FROM
		subscribers
	WHERE
		id = $1
	`

	SqlGetSubscriberByAccountId = `
	SELECT
		id,
		created_at,
		updated_at
	FROM
		subscribers
	WHERE
		account_id = $1
	`
)
