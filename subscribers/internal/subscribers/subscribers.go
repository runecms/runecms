// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscribers

import (
	"context"
	"database/sql"
	"fmt"

	"git.runecms.io/rune/runecms"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/ory/ladon"
)

func GetAccountId(db *sql.DB, id string) (string, error) {
	const unassigned = "unassigned"
	var accountId sql.NullString
	if err := db.QueryRow(`
        SELECT
            account_id
        FROM
            subscribers
        WHERE
            subscribers.id = $1
    `, id).Scan(&accountId); err != nil {
		return "", runecms.Error(runecms.Internal, err.Error())
	}
	if accountId.Valid {
		return accountId.String, nil
	}
	return unassigned, nil
}

// CreateSubscriber will create a new subscriber given the database connection
func CreateSubscriber(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	accountID string,
) (*models.Subscriber, error) {
	if err := runecms.Access(ident, w, "create", "subscribers:subscribers"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	var (
		model = &models.Subscriber{
			AccountId: accountID,
		}
	)

	row := db.QueryRowContext(ctx, SqlCreateSubscriber, accountID)

	if err := row.Scan(
		&model.Id,
		&model.CreatedAt,
		&model.UpdatedAt,
	); err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:subscribers", "create", model)
	return model, nil
}

// DeleteSubscriber will use the connected database to delete the subscriber
func DeleteSubscriber(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	id string,
) (*models.Subscriber, error) {
	accountId, err := GetAccountId(db, id)
	if err != nil {
		return nil, err
	}

	resource := fmt.Sprintf("subscribers:%s:", accountId)
	if err := runecms.Access(ident, w, "delete", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.Subscriber{
		Id: id,
	}

	row := db.QueryRowContext(ctx, SqlDeleteSubscriber, id)
	if err := row.Scan(&model.AccountId, &model.CreatedAt, &model.UpdatedAt, &model.DeletedAt); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:subscribers", "delete", model)
	return model, nil
}

// GetSubscriber will retrieve a subscriber given the database connection and the subscriber's id
func GetSubscriber(ctx context.Context, db *sql.DB, w *ladon.Ladon, ident runecms.Identity, id string) (*models.Subscriber, error) {
	accountId, err := GetAccountId(db, id)
	if err != nil {
		return nil, err
	}

	resource := fmt.Sprintf("subscribers:%s", accountId)
	if err := runecms.Access(ident, w, "read", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	var (
		deletedAt sql.NullString
		model     = &models.Subscriber{
			Id: id,
		}
	)

	row := db.QueryRowContext(ctx, SqlGetSubscriber, id)
	if err := row.Scan(&model.AccountId, &model.CreatedAt, &deletedAt, &model.UpdatedAt); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	model.DeletedAt = deletedAt.String
	return model, nil
}

// GetSubscriberByAccountId will retrieve a subscriber given the database connection and the account ID
func GetSubscriberByAccountId(ctx context.Context, db *sql.DB, w *ladon.Ladon, ident runecms.Identity, accountId string) (*models.Subscriber, error) {
	resource := fmt.Sprintf("subscribers:%s", accountId)
	if err := runecms.Access(ident, w, "read", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.Subscriber{
		AccountId: accountId,
	}
	row := db.QueryRowContext(ctx, SqlGetSubscriberByAccountId, accountId)
	if err := row.Scan(&model.Id, &model.CreatedAt, &model.UpdatedAt); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	return model, nil
}
