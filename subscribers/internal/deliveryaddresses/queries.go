package deliveryaddresses

var (
	SqlGetDeliveryAddress = `
	SELECT
		d.subscriber_id,
		d.address_id,
		d.created_at,
		d.deleted_at,
		a.address,
		a.city,
		a.state,
		a.zip,
		a.country
	FROM
		subscribers_delivery_addresses AS d,
		subscribers_addresses AS a
	WHERE
		d.id = $1
	AND
		a.id = d.address_id
	`

	SqlCreateDeliveryAddress = `
	INSERT INTO
		subscribers_delivery_addresses(
			address_id,
			subscriber_id
		)
	VALUES (
		$1,
		$2
	)
	RETURNING id, created_at
	`

	SqlDeleteDeliveryAddress = `
	UPDATE
		subscribers_delivery_addresses
	SET
		deleted_at=current_timestamp
	WHERE
		deleted_at IS NULL
	AND
		id = $1
	RETURNING deleted_at
	`
)
