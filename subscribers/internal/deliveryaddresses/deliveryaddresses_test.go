// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package deliveryaddresses

import (
	"context"
	"database/sql"
	"os"
	"testing"
	"time"

	"git.runecms.io/rune/runecms"
	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/addresses"
	"git.runecms.io/rune/runecms/subscribers/internal/subscribers"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"

	"github.com/google/go-cmp/cmp"
	"github.com/ory/ladon"
)

var (
	w              *ladon.Ladon
	db             *sql.DB
	ctx            = context.Background()
	valid_id       runecms.Identity
	invalid_id     runecms.Identity
	subscriber     *models.Subscriber
	address        *models.Address
	webhooksClient = &webhooks.TestClient{}
)

func GetTestDb(t *testing.T) {
	var err error
	if db != nil {
		return
	}
	w = runetestutil.GetTestLadon()
	db = runetestutil.GetTestDb(t)

	valid_id = runecms.Be("valid", "admin")
	invalid_id = runecms.Be("invalid", "invalid")

	subscriber, err = subscribers.CreateSubscriber(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID())
	if err != nil {
		t.Fatal(err.Error())
	}

	address, _ = addresses.CreateAddress(ctx, db, w, webhooksClient, valid_id, "Test Delivery Address", "deliveryaddresses", "ar", "00000", "usa")
}

func TestMain(m *testing.M) {
	i := m.Run()
	runetestutil.KillAll()
	os.Exit(i)
}

func TestNew(t *testing.T) {
	GetTestDb(t)
	t.Run("New deliveryaddress", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			model, err := CreateDeliveryAddress(ctx, db, w, webhooksClient, valid_id, subscriber.Id, address.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Fatal("Returned model is nil")
			}

			// Make sure we were given an id
			if model.Id == "" {
				t.Error("Returned model does not have an id")
			}

			if cmp.Equal(model.CreatedAt, time.Time{}) {
				t.Error("Returned model's CreatedAt was not set")
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := CreateDeliveryAddress(ctx, db, w, webhooksClient, invalid_id, subscriber.Id, address.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid input", func(t *testing.T) {
			_, err := CreateDeliveryAddress(ctx, db, w, webhooksClient, invalid_id, subscriber.Id, address.Id)
			if err == nil {
				t.Error("Providing an invalid input should return an error")
			}
		})
	})
}

func TestGet(t *testing.T) {
	GetTestDb(t)
	model, err := CreateDeliveryAddress(ctx, db, w, webhooksClient, valid_id, subscriber.Id, address.Id)
	if err != nil {
		t.Fatalf("Unable to set up test for Get. Error: %s", err.Error())
	}

	t.Run("Get deliveryaddress", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := GetDeliveryAddress(ctx, db, w, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Error("Returned model is nil")
			}

			if cmp.Equal(model, m) != true {
				t.Errorf("models do not match\n%s\n", cmp.Diff(model, m))
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := GetDeliveryAddress(ctx, db, w, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := GetDeliveryAddress(ctx, db, w, invalid_id, "invalid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})
}

func TestDelete(t *testing.T) {
	model, err := CreateDeliveryAddress(ctx, db, w, webhooksClient, valid_id, subscriber.Id, address.Id)
	if err != nil {
		t.Fatal("Unable to set up test for Delete")
	}

	t.Run("Delete deliveryaddress", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := DeleteDeliveryAddress(ctx, db, w, webhooksClient, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if m == nil {
				t.Fatal("Returned model is nil")
			}

			if m.DeletedAt == "" {
				t.Error("Returned model's DeletedAt is nil")
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := DeleteDeliveryAddress(ctx, db, w, webhooksClient, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := DeleteDeliveryAddress(ctx, db, w, webhooksClient, invalid_id, "invaliduuid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})
}
