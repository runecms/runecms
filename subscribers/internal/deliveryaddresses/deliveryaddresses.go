// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package deliveryaddresses

import (
	"context"
	"database/sql"
	"fmt"

	"git.runecms.io/rune/runecms"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/ory/ladon"
)

func getAccountId(db *sql.DB, id string) (string, error) {
	const unassigned = "unassigned"
	var accountId sql.NullString
	if err := db.QueryRow(`
		SELECT
			subscribers.account_id
		FROM
			subscribers,
			subscribers_delivery_addresses as delivery_addresses
		WHERE
			delivery_addresses.id = $1
		AND
			subscribers.id = delivery_addresses.subscriber_id
	`, id).Scan(&accountId); err != nil {
		return "", runecms.Error(runecms.Internal, err.Error())
	}
	if accountId.Valid {
		return accountId.String, nil
	}
	return unassigned, nil
}

// CreateDeliveryAddress will create a new delivery address in the provided database
func CreateDeliveryAddress(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	subscriberId string,
	addressId string,
) (*models.DeliveryAddress, error) {
	if err := runecms.Access(ident, w, "create", "subscribers:delivery_addresses"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.DeliveryAddress{
		AddressId:    addressId,
		SubscriberId: subscriberId,
	}

	if err := db.QueryRowContext(
		ctx,
		SqlCreateDeliveryAddress,
		model.AddressId,
		model.SubscriberId,
	).Scan(
		&model.Id,
		&model.CreatedAt,
	); err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	var err error
	model, err = GetDeliveryAddress(ctx, db, w, ident, model.Id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:delivery_addresses", "create", model)

	return model, nil
}

// DeleteDeliveryAddress will delete a delivery address when given a database connection
func DeleteDeliveryAddress(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	id string,
) (*models.DeliveryAddress, error) {
	accountId, err := getAccountId(db, id)
	if err != nil {
		return nil, err
	}

	resource := fmt.Sprintf("subscribers:%s:delivery_addresses:%s", accountId, id)
	if err := runecms.Access(ident, w, "delete", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if _, err := db.ExecContext(
		ctx,
		SqlDeleteDeliveryAddress,
		id,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Errorf(runecms.NotFound, "Could not find delivery address '%s'; it either has does not exist or has already been deleted", id)
		}
		return nil, runecms.Error(runecms.InvalidArgument, err.Error())
	}

	model, err := GetDeliveryAddress(ctx, db, w, ident, id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:delivery_addresses", "delete", model)
	return model, nil
}

// GetDeliveryAddress will retrieve a delivery address from the provided database
func GetDeliveryAddress(ctx context.Context, db *sql.DB, w *ladon.Ladon, ident runecms.Identity, id string) (*models.DeliveryAddress, error) {
	accountId, err := getAccountId(db, id)
	if err != nil {
		return nil, err
	}

	resource := fmt.Sprintf("subscribers:%s:delivery_addresses:%s", accountId, id)
	if err := runecms.Access(ident, w, "read", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	var (
		deletedAt sql.NullString
		model     = &models.DeliveryAddress{
			Address: &models.Address{},
			Id:      id,
		}
	)

	if err := db.QueryRowContext(
		ctx,
		SqlGetDeliveryAddress,
		model.Id,
	).Scan(
		&model.SubscriberId,
		&model.AddressId,
		&model.CreatedAt,
		&deletedAt,
		&model.Address.Address,
		&model.Address.City,
		&model.Address.State,
		&model.Address.Zip,
		&model.Address.Country,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
	}

	model.DeletedAt = deletedAt.String
	return model, nil
}
