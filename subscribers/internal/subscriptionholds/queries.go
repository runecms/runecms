// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscriptionholds

var (
	SqlGetSubscriptionHold = `
	SELECT
		a.start_at,
		a.end_at,
		a.subscription_id,
		a.subscription_hold_type_id,
		a.created_at,
		a.updated_at,
		a.deleted_at,
		b.id,
		b.name,
		b.description,
		b.created_at,
		b.deleted_at
	FROM
		subscribers_subscription_holds			AS a,
		subscribers_subscription_hold_types AS b
	WHERE
		a.id = $1
	AND
		b.id = a.subscription_hold_type_id
	`

	SqlCreateSubscriptionHold = `
	INSERT INTO
		subscribers_subscription_holds (
			start_at,
			end_at,
			subscription_id,
			subscription_hold_type_id
		)
	VALUES (
		$1,
		$2,
		$3,
		$4
	)
	RETURNING id, created_at
	`

	SqlDeleteSubscriptionHold = `
	UPDATE
		subscribers_subscription_holds
	SET
		deleted_at = current_timestamp
	WHERE
		id = $1
	AND
		deleted_at IS NULL
	`

	SqlUpdateSubscriptionHold = `
	UPDATE
		subscribers_subscription_holds
	SET
		start_at									= $1,
		end_at										= $2,
		subscription_hold_type_id = $3,
		updated_at                = current_timestamp
	WHERE
		id = $4
	`
)
