// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscriptionholds

import (
	"context"
	"database/sql"
	"os"
	"testing"
	"time"

	"git.runecms.io/rune/runecms"
	billingtest "git.runecms.io/rune/runecms/billing"
	billingpb "git.runecms.io/rune/runecms/billing/rpc"
	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/addresses"
	"git.runecms.io/rune/runecms/subscribers/internal/billingaccounts"
	"git.runecms.io/rune/runecms/subscribers/internal/markets"
	"git.runecms.io/rune/runecms/subscribers/internal/products"
	"git.runecms.io/rune/runecms/subscribers/internal/producttypes"
	"git.runecms.io/rune/runecms/subscribers/internal/publications"
	"git.runecms.io/rune/runecms/subscribers/internal/subscribers"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptionholdtypes"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptions"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptionstatuses"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptiontypes"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"

	"github.com/google/go-cmp/cmp"
	"github.com/ory/ladon"
)

var (
	ctx            = context.Background()
	w              *ladon.Ladon
	db             *sql.DB
	valid_id       runecms.Identity
	invalid_id     runecms.Identity
	address        *models.Address
	billing        *models.BillingAccount
	market         *models.Market
	publication    *models.Publication
	subscriber     *models.Subscriber
	sstatus        *models.SubscriptionStatus
	product        *models.Product
	producttype    *models.ProductType
	stype          *models.SubscriptionType
	shtype         *models.SubscriptionHoldType
	subscription   *models.Subscription
	billingClient  billingpb.BillingAccounts
	webhooksClient = &webhooks.TestClient{}
	now            string
	later          string
)

func GetTestDb(t *testing.T) {
	var err error
	if db != nil {
		return
	}
	w = runetestutil.GetTestLadon()
	db = runetestutil.GetTestDb(t)
	if err != nil {
		panic(err.Error())
	}

	billingClient = billingtest.NewTestClient(t)
	valid_id = runecms.Be("valid", "admin")
	invalid_id = runecms.Be("invalid", "invalid")
	market, err = markets.CreateMarket(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new subscription")
	if err != nil {
		t.Fatal(err.Error())
	}

	publication, err = publications.CreatePublication(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new subscription", market.Id, "{}")
	if err != nil {
		t.Fatal(err.Error())
	}

	sstatus, err = subscriptionstatuses.CreateSubscriptionStatus(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new subscription")
	if err != nil {
		t.Fatal(err.Error())
	}

	producttype, err = producttypes.CreateProductType(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint")
	if err != nil {
		t.Fatal(err.Error())
	}

	product, err = products.CreateProduct(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test product", 1000, producttype.Id, publication.Id)
	if err != nil {
		t.Fatal(err.Error())
	}

	stype, err = subscriptiontypes.CreateSubscriptionType(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint", product.Id)
	if err != nil {
		t.Fatal(err.Error())
	}

	shtype, err = subscriptionholdtypes.CreateSubscriptionHoldType(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new subscription")
	if err != nil {
		t.Fatal(err.Error())
	}

	subscriber, err = subscribers.CreateSubscriber(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID())
	if err != nil {
		t.Fatal(err.Error())
	}

	address, err = addresses.CreateAddress(ctx, db, w, webhooksClient, valid_id, "test subscription address", "ar", "test", "00000", "usa")
	if err != nil {
		t.Fatal(err.Error())
	}

	billing, err = billingaccounts.CreateBillingAccount(ctx, db, w, webhooksClient, billingClient, valid_id, "new subscription hold", false, false, subscriber.Id, runetestutil.GetUUID(), "4321", "Test User", time.Now().AddDate(1, 1, 0), models.BillingAccountType_GOOGLE_PAY)
	if err != nil {
		t.Fatal(err.Error())
	}

	subscription, err = subscriptions.CreateSubscription(ctx, db, w, webhooksClient, valid_id,
		true,
		1000,
		time.Now().UTC(),
		time.Now().Add(24*time.Hour*30*12).UTC(),
		sstatus.Id,
		stype.Id,
		subscriber.Id,
		billing.Id,
		nil,
	)
	if err != nil {
		t.Fatal(err.Error())
	}

	now = time.Now().UTC().Round(time.Hour * 24).Format(time.RFC3339)
	later = time.Now().Add(24 * time.Hour * 30).UTC().Round(time.Hour * 24).Format(time.RFC3339)
}

func TestMain(m *testing.M) {
	i := m.Run()
	runetestutil.KillAll()
	os.Exit(i)
}

func TestCreate(t *testing.T) {
	GetTestDb(t)
	t.Run("Create subscriptionhold", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			model, err := CreateSubscriptionHold(ctx, db, w, webhooksClient, valid_id,
				subscription.Id,
				shtype.Id,
				now,
				later,
			)

			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Fatal("Returned model is nil")
			}

			// Make sure we were given an id
			if model.Id == "" {
				t.Error("Returned model does not have an id")
			}

			if cmp.Equal(model.CreatedAt, time.Time{}) {
				t.Error("Returned model's CreatedAt was not set")
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := CreateSubscriptionHold(ctx, db, w, webhooksClient, invalid_id,
				subscription.Id,
				shtype.Id,
				now,
				later,
			)

			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})
	})
}

func TestGet(t *testing.T) {
	GetTestDb(t)
	model, err := CreateSubscriptionHold(ctx, db, w, webhooksClient, valid_id,
		subscription.Id,
		shtype.Id,
		now,
		later,
	)
	if err != nil {
		t.Fatal("Unable to set up test for Get")
	}

	t.Run("Get subscriptionhold", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := GetSubscriptionHold(ctx, db, w, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if m == nil {
				t.Error("Returned model is nil")
			}

			if cmp.Equal(model, m) != true {
				t.Errorf("models do not match\n%s\n", cmp.Diff(model, m))
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := GetSubscriptionHold(ctx, db, w, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := GetSubscriptionHold(ctx, db, w, invalid_id, "invalid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})

}

func TestDelete(t *testing.T) {
	model, err := CreateSubscriptionHold(ctx, db, w, webhooksClient, valid_id,
		subscription.Id,
		shtype.Id,
		now,
		later,
	)
	if err != nil {
		t.Fatal("Unable to set up test for Delete")
	}

	t.Run("Delete subscriptionhold", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := DeleteSubscriptionHold(ctx, db, w, webhooksClient, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if m == nil {
				t.Error("Returned model is nil")
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := DeleteSubscriptionHold(ctx, db, w, webhooksClient, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := DeleteSubscriptionHold(ctx, db, w, webhooksClient, invalid_id, "invalid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})
}
