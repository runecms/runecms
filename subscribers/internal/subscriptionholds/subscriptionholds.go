// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscriptionholds

import (
	"context"
	"database/sql"
	"fmt"

	"git.runecms.io/rune/runecms"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/ory/ladon"
)

func getAccountId(db *sql.DB, id string) (string, error) {
	var accountId string
	if err := db.QueryRow(`
		SELECT
			subscribers.account_id
		FROM
			subscribers,
			subscribers_subscriptions as subscriptions,
			subscribers_subscription_holds as subscription_holds
		WHERE
			subscription_holds.id = $1
		AND
			subscriptions.id = subscription_holds.subscription_id
		AND
			subscribers.id = subscriptions.subscriber_id
	`, id).Scan(&accountId); err != nil {
		return "", runecms.Error(runecms.Internal, err.Error())
	}

	return accountId, nil
}

// CreateSubscriptionHold will create a new subscription hold in the provided database
func CreateSubscriptionHold(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	subscriptionId string,
	subscriptionHoldTypeId string,
	startAt string,
	endAt string,
) (*models.SubscriptionHold, error) {
	if err := runecms.Access(ident, w, "create", "subscribers:subscription_holds"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	model := &models.SubscriptionHold{
		SubscriptionId:         subscriptionId,
		SubscriptionHoldTypeId: subscriptionHoldTypeId,
		StartAt:                startAt,
		EndAt:                  endAt,
	}

	if err := db.QueryRowContext(
		ctx,
		SqlCreateSubscriptionHold,
		model.StartAt,
		model.EndAt,
		model.SubscriptionId,
		model.SubscriptionHoldTypeId,
	).Scan(
		&model.Id,
		&model.CreatedAt,
	); err != nil {
		return nil, runecms.Error(runecms.Internal, "Error creating SubscriptionHold", err.Error())
	}

	model, err := GetSubscriptionHold(ctx, db, w, ident, model.Id)
	if err != nil {
		return nil, err
	}
	go webhooks.Distribute(webhooksClient, ident, "subscribers:subscription_holds", "create", model)
	return model, nil
}

// DeleteSubscriptionHold will delete a subscription hold from the provided database
func DeleteSubscriptionHold(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	id string,
) (*models.SubscriptionHold, error) {
	accountId, err := getAccountId(db, id)
	if err != nil {
		return nil, err
	}

	resource := fmt.Sprintf("subscribers:%s:subscription_holds:%s", accountId, id)
	if err := runecms.Access(ident, w, "delete", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if _, err := db.ExecContext(
		ctx,
		SqlDeleteSubscriptionHold,
		id,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, "Error occured when deleting the subscription hold", id, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, "Error occured when deleting the subscription hold", id, err.Error())
	}

	model, err := GetSubscriptionHold(ctx, db, w, ident, id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:subscription_holds", "delete", model)
	return model, nil
}

// GetSubscriptionHold will retrieve a subscription hold from the provided database
func GetSubscriptionHold(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	id string,
) (*models.SubscriptionHold, error) {
	accountId, err := getAccountId(db, id)
	if err != nil {
		return nil, err
	}

	resource := fmt.Sprintf("subscribers:%s:billing_accounts:%s", accountId, id)

	if err := runecms.Access(ident, w, "read", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.SubscriptionHold{
		Id:                   id,
		SubscriptionHoldType: &models.SubscriptionHoldType{},
	}

	var (
		aDeletedAt sql.NullString
		bDeletedAt sql.NullString
	)

	if err := db.QueryRowContext(
		ctx,
		SqlGetSubscriptionHold,
		id,
	).Scan(
		&model.StartAt,
		&model.EndAt,
		&model.SubscriptionId,
		&model.SubscriptionHoldTypeId,
		&model.CreatedAt,
		&model.UpdatedAt,
		&aDeletedAt,
		&model.SubscriptionHoldType.Id,
		&model.SubscriptionHoldType.Name,
		&model.SubscriptionHoldType.Description,
		&model.SubscriptionHoldType.CreatedAt,
		&bDeletedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, "Error when searching for subscription hold", id, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, "Error when searching for subscription hold", id, err.Error())
	}

	return model, nil
}

// UpdateSubscriptionHoldFromModel will update a subscription hold in the provided database given a protobuf model
func UpdateSubscriptionHoldFromModel(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	model *models.SubscriptionHold,
) (*models.SubscriptionHold, error) {
	accountId, err := getAccountId(db, model.GetId())
	if err != nil {
		return nil, err
	}

	resource := fmt.Sprintf("subscribers:%s:subscription_holds:%s", accountId, model.GetId())

	if err := runecms.Access(ident, w, "update", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if _, err := db.ExecContext(
		ctx,
		SqlUpdateSubscriptionHold,
		model.StartAt,
		model.EndAt,
		model.SubscriptionHoldTypeId,
		model.Id,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, "Error when updating subscription hold", model.Id, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, "Error when updating subscription hold", model.Id, err.Error())
	}

	res, err := GetSubscriptionHold(ctx, db, w, ident, model.Id)
	if err != nil {
		return nil, err
	}
	go webhooks.Distribute(webhooksClient, ident, "subscribers:subscription_holds", "update", res)
	return res, nil
}
