// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscriptionholdtypes

var (
	SqlCreateSubscriptionHoldType = `
	INSERT INTO
		subscribers_subscription_hold_types (
			name,
			description
		)
	VALUES (
		$1,
		$2
	)
	RETURNING id, created_at
	`

	SqlGetSubscriptionHoldType = `
	SELECT
		name,
		description,
		created_at,
		deleted_at
	FROM
		subscribers_subscription_hold_types
	WHERE
		id = $1
	`

	SqlGetSubscriptionHoldTypeByName = `
	SELECT
		id,
		description,
		created_at,
		deleted_at
	FROM
		subscribers_subscription_hold_types
	WHERE
		name = $1
	`

	SqlDeleteSubscriptionHoldType = `
	UPDATE
		subscribers_subscription_hold_types
	SET
		deleted_at = current_timestamp
	WHERE
		id = $1
	RETURNING
		deleted_at
	`
)
