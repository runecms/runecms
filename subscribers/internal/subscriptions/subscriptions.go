// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscriptions

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/deliveryaddresses"
	"git.runecms.io/rune/runecms/subscribers/internal/subscribers"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/ory/ladon"
)

func getAccountId(db *sql.DB, id string) (string, error) {
	const unassigned = "unassigned"
	var accountId sql.NullString
	if err := db.QueryRow(`
		SELECT
			subscribers.account_id
		FROM
			subscribers,
			subscribers_subscriptions as subscriptions
		WHERE
			subscriptions.id = $1
		AND
			subscribers.id = subscriptions.subscriber_id
	`, id).Scan(&accountId); err != nil {
		return "", runecms.Error(runecms.Internal, err.Error())
	}

	if accountId.Valid {
		return accountId.String, nil
	}
	return unassigned, nil
}

// GetAuthorizedUsers returns the list of AuthorizedUsers (Active and deleted) for the provided subscription
func GetAuthorizedUsers(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	subscriptionId string,
) ([]*models.AuthorizedUser, error) {
	rows, err := db.QueryContext(
		ctx,
		SqlGetAuthorizedUsers,
		subscriptionId,
	)
	if err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	authorizedUsers := []*models.AuthorizedUser{}
	for rows.Next() {
		a := &models.AuthorizedUser{
			SubscriptionId: subscriptionId,
			Subscriber:     &models.Subscriber{},
		}
		var (
			aDeletedAt sql.NullString
			bDeletedAt sql.NullString
		)

		if err := rows.Scan(
			&a.SubscriberId,
			&a.CreatedAt,
			&aDeletedAt,
			&a.Subscriber.AccountId,
			&a.Subscriber.CreatedAt,
			&bDeletedAt,
			&a.Subscriber.UpdatedAt,
		); err != nil {
			if err == sql.ErrNoRows {
				return nil, runecms.Error(runecms.NotFound, err.Error())
			}
			return nil, runecms.Error(runecms.Internal, err.Error())
		}
		a.DeletedAt = aDeletedAt.String
		a.Subscriber.DeletedAt = bDeletedAt.String
		authorizedUsers = append(authorizedUsers, a)
	}

	if err := rows.Close(); err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	return authorizedUsers, nil
}

// GetSubscriptionHolds returns the list of SubscriptionHolds (Active, expired, and deleted) for the provided subscription
func GetSubscriptionHolds(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	subscriptionId string,
) ([]*models.SubscriptionHold, error) {
	rows, err := db.QueryContext(
		ctx,
		SqlGetSubscriptionHolds,
		subscriptionId,
	)
	if err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	subscriptionHolds := []*models.SubscriptionHold{}
	for rows.Next() {
		a := &models.SubscriptionHold{
			SubscriptionId:       subscriptionId,
			SubscriptionHoldType: &models.SubscriptionHoldType{},
		}

		var (
			aDeletedAt sql.NullString
			bDeletedAt sql.NullString
		)

		if err := rows.Scan(
			&a.StartAt,
			&a.EndAt,
			&a.SubscriptionHoldTypeId,
			&a.CreatedAt,
			&aDeletedAt,
			&a.UpdatedAt,
			&a.SubscriptionHoldType.Name,
			&a.SubscriptionHoldType.Description,
			&a.SubscriptionHoldType.CreatedAt,
			&bDeletedAt,
		); err != nil {
			if err == sql.ErrNoRows {
				return nil, runecms.Error(runecms.Internal, err.Error())
			}
			return nil, runecms.Error(runecms.Internal, err.Error())
		}
		a.DeletedAt = aDeletedAt.String
		a.SubscriptionHoldType.DeletedAt = bDeletedAt.String
		subscriptionHolds = append(subscriptionHolds, a)
	}

	if err := rows.Close(); err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	return subscriptionHolds, nil
}

// CreateSubscription will create a new subscription in the given database
func CreateSubscription(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	isActive bool,
	rate int64,
	startAt time.Time,
	endAt time.Time,
	subscriptionStatusId string,
	subscriptionTypeId string,
	subscriberId string,
	billingAccountId string,
	authorizedUsers []*models.Subscriber,
) (*models.Subscription, error) {
	if err := runecms.Access(ident, w, "create", "subscribers:subscriptions"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	var (
		err   error
		model = &models.Subscription{
			Active:               isActive,
			Rate:                 rate,
			StartAt:              startAt.Format(time.RFC3339),
			EndAt:                endAt.Format(time.RFC3339),
			SubscriptionStatusId: subscriptionStatusId,
			SubscriptionTypeId:   subscriptionTypeId,
			SubscriberId:         subscriberId,
			BillingAccountId:     billingAccountId,
			Subscriber:           &models.Subscriber{},
			SubscriptionType:     &models.SubscriptionType{},
			SubscriptionStatus:   &models.SubscriptionStatus{},
			BillingAccount:       &models.BillingAccount{},
		}
	)

	if err := db.QueryRowContext(
		ctx,
		SqlCreateSubscription,
		model.Rate,
		model.Active,
		model.StartAt,
		model.EndAt,
		model.SubscriptionStatusId,
		model.SubscriptionTypeId,
		model.SubscriberId,
		model.BillingAccountId,
	).Scan(
		&model.Id,
		&model.CreatedAt,
	); err != nil {
		return nil, runecms.Error(runecms.InvalidArgument, err.Error())
	}

	subscriptionAccountId, err := subscribers.GetAccountId(db, model.GetSubscriberId())
	if err != nil {
		return nil, err
	}

	authorized := make([]*models.AuthorizedUser, len(authorizedUsers))
	for i, v := range authorizedUsers {
		v := v
		if v == nil {
			continue
		}

		a := &models.AuthorizedUser{
			SubscriberId:   v.GetId(),
			SubscriptionId: model.GetId(),
		}

		// Now create authorized users
		if err := db.QueryRowContext(
			ctx,
			SqlInsertAuthorizedUser,
			a.SubscriberId,
			a.SubscriptionId,
		).Scan(
			&a.Id,
			&a.CreatedAt,
		); err != nil {
			return nil, runecms.Error(runecms.Internal, err.Error())
		}

		authorized[i] = a

		authorizedUserAccountId, err := subscribers.GetAccountId(db, a.GetSubscriberId())
		if err != nil {
			return nil, err
		}

		// This will give the authorized user access to read this subscription
		if err := w.Manager.Create(&ladon.DefaultPolicy{
			ID:          fmt.Sprintf("subscription-%s-authorized-user-%s", model.GetId(), a.GetSubscriberId()),
			Description: fmt.Sprintf("Give authorized user %s read access to subscription %s", a.GetSubscriberId(), model.GetId()),
			Subjects:    []string{runecms.UserSubject(authorizedUserAccountId)},
			Effect:      ladon.AllowAccess,
			Resources:   []string{fmt.Sprintf("subscribers:%s:subscriptions:%s", subscriptionAccountId, authorizedUserAccountId)},
			Actions:     []string{"read"},
			Conditions:  nil,
		}); err != nil {
			return nil, runecms.Error(runecms.Internal, err.Error())
		}
	}

	model, err = GetSubscription(ctx, db, w, ident, model.Id)
	if err != nil {
		return nil, err
	}
	go webhooks.Distribute(webhooksClient, ident, "subscribers:subscriptions", "create", model)
	return model, nil
}

// CreatePhysicalSubscription is expected to be called after CreateSubscription when creating a subscription accessed physically. (Magazines, newspapers, etc)
func CreatePhysicalSubscription(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	webhooksClient webhookspb.Webhooks,
	subscriptionId string,
	deliveryAddressId string,
) (*models.PhysicalSubscription, error) {
	if err := runecms.Access(ident, w, "create", "subscribers:subscriptions"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.PhysicalSubscription{
		Subscription: &models.Subscription{},
		DeliveryAddress: &models.DeliveryAddress{
			Id:      deliveryAddressId,
			Address: &models.Address{},
		},
	}

	if _, err := db.ExecContext(
		ctx,
		SqlCreatePhysicalSubscription,
		subscriptionId,
		deliveryAddressId,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.InvalidArgument, err.Error())
		}

		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	var err error

	model.Subscription, err = GetSubscription(ctx, db, w, ident, subscriptionId)
	if err != nil {
		return nil, err
	}

	model.DeliveryAddress, err = deliveryaddresses.GetDeliveryAddress(ctx, db, w, ident, deliveryAddressId)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:physical_subscriptions", "create", model)

	return model, nil
}

// DeleteSubscription will delete a subscription in the provided database
func DeleteSubscription(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	id string,
) (*models.Subscription, error) {
	accountId, err := getAccountId(db, id)
	if err != nil {
		return nil, err
	}
	resource := fmt.Sprintf("subscribers:%s:subscriptions:%s", accountId, id)

	if err := runecms.Access(ident, w, "delete", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if _, err := db.ExecContext(
		ctx,
		SqlDeleteSubscription,
		id,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.InvalidArgument, err.Error())
	}

	model, err := GetSubscription(ctx, db, w, ident, id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:subscriptions", "delete", model)
	return model, nil
}

// GetSubscription will retrieve a subscription from the provided database
func GetSubscription(ctx context.Context, db *sql.DB, w *ladon.Ladon, ident runecms.Identity, id string) (*models.Subscription, error) {
	accountId, err := getAccountId(db, id)
	if err != nil {
		return nil, err
	}

	resource := fmt.Sprintf("subscribers:%s:subscriptions:%s", accountId, id)

	if err := runecms.Access(ident, w, "read", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.Subscription{
		Id: id,
		SubscriptionType: &models.SubscriptionType{
			Product: &models.Product{
				Publication: &models.Publication{
					Market: &models.Market{},
				},
				ProductType: &models.ProductType{},
			},
		},
		SubscriptionStatus: &models.SubscriptionStatus{},
		Subscriber:         &models.Subscriber{},
		BillingAccount:     &models.BillingAccount{},
	}

	// Nullable elements
	var (
		subscriptionDeletedAt      sql.NullString
		subscriptionNextBillAt     sql.NullString
		subscriptionLastBillFailed sql.NullBool
		typeDeletedAt              sql.NullString
		statusDeletedAt            sql.NullString
		subscriberDeletedAt        sql.NullString
		billingDeletedAt           sql.NullString
		productDeletedAt           sql.NullString
		productTypeDeletedAt       sql.NullString
		publicationDeletedAt       sql.NullString
		marketDeletedAt            sql.NullString
	)
	if err := db.QueryRowContext(
		ctx,
		SqlGetSubscriptionByID,
		model.Id,
	).Scan(
		&model.Rate,
		&model.Active,
		&model.StartAt,
		&model.EndAt,
		&model.SubscriptionStatusId,
		&model.SubscriptionTypeId,
		&model.SubscriberId,
		&model.BillingAccountId,
		&model.CreatedAt,
		&subscriptionDeletedAt,
		&model.UpdatedAt,
		&model.LastBillAt,
		&subscriptionNextBillAt,
		&model.Balance,
		&subscriptionLastBillFailed,

		&model.SubscriptionType.Name,
		&model.SubscriptionType.Description,
		&model.SubscriptionType.ProductId,
		&model.SubscriptionType.CreatedAt,
		&typeDeletedAt,

		&model.SubscriptionStatus.Name,
		&model.SubscriptionStatus.Description,
		&model.SubscriptionStatus.CreatedAt,
		&statusDeletedAt,

		&model.Subscriber.AccountId,
		&model.Subscriber.CreatedAt,
		&subscriberDeletedAt,
		&model.Subscriber.UpdatedAt,

		&model.BillingAccount.Name,
		&model.BillingAccount.SubscriberId,
		&model.BillingAccount.BillingAccountId,
		&model.BillingAccount.IsDefault,
		&model.BillingAccount.CreatedAt,
		&billingDeletedAt,

		&model.SubscriptionType.Product.Name,
		&model.SubscriptionType.Product.Description,
		&model.SubscriptionType.Product.Price,
		&model.SubscriptionType.Product.ProductTypeId,
		&model.SubscriptionType.Product.PublicationId,
		&model.SubscriptionType.Product.BillingProcessorData,
		&model.SubscriptionType.Product.CreatedAt,
		&productDeletedAt,

		&model.SubscriptionType.Product.ProductType.Name,
		&model.SubscriptionType.Product.ProductType.Description,
		&model.SubscriptionType.Product.ProductType.CreatedAt,
		&productTypeDeletedAt,

		&model.SubscriptionType.Product.Publication.Name,
		&model.SubscriptionType.Product.Publication.Description,
		&model.SubscriptionType.Product.Publication.MarketId,
		&model.SubscriptionType.Product.Publication.BillingProcessorData,
		&model.SubscriptionType.Product.Publication.CreatedAt,
		&publicationDeletedAt,

		&model.SubscriptionType.Product.Publication.Market.Name,
		&model.SubscriptionType.Product.Publication.Market.Description,
		&model.SubscriptionType.Product.Publication.Market.CreatedAt,
		&marketDeletedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Errorf(runecms.NotFound, "No subscription with id %s was found", model.Id)
		}
		return nil, runecms.Error(runecms.InvalidArgument, err.Error())
	}

	model.DeletedAt = subscriptionDeletedAt.String
	model.NextBillAt = subscriptionNextBillAt.String
	model.LastBillFailed = subscriptionLastBillFailed.Bool
	model.SubscriptionType.DeletedAt = typeDeletedAt.String
	model.SubscriptionStatus.DeletedAt = statusDeletedAt.String
	model.Subscriber.DeletedAt = subscriberDeletedAt.String
	model.BillingAccount.DeletedAt = billingDeletedAt.String
	model.SubscriptionType.Product.DeletedAt = productDeletedAt.String
	model.SubscriptionType.Product.ProductType.DeletedAt = productTypeDeletedAt.String
	model.SubscriptionType.Product.Publication.DeletedAt = publicationDeletedAt.String
	model.SubscriptionType.Product.Publication.Market.DeletedAt = marketDeletedAt.String

	model.AuthorizedUsers, err = GetAuthorizedUsers(ctx, db, w, ident, model.Id)
	if err != nil {
		return nil, err
	}

	model.SubscriptionHolds, err = GetSubscriptionHolds(ctx, db, w, ident, model.Id)
	if err != nil {
		return nil, err
	}

	return model, nil
}

func GetPhysicalSubscription(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	ident runecms.Identity,
	id string,
) (*models.PhysicalSubscription, error) {
	// this function will check the ladon permissions on this subscription, so we don't have to
	subscription, err := GetSubscription(ctx, db, w, ident, id)
	if err != nil {
		return nil, err
	}

	var (
		model = &models.PhysicalSubscription{
			DeliveryAddress: &models.DeliveryAddress{
				Address: &models.Address{},
			},
		}
		deliveryAddressDeletedAt sql.NullString
	)

	if err := db.QueryRowContext(
		ctx,
		SqlGetPhysicalSubscription,
		id,
	).Scan(
		&model.DeliveryAddress.Id,
		&model.DeliveryAddress.SubscriberId,
		&model.DeliveryAddress.IsDefault,
		&model.DeliveryAddress.CreatedAt,
		&deliveryAddressDeletedAt,
		&model.DeliveryAddress.Address.Id,
		&model.DeliveryAddress.Address.City,
		&model.DeliveryAddress.Address.State,
		&model.DeliveryAddress.Address.Zip,
		&model.DeliveryAddress.Address.Country,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}

		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	model.DeliveryAddress.DeletedAt = deliveryAddressDeletedAt.String
	model.Subscription = subscription

	return model, nil
}

// UpdateSubscription will update a subscription in the provided database given a protobuf message
func UpdateSubscription(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	model *models.Subscription,
) (*models.Subscription, error) {
	accountId, err := getAccountId(db, model.GetId())
	if err != nil {
		return nil, err
	}

	resource := fmt.Sprintf("subscribers:%s:subscriptions:%s", accountId, model.GetId())
	if err := runecms.Access(ident, w, "update", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if model == nil {
		return nil, runecms.Error(runecms.InvalidArgument, "The subscription must not be nil")
	}

	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	// Update the subscription itself
	if _, err := tx.ExecContext(
		ctx,
		SqlUpdateSubscription,
		model.Rate,
		model.Active,
		model.StartAt,
		model.EndAt,
		model.SubscriptionStatusId,
		model.SubscriptionTypeId,
		model.SubscriberId,
		model.BillingAccountId,
		model.LastBillAt,
		model.NextBillAt,
		model.Balance,
		model.LastBillFailed,
		model.Id,
	); err != nil {
		return nil, runecms.Errorf(runecms.NotFound, "Subscription with ID '%s' not found", model.Id)
	}

	// Remove and re-create the Authorized Users
	tx.ExecContext(
		ctx,
		SqlDeleteAuthorizedUsers,
		model.Id,
	)
	for _, v := range model.AuthorizedUsers {
		var (
			id        string
			createdAt string
		)
		if err := tx.QueryRowContext(
			ctx,
			SqlInsertAuthorizedUser,
			v.SubscriberId,
			model.Id,
		).Scan(
			&id,
			&createdAt,
		); err != nil {
			if err := tx.Rollback(); err != nil {
				return nil, runecms.Error(runecms.Internal, err.Error())
			}
			return nil, runecms.Error(runecms.InvalidArgument, err.Error())
		}
	}

	if err := tx.Commit(); err != nil {
		return nil, runecms.Errorf(runecms.InvalidArgument, "Failed to commit transaction. Error: %s", err.Error())
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:subscriptions", "update", model)
	return GetSubscription(ctx, db, w, ident, model.Id)
}

// UpdatePhysicalSubscription will update a subscription in the provided database given a protobuf message
func UpdatePhysicalSubscription(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	model *models.PhysicalSubscription,
) (*models.PhysicalSubscription, error) {
	if model == nil {
		return nil, runecms.Error(runecms.InvalidArgument, "The physical subscription must not be nil")
	}

	subscription, err := UpdateSubscription(ctx, db, w, webhooksClient, ident, model.Subscription)

	if err != nil {
		return nil, err
	}

	if _, err := db.ExecContext(
		ctx,
		SqlUpdatePhysicalSubscription,
		model.DeliveryAddressId,
		model.SubscriptionId,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}
	model, err = GetPhysicalSubscription(ctx, db, w, ident, subscription.Id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:physical_subscriptions", "update", model)
	return model, nil
}

// GetDueSubscriptions will retrieve a list of all billing accounts that are due for billing
func GetDueSubscriptions(ctx context.Context, db *sql.DB, w *ladon.Ladon, ident runecms.Identity) ([]*models.Subscription, error) {
	if err := runecms.Access(ident, w, "read", "subscribers:subscriptions"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	var (
		subscriptions []*models.Subscription
	)

	// // Get a list of all not-deleted subscriptions, currently active subscriptions
	// res := db.
	// 	Preload("SubscriptionType").
	// 	Preload("SubscriptionType.Product", "product_type.name=$1", "subscription").
	// 	Preload("SubscriptionType.Product.Rates", "end_date >= current_date() AND start_date <= current_date()").
	// 	Preload("SubscriptionType.Product.Rates.BillingFrequency").
	// 	Preload("SubscriptionType.Product.Rates.RateFrequency").
	// 	Preload("BillingAccount").
	// 	Find(&subscriptions, "end_date >= current_date() AND start_date <= current_date() AND next_bill_date = current_date() AND active = $1", "TRUE")

	// if res.Error != nil {
	// 	return nil, res.Error
	// }

	// if res.RecordNotFound() {
	// 	return nil, fmt.Errorf("No active subscriptions are due for billing today")
	// }

	return subscriptions, nil
}
