// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscriptions

import (
	"context"
	"database/sql"
	"os"
	"testing"
	"time"

	"git.runecms.io/rune/runecms"
	billingtest "git.runecms.io/rune/runecms/billing"
	billingpb "git.runecms.io/rune/runecms/billing/rpc"
	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/addresses"
	"git.runecms.io/rune/runecms/subscribers/internal/billingaccounts"
	"git.runecms.io/rune/runecms/subscribers/internal/deliveryaddresses"
	"git.runecms.io/rune/runecms/subscribers/internal/markets"
	"git.runecms.io/rune/runecms/subscribers/internal/products"
	"git.runecms.io/rune/runecms/subscribers/internal/producttypes"
	"git.runecms.io/rune/runecms/subscribers/internal/publications"
	"git.runecms.io/rune/runecms/subscribers/internal/subscribers"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptionstatuses"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptiontypes"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"

	"github.com/google/go-cmp/cmp"
	"github.com/ory/ladon"
)

var (
	w              *ladon.Ladon
	db             *sql.DB
	ctx            = context.Background()
	valid_id       runecms.Identity
	invalid_id     runecms.Identity
	address        *models.Address
	billing        *models.BillingAccount
	delivery       *models.DeliveryAddress
	market         *models.Market
	publication    *models.Publication
	producttype    *models.ProductType
	product        *models.Product
	subscriber     *models.Subscriber
	sstatus        *models.SubscriptionStatus
	stype          *models.SubscriptionType
	billingClient  billingpb.BillingAccounts
	webhooksClient = &webhooks.TestClient{}
	exampleUuid    = "63616665-6630-3064-6465-616462656562"
)

func GetTestDb(t *testing.T) {
	if db != nil {
		return
	}
	w = runetestutil.GetTestLadon()
	db = runetestutil.GetTestDb(t)

	valid_id = runecms.Be("valid", "admin")
	invalid_id = runecms.Be("invalid", "invalid")

	billingClient = billingtest.NewTestClient(t)
	market, _ = markets.CreateMarket(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new subscription")
	publication, _ = publications.CreatePublication(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new subscription", market.Id, "{}")
	sstatus, _ = subscriptionstatuses.CreateSubscriptionStatus(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new subscription")
	producttype, _ = producttypes.CreateProductType(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint")
	product, _ = products.CreateProduct(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test product", 1000, producttype.Id, publication.Id)
	stype, _ = subscriptiontypes.CreateSubscriptionType(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint", product.Id)
	subscriber, _ = subscribers.CreateSubscriber(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID())
	address, _ = addresses.CreateAddress(ctx, db, w, webhooksClient, valid_id, "test subscription address", "test", "ar", "00000", "usa")
	delivery, _ = deliveryaddresses.CreateDeliveryAddress(ctx, db, w, webhooksClient, valid_id, subscriber.Id, address.Id)
	billing, _ = billingaccounts.CreateBillingAccount(ctx, db, w, webhooksClient, billingClient, valid_id, "new subscription", false, false, subscriber.Id, exampleUuid, "4321", "Test User", time.Now().AddDate(1, 1, 0), models.BillingAccountType_CREDIT_CARD)
}

func TestMain(m *testing.M) {
	i := m.Run()
	runetestutil.KillAll()
	os.Exit(i)
}

func TestCreate(t *testing.T) {
	GetTestDb(t)
	t.Run("Create subscription", func(t *testing.T) {
		t.Run("Successful request (without deliveryaddress)", func(t *testing.T) {
			model, err := CreateSubscription(
				ctx,
				db,
				w, webhooksClient,
				valid_id,
				true,
				1000,
				time.Now().UTC(),
				time.Now().UTC(),
				sstatus.Id,
				stype.Id,
				subscriber.Id,
				billing.Id,
				nil,
			)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Fatal("Returned model is nil")
			}

			// Make sure we were given an id
			if model.Id == "" {
				t.Error("Returned model does not have an id")
			}

			if cmp.Equal(model.CreatedAt, time.Time{}) {
				t.Error("Returned model's CreatedAt was not set")
			}
		})
		t.Run("Create Physical subscription", func(t *testing.T) {
			model, err := CreateSubscription(
				ctx,
				db,
				w, webhooksClient,
				valid_id,
				true,
				1000,
				time.Now().UTC(),
				time.Now().UTC(),
				sstatus.Id,
				stype.Id,
				subscriber.Id,
				billing.Id,
				nil,
			)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Fatal("Returned model is nil")
			}

			// Make sure we were given an id
			if model.Id == "" {
				t.Error("Returned model does not have an id")
			}

			if cmp.Equal(model.CreatedAt, time.Time{}) {
				t.Error("Returned model's CreatedAt was not set")
			}

		})
		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := CreateSubscription(
				ctx,
				db,
				w, webhooksClient,
				invalid_id,
				true,
				1000,
				time.Now().UTC(),
				time.Now().UTC(),
				sstatus.Id,
				stype.Id,
				subscriber.Id,
				billing.Id,
				nil,
			)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("With Authorized users", func(t *testing.T) {
			s1, err := subscribers.CreateSubscriber(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID())
			if err != nil {
				t.Fatal("unable to setup test for CreateSubscription. Error:", err.Error())
			}
			s2, err := subscribers.CreateSubscriber(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID())
			if err != nil {
				t.Fatal("unable to setup test for CreateSubscription. Error:", err.Error())
			}
			s3, err := subscribers.CreateSubscriber(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID())
			if err != nil {
				t.Fatal("unable to setup test for CreateSubscription. Error:", err.Error())
			}
			authorizedUsers := []*models.Subscriber{s1, s2, s3}

			model, err := CreateSubscription(
				ctx,
				db,
				w,
				webhooksClient,
				valid_id,
				true,
				1000,
				time.Now().UTC(),
				time.Now().UTC(),
				sstatus.Id,
				stype.Id,
				subscriber.Id,
				billing.Id,
				authorizedUsers,
			)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Fatal("Returned model is nil")
			}

			// Make sure we were given an id
			if model.Id == "" {
				t.Error("Returned model does not have an id")
			}

			if cmp.Equal(model.CreatedAt, time.Time{}) {
				t.Error("Returned model's CreatedAt was not set")
			}

			if len(model.GetAuthorizedUsers()) != 3 {
				t.Fatalf("Unexpected Authorized Users.\n%s", cmp.Diff(model.AuthorizedUsers, authorizedUsers))
			}
		})
	})
}

func TestGet(t *testing.T) {
	GetTestDb(t)
	model, err := CreateSubscription(
		ctx,
		db,
		w, webhooksClient,
		valid_id,
		true,
		1000,
		time.Now().UTC().Round(time.Minute*60*24),
		time.Now().UTC().Round(time.Minute*60*24),
		sstatus.Id,
		stype.Id,
		subscriber.Id,
		billing.Id,
		nil,
	)
	if err != nil {
		t.Fatalf("Unable to set up test for Get: %s", err.Error())
	}

	t.Run("Get subscription", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := GetSubscription(ctx, db, w, valid_id, model.Id)
			if err != nil {
				t.Fatalf(err.Error())
			}

			if m == nil {
				t.Fatal("Returned model is nil")
			}

			if cmp.Equal(model, m) != true {
				t.Fatalf("models do not match\n%s\n", cmp.Diff(model, m))
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := GetSubscription(ctx, db, w, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := GetSubscription(ctx, db, w, invalid_id, "invalid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})

}

func TestDelete(t *testing.T) {
	model, err := CreateSubscription(
		ctx,
		db,
		w, webhooksClient,
		valid_id,
		true,
		1000,
		time.Now().UTC(),
		time.Now().UTC(),
		sstatus.Id,
		stype.Id,
		subscriber.Id,
		billing.Id,
		nil,
	)
	if err != nil {
		t.Fatalf("Unable to set up test for Delete: %s", err.Error())
	}

	t.Run("Delete subscription", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := DeleteSubscription(ctx, db, w, webhooksClient, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Error("Returned model is nil")
			}
			if m.DeletedAt == "" {
				t.Error("Returned model's DeletedAt is nil")
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := DeleteSubscription(ctx, db, w, webhooksClient, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := DeleteSubscription(ctx, db, w, webhooksClient, invalid_id, "invalid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})
}

func TestGetDueSubscriptions(t *testing.T) {
}
