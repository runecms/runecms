// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscriptions

var (
	SqlCreateSubscription = `
	INSERT INTO
		subscribers_subscriptions (
			rate,
			active,
			start_at,
			end_at,
			subscription_status_id,
			subscription_type_id,
			subscriber_id,
			billing_account_id
		)
	VALUES (
		$1,
		$2,
		$3,
		$4,
		$5,
		$6,
		$7,
		$8
	)
		RETURNING id, created_at
`

	SqlCreatePhysicalSubscription = `
	INSERT INTO
		subscribers_physical_subscriptions (
			subscription_id,
			delivery_address_id
		)
		VALUES (
			$1,
			$2
		)
	`

	SqlGetAuthorizedUsers = `
	SELECT
		a.subscriber_id,
		a.created_at,
		a.deleted_at,
		b.account_id,
		b.created_at,
		b.deleted_at,
		b.updated_at
	FROM
		subscribers_authorized_users	AS a,
		subscribers										AS b
	WHERE
		a.subscription_id = $1
	AND
		b.id = a.subscriber_id
	`

	SqlGetSubscriptionHolds = `
	SELECT
		a.start_at,
		a.end_at,
		a.subscription_hold_type_id,
		a.created_at,
		a.deleted_at,
		a.updated_at,
		b.name,
		b.description,
		b.created_at,
		b.deleted_at
	FROM
		subscribers_subscription_holds			AS a,
		subscribers_subscription_hold_types	AS b
	WHERE
		a.id = $1
	AND
		b.id = a.subscription_hold_type_id
	`

	SqlGetPhysicalSubscription = `
	SELECT
		d.id
		d.subscriber_id,
		d.is_default,
		d.created_at,
		d.deleted_at,
		a.id
		a.city,
		a.state,
		a.zip,
		a.country
	FROM
		subscribers_physical_subscriptions	AS p,
		subscribers_delivery_addresses			AS d,
		subscribers_addresses               AS a
	WHERE
		p.subscription_id = $1
	AND
		d.id = p.delivery_address_id
	AND
		a.id = d.address_id
	`

	SqlGetSubscriptionByID = `
	SELECT
		subscription.rate,
		subscription.active,
		subscription.start_at,
		subscription.end_at,
		subscription.subscription_status_id,
		subscription.subscription_type_id,
		subscription.subscriber_id,
		subscription.billing_account_id,
		subscription.created_at,
		subscription.deleted_at,
		subscription.updated_at,
		subscription.last_bill_at,
		subscription.next_bill_at,
		subscription.balance,
		subscription.last_bill_failed,
		type.name,
		type.description,
		type.product_id,
		type.created_at,
		type.deleted_at,
		status.name,
		status.description,
		status.created_at,
		status.deleted_at,
		subscriber.account_id,
		subscriber.created_at,
		subscriber.deleted_at,
		subscriber.updated_at,
		billing_account.name,
		billing_account.subscriber_id,
		billing_account.billing_account_id,
		billing_account.is_default,
		billing_account.created_at,
		billing_account.deleted_at,
		product.name,
		product.description,
		product.price,
		product.product_type_id,
		product.publication_id,
		product.billing_processor_data,
		product.created_at,
		product.deleted_at,
		product_type.name,
		product_type.description,
		product_type.created_at,
		product_type.deleted_at,
		publication.name,
		publication.description,
		publication.market_id,
		publication.billing_processor_data,
		publication.created_at,
		publication.deleted_at,
		market.name,
		market.description,
		market.created_at,
		market.deleted_at
	FROM
		subscribers_subscriptions					AS subscription,
		subscribers_subscription_types		AS type,
		subscribers_subscription_statuses AS status,
		subscribers_billing_accounts			AS billing_account,
		subscribers_products              AS product,
		subscribers_product_types         AS product_type,
		subscribers_publications					AS publication,
		subscribers_markets								AS market,
		subscribers                       AS subscriber
	WHERE
		subscription.id = $1
	AND
		subscriber.id = subscription.subscriber_id
	AND
		type.id = subscription.subscription_type_id
	AND
		status.id = subscription.subscription_status_id
	AND
		billing_account.id = subscription.billing_account_id
	AND
		product.id = type.product_id
	AND
		product_type.id = product.product_type_id
	AND
		publication.id = product.publication_id
	AND
		market.id = publication.market_id
	`

	SqlInsertAuthorizedUser = `
	INSERT INTO subscribers_authorized_users (
		subscriber_id,
		subscription_id
	)
	VALUES (
		$1, $2
	)
	RETURNING id, created_at
	`

	SqlUpdateSubscription = `
	UPDATE
		subscribers_subscriptions
	SET
		rate										= $1,
		active									= $2,
		start_at								= $3,
		end_at                  = $4,
		subscription_status_id  = $5,
		subscription_type_id    = $6,
		subscriber_id           = $7,
		billing_account_id      = $8,
		last_bill_at            = $9,
		next_bill_at            = $10,
		balance                 = $11,
		last_bill_failed        = $12
	WHERE
		id = $13
	`

	SqlUpdatePhysicalSubscription = `
	UPDATE
		subscribers_physical_subscriptions
	SET
		delivery_address_id = $1
	`

	// SqlDeleteAuthorizedUsers is intended to be used with a transaction that re-inserts the new list of "authorized users"
	SqlDeleteAuthorizedUsers = `
	DELETE FROM
		subscribers_authorized_users
	WHERE
		subscription_id = $1
	`

	SqlDeleteSubscription = `
	UPDATE
		subscribers_subscriptions
	SET
		deleted_at = current_timestamp
	WHERE
		id = $1
	`
)
