// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package billingaccounts

import (
	"context"
	"database/sql"
	"os"
	"strings"
	"testing"
	"time"

	"git.runecms.io/rune/runecms"
	billingtest "git.runecms.io/rune/runecms/billing"
	billingpb "git.runecms.io/rune/runecms/billing/rpc"
	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/subscribers"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"

	"github.com/google/go-cmp/cmp"
	"github.com/ory/ladon"
)

var (
	ctx            = context.Background()
	w              *ladon.Ladon
	db             *sql.DB
	billingClient  billingpb.BillingAccounts
	webhooksClient = &webhooks.TestClient{}
	valid_id       runecms.Identity
	invalid_id     runecms.Identity
	subscriber     *models.Subscriber
	exampleUuid    = "63616665-6630-3064-6465-616462656562"
	exampleUuid2   = "63616665-6630-3064-6465-616462656563"
	exampleUuid3   = "63616665-6630-3064-6465-616462656564"
)

func GetTestDb(t *testing.T) {
	var err error
	if db != nil {
		return
	}
	w = runetestutil.GetTestLadon()
	db = runetestutil.GetTestDb(t)
	billingClient = billingtest.NewTestClient(t)
	if err != nil {
		panic(err.Error())
	}

	valid_id = runecms.Be("valid", "admin")
	invalid_id = runecms.Be("invalid", "invalid")

	subscriber, _ = subscribers.CreateSubscriber(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID())
}

func getbillingaccount() (*models.BillingAccount, error) {
	s, err := subscribers.CreateSubscriber(ctx, db, w, webhooksClient, valid_id, runetestutil.GetUUID())
	if err != nil {
		return nil, err
	}

	return CreateBillingAccount(ctx, db, w, webhooksClient, billingClient, valid_id, "example billing account", false, false, s.Id, exampleUuid, "4321", "Test User", time.Now().AddDate(1, 1, 0), models.BillingAccountType_CREDIT_CARD)
}

func TestMain(m *testing.M) {
	i := m.Run()
	runetestutil.KillAll()
	os.Exit(i)
}

func TestNew(t *testing.T) {
	GetTestDb(t)
	t.Run("New billingaccount", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			model, err := CreateBillingAccount(ctx, db, w, webhooksClient, billingClient, valid_id, "test billing account", false, false, subscriber.Id, exampleUuid, "4321", "Test User", time.Now().AddDate(1, 1, 0), models.BillingAccountType_GOOGLE_PAY)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Fatal("Returned model is nil")
			}

			// Make sure we were given an id
			if model.Id == "" {
				t.Error("Returned model does not have an id")
			}

			if cmp.Equal(model.CreatedAt, time.Time{}) {
				t.Error("Returned model's CreatedAt was not set")
			}

			if model.GetBillingAccountType() != models.BillingAccountType_GOOGLE_PAY {
				t.Error("Returned model's BillingAccountType was not", models.BillingAccountType_GOOGLE_PAY)
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := CreateBillingAccount(ctx, db, w, webhooksClient, billingClient, invalid_id, "test billing account", false, false, subscriber.Id, exampleUuid, "4321", "Test User", time.Now().AddDate(1, 1, 0), models.BillingAccountType_PAYPAL)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid input", func(t *testing.T) {
			_, err := CreateBillingAccount(ctx, db, w, webhooksClient, billingClient, valid_id, "test billing account", false, false, "", exampleUuid, "4321", "Test User", time.Now().AddDate(1, 1, 0), models.BillingAccountType_PAYPAL)
			if err == nil {
				t.Error("Providing an invalid input should return an error")
			}
		})
	})
}

func TestGet(t *testing.T) {
	GetTestDb(t)
	model, err := CreateBillingAccount(ctx, db, w, webhooksClient, billingClient, valid_id, "test billing account", false, false, subscriber.Id, strings.ToUpper(runetestutil.GetUUID()), "4321", "Test User", time.Now().AddDate(1, 1, 0), models.BillingAccountType_ACH_BANK_ACCOUNT)
	if err != nil {
		t.Fatal("Unable to set up test for Get")
	}

	t.Run("Get billingaccount", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := GetBillingAccount(ctx, db, w, valid_id, model.Id)
			if err != nil {
				t.Fatal(err.Error())
			}

			if m == nil {
				t.Fatal("Returned model is nil")
			}
			model.BillingAccountId = strings.ToLower(model.BillingAccountId)
			m.BillingAccountId = strings.ToLower(m.BillingAccountId)

			if cmp.Equal(model, m) != true {
				t.Fatalf("models do not match\n%s\n", cmp.Diff(model, m))
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := GetBillingAccount(ctx, db, w, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := GetBillingAccount(ctx, db, w, invalid_id, "invaliduuid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})
}

func TestDelete(t *testing.T) {
	GetTestDb(t)
	model, err := CreateBillingAccount(ctx, db, w, webhooksClient, billingClient, valid_id, "test billing account", false, false, subscriber.Id, exampleUuid, "4321", "Test User", time.Now().AddDate(1, 1, 0), models.BillingAccountType_ACH_BANK_ACCOUNT)
	if err != nil {
		t.Fatal("Unable to set up test for Delete")
	}

	t.Run("Delete billingaccount", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := DeleteBillingAccount(ctx, db, w, webhooksClient, billingClient, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Error("Returned model is nil")
			}
			if m.DeletedAt == "" {
				t.Error("Returned model's DeletedAt is nil")
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := DeleteBillingAccount(ctx, db, w, webhooksClient, billingClient, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := DeleteBillingAccount(ctx, db, w, webhooksClient, billingClient, invalid_id, "baduuid")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})
}
