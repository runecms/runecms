// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package billingaccounts

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"git.runecms.io/rune/runecms"
	billingpb "git.runecms.io/rune/runecms/billing/rpc"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/ory/ladon"
)

func getAccountId(db *sql.DB, id string) (string, error) {
	const unassigned = "unassigned"
	var accountId sql.NullString

	if err := db.QueryRow(`
		SELECT
			subscribers.account_id
		FROM
			subscribers,
			subscribers_billing_accounts as billing_accounts
		WHERE
			billing_accounts.id = $1
		AND
			subscribers.id = billing_accounts.subscriber_id
	`, id).Scan(&accountId); err != nil {
		return "", runecms.Error(runecms.Internal, err.Error())
	}

	if accountId.Valid {
		return accountId.String, nil
	}

	return unassigned, nil
}

// CreateBillingAccount will create a new BillingAccount given the BillingAccounts interface and database connection
func CreateBillingAccount(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	billingClient billingpb.BillingAccounts,
	ident runecms.Identity,
	name string,
	isDefault bool,
	isExternal bool,
	subscriberId string,
	token string,
	lastFour string,
	nameOnAccount string,
	expirationDate time.Time,
	accountType models.BillingAccountType,
) (*models.BillingAccount, error) {
	if err := runecms.Access(ident, w, "create", "subscribers:billing_accounts"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if subscriberId == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "subscriberId can not be empty")
	}

	model := &models.BillingAccount{
		Name:               name,
		IsDefault:          isDefault,
		IsExternal:         isExternal,
		SubscriberId:       subscriberId,
		BillingAccountType: accountType,
	}

	if isExternal != true {
		res, err := billingClient.Create(ident.ToContext(), &billingpb.CreateBillingAccountRequest{
			LastFour:       lastFour,
			NameOnCard:     nameOnAccount,
			Token:          token,
			ExpirationDate: expirationDate.Format(time.RFC3339),
		})

		if err != nil {
			return nil, runecms.Error(runecms.Internal, err.Error())
		}
		model.BillingAccountId = res.GetBillingAccount().GetId()
	}

	row := db.QueryRowContext(
		ctx,
		SqlCreateBillingAccount,
		name,
		subscriberId,
		model.BillingAccountId,
		isDefault,
		isExternal,
		accountType,
	)
	if err := row.Scan(
		&model.Id,
		&model.CreatedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:billing_accounts", "create", model)
	return model, nil
}

func DeleteBillingAccount(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	billingClient billingpb.BillingAccounts,
	ident runecms.Identity,
	id string,
) (*models.BillingAccount, error) {
	accountId, err := getAccountId(db, id)
	if err != nil {
		return nil, err
	}

	resource := fmt.Sprintf("subscribers:%s:billing_accounts:%s", accountId, id)

	if err := runecms.Access(ident, w, "delete", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &models.BillingAccount{
		Id: id,
	}

	row := db.QueryRowContext(ctx, SqlDeleteBillingAccount, id)

	if err := row.Scan(
		&model.Name,
		&model.SubscriberId,
		&model.BillingAccountId,
		&model.IsDefault,
		&model.IsExternal,
		&model.BillingAccountType,
		&model.CreatedAt,
		&model.DeletedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	_, err = billingClient.Delete(ident.ToContext(), &billingpb.DeleteBillingAccountRequest{
		Id: model.BillingAccountId,
	})

	if err != nil {
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:billing_accounts", "delete", model)
	return model, nil
}

// GetBillingAccount will retrieve a billing account given the database connection
func GetBillingAccount(ctx context.Context, db *sql.DB, w *ladon.Ladon, ident runecms.Identity, id string) (*models.BillingAccount, error) {
	accountId, err := getAccountId(db, id)
	if err != nil {
		return nil, err
	}

	resource := fmt.Sprintf("subscribers:%s:billing_accounts:%s", accountId, id)
	if err := runecms.Access(ident, w, "read", resource); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	model := &models.BillingAccount{
		Id: id,
	}

	row := db.QueryRowContext(ctx, SqlGetBillingAccount, id)

	if err := row.Scan(
		&model.Name,
		&model.SubscriberId,
		&model.BillingAccountId,
		&model.IsDefault,
		&model.IsExternal,
		&model.BillingAccountType,
		&model.CreatedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}
	return model, nil
}

// CreateBillingAccountFromModel will accept a model from the protobuf and create a new BillingAccount from that
func CreateBillingAccountFromModel(
	ctx context.Context,
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	billingClient billingpb.BillingAccounts,
	ident runecms.Identity,
	model *models.BillingAccount,
	token string,
	lastFour string,
	nameOnCard string,
	expirationDate time.Time,
) error {
	if err := runecms.Access(ident, w, "create", "subscribers:billing_accounts"); err != nil {
		return runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if model == nil {
		return runecms.Error(runecms.InvalidArgument, "The billing account must not be nil")
	}

	// Check that the billingaccount is actually new
	if model.Id != "" {
		return runecms.Error(runecms.InvalidArgument, "The Id of the billingaccount passed to Create(...) is not empty.  It's likely already in the database")
	}

	if model.SubscriberId == "" {
		return runecms.Error(runecms.InvalidArgument, "A SubscriberId must be provided to BillingAccount")
	}

	res, err := billingClient.Create(ident.ToContext(), &billingpb.CreateBillingAccountRequest{
		LastFour:       lastFour,
		NameOnCard:     nameOnCard,
		Token:          token,
		ExpirationDate: expirationDate.Format(time.RFC3339),
	})
	if err != nil {
		return runecms.Error(runecms.Internal, err.Error())
	}
	model.BillingAccountId = res.GetBillingAccount().GetId()

	row := db.QueryRowContext(
		ctx,
		SqlCreateBillingAccount,
		model.Name,
		model.SubscriberId,
		model.BillingAccountId,
		model.IsDefault,
		model.IsExternal,
		model.BillingAccountType,
	)
	if err := row.Scan(
		&model.Id,
		&model.CreatedAt,
	); err != nil {
		if err == sql.ErrNoRows {
			return runecms.Error(runecms.NotFound, err.Error())
		}
		return runecms.Error(runecms.Internal, err.Error())
	}

	go webhooks.Distribute(webhooksClient, ident, "subscribers:billing_accounts", "create", model)
	return nil
}
