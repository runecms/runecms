// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package billingaccounts

var (
	SqlGetBillingAccount = `
	SELECT
		name,
		subscriber_id,
		billing_account_id,
		is_default,
		is_external,
		billing_account_type,
		created_at
	FROM
		subscribers_billing_accounts
	WHERE
		id = $1
	AND
		deleted_at IS NULL
	`

	SqlCreateBillingAccount = `
	INSERT INTO
		subscribers_billing_accounts (
			name,
			subscriber_id,
			billing_account_id,
			is_default,
			is_external,
			billing_account_type
		)
	VALUES (
		$1,
		$2,
		$3,
		$4,
		$5,
		$6
	)
	RETURNING id, created_at
	`

	SqlDeleteBillingAccount = `
	UPDATE
		subscribers_billing_accounts
	SET
		deleted_at = current_timestamp
	WHERE
		id = $1
	AND
		deleted_at IS NULL
	RETURNING
		name,
		subscriber_id,
		billing_account_id,
		is_default,
		is_external,
		billing_account_type,
		created_at,
		deleted_at
	`
)
