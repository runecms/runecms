// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptionholdtypes"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreateSubscriptionHoldType(c context.Context, req *pb.CreateSubscriptionHoldTypeRequest) (*pb.CreateSubscriptionHoldTypeResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptionholdtypes.CreateSubscriptionHoldType(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetName(), req.GetDescription())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.CreateSubscriptionHoldTypeResponse{
		SubscriptionHoldType: model,
	}, nil
}

func (s *Server) DeleteSubscriptionHoldType(c context.Context, req *pb.DeleteSubscriptionHoldTypeRequest) (*pb.DeleteSubscriptionHoldTypeResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptionholdtypes.DeleteSubscriptionHoldType(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.DeleteSubscriptionHoldTypeResponse{
		SubscriptionHoldType: model,
	}, nil
}

func (s *Server) GetSubscriptionHoldType(c context.Context, req *pb.GetSubscriptionHoldTypeRequest) (*pb.GetSubscriptionHoldTypeResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptionholdtypes.GetSubscriptionHoldType(c, s.Db, s.Warden, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetSubscriptionHoldTypeResponse{
		SubscriptionHoldType: model,
	}, nil
}

func (s *Server) GetSubscriptionHoldTypeByName(c context.Context, req *pb.GetSubscriptionHoldTypeByNameRequest) (*pb.GetSubscriptionHoldTypeByNameResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptionholdtypes.GetSubscriptionHoldTypeByName(c, s.Db, s.Warden, user, req.GetName())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetSubscriptionHoldTypeByNameResponse{
		SubscriptionHoldType: model,
	}, nil
}
