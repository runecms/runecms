// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/complainttypes"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreateComplaintType(c context.Context, req *pb.CreateComplaintTypeRequest) (*pb.CreateComplaintTypeResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := complainttypes.CreateComplaintType(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetName(), req.GetDescription())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.CreateComplaintTypeResponse{
		ComplaintType: model,
	}, nil
}

func (s *Server) DeleteComplaintType(c context.Context, req *pb.DeleteComplaintTypeRequest) (*pb.DeleteComplaintTypeResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := complainttypes.DeleteComplaintType(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.DeleteComplaintTypeResponse{
		ComplaintType: model,
	}, nil
}

func (s *Server) GetComplaintType(c context.Context, req *pb.GetComplaintTypeRequest) (*pb.GetComplaintTypeResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := complainttypes.GetComplaintType(c, s.Db, s.Warden, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetComplaintTypeResponse{
		ComplaintType: model,
	}, nil
}

func (s *Server) GetComplaintTypeByName(c context.Context, req *pb.GetComplaintTypeByNameRequest) (*pb.GetComplaintTypeByNameResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := complainttypes.GetComplaintTypeByName(c, s.Db, s.Warden, user, req.GetName())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetComplaintTypeByNameResponse{
		ComplaintType: model,
	}, nil
}
