// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/complaints"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreateComplaint(c context.Context, req *pb.CreateComplaintRequest) (*pb.CreateComplaintResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := complaints.CreateComplaint(c, s.Db, s.Warden, s.WebhooksClient, user,
		req.GetComplaint(),
		req.GetComplaintTypeId(),
		req.GetComplaintStatusId(),
		req.GetSubscriptionId(),
	)

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.CreateComplaintResponse{
		Complaint: model,
	}, nil
}

func (s *Server) DeleteComplaint(c context.Context, req *pb.DeleteComplaintRequest) (*pb.DeleteComplaintResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := complaints.DeleteComplaint(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.DeleteComplaintResponse{
		Complaint: model,
	}, nil
}

func (s *Server) GetComplaint(c context.Context, req *pb.GetComplaintRequest) (*pb.GetComplaintResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := complaints.GetComplaint(c, s.Db, s.Warden, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetComplaintResponse{
		Complaint: model,
	}, nil
}

func (s *Server) AcknowledgeComplaint(c context.Context, req *pb.AcknowledgeComplaintRequest) (*pb.AcknowledgeComplaintResponse, error) {
	return nil, nil
}

func (s *Server) AddNote(c context.Context, req *pb.CreateNoteRequest) (*pb.CreateNoteResponse, error) {
	return nil, nil
}

func (s *Server) UpdateComplaint(c context.Context, req *pb.UpdateComplaintRequest) (*pb.UpdateComplaintResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}
	model, err := complaints.UpdateComplaintFromModel(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetComplaint())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.UpdateComplaintResponse{
		Complaint: model,
	}, nil
}
