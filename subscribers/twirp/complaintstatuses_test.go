// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"
	"testing"

	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/complaintstatuses"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"
)

func get_complaintstatus(s *Server) (*pb.ComplaintStatus, error) {
	return complaintstatuses.CreateComplaintStatus(context.Background(), s.Db, s.Warden, s.WebhooksClient, valid_id, runetestutil.GetUUID(), "test grpc")
}

func TestCreateComplaintStatus(t *testing.T) {
	GetServer(t)
	t.Run("Server.New", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.CreateComplaintStatus(
				valid_context,
				&pb.CreateComplaintStatusRequest{
					Name:        runetestutil.GetUUID(),
					Description: "test grpc new",
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.CreateComplaintStatus(
				invalid_context,
				&pb.CreateComplaintStatusRequest{
					Name:        runetestutil.GetUUID(),
					Description: "test grpc new",
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestGetComplaintStatus(t *testing.T) {
	GetServer(t)
	m, err := get_complaintstatus(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Get", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.GetComplaintStatus(
				valid_context,
				&pb.GetComplaintStatusRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.GetComplaintStatus(
				invalid_context,
				&pb.GetComplaintStatusRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestDeleteComplaintStatus(t *testing.T) {
	m, err := get_complaintstatus(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Delete", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.DeleteComplaintStatus(
				valid_context,
				&pb.DeleteComplaintStatusRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.DeleteComplaintStatus(
				invalid_context,
				&pb.DeleteComplaintStatusRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}
