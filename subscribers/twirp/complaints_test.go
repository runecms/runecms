// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"
	"testing"

	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/complaints"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"
)

func GetComplaint(s *Server) (*pb.Complaint, error) {
	return complaints.CreateComplaint(context.Background(), s.Db, s.Warden, s.WebhooksClient, valid_id,
		runetestutil.GetUUID(),
		ctype.Id,
		cstatus.Id,
		subscription.Id,
	)
}

func TestCreateComplaint(t *testing.T) {
	GetServer(t)
	t.Run("Server.Create", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.CreateComplaint(
				valid_context,
				&pb.CreateComplaintRequest{
					Complaint:         "test new complaint via. grpc",
					SubscriptionId:    subscription.Id,
					ComplaintTypeId:   ctype.Id,
					ComplaintStatusId: cstatus.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.CreateComplaint(
				invalid_context,
				&pb.CreateComplaintRequest{
					Complaint:         "Test new complaint via. grpc",
					SubscriptionId:    subscription.Id,
					ComplaintTypeId:   ctype.Id,
					ComplaintStatusId: cstatus.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestGetComplaint(t *testing.T) {
	GetServer(t)
	m, err := GetComplaint(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Get", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.GetComplaint(
				valid_context,
				&pb.GetComplaintRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.GetComplaint(
				invalid_context,
				&pb.GetComplaintRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestDeleteComplaint(t *testing.T) {
	m, err := GetComplaint(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Delete", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.DeleteComplaint(
				valid_context,
				&pb.DeleteComplaintRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.DeleteComplaint(
				invalid_context,
				&pb.DeleteComplaintRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}
