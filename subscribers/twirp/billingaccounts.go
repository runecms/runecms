// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"
	"time"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/billingaccounts"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreateBillingAccount(c context.Context, req *pb.CreateBillingAccountRequest) (*pb.CreateBillingAccountResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	exp, err := time.Parse(time.RFC3339, req.GetExpirationDate())

	if err != nil {
		return nil, twirp.NewError(twirp.InvalidArgument, err.Error())
	}

	model, err := billingaccounts.CreateBillingAccount(c, s.Db, s.Warden, s.WebhooksClient, s.BillingClient, user, req.GetName(), req.GetIsDefault(), req.GetIsExternal(), req.GetSubscriberId(), req.GetToken(), req.GetLastFour(), req.GetNameOnAccount(), exp, req.GetBillingAccountType())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.CreateBillingAccountResponse{
		BillingAccount: model,
	}, nil
}

func (s *Server) DeleteBillingAccount(c context.Context, req *pb.DeleteBillingAccountRequest) (*pb.DeleteBillingAccountResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := billingaccounts.DeleteBillingAccount(c, s.Db, s.Warden, s.WebhooksClient, s.BillingClient, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	if model.DeletedAt == "" {
		return nil, twirp.NewError(twirp.NotFound, "Returned DeletedAt is nil")
	}

	return &pb.DeleteBillingAccountResponse{
		BillingAccount: model,
	}, nil
}

func (s *Server) GetBillingAccount(c context.Context, req *pb.GetBillingAccountRequest) (*pb.GetBillingAccountResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := billingaccounts.GetBillingAccount(c, s.Db, s.Warden, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetBillingAccountResponse{
		BillingAccount: model,
	}, nil
}
