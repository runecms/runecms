// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/complaintstatuses"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreateComplaintStatus(c context.Context, req *pb.CreateComplaintStatusRequest) (*pb.CreateComplaintStatusResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := complaintstatuses.CreateComplaintStatus(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetName(), req.GetDescription())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.CreateComplaintStatusResponse{
		ComplaintStatus: model,
	}, nil
}

func (s *Server) DeleteComplaintStatus(c context.Context, req *pb.DeleteComplaintStatusRequest) (*pb.DeleteComplaintStatusResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := complaintstatuses.DeleteComplaintStatus(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.DeleteComplaintStatusResponse{
		ComplaintStatus: model,
	}, nil
}

func (s *Server) GetComplaintStatus(c context.Context, req *pb.GetComplaintStatusRequest) (*pb.GetComplaintStatusResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := complaintstatuses.GetComplaintStatus(c, s.Db, s.Warden, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetComplaintStatusResponse{
		ComplaintStatus: model,
	}, nil
}

func (s *Server) GetComplaintStatusByName(c context.Context, req *pb.GetComplaintStatusByNameRequest) (*pb.GetComplaintStatusByNameResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := complaintstatuses.GetComplaintStatusByName(c, s.Db, s.Warden, user, req.GetName())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetComplaintStatusByNameResponse{
		ComplaintStatus: model,
	}, nil
}
