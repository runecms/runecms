// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"
	"testing"

	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/markets"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"
)

func get_market(s *Server) (*pb.Market, error) {
	return markets.CreateMarket(context.Background(), s.Db, s.Warden, s.WebhooksClient, valid_id, runetestutil.GetUUID(), "test grpc")
}

func TestCreateMarket(t *testing.T) {
	GetServer(t)
	t.Run("Server.Create", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.CreateMarket(
				valid_context,
				&pb.CreateMarketRequest{
					Name:        runetestutil.GetUUID(),
					Description: "test grpc new",
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.CreateMarket(
				invalid_context,
				&pb.CreateMarketRequest{
					Name:        runetestutil.GetUUID(),
					Description: "test grpc new",
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestGetMarket(t *testing.T) {
	GetServer(t)
	m, err := get_market(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Get", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.GetMarket(
				valid_context,
				&pb.GetMarketRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.GetMarket(
				invalid_context,
				&pb.GetMarketRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestDeleteMarket(t *testing.T) {
	GetServer(t)
	m, err := get_market(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Delete", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.DeleteMarket(
				valid_context,
				&pb.DeleteMarketRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.DeleteMarket(
				invalid_context,
				&pb.DeleteMarketRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}
