// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"
	"testing"
	"time"

	"git.runecms.io/rune/runecms/subscribers/internal/subscriptions"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"
)

func get_subscription(s *Server) (*pb.Subscription, error) {
	return subscriptions.CreateSubscription(
		context.Background(),
		s.Db,
		s.Warden,
		s.WebhooksClient,
		valid_id,
		true,
		1000,
		time.Now(),
		time.Now(),
		sstatus.Id,
		stype.Id,
		subscriber.Id,
		billing.Id,
		nil,
	)
}

func TestCreateSubscription(t *testing.T) {
	GetServer(t)
	t.Run("Server.Create", func(t *testing.T) {
		t.Run("Valid request (without DeliveryAddress)", func(t *testing.T) {
			res, err := s.CreateSubscription(
				valid_context,
				&pb.CreateSubscriptionRequest{
					Active:               true,
					Rate:                 1000,
					StartAt:              time.Now().Format(time.RFC3339),
					EndAt:                time.Now().AddDate(1, 0, 0).Format(time.RFC3339),
					SubscriptionStatusId: sstatus.Id,
					SubscriptionTypeId:   stype.Id,
					BillingAccountId:     billing.Id,
					SubscriberId:         subscriber.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.CreateSubscription(
				invalid_context,
				&pb.CreateSubscriptionRequest{
					Active:               true,
					Rate:                 1000,
					StartAt:              time.Now().Format(time.RFC3339),
					EndAt:                time.Now().AddDate(1, 0, 0).Format(time.RFC3339),
					SubscriptionStatusId: sstatus.Id,
					SubscriptionTypeId:   stype.Id,
					BillingAccountId:     billing.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestGetSubscription(t *testing.T) {
	GetServer(t)
	m, err := get_subscription(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Get", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.GetSubscription(
				valid_context,
				&pb.GetSubscriptionRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.GetSubscription(
				invalid_context,
				&pb.GetSubscriptionRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestDeleteSubscription(t *testing.T) {
	GetServer(t)
	m, err := get_subscription(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Delete", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.DeleteSubscription(
				valid_context,
				&pb.DeleteSubscriptionRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.DeleteSubscription(
				invalid_context,
				&pb.DeleteSubscriptionRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}
