// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"
	"testing"

	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptiontypes"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"
)

func get_subscriptiontype(s *Server) (*pb.SubscriptionType, error) {
	return subscriptiontypes.CreateSubscriptionType(context.Background(), s.Db, s.Warden, s.WebhooksClient, valid_id, runetestutil.GetUUID(), "test grpc", product.Id)
}

func TestCreateSubscriptionType(t *testing.T) {
	GetServer(t)
	t.Run("Server.Create", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.CreateSubscriptionType(
				valid_context,
				&pb.CreateSubscriptionTypeRequest{
					Name:        runetestutil.GetUUID(),
					Description: "test grpc new",
					ProductId:   product.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.CreateSubscriptionType(
				invalid_context,
				&pb.CreateSubscriptionTypeRequest{
					Name:        runetestutil.GetUUID(),
					Description: "test grpc new",
					ProductId:   product.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestGetSubscriptionType(t *testing.T) {
	GetServer(t)
	m, err := get_subscriptiontype(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Get", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.GetSubscriptionType(
				valid_context,
				&pb.GetSubscriptionTypeRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.GetSubscriptionType(
				invalid_context,
				&pb.GetSubscriptionTypeRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestDeleteSubscriptionType(t *testing.T) {
	m, err := get_subscriptiontype(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Delete", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.DeleteSubscriptionType(
				valid_context,
				&pb.DeleteSubscriptionTypeRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.DeleteSubscriptionType(
				invalid_context,
				&pb.DeleteSubscriptionTypeRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}
