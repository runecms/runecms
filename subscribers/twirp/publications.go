// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/publications"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreatePublication(c context.Context, req *pb.CreatePublicationRequest) (*pb.CreatePublicationResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := publications.CreatePublication(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetName(), req.GetDescription(), req.GetMarketId(), req.GetBillingProcessorData())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.CreatePublicationResponse{
		Publication: model,
	}, nil
}

func (s *Server) DeletePublication(c context.Context, req *pb.DeletePublicationRequest) (*pb.DeletePublicationResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := publications.DeletePublication(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.DeletePublicationResponse{
		Publication: model,
	}, nil
}

func (s *Server) GetPublicationByName(c context.Context, req *pb.GetPublicationByNameRequest) (*pb.GetPublicationByNameResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := publications.GetPublicationByName(c, s.Db, s.Warden, user, req.GetName())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetPublicationByNameResponse{
		Publication: model,
	}, nil
}

func (s *Server) GetPublication(c context.Context, req *pb.GetPublicationRequest) (*pb.GetPublicationResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := publications.GetPublication(c, s.Db, s.Warden, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetPublicationResponse{
		Publication: model,
	}, nil
}
