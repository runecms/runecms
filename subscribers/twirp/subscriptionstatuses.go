// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptionstatuses"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreateSubscriptionStatus(c context.Context, req *pb.CreateSubscriptionStatusRequest) (*pb.CreateSubscriptionStatusResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptionstatuses.CreateSubscriptionStatus(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetName(), req.GetDescription())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}
	return &pb.CreateSubscriptionStatusResponse{
		SubscriptionStatus: model,
	}, nil
}

func (s *Server) DeleteSubscriptionStatus(c context.Context, req *pb.DeleteSubscriptionStatusRequest) (*pb.DeleteSubscriptionStatusResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptionstatuses.DeleteSubscriptionStatus(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.DeleteSubscriptionStatusResponse{
		SubscriptionStatus: model,
	}, nil
}

func (s *Server) GetSubscriptionStatus(c context.Context, req *pb.GetSubscriptionStatusRequest) (*pb.GetSubscriptionStatusResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptionstatuses.GetSubscriptionStatus(c, s.Db, s.Warden, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetSubscriptionStatusResponse{
		SubscriptionStatus: model,
	}, nil
}

func (s *Server) GetSubscriptionStatusByName(c context.Context, req *pb.GetSubscriptionStatusByNameRequest) (*pb.GetSubscriptionStatusByNameResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptionstatuses.GetSubscriptionStatusByName(c, s.Db, s.Warden, user, req.GetName())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetSubscriptionStatusByNameResponse{
		SubscriptionStatus: model,
	}, nil
}
