// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptionholds"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreateSubscriptionHold(c context.Context, req *pb.CreateSubscriptionHoldRequest) (*pb.CreateSubscriptionHoldResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptionholds.CreateSubscriptionHold(c, s.Db, s.Warden, s.WebhooksClient, user,
		req.GetSubscriptionId(),
		req.GetSubscriptionHoldTypeId(),
		req.GetStartAt(),
		req.GetEndAt(),
	)

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.CreateSubscriptionHoldResponse{
		SubscriptionHold: model,
	}, nil
}

func (s *Server) DeleteSubscriptionHold(c context.Context, req *pb.DeleteSubscriptionHoldRequest) (*pb.DeleteSubscriptionHoldResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptionholds.DeleteSubscriptionHold(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.DeleteSubscriptionHoldResponse{
		SubscriptionHold: model,
	}, nil
}

func (s *Server) GetSubscriptionHold(c context.Context, req *pb.GetSubscriptionHoldRequest) (*pb.GetSubscriptionHoldResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptionholds.GetSubscriptionHold(c, s.Db, s.Warden, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetSubscriptionHoldResponse{
		SubscriptionHold: model,
	}, nil
}

func (s *Server) UpdateSubscriptionHold(c context.Context, req *pb.UpdateSubscriptionHoldRequest) (*pb.UpdateSubscriptionHoldResponse, error) {
	return nil, nil
}
