// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptiontypes"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreateSubscriptionType(c context.Context, req *pb.CreateSubscriptionTypeRequest) (*pb.CreateSubscriptionTypeResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptiontypes.CreateSubscriptionType(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetName(), req.GetDescription(), req.GetProductId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.CreateSubscriptionTypeResponse{
		SubscriptionType: model,
	}, nil
}

func (s *Server) DeleteSubscriptionType(c context.Context, req *pb.DeleteSubscriptionTypeRequest) (*pb.DeleteSubscriptionTypeResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptiontypes.DeleteSubscriptionType(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	if model.DeletedAt == "" {
		return nil, twirp.NewError(twirp.NotFound, "Returned DeletedAt is nil")
	}

	return &pb.DeleteSubscriptionTypeResponse{
		SubscriptionType: model,
	}, nil
}

func (s *Server) GetSubscriptionType(c context.Context, req *pb.GetSubscriptionTypeRequest) (*pb.GetSubscriptionTypeResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptiontypes.GetSubscriptionType(c, s.Db, s.Warden, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetSubscriptionTypeResponse{
		SubscriptionType: model,
	}, nil
}

func (s *Server) GetSubscriptionTypeByName(c context.Context, req *pb.GetSubscriptionTypeByNameRequest) (*pb.GetSubscriptionTypeByNameResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptiontypes.GetSubscriptionTypeByName(c, s.Db, s.Warden, user, req.GetName())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetSubscriptionTypeByNameResponse{
		SubscriptionType: model,
	}, nil
}
