// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"
	"testing"

	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/complainttypes"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"
)

func get_complainttype(s *Server) (*pb.ComplaintType, error) {
	return complainttypes.CreateComplaintType(context.Background(), s.Db, s.Warden, s.WebhooksClient, valid_id, runetestutil.GetUUID(), "test grpc")
}

func TestComplaint(t *testing.T) {
	GetServer(t)
	t.Run("Server.New", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.CreateComplaintType(
				valid_context,
				&pb.CreateComplaintTypeRequest{
					Name:        runetestutil.GetUUID(),
					Description: "test grpc new",
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.CreateComplaintType(
				invalid_context,
				&pb.CreateComplaintTypeRequest{
					Name:        runetestutil.GetUUID(),
					Description: "test grpc new",
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestGetComplaintType(t *testing.T) {
	GetServer(t)
	m, err := get_complainttype(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Get", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.GetComplaintType(
				valid_context,
				&pb.GetComplaintTypeRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.GetComplaintType(
				invalid_context,
				&pb.GetComplaintTypeRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestDeleteComplaintType(t *testing.T) {
	m, err := get_complainttype(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Delete", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.DeleteComplaintType(
				valid_context,
				&pb.DeleteComplaintTypeRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.DeleteComplaintType(
				invalid_context,
				&pb.DeleteComplaintTypeRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}
