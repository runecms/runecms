// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"
	"testing"

	"git.runecms.io/rune/runecms/subscribers/internal/subscriptionholds"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"
)

func get_subscriptionhold(s *Server) (*pb.SubscriptionHold, error) {
	return subscriptionholds.CreateSubscriptionHold(context.Background(), s.Db, s.Warden, s.WebhooksClient, valid_id,
		subscription.Id,
		shtype.Id,
		nowstr,
		laterstr,
	)
}

func TestCreateSubscriptionHold(t *testing.T) {
	GetServer(t)
	t.Run("Server.Create", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.CreateSubscriptionHold(
				valid_context,
				&pb.CreateSubscriptionHoldRequest{
					SubscriptionId:         subscription.Id,
					SubscriptionHoldTypeId: shtype.Id,
					StartAt:                nowstr,
					EndAt:                  laterstr,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.CreateSubscriptionHold(
				invalid_context,
				&pb.CreateSubscriptionHoldRequest{
					SubscriptionId:         subscription.Id,
					SubscriptionHoldTypeId: shtype.Id,
					StartAt:                nowstr,
					EndAt:                  laterstr,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestGetSubscriptionHold(t *testing.T) {
	GetServer(t)
	m, err := get_subscriptionhold(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Get", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.GetSubscriptionHold(
				valid_context,
				&pb.GetSubscriptionHoldRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.GetSubscriptionHold(
				invalid_context,
				&pb.GetSubscriptionHoldRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestDeleteSubscriptionHold(t *testing.T) {
	m, err := get_subscriptionhold(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Delete", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.DeleteSubscriptionHold(
				valid_context,
				&pb.DeleteSubscriptionHoldRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.DeleteSubscriptionHold(
				invalid_context,
				&pb.DeleteSubscriptionHoldRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}
