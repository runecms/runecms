// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/deliveryaddresses"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreateDeliveryAddress(c context.Context, req *pb.CreateDeliveryAddressRequest) (*pb.CreateDeliveryAddressResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := deliveryaddresses.CreateDeliveryAddress(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetSubscriberId(), req.GetAddressId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.CreateDeliveryAddressResponse{
		DeliveryAddress: model,
	}, nil
}

func (s *Server) DeleteDeliveryAddress(c context.Context, req *pb.DeleteDeliveryAddressRequest) (*pb.DeleteDeliveryAddressResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := deliveryaddresses.DeleteDeliveryAddress(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.DeleteDeliveryAddressResponse{
		DeliveryAddress: model,
	}, nil
}

func (s *Server) GetDeliveryAddress(c context.Context, req *pb.GetDeliveryAddressRequest) (*pb.GetDeliveryAddressResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := deliveryaddresses.GetDeliveryAddress(c, s.Db, s.Warden, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetDeliveryAddressResponse{
		DeliveryAddress: model,
	}, nil
}
