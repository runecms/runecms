// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Package twirpserver contains all of the twirp endpoints for the Subscribers services
package twirpserver

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"github.com/ory/ladon"

	"git.runecms.io/rune/runecms/billing/rpc"
	"git.runecms.io/rune/runecms/internal/twirputil"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks/rpc"
)

type Server struct {
	Db             *sql.DB
	Warden         *ladon.Ladon
	BillingClient  billingpb.BillingAccounts
	WebhooksClient webhookspb.Webhooks
}

func NewServer(db *sql.DB, w *ladon.Ladon, b billingpb.BillingAccounts, webhooks webhookspb.Webhooks) *Server {
	return &Server{
		Db:             db,
		Warden:         w,
		WebhooksClient: webhooks,
		BillingClient:  b,
	}
}

func (s *Server) Start(port int) error {
	log.Printf("Starting the subscriber service on 0.0.0.0:%d\n", port)
	var (
		addressesHandler             = pb.NewAddressesServer(s, nil)
		billingaccountsHandler       = pb.NewBillingAccountsServer(s, nil)
		complaintsHandler            = pb.NewComplaintsServer(s, nil)
		complainttypesHandler        = pb.NewComplaintTypesServer(s, nil)
		deliveryaddressesHandler     = pb.NewDeliveryAddressesServer(s, nil)
		marketsHandler               = pb.NewMarketsServer(s, nil)
		productsHandler              = pb.NewProductsServer(s, nil)
		producttypesHandler          = pb.NewProductTypesServer(s, nil)
		publicationsHandler          = pb.NewPublicationsServer(s, nil)
		subscribersHandler           = pb.NewSubscribersServer(s, nil)
		subscriptionholdsHandler     = pb.NewSubscriptionHoldsServer(s, nil)
		subscriptionholdtypesHandler = pb.NewSubscriptionHoldTypesServer(s, nil)
		subscriptionsHandler         = pb.NewSubscriptionsServer(s, nil)
		subscriptionstatusesHandler  = pb.NewSubscriptionStatusesServer(s, nil)
		subscriptiontypesHandler     = pb.NewSubscriptionTypesServer(s, nil)
	)

	r := http.NewServeMux()
	r.Handle(pb.AddressesPathPrefix, twirputil.AddMiddleware(addressesHandler))
	r.Handle(pb.BillingAccountsPathPrefix, twirputil.AddMiddleware(billingaccountsHandler))
	r.Handle(pb.ComplaintsPathPrefix, twirputil.AddMiddleware(complaintsHandler))
	r.Handle(pb.ComplaintTypesPathPrefix, twirputil.AddMiddleware(complainttypesHandler))
	r.Handle(pb.DeliveryAddressesPathPrefix, twirputil.AddMiddleware(deliveryaddressesHandler))
	r.Handle(pb.MarketsPathPrefix, twirputil.AddMiddleware(marketsHandler))
	r.Handle(pb.ProductsPathPrefix, twirputil.AddMiddleware(productsHandler))
	r.Handle(pb.ProductTypesPathPrefix, twirputil.AddMiddleware(producttypesHandler))
	r.Handle(pb.PublicationsPathPrefix, twirputil.AddMiddleware(publicationsHandler))
	r.Handle(pb.SubscribersPathPrefix, twirputil.AddMiddleware(subscribersHandler))
	r.Handle(pb.SubscriptionHoldsPathPrefix, twirputil.AddMiddleware(subscriptionholdsHandler))
	r.Handle(pb.SubscriptionHoldTypesPathPrefix, twirputil.AddMiddleware(subscriptionholdtypesHandler))
	r.Handle(pb.SubscriptionsPathPrefix, twirputil.AddMiddleware(subscriptionsHandler))
	r.Handle(pb.SubscriptionStatusesPathPrefix, twirputil.AddMiddleware(subscriptionstatusesHandler))
	r.Handle(pb.SubscriptionTypesPathPrefix, twirputil.AddMiddleware(subscriptiontypesHandler))
	return http.ListenAndServe(fmt.Sprintf(":%d", port), r)
}
