// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"
	"testing"

	"git.runecms.io/rune/runecms/subscribers/internal/deliveryaddresses"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"
)

func GetDeliveryAddress(s *Server) (*pb.DeliveryAddress, error) {
	return deliveryaddresses.CreateDeliveryAddress(context.Background(), s.Db, s.Warden, s.WebhooksClient, valid_id, subscriber.Id, address.Id)
}

func TestCreateDeliveryAddress(t *testing.T) {
	GetServer(t)
	t.Run("Server.New", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.CreateDeliveryAddress(
				valid_context,
				&pb.CreateDeliveryAddressRequest{
					SubscriberId: subscriber.Id,
					AddressId:    address.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.CreateDeliveryAddress(
				invalid_context,
				&pb.CreateDeliveryAddressRequest{
					SubscriberId: subscriber.Id,
					AddressId:    address.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestGetDeliveryAddress(t *testing.T) {
	GetServer(t)
	m, err := GetDeliveryAddress(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Get", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.GetDeliveryAddress(
				valid_context,
				&pb.GetDeliveryAddressRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.GetDeliveryAddress(
				invalid_context,
				&pb.GetDeliveryAddressRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestDeleteDeliveryAddress(t *testing.T) {
	m, err := GetDeliveryAddress(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Delete", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.DeleteDeliveryAddress(
				valid_context,
				&pb.DeleteDeliveryAddressRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.DeleteDeliveryAddress(
				invalid_context,
				&pb.DeleteDeliveryAddressRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}
