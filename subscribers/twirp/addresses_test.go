// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"
	"testing"

	"git.runecms.io/rune/runecms/subscribers/internal/addresses"
	models "git.runecms.io/rune/runecms/subscribers/rpc"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"
)

func get_address(s *Server) (*models.Address, error) {
	return addresses.CreateAddress(context.Background(), s.Db, s.Warden, s.WebhooksClient, valid_id, "123 Test Address", "test", "ar", "00000", "usa")
}

func TestNew(t *testing.T) {
	GetServer(t)
	t.Run("Server.New", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.CreateAddress(
				valid_context,
				&pb.CreateAddressRequest{
					Address: "123 Valid Street",
					State:   "ar",
					City:    "bentonville",
					Zip:     "72712",
					Country: "usa",
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.CreateAddress(
				invalid_context,
				&pb.CreateAddressRequest{
					Address: "123 Valid Street",
					State:   "ar",
					City:    "bentonville",
					Zip:     "72712",
					Country: "usa",
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestGetAddressById(t *testing.T) {
	GetServer(t)
	m, err := get_address(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Get", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.GetAddressById(
				valid_context,
				&pb.GetAddressByIdRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.GetAddressById(
				invalid_context,
				&pb.GetAddressByIdRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestDeleteAddress(t *testing.T) {
	m, err := get_address(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Delete", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.DeleteAddress(
				valid_context,
				&pb.DeleteAddressRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.DeleteAddress(
				invalid_context,
				&pb.DeleteAddressRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}
