// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"
	"os"
	"testing"
	"time"

	"git.runecms.io/rune/runecms"
	runebilling "git.runecms.io/rune/runecms/billing"
	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/addresses"
	"git.runecms.io/rune/runecms/subscribers/internal/billingaccounts"
	"git.runecms.io/rune/runecms/subscribers/internal/complaintstatuses"
	"git.runecms.io/rune/runecms/subscribers/internal/complainttypes"
	"git.runecms.io/rune/runecms/subscribers/internal/markets"
	"git.runecms.io/rune/runecms/subscribers/internal/products"
	"git.runecms.io/rune/runecms/subscribers/internal/producttypes"
	"git.runecms.io/rune/runecms/subscribers/internal/publications"
	"git.runecms.io/rune/runecms/subscribers/internal/subscribers"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptionholdtypes"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptions"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptionstatuses"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptiontypes"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"
	"git.runecms.io/rune/runecms/webhooks"
)

var (
	s *Server

	valid_context   context.Context
	invalid_context context.Context
	valid_id        runecms.Identity

	address      *pb.Address
	billing      *pb.BillingAccount
	ctype        *pb.ComplaintType
	cstatus      *pb.ComplaintStatus
	producttype  *pb.ProductType
	product      *pb.Product
	market       *pb.Market
	publication  *pb.Publication
	subscriber   *pb.Subscriber
	sstatus      *pb.SubscriptionStatus
	shtype       *pb.SubscriptionHoldType
	stype        *pb.SubscriptionType
	subscription *pb.Subscription

	now         = time.Now().UTC().Round(time.Second)
	later       = time.Now().Add(24 * time.Hour * 30).UTC().Round(time.Second)
	nowstr      = now.Format(time.RFC3339)
	laterstr    = later.Format(time.RFC3339)
	exampleUuid = "63616665-6630-3064-6465-616462656582"
)

func TestMain(m *testing.M) {
	i := m.Run()
	runetestutil.KillAll()
	os.Exit(i)
}

func GetServer(t *testing.T) {
	if s != nil {
		return
	}
	db := runetestutil.GetTestDb(t)
	warden := runetestutil.GetTestLadon()

	s = &Server{
		Db:             db,
		Warden:         warden,
		BillingClient:  runebilling.NewTestClient(t),
		WebhooksClient: &webhooks.TestClient{},
	}

	valid_id = runecms.Be("valid", "admin")
	invalid_id := runecms.Be("invalid", "")
	valid_context = valid_id.ToTwirpContext()
	invalid_context = invalid_id.ToTwirpContext()
	ctx := context.Background()

	market, _ = markets.CreateMarket(ctx, s.Db, s.Warden, s.WebhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint grpc")
	publication, _ = publications.CreatePublication(ctx, s.Db, s.Warden, s.WebhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint grpc", market.Id, "{}")
	sstatus, _ = subscriptionstatuses.CreateSubscriptionStatus(ctx, s.Db, s.Warden, s.WebhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint grpc")
	shtype, _ = subscriptionholdtypes.CreateSubscriptionHoldType(ctx, s.Db, s.Warden, s.WebhooksClient, valid_id, runetestutil.GetUUID(), "test new subscriptionhold grpc")
	producttype, _ = producttypes.CreateProductType(ctx, s.Db, s.Warden, s.WebhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint")
	product, _ = products.CreateProduct(ctx, s.Db, s.Warden, s.WebhooksClient, valid_id, runetestutil.GetUUID(), "test product", 1000, producttype.Id, publication.Id)
	stype, _ = subscriptiontypes.CreateSubscriptionType(ctx, s.Db, s.Warden, s.WebhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint", product.Id)
	ctype, _ = complainttypes.CreateComplaintType(ctx, s.Db, s.Warden, s.WebhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint grpc")
	cstatus, _ = complaintstatuses.CreateComplaintStatus(ctx, s.Db, s.Warden, s.WebhooksClient, valid_id, runetestutil.GetUUID(), "test new complaint grpc")
	subscriber, _ = subscribers.CreateSubscriber(ctx, s.Db, s.Warden, s.WebhooksClient, valid_id, runetestutil.GetUUID())
	address, _ = addresses.CreateAddress(ctx, s.Db, s.Warden, s.WebhooksClient, valid_id, "test new complaint grpc", "ar", "test", "00000", "usa")
	billing, _ = billingaccounts.CreateBillingAccount(ctx, s.Db, s.Warden, s.WebhooksClient, s.BillingClient, valid_id, "new complaint", false, false, subscriber.Id, runetestutil.GetUUID(), "4321", "Test User", time.Now().AddDate(1, 1, 0), pb.BillingAccountType_GOOGLE_PAY)
	subscription, _ = subscriptions.CreateSubscription(ctx, s.Db, s.Warden, s.WebhooksClient, valid_id,
		true,
		1000,
		time.Now(),
		time.Now().AddDate(1, 0, 0),
		sstatus.Id,
		stype.Id,
		subscriber.Id,
		billing.Id,
		nil,
	)
}
