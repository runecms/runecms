// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/markets"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreateMarket(c context.Context, req *pb.CreateMarketRequest) (*pb.CreateMarketResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := markets.CreateMarket(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetName(), req.GetDescription())

	if err != nil {
		return nil, err
	}

	return &pb.CreateMarketResponse{
		Market: model,
	}, nil
}

func (s *Server) DeleteMarket(c context.Context, req *pb.DeleteMarketRequest) (*pb.DeleteMarketResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := markets.DeleteMarket(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetId())

	if err != nil {
		return nil, err
	}

	return &pb.DeleteMarketResponse{
		Market: model,
	}, nil
}

func (s *Server) GetMarketByName(c context.Context, req *pb.GetMarketByNameRequest) (*pb.GetMarketByNameResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := markets.GetMarketByName(c, s.Db, s.Warden, user, req.GetName())

	if err != nil {
		return nil, err
	}

	return &pb.GetMarketByNameResponse{
		Market: model,
	}, nil
}

func (s *Server) GetMarket(c context.Context, req *pb.GetMarketRequest) (*pb.GetMarketResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := markets.GetMarket(c, s.Db, s.Warden, user, req.GetId())

	if err != nil {
		return nil, err
	}

	return &pb.GetMarketResponse{
		Market: model,
	}, nil
}
