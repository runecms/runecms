// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/products"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreateProduct(c context.Context, req *pb.CreateProductRequest) (*pb.CreateProductResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	product := &pb.Product{
		Name:                 req.GetName(),
		Description:          req.GetName(),
		Price:                req.GetPrice(),
		ProductTypeId:        req.GetProductTypeId(),
		PublicationId:        req.GetPublicationId(),
		BillingProcessorData: req.GetBillingProcessorData(),
		Rates:                req.GetRates(),
	}

	model, err := products.CreateProductFromModel(c, s.Db, s.Warden, s.WebhooksClient, user, product)
	// model, err := products.CreateProduct(c, s.Db, s.Warden, user, req.GetName(), req.GetDescription(), req.GetPrice(), req.GetProductTypeId(), req.GetPublicationId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.CreateProductResponse{
		Product: model,
	}, nil
}

func (s *Server) DeleteProduct(c context.Context, req *pb.DeleteProductRequest) (*pb.DeleteProductResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := products.DeleteProduct(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.DeleteProductResponse{
		Product: model,
	}, nil
}

func (s *Server) GetProductByName(c context.Context, req *pb.GetProductByNameRequest) (*pb.GetProductByNameResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := products.GetProductByName(c, s.Db, s.Warden, user, req.GetName())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetProductByNameResponse{
		Product: model,
	}, nil
}

func (s *Server) GetProduct(c context.Context, req *pb.GetProductRequest) (*pb.GetProductResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := products.GetProduct(c, s.Db, s.Warden, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetProductResponse{
		Product: model,
	}, nil
}
