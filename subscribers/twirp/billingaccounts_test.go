// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"
	"testing"
	"time"

	runetestutil "git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/subscribers/internal/billingaccounts"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"
)

func get_billingaccount(s *Server) (*pb.BillingAccount, error) {
	return billingaccounts.CreateBillingAccount(context.Background(), s.Db, s.Warden, s.WebhooksClient, s.BillingClient, valid_id, "test billing account", false, false, subscriber.Id, exampleUuid, "4321", "Example User", time.Now().AddDate(1, 1, 0), pb.BillingAccountType_GOOGLE_PAY)
}

func TestCreateBillingAccount(t *testing.T) {
	GetServer(t)
	t.Run("Server.Create", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.CreateBillingAccount(
				valid_context,
				&pb.CreateBillingAccountRequest{
					SubscriberId:       subscriber.Id,
					Name:               "Test Billing Account",
					IsDefault:          false,
					IsExternal:         false,
					Token:              runetestutil.GetUUID(),
					LastFour:           "4321",
					NameOnAccount:      "Example User",
					ExpirationDate:     time.Now().AddDate(1, 1, 0).Format(time.RFC3339),
					BillingAccountType: pb.BillingAccountType_GOOGLE_PAY,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.CreateBillingAccount(
				invalid_context,
				&pb.CreateBillingAccountRequest{
					SubscriberId:       subscriber.Id,
					Name:               "Test Billing Account",
					IsDefault:          false,
					Token:              runetestutil.GetUUID(),
					LastFour:           "4321",
					NameOnAccount:      "Example User",
					ExpirationDate:     time.Now().AddDate(1, 1, 0).Format(time.RFC3339),
					BillingAccountType: pb.BillingAccountType_GOOGLE_PAY,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestGetBillingAccount(t *testing.T) {
	GetServer(t)
	m, err := get_billingaccount(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Get", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.GetBillingAccount(
				valid_context,
				&pb.GetBillingAccountRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.GetBillingAccount(
				invalid_context,
				&pb.GetBillingAccountRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}

func TestDeleteBillingAccount(t *testing.T) {
	GetServer(t)
	m, err := get_billingaccount(s)
	if err != nil {
		t.Fatalf("Failed to set up test: %s", err.Error())
	}

	t.Run("Server.Delete", func(t *testing.T) {
		t.Run("Valid request", func(t *testing.T) {
			res, err := s.DeleteBillingAccount(
				valid_context,
				&pb.DeleteBillingAccountRequest{
					Id: m.Id,
				},
			)

			if err != nil {
				t.Error(err)
			}

			if res == nil {
				t.Error("Response to a valid request should not be nil")
			}
		})

		t.Run("Valid request with invalid context", func(t *testing.T) {
			_, err := s.DeleteBillingAccount(
				invalid_context,
				&pb.DeleteBillingAccountRequest{
					Id: m.Id,
				},
			)

			if err == nil {
				t.Error("Invalid context should return an error")
			}
		})
	})
}
