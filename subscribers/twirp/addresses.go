// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/addresses"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreateAddress(c context.Context, req *pb.CreateAddressRequest) (*pb.CreateAddressResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := addresses.CreateAddress(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetAddress(), req.GetState(), req.GetCity(), req.GetZip(), req.GetCountry())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.CreateAddressResponse{
		Address: model,
	}, nil
}

func (s *Server) DeleteAddress(c context.Context, req *pb.DeleteAddressRequest) (*pb.DeleteAddressResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := addresses.DeleteAddress(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.DeleteAddressResponse{
		Address: model,
	}, nil
}

func (s *Server) GetAddressById(c context.Context, req *pb.GetAddressByIdRequest) (*pb.GetAddressByIdResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := addresses.GetAddressById(c, s.Db, s.Warden, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetAddressByIdResponse{
		Address: model,
	}, nil
}

func (s *Server) GetAddress(c context.Context, req *pb.GetAddressRequest) (*pb.GetAddressResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := addresses.FindAddress(c, s.Db, s.Warden, user, req.GetAddress(), req.GetCity(), req.GetState(), req.GetZip(), req.GetCountry())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetAddressResponse{
		Address: model,
	}, nil
}
