// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirpserver

import (
	"context"
	"time"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/subscribers/internal/subscriptions"
	pb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) CreateSubscription(c context.Context, req *pb.CreateSubscriptionRequest) (*pb.CreateSubscriptionResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	startAt, err := time.Parse(time.RFC3339, req.GetStartAt())
	if err != nil {
		return nil, twirp.NewError(twirp.Internal, err.Error())
	}

	endAt, err := time.Parse(time.RFC3339, req.GetEndAt())
	if err != nil {
		return nil, twirp.NewError(twirp.Internal, err.Error())
	}

	model, err := subscriptions.CreateSubscription(
		c, s.Db,
		s.Warden,
		s.WebhooksClient,
		user,
		req.GetActive(),
		req.GetRate(),
		startAt,
		endAt,
		req.GetSubscriptionStatusId(),
		req.GetSubscriptionTypeId(),
		req.GetSubscriberId(),
		req.GetBillingAccountId(),
		req.GetAuthorizedUsers(),
	)

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	if req.GetDeliveryAddressId() != "" {
		phys, err := subscriptions.CreatePhysicalSubscription(c, s.Db, s.Warden, user, s.WebhooksClient, model.GetId(), req.GetDeliveryAddressId())
		if err != nil {
			return nil, err.(runecms.RuneError).GetTwirpError()
		}
		return &pb.CreateSubscriptionResponse{
			Subscription: &pb.CreateSubscriptionResponse_PhysicalSubscription{
				PhysicalSubscription: phys,
			},
		}, nil
	}

	return &pb.CreateSubscriptionResponse{
		Subscription: &pb.CreateSubscriptionResponse_OnlineSubscription{
			OnlineSubscription: model,
		},
	}, nil
}

func (s *Server) DeleteSubscription(c context.Context, req *pb.DeleteSubscriptionRequest) (*pb.DeleteSubscriptionResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptions.DeleteSubscription(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.DeleteSubscriptionResponse{
		Subscription: model,
	}, nil
}

func (s *Server) GetSubscription(c context.Context, req *pb.GetSubscriptionRequest) (*pb.GetSubscriptionResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := subscriptions.GetSubscription(c, s.Db, s.Warden, user, req.GetId())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	// Attempt to get the physical subscription; if it fails, then this is (most likely) just an online subscription
	phys, err := subscriptions.GetPhysicalSubscription(c, s.Db, s.Warden, user, req.GetId())
	if err == nil && phys != nil {
		return &pb.GetSubscriptionResponse{
			Subscription: &pb.GetSubscriptionResponse_PhysicalSubscription{
				PhysicalSubscription: phys,
			},
		}, nil
	}

	return &pb.GetSubscriptionResponse{
		Subscription: &pb.GetSubscriptionResponse_OnlineSubscription{
			OnlineSubscription: model,
		},
	}, nil
}

func (s *Server) UpdatePhysicalSubscription(c context.Context, req *pb.UpdatePhysicalSubscriptionRequest) (*pb.UpdatePhysicalSubscriptionResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}
	model, err := subscriptions.UpdatePhysicalSubscription(c, s.Db, s.Warden, s.WebhooksClient, user, req.GetSubscription())

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.UpdatePhysicalSubscriptionResponse{
		Subscription: model,
	}, nil
}

func (s *Server) UpdateSubscription(c context.Context, req *pb.UpdateSubscriptionRequest) (*pb.UpdateSubscriptionResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}
	sub := req.GetSubscription()
	model, err := subscriptions.UpdateSubscription(c, s.Db, s.Warden, s.WebhooksClient, user, sub)

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.UpdateSubscriptionResponse{
		Subscription: model,
	}, nil
}
