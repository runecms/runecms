+++
title = "configuration"
weight = 2
+++

# Environment Variables

| name | description | default value |
|------|-------------|---------------|
| RUNE_PORT | The port the application will listen for connections on | 3000 |
| RUNE_DATABASE_HOST | The (postgres/cockroach) database to store the application data | postgres |
| RUNE_DATABASE_PORT | The (postgres/cockroach) database port | 5432 |
| RUNE_DATABASE_USER | The database user | postgres |
| RUNE_DATABASE_PASS | The database password | postgres |
| RUNE_DATABASE_NAME | The name of the database the application will use | rune |
| RUNE_DATABASE_SCHEMA | The name of the schema the application will use | subscribers |
| RUNE_LADON_HOST | The (postgres/cockroach) database to check access policies | ladon |
| RUNE_LADON_PORT | The (postgres/cockroach) database port | 5432 |
| RUNE_LADON_USER | The database user | ladon |
| RUNE_LADON_PASS | The database password | ladon |
| RUNE_LADON_SCHEMA | The database/schema where the Ladon data lives | ladon |
