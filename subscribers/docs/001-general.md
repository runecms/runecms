+++
title = "general"
weight = 1
+++

## Subscribers

The Subscribers service is by far the largest service in Rune.

The Subscribers service is primarily responsible for managing subscriber profiles, their subscriptions, and how they pay for them.

It is responsible for managing the following resources:

* [Billing Accounts](#data-billing-accounts)
* [Complaints](#data-complaints)
* [Delivery Addresses](#data-delivery-addresses)
* [Subscribers](#data-subscribers)
* [Subscriptions](#data-subscriptions)
* [Subscription Holds](#data-subscription-holds)
* [Subscription Hold Types](#data-subscription-hold-types)
* [Subscription Types](#data-subscription-types)
