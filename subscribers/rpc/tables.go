// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscriberspb

// This file defines table names for the generated models

func (Address) TableName() string {
	return "subscribers_addresses"
}
func (BillingAccount) TableName() string {
	return "subscribers_billing_accounts"
}
func (ComplaintType) TableName() string {
	return "subscribers_complaint_types"
}
func (ComplaintStatus) TableName() string {
	return "subscribers_complaint_statuses"
}
func (Complaint) TableName() string {
	return "subscribers_complaints"
}
func (DeliveryAddress) TableName() string {
	return "subscribers_delivery_addresses"
}
func (Market) TableName() string {
	return "subscribers_markets"
}
func (Note) TableName() string {
	return "subscribers_notes"
}
func (ProductFrequency) TableName() string {
	return "subscribers_product_frequencies"
}
func (ProductRate) TableName() string {
	return "subscribers_product_rates"
}
func (ProductType) TableName() string {
	return "subscribers_product_types"
}
func (Product) TableName() string {
	return "subscribers_products"
}
func (Publication) TableName() string {
	return "subscribers_publications"
}
func (Subscriber) TableName() string {
	return "subscribers"
}
func (SubscriptionHold) TableName() string {
	return "subscribers_subscription_holds"
}
func (SubscriptionHoldType) TableName() string {
	return "subscribers_subscription_hold_types"
}
func (SubscriptionStatus) TableName() string {
	return "subscribers_subscription_statuses"
}
func (SubscriptionType) TableName() string {
	return "subscribers_subscription_types"
}
func (PhysicalSubscription) TableName() string {
	return "subscribers_physical_subscriptions"
}
func (Subscription) TableName() string {
	return "subscribers_subscriptions"
}
func (AuthorizedUser) TableName() string {
	return "subscribers_authorized_users"
}
