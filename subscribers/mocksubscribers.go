// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscribers

import (
	"context"

	"git.runecms.io/rune/runecms/subscribers/rpc"
)

type MockSubscribers struct {
}

func (p *MockSubscribers) CreateSubscriber(ctx context.Context, req *subscriberspb.CreateSubscriberRequest) (*subscriberspb.CreateSubscriberResponse, error) {
	return nil, nil
}

func (p *MockSubscribers) DeleteSubscriber(ctx context.Context, req *subscriberspb.DeleteSubscriberRequest) (*subscriberspb.DeleteSubscriberResponse, error) {
	return nil, nil
}

func (p *MockSubscribers) GetSubscriber(ctx context.Context, req *subscriberspb.GetSubscriberRequest) (*subscriberspb.GetSubscriberResponse, error) {
	return nil, nil
}

func (p *MockSubscribers) GetSubscriberByAccountId(ctx context.Context, req *subscriberspb.GetSubscriberByAccountIdRequest) (*subscriberspb.GetSubscriberByAccountIdResponse, error) {
	return nil, nil
}
