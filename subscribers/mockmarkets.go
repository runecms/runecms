// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package subscribers

import (
	"context"

	"git.runecms.io/rune/runecms/subscribers/rpc"
)

type MockMarkets struct {
}

func (p *MockMarkets) CreateMarket(ctx context.Context, req *subscriberspb.CreateMarketRequest) (*subscriberspb.CreateMarketResponse, error) {
	return nil, nil
}

func (p *MockMarkets) DeleteMarket(ctx context.Context, req *subscriberspb.DeleteMarketRequest) (*subscriberspb.DeleteMarketResponse, error) {
	return nil, nil
}

func (p *MockMarkets) GetMarket(ctx context.Context, req *subscriberspb.GetMarketRequest) (*subscriberspb.GetMarketResponse, error) {
	return nil, nil
}

func (p *MockMarkets) GetMarketByName(ctx context.Context, req *subscriberspb.GetMarketByNameRequest) (*subscriberspb.GetMarketByNameResponse, error) {
	return nil, nil
}
