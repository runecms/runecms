// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package runecms

import (
	"github.com/caarlos0/env"
)

// Config struct stores rune configs passed via. environment variables
type Config struct {
	// Rune database configs from 'rune-config' configmap
	RuneDBURL    string `env:"RUNE_DATABASE_URL"`
	RuneRedisURL string `env:"RUNE_REDIS_URL"`

	// Ladon database configs from 'ladon-database-config' configmap
	LadonDBURL    string `env:"LADON_DATABASE_URL"`
	LadonRedisURL string `env:"LADON_REDIS_URL"`

	// Rune internal service URLs from 'rune-services' configmap
	RuneBillingAddress     string `env:"RUNE_BILLING_ADDRESS"`
	RuneIntegrationAddress string `env:"RUNE_INTEGRATION_ADDRESS"`
	RuneMetadataAddress    string `env:"RUNE_METADATA_ADDRESS"`
	RunePromotionsAddress  string `env:"RUNE_PROMOTIONS_ADDRESS"`
	RuneSubscribersAddress string `env:"RUNE_SUBSCRIBERS_ADDRESS"`
	RuneUsersAddress       string `env:"RUNE_USERS_ADDRESS"`
	RuneWebhooksAddress    string `env:"RUNE_WEBHOOKS_ADDRESS"`

	// General rune configs from 'rune-config' configmap
	DatabaseConnectionLimit int `env:"DATABASE_CONNECTION_LIMIT"`
	ListenPort              int `env:"LISTEN_PORT" envDefault:"3000"`

	// Jaeger address for tracing. hostname:ip format
	JargerAddress string `env:"JAEGER_ADDRESS"`
}

// GetConfig will return a rune Config struct based on the current environment variables
func GetConfig() (*Config, error) {
	cfg := &Config{}
	if err := env.Parse(cfg); err != nil {
		return nil, err
	}
	return cfg, nil
}
