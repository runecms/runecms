// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"html/template"
	"net/http"
	"path/filepath"
)

type Consumer struct {
	ID        string
	Templates *template.Template
	AssetsFs  http.Handler
}

func (c *Consumer) Load(rootDir string) error {
	var err error
	c.Templates, err = template.ParseGlob(filepath.Join(rootDir, c.ID, "templates", "*"))
	if err != nil {
		return err
	}

	static := filepath.Join(rootDir, c.ID, "static")

	c.AssetsFs = http.FileServer(http.Dir(static))

	return nil
}
