// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

// FormStatus is provided to the LoginPage HTML template to
// represent the current status of the form.
type FormStatus map[string]FormField

// FormField represents an individual field on the form
type FormField struct {
	Value string
}

// Status and messaging for Reset Password page
type resetStatus struct {
	Status  bool
	ErrMsg  string
	Email   string
	Message string
}
