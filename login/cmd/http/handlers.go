// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"log"
	"net/http"
	"time"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/internal/auth"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/ory/hydra/sdk/go/hydra/swagger"
	"github.com/pborman/uuid"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

func handleConsent(w http.ResponseWriter, r *http.Request) {
	consentRequestID := r.URL.Query().Get("consent")
	if consentRequestID == "" {
		http.Error(w, errors.New("Consent endpoint was called without a consent request ID").Error(), http.StatusBadRequest)
		return
	}

	consentRequest, response, err := client.GetOAuth2ConsentRequest(consentRequestID)
	if err != nil {
		http.Error(w, errors.Wrap(err, "The consent request endpoint is not responding").Error(), http.StatusBadRequest)
		return
	} else if response.StatusCode != http.StatusOK {
		http.Error(w, errors.Wrapf(err, "Consent request endpoint gave status code %d but expected %d", response.StatusCode, http.StatusOK).Error(), http.StatusBadRequest)
		return
	}

	// If the user is not authenticated, redirect them to the login page
	claims, ok := auth.IsAuthenticated(r)
	if !ok {
		http.Redirect(w, r, "/login?consent="+consentRequestID+"&client-id="+consentRequest.ClientId, http.StatusFound)
		return
	}
	log.Println("User is authenticated, proceeding with consent...")

	response, err = client.AcceptOAuth2ConsentRequest(consentRequestID, swagger.ConsentRequestAcceptance{
		Subject:          claims.StandardClaims.Subject,
		GrantScopes:      []string{"offline", "openid"},
		AccessTokenExtra: map[string]interface{}{"foo": "bar"},
		IdTokenExtra:     map[string]interface{}{"foo": "baz"},
	})

	log.Println("Response received...")

	if err != nil {
		http.Error(w, errors.Wrap(err, "The accept consent request endpoint encountered a network error").Error(), http.StatusInternalServerError)
		return
	} else if response.StatusCode != http.StatusNoContent {
		http.Error(w, errors.Wrapf(err, "Accept request endpoint gave status code %d but expected %d", response.StatusCode, http.StatusNoContent).Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, consentRequest.RedirectUrl, http.StatusFound)
}

func handleLogin(w http.ResponseWriter, r *http.Request) {
	consentRequestID := r.URL.Query().Get("consent")
	consumer := r.URL.Query().Get("client-id")

	if _, ok := consumers[consumer]; !ok {
		log.Panicf("Unknown consumer %s", consumer)
	}

	loginTmpl := consumers[consumer].Templates.Lookup("login.tmpl")
	if loginTmpl == nil {
		log.Panicf("Unable to fetch login template for consumer %s", consumer)
	}

	if r.Method == http.MethodPost {

		if err := r.ParseForm(); err != nil {
			http.Error(w, errors.Wrap(err, "Could not parse form").Error(), http.StatusBadRequest)
			return
		}

		email := r.Form.Get("email")
		password := r.Form.Get("password")

		f := FormStatus{
			"consent": {
				Value: consentRequestID,
			},
			"consumer": {
				Value: consumer,
			},
			"email": {
				Value: email,
			},
			"password": {
				Value: password,
			},
		}

		if len(email) > 0 {
			user, err := auth.Validate(runecms.Be("services:consent", "group:rune-internal"), email, password)
			if err != nil {
				http.Error(w, errors.Wrap(err, "Error validating credentials").Error(), http.StatusBadRequest)
				return
			}
			claims := auth.JWTClaims{
				UserID:    user.Id,
				FirstName: user.FirstName,
				LastName:  user.LastName,
				Email:     email,
				StandardClaims: jwt.StandardClaims{
					Subject:   email,
					ExpiresAt: time.Now().Add(time.Hour * 24 * 14).Unix(),
					IssuedAt:  time.Now().Unix(),
					Id:        uuid.New(),
				},
			}
			token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
			tokenString, err := token.SignedString([]byte(viper.GetString("jwt-secret-key")))
			if err != nil {
				log.Panicf("Error signing jwt: %v", err)
			}

			session, _ := store.Get(r, viper.GetString("session-name"))
			session.Values["jwt"] = tokenString

			log.Printf("New token: %s\n", tokenString)

			if err := store.Save(r, w, session); err != nil {
				http.Error(w, errors.Wrap(err, "Could not persist cookie").Error(), http.StatusBadRequest)
				return
			}

			// login complete: redirect user
			http.Redirect(w, r, "/consent?consent="+consentRequestID, http.StatusFound)
			return
		}

		err := loginTmpl.Execute(w, f)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	err := loginTmpl.Execute(w, FormStatus{
		"consent": {
			Value: consentRequestID,
		},
		"consumer": {
			Value: consumer,
		},
	})
	if err != nil {
		log.Fatal(err)
	}
}

func handleReset(w http.ResponseWriter, r *http.Request) {
	consumer := r.URL.Query().Get("client-id")

	if _, ok := consumers[consumer]; !ok {
		consumer = "default"
	}

	s := resetStatus{
		Status:  false,
		ErrMsg:  "",
		Email:   "",
		Message: "",
	}

	if r.Method == http.MethodPost {
		email := r.FormValue("email")
		rq, msg := auth.ResetPassword(
			email,
			viper.GetString("db-conn"),
			viper.GetString("email-address"),
			viper.GetString("email-password"),
			viper.GetString("email-host"),
			viper.GetString("support-email"),
		)

		if rq == true {
			s.Status = true
			s.Message = "Your password has been reset. Check your email for your new password."
		} else {
			s.ErrMsg = msg
		}

		s.Email = email

		err := consumers[consumer].Templates.Lookup("reset.tmpl").Execute(w, s)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	if r.Method == http.MethodGet {
		err := consumers[consumer].Templates.Lookup("reset.tmpl").Execute(w, s)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

}
