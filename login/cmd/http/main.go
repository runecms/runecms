// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"git.runecms.io/rune/runecms/internal/auth"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/jmoiron/sqlx"
	"github.com/ory/hydra/sdk/go/hydra"
	"github.com/ory/ladon"
	manager "github.com/ory/ladon/manager/sql"
	"github.com/spf13/viper"
)

var (
	store  *sessions.CookieStore
	client hydra.SDK
	warden ladon.Warden

	consumers map[string]*Consumer

	runeConnString  string
	ladonConnString string
)

func init() {
	viper.AutomaticEnv()

	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))

	viper.SetDefault("hydra-client-id", "login-app")
	viper.SetDefault("hydra-client-secret", "login-secret")
	viper.SetDefault("hydra-cluster-url", "http://localhost:4444/")
	viper.SetDefault("scopes", []string{"hydra.consent"})
	viper.SetDefault("ladon-conn", "postgres://postgres:secret@localhost:5432/ladon?sslmode=disable")
	viper.SetDefault("jwt-secret-key", "Crispy chicken voicemail and ready for diapers like a dark jacket york!")
	viper.SetDefault("cookie-secret", "cookie-secret")
	viper.SetDefault("session-name", "rune-sso")
	viper.SetDefault("email-address", "")
	viper.SetDefault("email-password", "")
	viper.SetDefault("support-email", "support@runecms.io")
	viper.SetDefault("email-host", "")
	viper.SetDefault("assets", "./rootfs/assets")

	store = sessions.NewCookieStore([]byte(viper.GetString("cookie-secret")))
}

func main() {
	config, err := runecms.GetConfig()
	if err != nil {
		log.Panicln("Unable to get config. Error:", err.Error())
	}

	log.Println("Connecting to databases...")
	runeDb, err := sql.Open("postgres", config.RuneDBURL)
	if err != nil {
		log.Panicln(err)
	}

	ladonDb, err := sqlx.Open("postgres", config.LadonDBURL)
	if err != nil {
		log.Panicln(err)
	}

	defer runeDb.Close()
	defer ladonDb.Close()

	err = runeDb.Ping()
	if err != nil {
		log.Panicf("Could not connect to the Rune database: %v", err)
	}

	err = ladonDb.Ping()
	if err != nil {
		log.Panicf("Could not connect to the Ladon database: %v", err)
	}

	runeDb.SetMaxOpenConns(config.DatabaseConnectionLimit)
	ladonDb.SetMaxOpenConns(config.DatabaseConnectionLimit)

	warden = &ladon.Ladon{
		Manager: manager.NewSQLManager(ladonDb, nil),
	}

	// Configure auth library
	auth.SessionName = viper.GetString("session-name")
	auth.CookieSecret = viper.GetString("cookie-secret")
	auth.JWTSecretKey = viper.GetString("jwt-secret-key")
	auth.Warden = warden
	auth.RuneDB = runeDb
	auth.UsersService = viper.GetString("users-service")

	client, err = hydra.NewSDK(&hydra.Configuration{
		ClientID:     viper.GetString("hydra-client-id"),
		ClientSecret: viper.GetString("hydra-client-secret"),
		EndpointURL:  viper.GetString("hydra-cluster-url"),
		Scopes:       viper.GetStringSlice("scopes"),
	})
	if err != nil {
		log.Fatalf("Unable to connect to the Hydra cluster: %v", err)
	}

	router := mux.NewRouter()
	router.HandleFunc("/reset", handleReset)
	router.HandleFunc("/login", handleLogin)
	router.HandleFunc("/consent", handleConsent)

	consumerDirectories, err := ioutil.ReadDir(viper.GetString("assets"))
	if err != nil {
		log.Panicf("Error reading client assets folder: %v", err)
	}
	consumers = make(map[string]*Consumer)
	for _, dir := range consumerDirectories {
		consumer := &Consumer{ID: dir.Name()}
		err := consumer.Load(viper.GetString("assets"))
		if err != nil {
			log.Panicf("Error loading consumer assets: %v", err)
		}
		log.Printf("Consumer Assets: %s\n%#v\n", "/assets/"+consumer.ID+"/", consumer.AssetsFs)
		router.PathPrefix("/assets/" + consumer.ID + "/").
			Handler(
				http.StripPrefix("/assets/"+consumer.ID+"/", consumer.AssetsFs),
			)

		consumers[consumer.ID] = consumer
	}

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.ListenPort), router))
}

func httplog(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("%s %s %s\n", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}
