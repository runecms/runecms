// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package runecms

import (
	"fmt"
	"strings"
)

// RuneError is a an error type that stores a standardized error message and a description
type RuneError struct {
	Err         string `json:"error"`
	Description string `json:"description"`
}

func (r RuneError) Error() string {
	if r.Description == "" {
		return r.Err
	}

	return fmt.Sprintf("Error: %s. Description: %s", r.Err, r.Description)
}

func NewError(err string, desc ...string) RuneError {
	if len(desc) == 0 {
		return RuneError{
			Err: err,
		}
	}
	return RuneError{
		Err:         err,
		Description: strings.Join(desc, " / "),
	}
}

func Error(err string, desc ...string) RuneError {
	return NewError(err, desc...)
}

func Errorf(err string, format string, elem ...string) RuneError {
	return NewError(err, fmt.Sprintf(format, elem))
}
