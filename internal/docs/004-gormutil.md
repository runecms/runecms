+++
title = "gormutil"
weight = 4
+++
## gormutil

The [subscribers](https://docs.gorune.io/subscribers/) service uses an ORM called `gorm`. `gorm`, however does not play nicely with [cockroachdb](https://www.cockroachlabs.com/).

Because of this, the `gormutil` package was created, simply to add a couple of hacks for supporting timestamps in the format that `cockroachdb` expects.
