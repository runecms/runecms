+++
title = "general"
weight = 1
+++

## Configuration

## Packages

These are `go` packages used throughout the various services in Rune.

These packages are typically intended to be very versatile, and therefore not tied to the Rune database.
