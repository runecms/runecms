// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package mail

import (
	"io"

	gomail "gopkg.in/gomail.v2"
)

type Message struct {
	From        string
	ReplyTo     string
	To          []string
	CC          []string
	BCC         []string
	Subject     string
	Attachments []Attachment
	Embeds      []Attachment
	BodyPlain   string
	BodyHTML    string
}

type Attachment struct {
	Filename string
	Rename   string
}

func (m *Message) WriteTo(w io.Writer) (int64, error) {
	msg := gomail.NewMessage()
	msg.SetHeaders(map[string][]string{
		"From":     {m.From},
		"Reply-To": {coalesce(m.ReplyTo, m.From)},
		"To":       m.To,
		"Cc":       m.CC,
		"Bcc":      m.BCC,
		"Subject":  {m.Subject},
	})
	for _, file := range m.Attachments {
		if file.Rename != "" {
			msg.Attach(file.Filename, gomail.Rename(file.Rename))
		} else {
			msg.Attach(file.Filename)
		}
	}
	for _, file := range m.Embeds {
		if file.Rename != "" {
			msg.Embed(file.Filename, gomail.Rename(file.Rename))
		} else {
			msg.Embed(file.Filename)
		}
	}
	msg.SetBody("text/plain", m.BodyPlain)
	msg.AddAlternative("text/html", m.BodyHTML)

	return msg.WriteTo(w)
}

func coalesce(s ...string) string {
	for _, v := range s {
		if v != "" {
			return v
		}
	}
	return ""
}
