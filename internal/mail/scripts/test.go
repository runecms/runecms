// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"log"

	"git.runecms.io/rune/runecms/internal/mail"
)

func main() {
	msg := mail.Message{
		From:      "Rune Test <test@runecms.io>",
		To:        []string{"Jason Hutchinson <thezikes@gmail.com>"},
		Subject:   "Test Email",
		BodyPlain: "This is a test.",
		BodyHTML: `<strong>This</strong> is a <em>test</em>.

<img src="cid:digging.gif" alt="Digging" />`,
		Embeds: []mail.Attachment{
			{
				Filename: `/Users/jasonhutchinson/Downloads/animation (1).gif`,
				Rename:   `digging.gif`,
			},
		},
	}

	err := mail.Send(msg)
	if err != nil {
		log.Print(err)
	}
}
