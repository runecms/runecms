// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package mail

import (
	"bufio"
	"bytes"
	"fmt"
	"net/mail"
	"net/smtp"
	"strings"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

func init() {
	viper.AutomaticEnv()
	viper.SetEnvPrefix("mail")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	viper.SetDefault("provider", "smtp")

	viper.SetDefault("smtp-host", "smtp.mailgun.org")
	viper.SetDefault("smtp-port", 2525)
	viper.SetDefault("smtp-user", "postmaster@mail.runecms.io")
	viper.SetDefault("smtp-pass", "")
}

func Send(m Message) error {
	switch viper.GetString("provider") {
	case "smtp":
		var msgBuffer bytes.Buffer
		writer := bufio.NewWriter(&msgBuffer)
		m.WriteTo(writer)
		writer.Flush()

		auth := smtp.PlainAuth(
			"",
			viper.GetString("smtp-user"),
			viper.GetString("smtp-pass"),
			viper.GetString("smtp-host"),
		)

		fromAddress, err := mail.ParseAddress(m.From)
		if err != nil {
			return errors.Wrapf(err, "Unable to parse From address: %s", m.From)
		}
		recipients := []string{}

		for _, to := range m.To {
			address, err := mail.ParseAddress(to)
			if err != nil {
				return errors.Wrapf(err, "Unable to parse To address: %s", to)
			}
			recipients = append(recipients, address.Address)
		}

		for _, to := range m.CC {
			address, err := mail.ParseAddress(to)
			if err != nil {
				return errors.Wrapf(err, "Unable to parse CC address: %s", to)
			}
			recipients = append(recipients, address.Address)
		}

		for _, to := range m.BCC {
			address, err := mail.ParseAddress(to)
			if err != nil {
				return errors.Wrapf(err, "Unable to parse BCC address: %s", to)
			}
			recipients = append(recipients, address.Address)
		}

		return smtp.SendMail(
			fmt.Sprintf("%s:%d", viper.GetString("smtp-host"), viper.GetInt("smtp-port")),
			auth,
			fromAddress.Address,
			recipients,
			msgBuffer.Bytes(),
		)
	}
	return nil
}
