// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirputil

import (
	"context"
	"net/http"

	"github.com/golang/glog"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/twitchtv/twirp"
)

const defaultComponentName = "net/http"

type statusCodeTracker struct {
	http.ResponseWriter
	status int
}

func (w *statusCodeTracker) WriteHeader(status int) {
	w.status = status
	w.ResponseWriter.WriteHeader(status)
}

func WithTracing(base http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		var sp opentracing.Span

		ctx, err := opentracing.GlobalTracer().Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(r.Header))
		if err != nil {
			glog.V(1).Infoln("[Tracing] Error extracting opentracing headers:", err.Error())
		}
		if ctx != nil {
			sp = opentracing.GlobalTracer().StartSpan("HTTP "+r.Method, ext.RPCServerOption(ctx))
			defer sp.Finish()
			ext.HTTPMethod.Set(sp, r.Method)
			ext.HTTPUrl.Set(sp, r.URL.String())

			// set component name, use "net/http" if caller does not specify
			componentName := defaultComponentName
			ext.Component.Set(sp, componentName)

			w = &statusCodeTracker{w, 200}
			r = r.WithContext(opentracing.ContextWithSpan(r.Context(), sp))
		}
		base.ServeHTTP(w, r)

		if sp != nil {
			ext.HTTPStatusCode.Set(sp, uint16(w.(*statusCodeTracker).status))
		}
	}
	return http.HandlerFunc(fn)
}

// InjectTwirpHeaders takes a context twirp values and adds opentracing headers using twirp's stdlib
func InjectTwirpHeaders(ctx context.Context) context.Context {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		carrier := &opentracing.HTTPHeadersCarrier{}
		if err := opentracing.GlobalTracer().Inject(span.Context(), opentracing.HTTPHeaders, carrier); err != nil {
			glog.V(1).Infoln("[Tracing] Error injecting tracing headers to carrier", err.Error())
		}
		// Whenever a response is ready to be sent, add the headers to the twirp context
		if err := carrier.ForeachKey(func(key string, value string) error {
			if err := twirp.SetHTTPResponseHeader(ctx, key, value); err != nil {
				return err
			}
			return nil
		}); err != nil {
			glog.V(1).Infoln("Error adding http header:", err.Error())
		}
	}
	return ctx
}
