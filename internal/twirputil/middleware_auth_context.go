// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package twirputil

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"git.runecms.io/rune/runecms"
)

const JWTHeaderName = "X-Jwt-Payload"

func WithJWTPayload(base http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Decoding header %s...", JWTHeaderName)
		var (
			ctx     = r.Context()
			payload = r.Header.Get(JWTHeaderName)
			ident   = runecms.Identity{}
		)

		// Ignore the error here because it will leave the identity empty
		err := json.Unmarshal([]byte(payload), &ident)
		if err != nil {
			log.Println("Error unmarshalling header content", err.Error())
		}
		log.Printf("Got identity for user \"%s\"", ident.User)
		// Empty identity called with ToContext() will leave an empty username / group
		// Let the service handle empty users / groups
		ctx = context.WithValue(ctx, "identity", ident)
		r = r.WithContext(ctx)

		base.ServeHTTP(w, r)
	})
}
