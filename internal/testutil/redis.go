// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package testutil

import (
	"fmt"
	"os"
	"testing"

	"github.com/go-redis/redis"
	"github.com/ory/dockertest"
)

var (
	r *redis.Client
)

func GetTestRedis(t *testing.T) *redis.Client {
	var err error
	if r != nil {
		return r
	}

	if url, ok := os.LookupEnv("TEST_REDIS_URL"); ok {
		t.Log("Using CI redis")
		opt, err := redis.ParseURL(url)
		if err != nil {
			t.Fatal(err)
		}
		db := redis.NewClient(opt)
		_, err = db.Ping().Result()
		if err != nil {
			t.Fatal(err)
		}
		r = db
		return r
	}

	t.Log("Using dockertest redis")
	if pool == nil {
		pool, err = dockertest.NewPool("")
		if err != nil {
			t.Fatal(err)
		}
	}
	resource, err := pool.Run("redis", "3.2.11", nil)
	if err != nil {
		t.Fatal(err)
	}
	opt, err := redis.ParseURL(fmt.Sprintf("redis://localhost:%s", resource.GetPort("6379/tcp")))
	if err != nil {
		t.Fatal(err)
	}
	db := redis.NewClient(opt)
	if err := pool.Retry(func() error {
		_, err := db.Ping().Result()
		if err != nil {
			return err
		}
		return nil
	}); err != nil {
		t.Fatal(err)
	}
	r = db
	return r
}
