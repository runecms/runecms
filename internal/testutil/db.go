// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package testutil

import (
	"database/sql"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"github.com/lib/pq"
	"github.com/ory/dockertest"

	"github.com/zikes/migrate"
	"github.com/zikes/migrate/database/cockroachdb"
	_ "github.com/zikes/migrate/source/file"
)

var (
	db        *sql.DB
	resources []*dockertest.Resource
	pool      *dockertest.Pool
)

func init() {
	if !driverExists(sql.Drivers(), "cockroachdb") {
		sql.Register("cockroachdb", &pq.Driver{})
	}
}

func KillAll() {
	time.Sleep(time.Millisecond * 500)
	for _, resource := range resources {
		pool.Purge(resource)
	}
	resources = []*dockertest.Resource{}
}

func PopulateDb(db *sql.DB) error {
	projectPath := os.Getenv("TEST_PROJECT_PATH")
	if projectPath == "" {
		projectPath = filepath.Join(os.Getenv("GOPATH"), "src/git.runecms.io/rune/runecms")
	}
	services := []string{
		"billing",
		"images",
		"metadata",
		"promotions",
		"sites",
		"staff",
		"stories",
		"subscribers",
		"users",
		"webhooks",
	}

	for _, service := range services {
		migrationsPath := filepath.Join(projectPath, service, "db")

		if e, _ := exists(migrationsPath); !e {
			continue
		}

		instance, err := cockroachdb.WithInstance(db, &cockroachdb.Config{
			MigrationsTable: fmt.Sprintf("schema_%s_migrations", service),
			LockTable:       fmt.Sprintf("schema_%s_lock", service),
		})
		if err != nil {
			return err
		}

		migration, err := migrate.NewWithDatabaseInstance(
			fmt.Sprintf("file://%s", migrationsPath),
			"cockroachdb",
			instance,
		)
		if err != nil {
			return err
		}

		if err := migration.Up(); err != nil {
			return err
		}
	}

	return nil
}

func GetTestDb(t *testing.T) *sql.DB {
	var (
		err          error
		databaseName string

		dbHost string
		dbUser string
		dbPort string
		ok     bool
	)
	t.Log("Requesting database")
	if db != nil {
		t.Log("Returning existing database connection")
		return db
	}

	databaseName = strings.ToLower(GetRandString(8))

	if dbHost, ok = os.LookupEnv("TEST_DATABASE_HOST"); ok {
		t.Log("Using CI database")
		dbUser = os.Getenv("TEST_DATABASE_USER")
		dbPort = os.Getenv("TEST_DATABASE_PORT")

		db, err = sql.Open(
			"cockroachdb",
			fmt.Sprintf(
				"postgres://%s@%s:%s/%s?sslmode=disable",
				dbUser,
				dbHost,
				dbPort,
				databaseName,
			),
		)
		if err != nil {
			t.Fatal(err.Error())
		}
		if err = db.Ping(); err != nil {
			t.Fatalf("Unable to ping database: %v", err)
		}
	} else {
		t.Log("Using dockertest database")
		pool, err = dockertest.NewPool("")
		if err != nil {
			t.Fatal(err.Error())
		}
		resource, err := pool.RunWithOptions(&dockertest.RunOptions{
			Repository: "cockroachdb/cockroach",
			Tag:        "v2.0.0",
			Cmd:        []string{"start", "--insecure"},
		})
		if err != nil {
			t.Fatal(err.Error())
		}
		if err = pool.Retry(func() error {
			var err error
			db, err = sql.Open(
				"cockroachdb",
				fmt.Sprintf(
					"postgres://root@localhost:%s/%s?sslmode=disable",
					resource.GetPort("26257/tcp"),
					databaseName,
				),
			)
			if err != nil {
				return err
			}
			return db.Ping()
		}); err != nil {
			t.Fatalf("Could not connect to docker: %s", err)
		}
		resources = append(resources, resource)
	}
	_, err = db.Exec(fmt.Sprintf("CREATE DATABASE %s", databaseName))
	if err != nil {
		t.Fatalf("error creating database: %v", err)
	}
	err = PopulateDb(db)
	if err != nil {
		t.Fatal(err.Error())
	}
	return db
}

func driverExists(drivers []string, driver string) bool {
	for _, a := range drivers {
		if a == driver {
			return true
		}
	}
	return false
}

func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}
