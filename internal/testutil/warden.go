// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package testutil

import (
	"github.com/ory/ladon"
	manager "github.com/ory/ladon/manager/memory"
)

var (
	warden *ladon.Ladon
)

func GetTestLadon() *ladon.Ladon {
	if warden != nil {
		return warden
	}
	GetTestWarden()
	return warden
}

func GetTestWarden() ladon.Warden {
	if warden != nil {
		return warden
	}

	warden = &ladon.Ladon{
		Manager: manager.NewMemoryManager(),
	}

	warden.Manager.Create(&ladon.DefaultPolicy{
		ID:          GetUUID(),
		Description: "Allow user:test and group:admin to read, create, update, internal resources",
		Subjects:    []string{"users:test", "groups:admin"},
		Effect:      ladon.AllowAccess,
		Resources: []string{
			"subscribers:<.*>",
			"metadata:<.*>",
			"webhooks:<.*>",
			"billing:<.*>",
			"users:<.*>",
			"promotions:<.*>",
		},
		Actions:    []string{"read", "create", "update", "delete"},
		Conditions: ladon.Conditions{},
	})

	return warden
}
