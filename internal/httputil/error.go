// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package httputil

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func WriteError(w http.ResponseWriter, message string, code int, data []interface{}) {
	w.Header().Set("Content-Type", "text/json")
	model := ApiError{
		Message: message,
		Data:    data,
	}
	body, err := json.Marshal(model)
	if err != nil {
		http.Error(w, fmt.Sprintf("Error in WriteError. Could not encode data provided to error function. Error: %s", err.Error()), 400)
		return
	}

	http.Error(w, string(body), code)
}
