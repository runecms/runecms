// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package webhookutil

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"git.runecms.io/rune/runecms/internal/httputil"
	userspb "git.runecms.io/rune/runecms/users/rpc"
	webhookspb "git.runecms.io/rune/runecms/webhooks/rpc"
)

const service = "users:users"

type UsersReceiver interface {
	Create(*userspb.User) error
	Delete(*userspb.User) error
	Update(*userspb.User) error
}

type UsersWebhookReceiver struct {
	Receiver UsersReceiver
}

func (s *UsersWebhookReceiver) ListenAndServe(addr string) error {
	http.HandleFunc("/", s.ReceiverHandler)
	return http.ListenAndServe(addr, nil)
}

func (s *UsersWebhookReceiver) ReceiverHandler(w http.ResponseWriter, r *http.Request) {
	body, err := r.GetBody()
	if err != nil {
		log.Println("Error getting response body. Error:", err.Error())
		httputil.WriteError(w, err.Error(), http.StatusBadRequest, nil)
		return
	}

	defer func() {
		err := body.Close()
		if err != nil {
			log.Println("Error closing body. Error:", err.Error())
			httputil.WriteError(w, err.Error(), http.StatusInternalServerError, nil)
			return
		}
	}()

	b, err := ioutil.ReadAll(body)
	if err != nil {
		log.Println("Error reading from body reader. Error:", err.Error())
		httputil.WriteError(w, err.Error(), http.StatusBadRequest, nil)
		return
	}

	t := &webhookspb.WebhookTransactionFormat{}
	if err := json.Unmarshal(b, t); err != nil {
		log.Println("Error unmarshalling webhook transaction from json. Error:", err.Error())
		httputil.WriteError(w, err.Error(), http.StatusBadRequest, nil)
		return
	}

	if t.GetServiceName() != service {
		log.Printf("Received service %s, expected %s", t.GetServiceName(), service)
		httputil.WriteError(w, fmt.Sprintf("Received service %s, expected %s", t.GetServiceName(), service), http.StatusNotImplemented, nil)
		return
	}

	user := &userspb.User{}
	if err := json.Unmarshal(t.GetData(), user); err != nil {
		log.Println("Error unmarshalling user from json in webhook transaction. Error:", err.Error())
		httputil.WriteError(w, err.Error(), http.StatusInternalServerError, nil)
		return
	}

	switch t.GetAction() {
	case "create":
		err = s.Receiver.Create(user)
	case "delete":
		err = s.Receiver.Delete(user)
	case "update":
		err = s.Receiver.Update(user)
	default:
		log.Printf("Received unexpected action, %s", t.GetAction())
		httputil.WriteError(w, fmt.Sprintf("Received unexpected action %s", t.GetServiceName()), http.StatusBadRequest, nil)
		return
	}

	if err != nil {
		log.Printf("Error when calling receiver function for action %s.Error: %s", t.GetAction(), err.Error())
		httputil.WriteError(w, err.Error(), http.StatusInternalServerError, nil)
		return
	}
}
