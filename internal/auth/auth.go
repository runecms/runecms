// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package auth

import (
	"context"
	"database/sql"
	"errors"
	"log"
	"math/rand"
	"net/http"
	"net/smtp"

	"git.runecms.io/rune/runecms"
	users "git.runecms.io/rune/runecms/users/rpc"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/sessions"
	"github.com/ory/ladon"
	"golang.org/x/crypto/bcrypt"

	_ "github.com/lib/pq"
)

var auth smtp.Auth

var CookieSecret string
var SessionName string
var JWTSecretKey string
var UsersService string
var RuneDB *sql.DB
var Warden ladon.Warden

//Request struct
type Request struct {
	from    string
	to      []string
	subject string
	body    string
}

type JWTClaims struct {
	UserID    string `json:"id"`
	Email     string `json:"email"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	jwt.StandardClaims
}

func validateGlobals() error {
	if CookieSecret == "" {
		return errors.New("CookieSecret not set")
	}
	if SessionName == "" {
		return errors.New("SessionName not set")
	}
	if JWTSecretKey == "" {
		return errors.New("JWTSecretKey not set")
	}
	return nil
}

func IsAuthenticated(r *http.Request) (*JWTClaims, bool) {
	var token *jwt.Token
	var err error
	var jwtInt interface{}
	var tokenString string
	var ok bool

	if err := validateGlobals(); err != nil {
		log.Fatal(err)
	}

	store := sessions.NewCookieStore([]byte(CookieSecret))

	session, _ := store.Get(r, SessionName)
	if jwtInt, ok = session.Values["jwt"]; !ok {
		log.Println("Token not found")
		return nil, false
	}
	if tokenString, ok = jwtInt.(string); !ok {
		log.Println("Unknown token type")
		return nil, false
	}
	log.Println(tokenString)
	if token, err = jwt.ParseWithClaims(tokenString, &JWTClaims{}, func(tok *jwt.Token) (interface{}, error) {
		return []byte(JWTSecretKey), nil
	}); err != nil {
		log.Println("Unable to parse token")
		return nil, false
	}

	if claims, ok := token.Claims.(*JWTClaims); ok && token.Valid {
		log.Printf("%#v\n", claims)
		return claims, true
	}

	return nil, false
}

func Validate(ident runecms.Identity, email, password string) (users.User, error) {
	if err := runecms.Access(ident, Warden, "validate", "users"); err != nil {
		return users.User{}, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if err := validateGlobals(); err != nil {
		log.Fatal(err)
	}

	client := users.NewUsersProtobufClient(UsersService, &http.Client{})
	res, err := client.Authenticate(
		context.Background(),
		&users.AuthenticateUserRequest{
			Email:    email,
			Password: password,
		},
	)

	return *res.User, err
}

func ResetPassword(email string, dbConn string, fromEmail string, fromPassword string, emailHost string, supportEmail string) (bool, string) {
	result := false
	// var msg string

	// db connect
	db, err := sql.Open("postgres", dbConn)
	if err != nil {
		log.Fatal(err)
		return result, "Unable to connect to database"
	}

	rows, err := db.Query("SELECT userid, email FROM users WHERE email= $1", email)
	defer rows.Close()
	if err != nil {
		log.Fatal(err)
		return result, "Query failure"
	}

	var dbEmail, userID string

	// row count
	c := 0

	// verify email exists
	for rows.Next() {
		if err = rows.Scan(&userID, &dbEmail); err != nil {
			log.Fatal(err)
			return result, "Invalid email address"
		} else {
			c = c + 1
		}
	}

	if c == 0 {
		return false, "Invalid email address"
	}

	t := tempPassword(12)

	hash, err := bcrypt.GenerateFromPassword(t, 8)

	// send email
	type emailData struct {
		ToEmail  []string
		Password string
	}

	e := emailData{[]string{dbEmail}, string(t)}

	auth := smtp.PlainAuth("", fromEmail, fromPassword, emailHost)

	emailHost = emailHost + ":25"

	emailMsg := []byte("To:" + e.ToEmail[0] + "\r\n" +
		"Subject: Password Reset\r\n" +
		"\r\n" + "Your password has been reset. Your new password is " +
		string(t) + ". If you have any questions, please contact " + supportEmail + "\r\n")

	err = smtp.SendMail(emailHost, auth, fromEmail, e.ToEmail, emailMsg)
	if err != nil {
		log.Print(err)
		return false, "Unable to send recovery email at this time. Please try again later."
	}

	strHash := string(hash)

	// update database
	rows, err = db.Query("UPDATE users SET password = $1 WHERE userid= $2", strHash, userID)
	defer rows.Close()
	if err != nil {
		log.Print(err)
		return result, "Update query failure"
	}

	return true, "success"

}

func tempPassword(n int) []byte {
	const validLetters = "ZYXUVRSTPNMLHJKFEDCBA98765432"
	b := make([]byte, n)
	for i := range b {
		b[i] = validLetters[rand.Intn(len(validLetters))]
	}
	return b
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
