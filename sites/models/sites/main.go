// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package sites

import (
	"database/sql"
	"fmt"
	"time"

	"git.runecms.io/rune/runecms"

	"github.com/ory/ladon"
)

type Sites struct {
	read   *sql.Stmt
	create *sql.Stmt
	update *sql.Stmt
	delete *sql.Stmt
	Warden ladon.Warden
}

func NewSites(db *sql.DB, w ladon.Warden) (*Sites, error) {
	read, err := db.Prepare(`
		SELECT
			id,
			name,
			created_at,
			updated_at,
			deleted_at
		FROM
			sites
		WHERE
			id = ?`,
	)
	if err != nil {
		return &Sites{}, err
	}

	create, err := db.Prepare(`INSERT INTO sites(name) VALUES(?) RETURNING id`)
	if err != nil {
		return &Sites{}, err
	}

	update, err := db.Prepare(`UPDATE sites SET name=?, updated_at=current_timestamp WHERE id=?`)
	if err != nil {
		return &Sites{}, err
	}

	delete, err := db.Prepare(`UPDATE sites SET deleted_at=current_timestamp WHERE id=?`)
	if err != nil {
		return &Sites{}, err
	}

	return &Sites{
		read:   read,
		create: create,
		update: update,
		delete: delete,
		Warden: w,
	}, nil
}

type Site struct {
	Id        int64
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}

func (s *Sites) Read(ident runecms.Identity, id int64) (*Site, error) {
	if err := runecms.Access(ident, s.Warden, "read", "content:sites"); err != nil {
		return nil, err
	}

	out := Site{}
	err := s.read.QueryRow(id).Scan(
		&out.Id,
		&out.Name,
		&out.CreatedAt,
		&out.UpdatedAt,
		&out.DeletedAt,
	)
	if err == sql.ErrNoRows {
		err = fmt.Errorf("No sites with ID %v", id)
	}
	return &out, err
}

func (s *Sites) Create(ident runecms.Identity, site *Site) (int64, error) {

	if err := runecms.Access(ident, s.Warden, "create", "content:sites"); err != nil {
		return 0, err
	}

	var id int64
	err := s.create.QueryRow(site.Name).Scan(
		&id,
	)
	if err != nil {
		return 0, fmt.Errorf("Error creating site in database: %v", err)
	}
	return id, nil
}

func (s *Sites) Update(ident runecms.Identity, site *Site) (*Site, error) {

	if err := runecms.Access(ident, s.Warden, "update", "content:sites"); err != nil {
		return nil, err
	}

	out := Site{}
	res, err := s.update.Exec(site.Name, site.Id)
	if err != nil {
		return nil, err
	}
	if rows, _ := res.RowsAffected(); rows == 0 {
		return nil, fmt.Errorf("No sites with ID %d", site.Id)
	}
	err = s.read.QueryRow(site.Id).Scan(
		&out.Id,
		&out.Name,
		&out.CreatedAt,
		&out.UpdatedAt,
		&out.DeletedAt,
	)
	return &out, err
}

func (s *Sites) Delete(ident runecms.Identity, site *Site) error {
	if err := runecms.Access(ident, s.Warden, "delete", "content:sites"); err != nil {
		return err
	}

	res, err := s.delete.Exec(site.Id)
	if err != nil {
		return err
	}
	if rows, _ := res.RowsAffected(); rows == 0 {
		return fmt.Errorf("No sites with ID %d", site.Id)
	}
	return nil
}
