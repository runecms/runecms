// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"database/sql"
	"flag"
	"fmt"
	"log"
	"net"

	"golang.org/x/net/context"

	"github.com/ory/ladon"
	manager "github.com/ory/ladon/manager/sql"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"github.com/golang/protobuf/ptypes"
	ts "github.com/golang/protobuf/ptypes/timestamp"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/sites/models/sites"
	rpc "git.runecms.io/rune/runecms/sites/rpc"
)

var (
	port = flag.Int("port", 3000, "server port")

	dbhost   = flag.String("dbhost", "database.runecms.svc.cluster.local", "content database host")
	dbuser   = flag.String("dbuser", "user", "content database username")
	dbpass   = flag.String("dbpass", "password", "content database password")
	dbschema = flag.String("dbschema", "runecms", "content database schema")

	ladonHost   = flag.String("ladon-host", "database.runecms.svc.cluster.local", "ladon database host")
	ladonUser   = flag.String("ladon-user", "user", "ladon database user")
	ladonPass   = flag.String("ladon-pass", "password", "ladon database password")
	ladonSchema = flag.String("ladon-schema", "ladon", "ladon database schema")
)

type SitesRPCServer struct {
	sites *sites.Sites
}

func NewSitesRPCServer(db *sql.DB, w ladon.Warden) (*SitesRPCServer, error) {
	s, err := sites.NewSites(db, w)
	return &SitesRPCServer{sites: s}, err
}

func (s *SitesRPCServer) Read(ctx context.Context, readRequest *rpc.ReadRequest) (*rpc.ReadResponse, error) {
	response := &rpc.ReadResponse{
		CreatedAt: &ts.Timestamp{},
		UpdatedAt: &ts.Timestamp{},
		DeletedAt: &ts.Timestamp{},
	}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}

	site, err := s.sites.Read(user, readRequest.GetId())
	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}

	response.Id = site.Id
	response.Name = site.Name

	if response.CreatedAt, err = ptypes.TimestampProto(site.CreatedAt); err != nil {
		return response, status.Errorf(codes.DataLoss, "%v", err)
	}

	if response.UpdatedAt, err = ptypes.TimestampProto(site.UpdatedAt); err != nil {
		return response, status.Errorf(codes.DataLoss, "%v", err)
	}

	if response.DeletedAt, err = ptypes.TimestampProto(site.DeletedAt); err != nil {
		return response, status.Errorf(codes.DataLoss, "%v", err)
	}

	return response, status.Error(codes.OK, "")
}

func (s *SitesRPCServer) Create(ctx context.Context, createRequest *rpc.CreateRequest) (*rpc.CreateResponse, error) {
	response := &rpc.CreateResponse{}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}

	id, err := s.sites.Create(user, &sites.Site{Name: createRequest.GetName()})
	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}
	response.Id = id
	return response, status.Error(codes.OK, "")
}

func (s *SitesRPCServer) Update(ctx context.Context, updateRequest *rpc.UpdateRequest) (*rpc.UpdateResponse, error) {
	response := &rpc.UpdateResponse{}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}

	_, err = s.sites.Update(user, &sites.Site{
		Id:   updateRequest.GetId(),
		Name: updateRequest.GetName(),
	})

	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}

	return response, status.Error(codes.OK, "")
}

func (s *SitesRPCServer) Delete(ctx context.Context, deleteRequest *rpc.DeleteRequest) (*rpc.DeleteResponse, error) {
	response := &rpc.DeleteResponse{}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}
	err = s.sites.Delete(user, &sites.Site{Id: deleteRequest.GetId()})
	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}
	return response, status.Error(codes.OK, "")
}

func main() {
	flag.Parse()

	// Listen on port
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// Connect to database
	db, err := sql.Open(
		"postgres",
		fmt.Sprintf(
			"postgres://%s:%s@%s/%s?sslmode=disable",
			*dbuser,
			*dbpass,
			*dbhost,
			*dbschema,
		),
	)
	if err != nil {
		log.Fatalf("Unable to connect to content database: %v", err)
	}

	ladonDb, err := sqlx.Open(
		"postgres",
		fmt.Sprintf(
			"postgres://%s:%s@%s/%s?sslmode=disable",
			*ladonUser,
			*ladonPass,
			*ladonHost,
			*ladonSchema,
		),
	)
	if err != nil {
		log.Fatalf("Unable to connect to ladon database: %v", err)
	}

	warden := &ladon.Ladon{
		Manager: manager.NewSQLManager(ladonDb, nil),
	}

	// Initialize server
	sitesServer, err := NewSitesRPCServer(db, warden)
	if err != nil {
		log.Fatalf("Unable to initialize server: %v", err)
	}

	// Serve gRPC
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	rpc.RegisterSitesServer(grpcServer, sitesServer)
	reflection.Register(grpcServer)
	grpcServer.Serve(lis)
}
