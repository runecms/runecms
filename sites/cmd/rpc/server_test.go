// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"context"
	"log"
	"testing"
	"time"

	"google.golang.org/grpc/metadata"

	rpc "git.runecms.io/rune/runecms/sites/rpc"
	ptypes "github.com/golang/protobuf/ptypes"
	"github.com/google/go-cmp/cmp"
	"github.com/ory/ladon"
	manager "github.com/ory/ladon/manager/memory"
	"github.com/pborman/uuid"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func newWarden() ladon.Warden {
	warden := &ladon.Ladon{
		Manager: manager.NewMemoryManager(),
	}
	warden.Manager.Create(&ladon.DefaultPolicy{
		ID:          uuid.New(),
		Description: "Allow user:jason and group:admin to read, create, update, delete content:sites.",
		Subjects:    []string{"user:jason", "group:admin"},
		Effect:      ladon.AllowAccess,
		Resources:   []string{"content:sites"},
		Actions:     []string{"read", "create", "update", "delete"},
		Conditions:  ladon.Conditions{},
	})
	warden.Manager.Create(&ladon.DefaultPolicy{
		ID:          uuid.New(),
		Description: "Deny user:bob and group:editor from read, create, update, delete on content:sites.",
		Subjects:    []string{"user:bob", "group:editor"},
		Effect:      ladon.DenyAccess,
		Resources:   []string{"content:sites"},
		Actions:     []string{"read", "create", "update", "delete"},
		Conditions:  ladon.Conditions{},
	})
	return warden
}

func incomingContextMd(md map[string][]string) context.Context {
	return metadata.NewIncomingContext(context.Background(), metadata.MD(md))
}

func TestSitesRPCServer(t *testing.T) {
	t.Logf("Testing SitesRPCServer")
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("failed to open mock database: %v", err)
	}
	defer db.Close()
	warden := newWarden()

	expectedRead := mock.ExpectPrepare(`
		SELECT
			id,
			name,
			created_at,
			updated_at,
			deleted_at
		FROM
			sites
		WHERE
			id = \?`,
	)
	expectedCreate := mock.ExpectPrepare(`INSERT INTO sites\(name\) VALUES\(\?\) RETURNING id`)
	expectedUpdate := mock.ExpectPrepare(`UPDATE sites SET name=\?, updated_at=current_timestamp WHERE id=\?`)
	expectedDelete := mock.ExpectPrepare(`UPDATE sites SET deleted_at=current_timestamp WHERE id=\?`)

	server, err := NewSitesRPCServer(db, warden)
	if err != nil {
		t.Errorf("Failed to initialize new sites server: %v", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Unfulfilled database expectations: %v", err)
	}

	// ======Test Read Operations======
	t.Run("Read Operations", func(t *testing.T) {
		t.Run("Operation allowed, site found", func(t *testing.T) {
			now := time.Now()
			readRows := sqlmock.NewRows([]string{"id", "name", "created_at", "updated_at", "deleted_at"}).
				AddRow(1, "Times Free Press", now, now, time.Time{})
			expectedRead.ExpectQuery().WithArgs(1).WillReturnRows(readRows)
			readResponse, err := server.Read(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin", "author"},
				}),
				&rpc.ReadRequest{Id: 1},
			)
			if err != nil {
				t.Errorf("Error in SitesRPCServer.Read: %v", err)
			}
			if err = mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
			tsCreatedAt, err := ptypes.TimestampProto(now)
			if err != nil {
				t.Errorf("Error converting time to proto: %v", err)
			}
			tsUpdatedAt, err := ptypes.TimestampProto(now)
			if err != nil {
				t.Errorf("Error converting time to proto: %v", err)
			}
			tsDeletedAt, err := ptypes.TimestampProto(time.Time{})
			if err != nil {
				t.Errorf("Error converting time to proto: %v", err)
			}
			expect := &rpc.ReadResponse{
				Id:        1,
				Name:      "Times Free Press",
				CreatedAt: tsCreatedAt,
				UpdatedAt: tsUpdatedAt,
				DeletedAt: tsDeletedAt,
			}
			if !cmp.Equal(readResponse, expect) {
				t.Errorf("Response mismatch:\n%v", cmp.Diff(readResponse, expect))
			}
		})

		t.Run("Operation not allowed", func(t *testing.T) {
			_, err = server.Read(
				incomingContextMd(map[string][]string{
					"user":   []string{"bob"},
					"groups": []string{"admin"},
				}),
				&rpc.ReadRequest{Id: 1},
			)
			if err == nil {
				t.Errorf("Expected error in SitesRPCServer.Read, but got none")
			} else if err.Error() != "rpc error: code = PermissionDenied desc = The user is not authorized to access this content." {
				t.Errorf("Unexpected error in SitesRPCServer.Read: %v", err)
			}
		})

		t.Run("Operation allowed, site not found", func(t *testing.T) {
			expectedRead.ExpectQuery().WithArgs(2).WillReturnRows(
				sqlmock.NewRows([]string{"id", "name", "created_at", "updated_at", "deleted_at"}),
			)
			_, err := server.Read(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.ReadRequest{Id: 2},
			)
			if err == nil {
				t.Errorf("Expected error, got nil")
			}
			if err.Error() != "rpc error: code = Unknown desc = No sites with ID 2" {
				t.Errorf(`Expected error "rpc error: code = Unknown desc = No sites with ID 2" got "%s"`, err)
			}
			if err = mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
		})
	})

	t.Run("Create Operations", func(t *testing.T) {
		t.Run("Operation allowed, site created", func(t *testing.T) {
			expectedCreate.ExpectQuery().WithArgs("Times Free Press").WillReturnRows(
				sqlmock.NewRows([]string{"id"}).AddRow(1),
			)
			createResponse, err := server.Create(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.CreateRequest{Name: "Times Free Press"},
			)
			if err != nil {
				t.Errorf("Unexpected error: %v", err)
			}
			if err = mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
			expectedResponse := &rpc.CreateResponse{
				Id: 1,
			}
			if !cmp.Equal(createResponse, expectedResponse) {
				t.Errorf("Response mismatch:\n%v", cmp.Diff(createResponse, expectedResponse))
			}
		})

		t.Run("Operation not allowed", func(t *testing.T) {
			// Operation should not create the site
			_, err := server.Create(
				incomingContextMd(map[string][]string{
					"user":   []string{"bob"},
					"groups": []string{"admin"},
				}),
				&rpc.CreateRequest{Name: "Times Free Press"},
			)
			if err == nil {
				t.Errorf("Expected error but got none")
			} else if err.Error() != "rpc error: code = PermissionDenied desc = The user is not authorized to access this content." {
				t.Errorf("Unexpected error in SiteServer.Create: %v", err)
			}
		})
	})

	t.Run("Update Operations", func(t *testing.T) {
		t.Run("Operation allowed, site found and updated", func(t *testing.T) {
			expectedUpdate.ExpectExec().
				WithArgs("Times Free Press", 1).
				WillReturnResult(
					sqlmock.NewResult(
						0, // lastInsertId
						1, // rowsAffected
					),
				)

			now := time.Now()
			readRows := sqlmock.NewRows([]string{"id", "name", "created_at", "updated_at", "deleted_at"}).
				AddRow(1, "Times Free Press", now, now, time.Time{})
			expectedRead.ExpectQuery().WithArgs(1).WillReturnRows(readRows)

			_, err := server.Update(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.UpdateRequest{
					Id:   1,
					Name: "Times Free Press",
				},
			)
			if err != nil {
				t.Errorf("Unexpected error: %v", err)
			}
			if err = mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
		})

		t.Run("Operation not allowed", func(t *testing.T) {
			_, err := server.Update(
				incomingContextMd(map[string][]string{
					"user":   []string{"bob"},
					"groups": []string{"admin"},
				}),
				&rpc.UpdateRequest{
					Id:   1,
					Name: "Times Free Press",
				},
			)
			if err == nil {
				t.Errorf("Expected error in SitesRPCServer.Update, but got none")
			} else if err.Error() != "rpc error: code = PermissionDenied desc = The user is not authorized to access this content." {
				t.Errorf("Unexpected error in SitesRPCServer.Update: %v", err)
			}
		})

		t.Run("Operation allowed, site not found", func(t *testing.T) {
			expectedUpdate.ExpectExec().
				WithArgs("Times Free Press", 2).
				WillReturnResult(
					sqlmock.NewResult(
						0, // lastInsertId
						0, // rowsAffected
					),
				)
			_, err := server.Update(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.UpdateRequest{
					Id:   2,
					Name: "Times Free Press",
				},
			)
			if err == nil {
				t.Errorf("Expected error in SiteServer.Update, but got none")
			} else if err.Error() != "rpc error: code = Unknown desc = No sites with ID 2" {
				t.Errorf("Unexpected error in SitesRPCServer.Update: %v", err)
			}
			if err = mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
		})
	})

	t.Run("Delete Operations", func(t *testing.T) {
		t.Run("Operation allowed, site found and deleted", func(t *testing.T) {
			expectedDelete.ExpectExec().
				WithArgs(1).
				WillReturnResult(
					sqlmock.NewResult(
						0, // lastInsertId
						1, // rowsAffected
					),
				)
			_, err := server.Delete(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.DeleteRequest{Id: 1},
			)
			if err != nil {
				t.Errorf("Unexpected error: %v", err)
			}
			if err = mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
		})

		t.Run("Operation not allowed", func(t *testing.T) {
			_, err := server.Delete(
				incomingContextMd(map[string][]string{
					"user":   []string{"bob"},
					"groups": []string{"admin"},
				}),
				&rpc.DeleteRequest{Id: 1},
			)
			if err == nil {
				t.Errorf("Expected error in SitesRPCServer.Delete, but got none")
			} else if err.Error() != "rpc error: code = PermissionDenied desc = The user is not authorized to access this content." {
				t.Errorf("Unexpected error in SitesRPCServer.Delete: %v", err)
			}
		})

		t.Run("Operation allowed, site not found", func(t *testing.T) {
			expectedDelete.ExpectExec().
				WithArgs(2).
				WillReturnResult(
					sqlmock.NewResult(
						0, // lastInsertId
						0, // rowsAffected
					),
				)
			_, err := server.Delete(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.DeleteRequest{Id: 2},
			)
			if err == nil {
				t.Errorf("Expected error in SiteServer.Delete, but got none")
			} else if err.Error() != "rpc error: code = Unknown desc = No sites with ID 2" {
				t.Errorf("Unexpected error in SitesRPCServer.Delete: %v", err)
			}
			if err = mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
		})
	})
}
