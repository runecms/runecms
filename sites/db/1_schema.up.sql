CREATE TABLE sites(
  id serial NOT NULL,
  name varchar NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,
  deleted_at timestamp with time zone,
  CONSTRAINT id_primary_key PRIMARY KEY (id)
);
