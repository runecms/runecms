# Webhooks

The webhooks service is responsible for storing and maintaining webhook endpoints. Webhook endpoints are configured in the admin interface, or via. the gRPC / REST API, and providing an API to send data to these webhooks.

The webhooks service does not provide any categorization or filtering; all webhook transactions are sent to every webhook. The endpoints themselves are responsible for deciding, based on the "action", and "service" fields provided, if the transaction is relevant.

# HTTP Endpoints

| Method | Endpoint | Body | Response |
|--------|----------|------|----------|
| GET | /webhooks/:id | | Webhook{} |
| POST | /webhooks | Webhook{} | |

# Sending webhooks

```
import (
  "git.runecms.io/rune/runecms/webhooks/pkg/webhooks"
)

func myfunc() {
  // payload accepts an interface{}
  payload := map[string]string{"example":"data"}

  // ... initialize DB, warden, and identity
  go webhooks.NewTransaction(db, &warden, identity, "my-service-name", "my-action-name", payload)
}
```
