// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package webhooks

import (
	"encoding/json"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/webhooks/rpc"
)

// Distribute is a simple helper function will call the webhooks Twirp
// endpoint with the provided data, given the interface
func Distribute(client webhookspb.Webhooks, identity runecms.Identity, service string, action string, data interface{}) ([]*webhookspb.WebhookTransaction, error) {
	dataJSON, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	// Build the context for the request using the identity
	ctx, err := identity.ToRemoteTwirpContext()
	if err != nil {
		return nil, err
	}

	res, err := client.Distribute(ctx, &webhookspb.DistributeRequest{
		ServiceName: service,
		Action:      action,
		Data:        dataJSON,
	})

	if err != nil {
		return nil, err
	}

	return res.GetTransactions(), nil
}
