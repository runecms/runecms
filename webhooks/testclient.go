// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package webhooks

import (
	"context"

	"git.runecms.io/rune/runecms/webhooks/rpc"
)

type TestClient struct {
}

func (t *TestClient) Create(c context.Context, req *webhookspb.CreateWebhookRequest) (*webhookspb.CreateWebhookResponse, error) {
	return nil, nil
}

func (t *TestClient) Delete(c context.Context, req *webhookspb.DeleteWebhookRequest) (*webhookspb.DeleteWebhookResponse, error) {
	return nil, nil
}

func (t *TestClient) GetById(c context.Context, req *webhookspb.GetWebhookByIdRequest) (*webhookspb.GetWebhookByIdResponse, error) {
	return nil, nil
}

func (t *TestClient) Get(c context.Context, req *webhookspb.GetWebhookRequest) (*webhookspb.GetWebhookResponse, error) {
	return nil, nil
}

func (t *TestClient) Distribute(c context.Context, req *webhookspb.DistributeRequest) (*webhookspb.DistributeResponse, error) {
	return nil, nil
}
