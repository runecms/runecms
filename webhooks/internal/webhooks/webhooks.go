// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package webhooks

import (
	"database/sql"
	"log"
	"net/url"
	"strings"
	"time"

	"git.runecms.io/rune/runecms"
	models "git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/lib/pq"
	"github.com/ory/ladon"
)

func CreateWebhook(db *sql.DB, w ladon.Warden, ident runecms.Identity, name string, description string, webhookUrl string) (*models.Webhook, error) {
	log.Println("Checking permissions of identity: ", ident.User, ident.Groups)
	if err := runecms.Access(ident, w, "create", "webhooks:webhooks"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if name == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "name can not be empty")
	}
	name = strings.ToLower(name)

	if description == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "description can not be empty")
	}
	description = strings.ToLower(description)

	if webhookUrl == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "url can not be empty")
	}

	uri, err := url.Parse(webhookUrl)
	if err != nil {
		return nil, runecms.Errorf(runecms.InvalidArgument, "%s is not a valid url", webhookUrl)
	}

	row := db.QueryRow(query_create_webhook, name, description, uri.String())

	var (
		id        string
		createdAt time.Time
	)

	if err := row.Scan(&id, &createdAt); err != nil {
		log.Printf("Error inserting webhook %s. Error: %s", uri.String(), err.Error())
		return nil, runecms.Errorf(runecms.Internal, "Error inserting webhook %s. Error: %s", uri.String(), err.Error())
	}

	return GetWebhookById(db, w, ident, id)
}

func DeleteWebhook(db *sql.DB, w ladon.Warden, ident runecms.Identity, id string) (*time.Time, error) {
	log.Println("Checking permissions of identity: ", ident.User, ident.Groups)
	if err := runecms.Access(ident, w, "delete", "webhooks:webhooks"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	if id == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "Id can not be empty")
	}

	row := db.QueryRow(query_delete_webhook, id)

	var deletedAt pq.NullTime
	if err := row.Scan(&deletedAt); err != nil {
		log.Printf("Error deleting webhook %s. Error: %s", id, err.Error())
		return nil, runecms.Errorf(runecms.Internal, "Error deleting webhook %d. Error: %s", id, err.Error())
	}

	if deletedAt.Valid != true {
		return nil, runecms.Error(runecms.Internal, "Result for 'deleted_at' from database is NULL")
	}

	return &deletedAt.Time, nil
}

func GetWebhookById(db *sql.DB, w ladon.Warden, ident runecms.Identity, id string) (*models.Webhook, error) {
	if err := runecms.Access(ident, w, "read", "webhooks:webhooks"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	log.Printf("Retrieving webhook %s...", id)
	if id == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "Id can not be empty")
	}
	model := &models.Webhook{
		Id: id,
	}

	var deletedAt pq.NullTime
	row := db.QueryRow(query_select_webhook, id)
	if err := row.Scan(&model.Name, &model.Description, &model.Url, &model.CreatedAt, &deletedAt); err != nil {
		log.Printf("Error retrieving webhook %s. Error: %s", id, err.Error())
		return nil, runecms.Errorf(runecms.Internal, "Error retrieving webhook %d. Error: %s", id, err.Error())
	}
	if deletedAt.Valid {
		model.DeletedAt = deletedAt.Time.Format(time.RFC3339)
	}
	return model, nil
}

func GetWebhook(db *sql.DB, w ladon.Warden, ident runecms.Identity, name string) (*models.Webhook, error) {
	log.Println("Checking permissions of identity: ", ident.User, ident.Groups)
	if err := runecms.Access(ident, w, "read", "webhooks:webhooks"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	log.Printf("Retrieving webhook %s by name...", name)
	if name == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "Id can not be empty")
	}
	model := &models.Webhook{
		Name: name,
	}

	var deletedAt pq.NullTime
	row := db.QueryRow(query_select_webhook_by_name, name)
	if err := row.Scan(&model.Id, &model.Description, &model.Url, &model.CreatedAt, &deletedAt); err != nil {
		log.Printf("Error retrieving webhook %s. Error: %s", name, err.Error())
		return nil, runecms.Errorf(runecms.Internal, "Error retrieving webhook %d. Error: %s", name, err.Error())
	}
	if deletedAt.Valid {
		model.DeletedAt = deletedAt.Time.Format(time.RFC3339)
	}
	return model, nil
}
