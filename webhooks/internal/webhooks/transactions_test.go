// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package webhooks

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
	"time"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/internal/testutil"

	"github.com/google/go-cmp/cmp"
)

func init() {
	var err error
	warden = testutil.GetTestWarden()
	if err != nil {
		panic(err.Error())
	}

	valid_id = runecms.Be("valid", "admin")
	invalid_id = runecms.Be("invalid", "invalid")
}

func TestTransaction(t *testing.T) {
	wg := &sync.WaitGroup{}
	webhook_body := map[string]string{"test": "data"}
	const (
		// This should match the # of elements in the slice literal below
		webhook_count   = 5
		webhook_service = "test"
		webhook_action  = "example"
	)

	wg.Add(webhook_count)
	transactionHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer wg.Done()
		t.Logf("Received webhook...\n[%s] [%s] [%s]", r.Host, r.Method, r.URL.Path)
		data, _ := ioutil.ReadAll(r.Body)
		t.Log(string(data))
	})

	db := testutil.GetTestDb(t)
	httpServers := []*httptest.Server{
		httptest.NewServer(transactionHandler),
		httptest.NewServer(transactionHandler),
		httptest.NewServer(transactionHandler),
		httptest.NewServer(transactionHandler),
		httptest.NewServer(transactionHandler),
	}

	for _, v := range httpServers {
		defer v.Close()
	}

	_, err := db.Exec("DELETE FROM webhooks_transactions; DELETE FROM webhooks;")
	if err != nil {
		t.Fatalf("Could not empty webhooks table for transaction tests. Error: %s", err.Error())
	}

	// Create webhook entries...
	for i, v := range httpServers {
		t.Logf("Creating webhook %s", v.URL)
		_, err := CreateWebhook(db, warden, valid_id, fmt.Sprintf("webhook-transaction-test-%d", i), "actually testing the webhooks", v.URL)
		if err != nil {
			t.Fatalf("Could not set up test for transactions. Error: %s", err.Error())
		}
	}

	t.Run("Successful transaction", func(t *testing.T) {
		transactions, err := NewTransaction(db, warden, valid_id, webhook_service, webhook_action, webhook_body)
		if err != nil {
			t.Fatalf(err.Error())
		}

		if waitTimeout(wg, time.Second*15) {
			t.Errorf("Waitgroup timed out; NewTransaction did not complete it time. It is likely that the httptest.Server did not start correctly.")
		}
		t.Run("Verify transaction rows in db", func(t *testing.T) {
			if len(transactions) != len(httpServers) {
				t.Errorf("Returned list of transactions should be the same size as the number of webhooks. Transactions: %d, Servers: %d.\n%+v", len(transactions), len(httpServers), transactions)
			}
			rows, err := db.Query("SELECT initiated_by, service, action, payload, response_code, created_at FROM webhooks_transactions")
			if err != nil {
				t.Fatal(err.Error())
			}
			defer rows.Close()
			for rows.Next() {
				var (
					user          string
					service       string
					action        string
					payload       string
					response_code int64
					created_at    time.Time
				)
				if err := rows.Scan(&user, &service, &action, &payload, &response_code, &created_at); err != nil {
					t.Fatalf("Could not scan row for service %s, error: %s", service, err.Error())
				}
				if user != valid_id.User {
					t.Errorf("User should match %s, received %s", valid_id.User, user)
				}
				if cmp.Equal(created_at, time.Time{}) {
					t.Error("Created At returned empty")
				}
				if webhook_service != service {
					t.Error("webhook_service != service")
				}
				if webhook_action != action {
					t.Error("webhook_action != action")
				}
				if response_code != 200 {
					t.Error("response_code != 200")
				}
				if payload == "" {
					t.Error("Payload is empty")
				}
			}
		})
	})
}

// waitTimeout waits for the waitgroup for the specified max timeout.
// Returns true if waiting timed out.
func waitTimeout(wg *sync.WaitGroup, timeout time.Duration) bool {
	c := make(chan struct{})
	go func() {
		defer close(c)
		wg.Wait()
	}()
	select {
	case <-c:
		return false // completed normally
	case <-time.After(timeout):
		return true // timed out
	}
}
