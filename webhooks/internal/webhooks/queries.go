// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package webhooks

const (
	query_select_webhook = `
SELECT
	name,
	description,
	url,
	created_at,
	deleted_at
FROM
	webhooks
WHERE
	id=$1
	`
	query_select_webhook_by_name = `
SELECT
	id,
	description,
	url,
	created_at,
	deleted_at
FROM
	webhooks
WHERE
	name=$1
	`

	query_list_webhooks = `
SELECT
	id,
	name,
	description,
	url,
	created_at
FROM
	webhooks
WHERE
	deleted_at IS NULL
ORDER BY
	created_at DESC
	`

	query_delete_webhook = `
UPDATE
	webhooks
SET
	deleted_at=now()
WHERE
	id=$1
AND
	deleted_at IS NULL
RETURNING
	deleted_at
	`

	query_create_webhook = `
INSERT INTO webhooks (
	name,
	description,
	url
)
VALUES (
	$1,
	$2,
	$3
)
RETURNING id, created_at
`
	query_create_webhook_transaction = `
INSERT INTO webhooks_transactions (
	webhook_id,
	initiated_by,
	service,
	action,
	payload,
	response_body,
	response_code
)
VALUES (
	$1,
	$2,
	$3,
	$4,
	$5,
	$6,
	$7
)
RETURNING (
	id
)
	`
	query_update_webhook_transaction = `
UPDATE
	webhooks_transactions
SET (
	webhook_id,
	service,
	action,
	payload,
	response_body,
	response_code
) = (
	$1,
	$2,
	$3,
	$4,
	$5,
	$6
)
WHERE id = $7
RETURNING created_at, initiated_by
`
)
