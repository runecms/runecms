// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package webhooks

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"

	"git.runecms.io/rune/runecms"
	models "git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/ory/ladon"
)

func NewTransactionWithBytes(db *sql.DB, w ladon.Warden, ident runecms.Identity, service string, action string, data []byte) ([]*models.WebhookTransaction, error) {
	if err := runecms.Access(ident, w, "create", "webhooks:webhooks"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	if service == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "service can not be empty")
	}
	if action == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "action can not be empty")
	}
	log.Printf("Initiating transaction for service %s, action %s...", service, action)

	// Create the transaction(s)
	var (
		wg     sync.WaitGroup
		mt     = &sync.Mutex{}
		client = http.Client{
			Timeout: time.Duration(10 * time.Second),
		}
		webhooks              = []models.Webhook{}
		transactions          = map[string]models.Webhook{}
		completedTransactions = []*models.WebhookTransaction{}
	)

	// Select a list of webhooks to POST from the database
	rows, err := db.Query(query_list_webhooks)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		var model models.Webhook
		if err := rows.Scan(&model.Id, &model.Name, &model.Description, &model.Url, &model.CreatedAt); err != nil {
			return nil, runecms.Errorf(runecms.Internal, "Unable to retrieve webhook from multi-row select. Error: %s", err.Error())
		}

		webhooks = append(webhooks, model)
	}

	if err := rows.Err(); err != nil {
		log.Println(err.Error())
	}

	// Start by creating pending transaction entries in the database to be modified later
	for _, v := range webhooks {
		var id string
		// These shouldn't fail
		if err := db.QueryRow(query_create_webhook_transaction, v.Id, ident.User, service, "pending", "", "", 0).Scan(&id); err != nil {
			return nil, runecms.Error(runecms.Internal, err.Error())
		}
		transactions[id] = v
		log.Printf("Created webhook transaction in database; id: %s", v)
	}

	var errorlist []string
	// Send the POSTs, and then update the transaction accordingly
	for i, v := range transactions {
		// Build the payload body to send to the webhook locations
		formatted := &models.WebhookTransactionFormat{
			Id:          i,
			ServiceName: service,
			Action:      action,
			Data:        data,
		}
		wg.Add(1)
		go func(w models.Webhook, t *models.WebhookTransactionFormat) {
			defer wg.Done()
			payload, err := json.Marshal(t)
			if err != nil {
				log.Printf("Error while marshalling payload for transaction %s, error: %s", t.Id, err.Error())
				mt.Lock()
				errorlist = append(errorlist, err.Error())
				mt.Unlock()
				return
			}

			res, http_err := client.Post(w.Url, "application/json", bytes.NewBuffer(payload))
			// res will be nil if err is not, so store information elsewhere
			var (
				body string
				code int64
			)

			// http_err will most likely not be nil if the host could not be reached.
			if http_err != nil {
				body = http_err.Error()
				code = 0
			} else {
				buf := &bytes.Buffer{}
				buf.ReadFrom(res.Body)
				body = buf.String()
				code = int64(res.StatusCode)
			}
			ct := &models.WebhookTransaction{
				Id:           t.GetId(),
				WebhookId:    w.GetId(),
				ServiceName:  service,
				Action:       action,
				Payload:      string(payload),
				ResponseBody: body,
				ResponseCode: code,
			}

			row := db.QueryRow(query_update_webhook_transaction, w.Id, service, action, string(payload), body, code, t.Id)
			if err := row.Scan(&ct.CreatedAt, &ct.InitiatedBy); err != nil {
				log.Printf("Error adding update to transaction (service: %s, action: %s). Error: %s", service, action, err.Error())
				errorlist = append(errorlist, err.Error())
			}

			completedTransactions = append(completedTransactions, ct)
		}(v, formatted)
	}
	wg.Wait()
	if len(errorlist) != 0 {
		return nil, runecms.Errorf(runecms.Internal, strings.Join(errorlist, "\n"))
	}

	return completedTransactions, nil
}

func NewTransaction(db *sql.DB, w ladon.Warden, ident runecms.Identity, service string, action string, data interface{}) ([]*models.WebhookTransaction, error) {
	if err := runecms.Access(ident, w, "create", "webhooks:webhooks"); err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	dataBytes, err := json.Marshal(data)
	if err != nil {
		log.Printf("In webhook transaction '%s' for '%s', unable to marshal data into JSON. Error: %s", action, service, err.Error())
		return nil, runecms.Error(runecms.InvalidArgument, err.Error())
	}

	return NewTransactionWithBytes(db, w, ident, service, action, dataBytes)
}
