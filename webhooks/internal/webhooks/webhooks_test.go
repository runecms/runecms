// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package webhooks

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/internal/testutil"

	"github.com/google/go-cmp/cmp"
	"github.com/ory/ladon"
)

var (
	warden     ladon.Warden
	valid_id   runecms.Identity
	invalid_id runecms.Identity
)

func init() {
	var err error
	warden = testutil.GetTestWarden()
	if err != nil {
		panic(err.Error())
	}

	valid_id = runecms.Be("valid", "admin")
	invalid_id = runecms.Be("invalid", "invalid")
}

func TestNew(t *testing.T) {
	db := testutil.GetTestDb(t)
	httpserver := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	}))
	defer httpserver.Close()

	t.Run("New webhook", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			model, err := CreateWebhook(db, warden, valid_id, testutil.GetUUID(), "test create webhook", httpserver.URL)

			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Fatal("Returned model is nil")
			}

			// Make sure we were given an id
			if model.Id == "" {
				t.Error("Returned model does not have an id")
			}

			if cmp.Equal(model.CreatedAt, time.Time{}) {
				t.Error("Returned model's CreatedAt was not set")
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := CreateWebhook(db, warden, invalid_id, testutil.GetUUID(), "unauthorized webhook", httpserver.URL)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an empty name", func(t *testing.T) {
			_, err := CreateWebhook(db, warden, valid_id, "", "valid input", httpserver.URL)
			if err == nil {
				t.Error("Providing an emptystring for name should return an error")
			}
		})
		t.Run("Use an empty description", func(t *testing.T) {
			_, err := CreateWebhook(db, warden, valid_id, testutil.GetUUID(), "", httpserver.URL)
			if err == nil {
				t.Error("Providing an emptystring for description should return an error")
			}
		})
		t.Run("Use an empty url", func(t *testing.T) {
			_, err := CreateWebhook(db, warden, valid_id, testutil.GetUUID(), "valid description", "")
			if err == nil {
				t.Error("Providing an emptystring for url should return an error")
			}
		})
		// I'm commenting out this test because it's literally impossible
		// for me to find a URL that will return an error
		// t.Run("Use an invalid url", func(t *testing.T) {
		// 	_, err := CreateWebhook(db, &w, valid_id, testutil.GetUUID(), "valid description", "I'm| a|n i/\n::::::valid URL")
		// 	if err == nil {
		// 		t.Error("Providing an invalid url should return an error")
		// 	}

		// 	u, err := url.Parse("I'm| a|n i/\n::::::valid URL")
		// 	t.Log(u.String())
		// 	if err != nil {
		// 		t.Error(err.Error())
		// 	}
		// })

		t.Run("Duplicate names", func(t *testing.T) {
			name := testutil.GetUUID()
			_, err := CreateWebhook(db, warden, valid_id, name, "test create webhook", httpserver.URL)
			if err != nil {
				t.Error(err.Error())
			}

			_, err = CreateWebhook(db, warden, valid_id, name, "test create webhook", httpserver.URL)
			if err == nil {
				t.Error("Providing duplicate names should result in an error")
			}
		})
	})
}

func TestGet(t *testing.T) {
	db := testutil.GetTestDb(t)
	httpserver := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	}))
	defer httpserver.Close()

	model, err := CreateWebhook(db, warden, valid_id, testutil.GetUUID(), "get webhook", httpserver.URL)
	if err != nil {
		t.Fatal("Unable to set up test for Get")
	}

	t.Run("Get webhook", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			m, err := GetWebhookById(db, warden, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if model == nil {
				t.Error("Returned model is nil")
			}

			if cmp.Equal(model, m) != true {
				t.Errorf("models do not match\n%s\n", cmp.Diff(model, m))
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := GetWebhookById(db, warden, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := GetWebhookById(db, warden, invalid_id, "asdf")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})

}

func TestDelete(t *testing.T) {
	httpserver := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	}))
	defer httpserver.Close()

	db := testutil.GetTestDb(t)
	model, err := CreateWebhook(db, warden, valid_id, testutil.GetUUID(), "delete webhook", httpserver.URL)
	if err != nil {
		t.Fatal("Unable to set up test for Delete")
	}

	t.Run("Delete webhook", func(t *testing.T) {
		t.Run("Successful request", func(t *testing.T) {
			da, err := DeleteWebhook(db, warden, valid_id, model.Id)
			if err != nil {
				t.Errorf(err.Error())
			}

			if da == nil {
				t.Error("Returned deleted_at is nil")
			}
		})

		t.Run("Use an invalid identity", func(t *testing.T) {
			_, err := DeleteWebhook(db, warden, invalid_id, model.Id)
			if err == nil {
				t.Error("Providing an invalid identity should return an error")
			}
		})

		t.Run("Use an invalid id", func(t *testing.T) {
			_, err := DeleteWebhook(db, warden, invalid_id, "asdf")
			if err == nil {
				t.Error("Providing an invalid id should return an error")
			}
		})
	})
}
