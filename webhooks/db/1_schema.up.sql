CREATE TABLE webhooks(
  id          UUID    PRIMARY KEY                DEFAULT gen_random_uuid(),
  name        text    UNIQUE            NOT NULL,
  description text                      NOT NULL,
  url         text                      NOT NULL,
  created_at  TIMESTAMP WITH TIME ZONE  NOT NULL DEFAULT current_timestamp,
  deleted_at  TIMESTAMP WITH TIME ZONE
);

CREATE TABLE webhooks_transactions(
  id              UUID        PRIMARY KEY DEFAULT gen_random_uuid(),
  webhook_id      UUID        NOT NULL REFERENCES webhooks(id),
  initiated_by    text        NOT NULL,
  service         text        NOT NULL,
  action          text        NOT NULL,
  payload         text        NOT NULL,
  response_body   text        NOT NULL,
  response_code   integer     NOT NULL,
  created_at      TIMESTAMP   WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at      TIMESTAMP   WITH TIME ZONE
);
