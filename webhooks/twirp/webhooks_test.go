// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package webhookstwirp

import (
	"context"
	"strings"
	"testing"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/internal/testutil"
	// "git.runecms.io/rune/runecms/webhooks/pkg/models"
	// "git.runecms.io/rune/runecms/webhooks/pkg/webhooks"
	pb "git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/google/go-cmp/cmp"
)

var (
	valid_context   context.Context
	invalid_context context.Context
	valid_id        runecms.Identity
)

func init() {
	valid_id = runecms.Be("valid", "admin")
	invalid_id := runecms.Be("invalid", "")
	valid_context = valid_id.ToTwirpContext()
	invalid_context = invalid_id.ToTwirpContext()
}

func TestCreate(t *testing.T) {
	var (
		db     = testutil.GetTestDb(t)
		warden = testutil.GetTestWarden()
		server = &Server{
			Db:     db,
			Warden: warden,
		}
	)

	t.Run("Valid request", func(t *testing.T) {
		var (
			name        = strings.ToLower(testutil.GetUUID())
			description = strings.ToLower(testutil.GetUUID())
			url         = "example.com/" + testutil.GetUUID()
		)

		res, err := server.Create(valid_context, &pb.CreateWebhookRequest{
			Name:        name,
			Description: description,
			Url:         url,
		})

		if err != nil {
			t.Error(err.Error())
		}
		if res.GetWebhook().GetId() == "" {
			t.Error("Webhook response is empty")
		}

		expected := &pb.CreateWebhookResponse{
			Webhook: &pb.Webhook{
				Id:          res.GetWebhook().GetId(),
				Name:        name,
				Description: description,
				Url:         url,
				CreatedAt:   res.GetWebhook().GetCreatedAt(),
			},
		}

		if cmp.Equal(res, expected) != true {
			t.Errorf("Unexpected response. %s", cmp.Diff(res, expected))
		}
	})

	t.Run("Invalid requests", func(t *testing.T) {
		t.Run("Duplicate names", func(t *testing.T) {
			var (
				name        = testutil.GetUUID()
				description = testutil.GetUUID()
				url         = "example.com/" + testutil.GetUUID()
			)

			_, err := server.Create(valid_context, &pb.CreateWebhookRequest{
				Name:        name,
				Description: description,
				Url:         url,
			})

			if err != nil {
				t.Error(err.Error())
			}

			_, err = server.Create(valid_context, &pb.CreateWebhookRequest{
				Name:        name,
				Description: description,
				Url:         url,
			})

			if err == nil {
				t.Error("Providing a duplicate name to server.Create should return an error")
			}
		})
		t.Run("Empty name", func(t *testing.T) {
			var (
				name        = ""
				description = testutil.GetUUID()
				url         = "example.com/" + testutil.GetUUID()
			)

			_, err := server.Create(valid_context, &pb.CreateWebhookRequest{
				Name:        name,
				Description: description,
				Url:         url,
			})

			if err == nil {
				t.Error("Providing an empty name to server.Create should return an error")
			}
		})
		t.Run("Empty url", func(t *testing.T) {
			var (
				name        = testutil.GetUUID()
				description = testutil.GetUUID()
				url         = ""
			)

			_, err := server.Create(valid_context, &pb.CreateWebhookRequest{
				Name:        name,
				Description: description,
				Url:         url,
			})

			if err == nil {
				t.Error("Providing an empty url to server.Create should return an error")
			}
		})
	})

	t.Run("Invalid context", func(t *testing.T) {
		var (
			name        = testutil.GetUUID()
			description = testutil.GetUUID()
			url         = "example.com/" + testutil.GetUUID()
		)

		_, err := server.Create(invalid_context, &pb.CreateWebhookRequest{
			Name:        name,
			Description: description,
			Url:         url,
		})

		if err == nil {
			t.Error("Providing an invalid context to server.Create should return an error")
		}
	})
}

func TestDelete(t *testing.T) {
	var (
		db     = testutil.GetTestDb(t)
		warden = testutil.GetTestWarden()
		server = &Server{
			Db:     db,
			Warden: warden,
		}
	)
	t.Run("Valid request", func(t *testing.T) {
		var (
			name        = testutil.GetUUID()
			description = testutil.GetUUID()
			url         = "example.com/" + testutil.GetUUID()
		)

		res, err := server.Create(valid_context, &pb.CreateWebhookRequest{
			Name:        name,
			Description: description,
			Url:         url,
		})

		if err != nil {
			t.Fatal()
		}
		delRes, err := server.Delete(valid_context, &pb.DeleteWebhookRequest{
			Id: res.GetWebhook().GetId(),
		})

		if err != nil {
			t.Error(err.Error())
		}

		if delRes.GetWebhook().GetDeletedAt() == "" {
			t.Error("Response's DeletedAt is empty")
		}
	})

	t.Run("Invalid request", func(t *testing.T) {
		t.Run("Invalid id", func(t *testing.T) {
			_, err := server.Delete(valid_context, &pb.DeleteWebhookRequest{
				Id: "asdf",
			})

			if err == nil {
				t.Error("Giving Delete an invalid ID should return an error")
			}
		})
	})

	t.Run("Invalid context", func(t *testing.T) {
		var (
			name        = testutil.GetUUID()
			description = testutil.GetUUID()
			url         = "example.com/" + testutil.GetUUID()
		)

		res, err := server.Create(valid_context, &pb.CreateWebhookRequest{
			Name:        name,
			Description: description,
			Url:         url,
		})

		if err != nil {
			t.Fatal()
		}

		_, err = server.Delete(invalid_context, &pb.DeleteWebhookRequest{
			Id: res.GetWebhook().GetId(),
		})

		if err == nil {
			t.Error("Providing an invalid context to Delete should return an error")
		}
	})
}

func TestGet(t *testing.T) {
	var (
		db     = testutil.GetTestDb(t)
		warden = testutil.GetTestWarden()
		server = &Server{
			Db:     db,
			Warden: warden,
		}
	)
	t.Run("Valid request", func(t *testing.T) {
		var (
			name        = testutil.GetUUID()
			description = testutil.GetUUID()
			url         = "example.com/" + testutil.GetUUID()
		)

		res, err := server.Create(valid_context, &pb.CreateWebhookRequest{
			Name:        name,
			Description: description,
			Url:         url,
		})

		if err != nil {
			t.Fatal(err.Error())
		}
		if res == nil {
			t.Fatal("response is nil")
		}

		getRes, err := server.GetById(valid_context, &pb.GetWebhookByIdRequest{
			Id: res.GetWebhook().GetId(),
		})

		if err != nil {
			t.Error(err.Error())
		}
		if getRes.GetWebhook().GetCreatedAt() == "" {
			t.Error("Get returned an empty CreatedAt")
		}
		expected := &pb.GetWebhookByIdResponse{
			Webhook: &pb.Webhook{
				Id:          res.GetWebhook().GetId(),
				CreatedAt:   res.GetWebhook().GetCreatedAt(),
				Name:        strings.ToLower(name),
				Description: strings.ToLower(description),
				Url:         url,
				DeletedAt:   getRes.GetWebhook().GetDeletedAt(),
			},
		}

		if cmp.Equal(expected, getRes) != true {
			t.Errorf("Unexpected response\n%s", cmp.Diff(expected, getRes))
		}
	})

	t.Run("Invalid request", func(t *testing.T) {
		t.Run("Invalid ID", func(t *testing.T) {
			_, err := server.GetById(valid_context, &pb.GetWebhookByIdRequest{
				Id: "asdf",
			})

			if err == nil {
				t.Error("Giving Get an invalid ID should return an error")
			}
		})
	})

	t.Run("Invalid context", func(t *testing.T) {
		var (
			name        = testutil.GetUUID()
			description = testutil.GetUUID()
			url         = "example.com/" + testutil.GetUUID()
		)

		res, err := server.Create(valid_context, &pb.CreateWebhookRequest{
			Name:        name,
			Description: description,
			Url:         url,
		})

		if err != nil {
			t.Fatal()
		}

		_, err = server.GetById(invalid_context, &pb.GetWebhookByIdRequest{
			Id: res.GetWebhook().GetId(),
		})

		if err == nil {
			t.Error("Providing an invalid context to Get should return an error")
		}
	})
}
