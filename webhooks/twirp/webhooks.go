// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package webhookstwirp

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/webhooks/internal/webhooks"
	pb "git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) Create(c context.Context, req *pb.CreateWebhookRequest) (*pb.CreateWebhookResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := webhooks.CreateWebhook(s.Db, s.Warden, user, req.GetName(), req.GetDescription(), req.GetUrl())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.CreateWebhookResponse{
		Webhook: model,
	}, nil
}

func (s *Server) Delete(c context.Context, req *pb.DeleteWebhookRequest) (*pb.DeleteWebhookResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	_, err = webhooks.DeleteWebhook(s.Db, s.Warden, user, req.GetId())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	model, err := webhooks.GetWebhookById(s.Db, s.Warden, user, req.GetId())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.DeleteWebhookResponse{
		Webhook: model,
	}, nil
}

func (s *Server) GetById(c context.Context, req *pb.GetWebhookByIdRequest) (*pb.GetWebhookByIdResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := webhooks.GetWebhookById(s.Db, s.Warden, user, req.GetId())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetWebhookByIdResponse{
		Webhook: model,
	}, nil
}

func (s *Server) Get(c context.Context, req *pb.GetWebhookRequest) (*pb.GetWebhookResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := webhooks.GetWebhook(s.Db, s.Warden, user, req.GetName())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetWebhookResponse{
		Webhook: model,
	}, nil
}

func (s *Server) Distribute(c context.Context, req *pb.DistributeRequest) (*pb.DistributeResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	transactions, err := webhooks.NewTransaction(s.Db, s.Warden, user, req.GetServiceName(), req.GetAction(), req.GetData())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.DistributeResponse{
		Transactions: transactions,
	}, nil
}
