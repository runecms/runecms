// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package webhookstwirp

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"git.runecms.io/rune/runecms/internal/twirputil"
	pb "git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/ory/ladon"
)

type Server struct {
	Db     *sql.DB
	Warden ladon.Warden
}

func NewServer(db *sql.DB, w ladon.Warden) *Server {
	return &Server{
		Db:     db,
		Warden: w,
	}
}

func (s *Server) Start(port int) error {
	log.Printf("Starting the webhooks service on 0.0.0.0:%d\n", port)
	var (
		webhooksHandler = pb.NewWebhooksServer(s, nil)
		r               = http.NewServeMux()
	)

	r.Handle(pb.WebhooksPathPrefix, twirputil.AddMiddleware(webhooksHandler))

	return http.ListenAndServe(fmt.Sprintf(":%d", port), r)
}
