.PHONY: include-test
include-test:
ifneq ("", "$(shell go list ./... 2>/dev/null)")
	@go test -timeout 2m ./...
endif
