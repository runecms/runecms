.PHONY: include-migrate
include-migrate: $(MIGRATE_CLI)
	$(MIGRATE_CLI) -database "$(MIGRATE_DATABASE)" -path $(MIGRATIONS_DIR) up

# make migrate-to TARGET=version
.PHONY: include-migrate-to
include-migrate-to: $(MIGRATE_CLI)
	$(MIGRATE_CLI) -database "$(MIGRATE_DATABASE)" -path $(MIGRATIONS_DIR) goto $(TARGET)

$(MIGRATE_CLI):
	$(info $(M) MIGRATE_CLI ($(MIGRATE_CLI)) not found, building...)
	@go get -u -d github.com/mattes/migrate/cli github.com/cockroachdb/cockroach-go/crdb
	@go build -tags 'cockroachdb' -o $(MIGRATE_CLI) github.com/mattes/migrate/cli

$(MIGRATE_DOCKER_CLI): $(BUILD_DIR)
	$(info $(M) MIGRATE_DOCKER_CLI ($(MIGRATE_DOCKER_CLI)) not found, building...)
	@go get -u -d github.com/mattes/migrate/cli github.com/cockroachdb/cockroach-go/crdb
	@GOOS=$(GOOS) GOARCH=$(GOARCH) go build -tags 'cockroachdb' -a -o ${MIGRATE_DOCKER_CLI} github.com/mattes/migrate/cli

