###############################################################################
# Build Protocol Buffer Output
#
# Collects all files matching $(RPC_DIR)/*.proto and compiles them.
###############################################################################

.PHONY: include-build-twirp
include-build-twirp: $(GOPATH)/bin/protoc-go-inject-tag
ifneq ("","$(wildcard $(RPC_DIR))")
	$(info $(M) building protos...)
	@protoc -I $(RPC_DIR) --proto_path=$(GOPATH)/src:. --twirp_out=$(RPC_DIR) --go_out=$(RPC_DIR) $(PROTOS)
	protoc-go-inject-tag -input=$(RPC_DIR)/rpc.pb.go
endif

$(GOPATH)/bin/protoc-go-inject-tag:
	@go get github.com/favadi/protoc-go-inject-tag
