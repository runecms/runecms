.PHONY: include-build-yaml
include-build-yaml: $(ENVSUBST_BIN)
ifneq ("","$(wildcard $(K8S_DIR))")
	@mkdir -p $(BUILD_DIR)/kubernetes
ifneq ("","$(wildcard $(K8S_DIR)/*.yaml)")
	@cp $(K8S_DIR)/*.yaml $(BUILD_DIR)/kubernetes
endif
	$(info $(M) building yaml...)
	for tmpl in $(K8S_TMPL); do \
		VERSION=$(VERSION) \
		VERSION_SED=$(shell echo $(VERSION) | sed "s/\./-/g") \
		SERVICE=$(SERVICE) \
		DOCKER_REGISTRY_DOMAIN=$(DOCKER_REGISTRY_DOMAIN) \
		DOCKER_IMAGE=$(DOCKER_IMAGE) \
		PACKAGE=$(PACKAGE) \
		$(ENVSUBST_BIN) $(K8S_DIR)/$$tmpl "$(BUILD_DIR)/kubernetes/$${tmpl%.tmpl}" ; \
	done
endif

.PHONY: include-deploy
include-deploy: deploy-docker build-yaml
ifneq ("","$(wildcard $(K8S_DIR))")
	$(info $(M) deploy...)
	kubectl apply -f $(BUILD_DIR)/kubernetes
endif
