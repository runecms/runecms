.PHONY: include-docker
include-docker:
ifneq ("","$(wildcard $(DOCKERFILE))")
	@$(MAKE) -f $(THIS_FILE) deploy-docker
else
	$(info $(M) $(DOCKERFILE) not found, skipping...)
endif

.PHONY: include-build-docker
include-build-docker: build $(BUILD_DIR) $(MIGRATE_CLI)
ifneq ("","$(wildcard $(DOCKERFILE))")
	$(info $(M) dockerizing...)
	cp ${DOCKERFILE} ${BUILD_DIR}/
	cp ${MIGRATE_CLI} ${BUILD_DIR}/bin/migrate
ifneq ("","$(wildcard $(MIGRATIONS_DIR))")
	@cp -r ${MIGRATIONS_DIR} ${BUILD_DIR}/
endif
	docker build -t "${DOCKER_IMAGE}" ${BUILD_DIR}
else
	$(info $(M) $(DOCKERFILE) not found, skipping...)
endif

.PHONY: include-deploy-docker
include-deploy-docker: build-docker
ifneq ("", "$(wildcard $(DOCKERFILE))")
	gcloud docker -- push "${DOCKER_IMAGE}"
endif
