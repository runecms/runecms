.PHONY: lint
include-lint:
	$(info $(M) linting code...)
	@$(MAKE) -f $(THIS_FILE) golint
	@$(MAKE) -f $(THIS_FILE) govet
	@$(MAKE) -f $(THIS_FILE) gofmt

$(GOIMPORTS):
	@go get golang.org/x/tools/cmd/goimports

.PHONY: gofmt
include-gofmt: $(GOIMPORTS)
	@ret=0 && for d in $$(go list -f '{{.Dir}}' ./... | grep -v $(RPC_DIR)); do \
		$(GOFMT) -l -w $$d/*.go || ret=$$? ;\
	done ; exit $$ret

$(GOLINT):
	@go get github.com/golang/lint/golint

.PHONY: golint
include-golint: $(GOLINT)
	@for f in $(shell go list ./... | grep -v $(RPC_DIR)); do $(GOLINT) $$f; done

.PHONY: govet
include-govet:
	@for f in $(shell go list ./... | grep -v $(RPC_DIR)); do go vet $$f; done

