###############################################################################
# Build Protocol Buffer Output
#
# Collects all files matching $(RPC_DIR)/*.proto and compiles them.
###############################################################################

.PHONY: include-build-grpc
include-build-grpc:
ifneq ("","$(wildcard $(RPC_DIR))")
	$(info $(M) building protos...)
	@protoc -I $(RPC_DIR) --go_out=plugins=grpc:$(RPC_DIR) $(PROTOS)
endif
