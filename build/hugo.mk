.PHONY: include-hugo-setup
include-hugo-setup:
	JULES_PATH=${RUNE_ROOT_PATH} go run .scripts/import.go

.PHONY: include-hugo-build
include-hugo-build: hugo-setup $(HUGO_BIN) $(BUILD_DIR)
	${HUGO_BIN} -d ${BUILD_DIR}/public

.PHONY: include-hugo-serve
include-hugo-serve: $(HUGO_BIN)
	${HUGO_BIN} server --watch

$(HUGO_BIN):
	@go get -v github.com/gohugoio/hugo
