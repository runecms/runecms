###############################################################################
# Build Executables
#
# Runs `go build` for all folders under $(CMD_DIR) and puts the binaries in
# ${ROOTFS}/bin/
###############################################################################

.PHONY: include-build
include-build: | clean $(BUILD_DIR)
ifneq ("","$(wildcard $(CMD_DIR))")
	$(info $(M) building executables...)
	for cmd in $(CMDS); do \
		GOOS=$(GOOS) GOARCH=$(GOARCH) go build -a -ldflags "-X $$cmd.VERSION=${VERSION} -X $$cmd.COMMIT=${COMMIT} -X $$cmd.BUILDDATE=${DATE}" \
		-o ${BUILD_DIR}/bin/$$cmd ${CMD_DIR}/$$cmd; \
	done
endif

.PHONY: include-build-static
include-build-static: | clean $(BUILD_DIR)
ifneq ("","$(wildcard $(CMD_DIR))")
	$(info $(M) building executables without CGO...)
	for cmd in $(CMDS); do \
		CGO_ENABLED=0 GOOS=$(GOOS) GOARCH=$(GOARCH) go build -a -ldflags "-w -X $$cmd.VERSION=${VERSION} -X $$cmd.COMMIT=${COMMIT} -X $$cmd.BUILDDATE=${DATE}" \
		-o ${BUILD_DIR}/bin/$$cmd ${CMD_DIR}/$$cmd; \
	done
endif

$(ROOTFS):
	@mkdir $(ROOTFS)

$(BUILD_DIR): $(ROOTFS)
	@cp -r $(ROOTFS) $(BUILD_DIR)
	@mkdir -p $(BUILD_DIR)/bin

.PHONY: include-clean
include-clean:
	@rm -rf $(BUILD_DIR)
