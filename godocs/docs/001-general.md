+++
title = "about"
weight = 1
+++

## Godocs

The godocs for Rune can be located here: https://godocs.gorune.io/pkg/git.runecms.io/rune/runecms/
