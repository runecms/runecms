// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package metadatapb

import (
	"encoding/json"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMarshal(t *testing.T) {
	type Case struct {
		Description string
		Input       MetadataSet
		Output      []byte
	}

	cases := []Case{
		{
			Description: "Basic valid input",
			Input: MetadataSet{
				Keys:     []string{"rune:test", "dsi.example.com:1234"},
				Data:     "example metadata hombre",
				ObjectId: "obj-id",
			},
			Output: []byte(`{"rune":"test","dsi.example.com":"1234","metadata":"example metadata hombre","metadata_object_id":"obj-id"}`),
		},
		{
			Description: "Input with multiple ':'",
			Input: MetadataSet{
				Keys:     []string{"rune:1234", "dsi.example.com:1234-1234-1234:test"},
				Data:     "example metadata hombre",
				ObjectId: "obj-id",
			},
			Output: []byte(`{"rune":"1234","dsi.example.com":["1234-1234-1234","test"],"metadata":"example metadata hombre","metadata_object_id":"obj-id"}`),
		},
		{
			Description: "Input with multiple keys with multiple values, some values empty",
			Input: MetadataSet{
				Keys:     []string{"rune:1234:test:value:4321:::test", "dsi.example.com:1234-1234-1234::test::"},
				Data:     "example metadata hombre",
				ObjectId: "obj-id",
			},
			Output: []byte(`{"rune":["1234","test","value","4321","","","test"],"dsi.example.com":["1234-1234-1234","","test","",""],"metadata":"example metadata hombre","metadata_object_id":"obj-id"}`),
		},
		{
			Description: "Input with multiple keys with multiple values",
			Input: MetadataSet{
				Keys:     []string{"rune:1234:test:value:4321", "dsi.example.com:1234-1234-1234:test:6543"},
				Data:     "example metadata hombre",
				ObjectId: "obj-id",
			},
			Output: []byte(`{"rune":["1234","test","value","4321"],"dsi.example.com":["1234-1234-1234","test","6543"],"metadata":"example metadata hombre","metadata_object_id":"obj-id"}`),
		},
		{
			Description: "Input with duplicate values",
			Input: MetadataSet{
				Keys:     []string{"rune:1234", "dsi.example.com:1234"},
				Data:     "example metadata hombre",
				ObjectId: "obj-id",
			},
			Output: []byte(`{"rune":"1234","dsi.example.com":"1234","metadata":"example metadata hombre","metadata_object_id":"obj-id"}`),
		},
		{
			Description: "Input with no ':'",
			Input: MetadataSet{
				Keys:     []string{"rune"},
				Data:     "example metadata hombre",
				ObjectId: "obj-id",
			},
			Output: []byte(`{"rune":"","metadata":"example metadata hombre","metadata_object_id":"obj-id"}`),
		},
		{
			Description: "Input with duplicate values",
			Input: MetadataSet{
				Keys:     []string{"rune", "dsi.example.com:1234"},
				Data:     "example metadata hombre",
				ObjectId: "obj-id",
			},
			Output: []byte(`{"rune":"","dsi.example.com":"1234","metadata":"example metadata hombre","metadata_object_id":"obj-id"}`),
		},
		{
			Description: "Input with no keys",
			Input: MetadataSet{
				Keys:     []string{},
				Data:     "example metadata hombre",
				ObjectId: "obj-id",
			},
			Output: []byte(`{"metadata":"example metadata hombre","metadata_object_id":"obj-id"}`),
		},
		{
			Description: "Input with emptystring key",
			Input: MetadataSet{
				Keys:     []string{""},
				Data:     "example metadata hombre",
				ObjectId: "obj-id",
			},
			Output: []byte(`{"metadata":"example metadata hombre","metadata_object_id":"obj-id"}`),
		},
	}

	error_cases := []Case{}

	for _, v := range cases {
		t.Run(v.Description, func(t *testing.T) {
			data, err := json.Marshal(v.Input)
			if err != nil {
				t.Fatal(err.Error())
			}

			var (
				v1 map[string]interface{}
				v2 map[string]interface{}
			)

			json.Unmarshal(data, &v1)
			json.Unmarshal(v.Output, &v2)
			if cmp.Equal(v1, v2) != true {
				t.Errorf("Output does not match input:\nReceived: %s\nExpected: %s\n", v1, v2)
				t.Errorf("JSON:\nReceived: %s\nExpected: %s\n", string(data), string(v.Output))
			}
		})
	}

	t.Run("Check invalid marshalling", func(t *testing.T) {
		for _, v := range error_cases {
			t.Logf("Testing %s...", v.Description)
			value, err := json.Marshal(v.Input)
			if err == nil {
				t.Errorf("input %s should return an error.\nReceived:%s", v.Input, string(value))
			}
		}
	})
}
