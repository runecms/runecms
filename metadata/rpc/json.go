// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package metadatapb

import (
	"encoding/json"
	"strings"
)

func (m MetadataSet) MarshalJSON() ([]byte, error) {
	data := map[string]interface{}{}
	for _, v := range m.Keys {
		s := strings.Split(v, ":")
		switch len(s) {
		case 1:
			if s[0] == "" {
				break
			}
			data[s[0]] = ""
		case 2:
			data[s[0]] = s[1]
		default:
			data[s[0]] = s[1:]
		}
	}
	data["metadata"] = string(m.Data)
	data["metadata_object_id"] = m.ObjectId
	return json.Marshal(&data)
}
