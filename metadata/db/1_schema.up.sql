CREATE TABLE metadata_objects(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  data BYTEA NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp
);

CREATE TABLE metadata_keys(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  key TEXT NOT NULL,
  object_id UUID NOT NULL REFERENCES metadata_objects(id),
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp
);
