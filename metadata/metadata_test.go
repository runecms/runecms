// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package metadata

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFindKey(t *testing.T) {
	keys := []string{"test:123", "test2:321", "test3:4321", "duplicate:123", "duplicate:321"}
	t.Run("Valid request", func(t *testing.T) {
		expected := []string{"321"}
		val, err := FindKey("test2", keys)
		if err != nil {
			t.Fatal("When sending a valid request to FindKey, err should be nil")
		}

		if cmp.Equal(expected, val) != true {
			t.Fatalf("Expected %v but got %v", expected, val)
		}
	})
	t.Run("Duplicate keys", func(t *testing.T) {
		expected := []string{"123", "321"}
		val, err := FindKey("duplicate", keys)
		if err != nil {
			t.Fatal("When sending a valid request to FindKey, err should be nil")
		}

		if cmp.Equal(expected, val) != true {
			t.Fatalf("Expected %v but got %v", expected, val)
		}
	})
	t.Run("Key does not exist", func(t *testing.T) {
		_, err := FindKey("nonexistent", keys)
		if err == nil {
			t.Fatal("When searching for a nonexistent key, FindKey should return an error")
		}
	})
}
