// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package metadatatwirp

import (
	"fmt"
	"log"
	"net/http"

	"git.runecms.io/rune/runecms/internal/twirputil"
	pb "git.runecms.io/rune/runecms/metadata/rpc"
	"github.com/go-redis/redis"

	"github.com/ory/ladon"
)

type Server struct {
	Redis  *redis.Client
	Warden ladon.Warden
}

func NewServer(r *redis.Client, w ladon.Warden) *Server {
	return &Server{
		Redis:  r,
		Warden: w,
	}
}

func (s *Server) Start(port int) error {
	log.Printf("Starting the metadata service on 0.0.0.0:%d\n", port)
	var (
		metadataHandler = pb.NewMetadataServer(s, nil)
		r               = http.NewServeMux()
	)

	r.Handle(pb.MetadataPathPrefix, twirputil.AddMiddleware(metadataHandler))
	return http.ListenAndServe(fmt.Sprintf(":%d", port), r)
}
