// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package metadatatwirp

import (
	"context"
	"strings"
	"testing"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/metadata/internal/metadata"
	pb "git.runecms.io/rune/runecms/metadata/rpc"

	"github.com/google/go-cmp/cmp"
)

var (
	valid_context   context.Context
	invalid_context context.Context
	valid_id        runecms.Identity
)

func init() {
	valid_id = runecms.Be("valid", "admin")
	invalid_id := runecms.Be("invalid", "user")

	valid_context = valid_id.ToTwirpContext()

	invalid_context = invalid_id.ToTwirpContext()
}

func TestGet(t *testing.T) {
	// Create a new metadata item to call Get on
	var (
		key    = testutil.GetUUID()
		value  = testutil.GetUUID()
		kvp    = strings.ToLower(key + ":" + value)
		data   = testutil.GetUUID()
		db     = testutil.GetTestRedis(t)
		warden = testutil.GetTestWarden()
		s      = &Server{
			Redis:  db,
			Warden: warden,
		}
	)

	mdset, err := metadata.Set(context.Background(), db, warden, valid_id, &pb.MetadataItem{
		Key:  kvp,
		Data: data,
	})
	if err != nil {
		t.Fatalf("Unable to set up test for grpc.Get. Error: %s", err.Error())
	}

	t.Run("Valid Get", func(t *testing.T) {
		res, err := s.Get(valid_context, &pb.GetMetadataRequest{
			Key: kvp,
		})

		if err != nil {
			t.Error(err.Error())
		}
		expected := &pb.GetMetadataResponse{
			Metadata: &pb.MetadataSet{
				Keys:     []string{kvp},
				Data:     data,
				ObjectId: mdset.ObjectId,
			},
		}
		if cmp.Equal(res, expected) != true {
			t.Fatalf("Unexpected response.%s\n", cmp.Diff(res, expected))
		}
	})

	t.Run("Invalid Get", func(t *testing.T) {
		_, err := s.Get(valid_context, &pb.GetMetadataRequest{
			Key: "idontexist",
		})

		if err == nil {
			t.Error("s.Get should return an error when given an invalid key")
		}
	})

	t.Run("Invalid context", func(t *testing.T) {
		_, err := s.Get(invalid_context, &pb.GetMetadataRequest{
			Key: kvp,
		})

		if err == nil {
			t.Error("s.Get should return an error when given an invalid key")
		}
	})
}

func TestSet(t *testing.T) {
	// Create a new metadata item to call Get on
	var (
		key    = testutil.GetUUID()
		value  = testutil.GetUUID()
		kvp    = strings.ToLower(key + ":" + value)
		data   = testutil.GetUUID()
		db     = testutil.GetTestRedis(t)
		warden = testutil.GetTestWarden()
		s      = &Server{
			Redis:  db,
			Warden: warden,
		}
	)

	t.Run("Valid Set", func(t *testing.T) {
		res, err := s.Set(valid_context, &pb.SetMetadataRequest{
			Key:  kvp,
			Data: data,
		})

		if err != nil {
			t.Error(err.Error())
		}

		expected := &pb.SetMetadataResponse{
			Metadata: &pb.MetadataSet{
				Keys:     []string{kvp},
				Data:     data,
				ObjectId: res.GetMetadata().GetObjectId(),
			},
		}

		if cmp.Equal(res, expected) != true {
			t.Fatalf("Unexpected response.%s\n", cmp.Diff(res, expected))
		}

		t.Run("Use that same ObjectId", func(t *testing.T) {
			var (
				kvp2 = strings.ToLower(testutil.GetUUID() + ":" + testutil.GetUUID())
			)
			res2, err := s.Set(valid_context, &pb.SetMetadataRequest{
				Key:      kvp2,
				ObjectId: res.GetMetadata().GetObjectId(),
			})

			if err != nil {
				t.Error(err.Error())
			}

			expected2 := &pb.SetMetadataResponse{
				Metadata: &pb.MetadataSet{
					Keys:     []string{kvp2, kvp},
					Data:     data,
					ObjectId: res.GetMetadata().GetObjectId(),
				},
			}

			if cmp.Equal(res2, expected2) != true {
				t.Fatalf("Unexpected response.%s\n", cmp.Diff(res2, expected2))
			}
		})
	})

	t.Run("Invalid Set", func(t *testing.T) {
		_, err := s.Set(valid_context, &pb.SetMetadataRequest{
			Key:  "idonthaveacolon",
			Data: data,
		})

		if err == nil {
			t.Error("s.Set should return an error when given an invalid key")
		}
	})

	t.Run("Invalid context", func(t *testing.T) {
		_, err := s.Set(invalid_context, &pb.SetMetadataRequest{
			Key:  kvp,
			Data: data,
		})

		if err == nil {
			t.Error("s.Set should return an error when given an invalid context")
		}
	})
}
