// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package metadatatwirp

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/metadata/internal/metadata"
	pb "git.runecms.io/rune/runecms/metadata/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) Get(c context.Context, req *pb.GetMetadataRequest) (*pb.GetMetadataResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := metadata.Get(c, s.Redis, s.Warden, user, req.GetKey())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetMetadataResponse{
		Metadata: model,
	}, nil
}

func (s *Server) Set(c context.Context, req *pb.SetMetadataRequest) (*pb.SetMetadataResponse, error) {
	user, err := runecms.IdentityFromTwirp(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := metadata.Set(c, s.Redis, s.Warden, user, &pb.MetadataItem{
		Key:      req.GetKey(),
		ObjectId: req.GetObjectId(),
		Data:     req.GetData(),
	})

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.SetMetadataResponse{
		Metadata: model,
	}, nil
}

func (s *Server) Delete(c context.Context, req *pb.DeleteMetadataRequest) (*pb.DeleteMetadataResponse, error) {
	user, err := runecms.IdentityFromRPC(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	err = metadata.Delete(c, s.Redis, s.Warden, user, req.GetKey())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	model, err := metadata.Get(c, s.Redis, s.Warden, user, req.GetKey())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.DeleteMetadataResponse{
		Metadata: model,
	}, nil
}
