// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"flag"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/metadata/twirp"

	"github.com/go-redis/redis"
	"github.com/golang/glog"
	_ "github.com/lib/pq"
	"github.com/opentracing/opentracing-go"
	"github.com/ory/ladon"
	jaegercfg "github.com/uber/jaeger-client-go/config"
	manager "github.com/wehco/ladon-community/manager/redis"
)

func init() {
	flag.Parse()
}

func main() {
	cfg, err := jaegercfg.FromEnv()
	if err != nil {
		// parsing errors might happen here, such as when we get a string where we expect a number
		glog.Fatalf("Could not parse Jaeger env vars: %s", err.Error())
	}

	tracer, closer, err := cfg.NewTracer()
	if err != nil {
		glog.Fatalf("Could not initialize jaeger tracer: %s", err.Error())
	}
	defer closer.Close()

	opentracing.SetGlobalTracer(tracer)

	v := flag.Lookup("v")
	glog.Infof("Verbosity %s enabled...", v.Value.String())

	config, err := runecms.GetConfig()
	if err != nil {
		glog.Fatalln("Unable to get config. Error:", err.Error())
	}
	glog.Infoln("Connecting to redis...")

	opt, err := redis.ParseURL(config.RuneRedisURL)
	if err != nil {
		glog.Fatalln("Error parsing redis URL. Error:", err.Error())
	}
	runeRedisClient := redis.NewClient(opt)
	if err := runeRedisClient.Ping().Err(); err != nil {
		glog.Fatalln("Unable to connect to", config.RuneRedisURL)
	}
	opt, err = redis.ParseURL(config.LadonRedisURL)
	if err != nil {
		glog.Fatalln("Error parsing redis URL. Error:", err.Error())
	}
	glog.Infoln("Connecting to ladon (redis) database...")

	ladonRedisClient := redis.NewClient(opt)
	if err := ladonRedisClient.Ping().Err(); err != nil {
		glog.Fatalln("Unable to connect to", config.RuneRedisURL)
	}

	warden := &ladon.Ladon{
		Manager: manager.NewRedisManager(ladonRedisClient, "ladon"),
	}

	s := metadatatwirp.NewServer(runeRedisClient, warden)

	glog.Fatal(s.Start(config.ListenPort))
}
