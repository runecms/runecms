+++
title = "general"
weight = 1
+++

## The Metadata Service

The metadata service is responsible for maintaining metadata about any resource in Rune.

This metadata can be used for anything, however it is most often used for tracking information related to external services that are not part of the Rune suite of apps.

For example, if you want to build a service that extends the `complaints` feature in [`runecms.subscribers`](/subscribers) to also create an entry in your CRM software, you can use the metadata service to keep track of the matching unique IDs, so future queries against either service are easily maintained.

## Format

The format that hte Metadata service stores data in is a little unorthodox.

Normal key/value storage uses a single unique string as a `key`, which has an associated value.

With the Metadata service, each key/value pair is unique, but each `key` is not.

Multiple key/value pairs are associated together.

#### Example

If I have created the following keys and values to be associated together:

```
key1: value
key2: value
key3: value2
```

And I want to get more information about key1/value:

```
GET key1/value
```

I'll get back:

```
key1: value
key2: value
key3: value2
```

#### Example

A more practical example might be an integration with CRM software.

Let's say whenever a new complaint is made in Rune, you want to create a new ticket in your support software. Not only that, you want to keep track of the ID of the Rune complaint, and the ID of the support ticket, so that you can automatically close or update it later on down the road.

So you've created a [`webhook`](/webhooks), which, upon receiving a [`complaint`](/subscribers/#complaint), creates a new entry in your CRM, and a metadata entry, like so:

**note** this will actually return a new `metadata_object_id`, which will be used later on down the road to create more key/value associations with this one.

```
POST runecms.subscribers.complaints/<complaint_id>
```

And then whenever you get your CRM ticket ID, you'll grab that `metadata_object_id`, and create a new metadata entry:

```
POST crm.ticket/<ticket_id>
BODY {"metadata_object_id":<metadata_object_id>}
```

So now, given either the crm ticket id or the Rune `complaint_id`, you'll easily be able to get the other ID.

```
GET runecms.subscribers.complaint/<complaint_id>
```

Will return:

```json
{
  "runecms.subscribers.complaint": "<complaint_id>",
  "crm.ticket": "<ticket_id>",
  "metadata_object_id": "<metadata_object_id>",
  "metadata": {}
}
```
