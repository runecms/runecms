// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package metadata

import (
	"context"
	"fmt"
	"strings"

	"git.runecms.io/rune/runecms"
	models "git.runecms.io/rune/runecms/metadata/rpc"

	"github.com/go-redis/redis"
	"github.com/golang/glog"
	"github.com/google/uuid"
	"github.com/opentracing/opentracing-go"
	tags "github.com/opentracing/opentracing-go/ext"
	"github.com/ory/ladon"
)

func IsValidKey(key string) bool {
	return strings.Count(key, ":") == 1
}

func finishError(s opentracing.Span, err error) {
	if s != nil {
		s.SetTag("error", true)
		s.SetTag("error.description", err.Error())
		s.Finish()
	}
}

// Get will retrieve all of the keys that have the same object_id as the given key
// as well as the metadata associated with all of them
func Get(ctx context.Context, redis *redis.Client, w ladon.Warden, ident runecms.Identity, key string) (*models.MetadataSet, error) {
	var (
		span  = opentracing.SpanFromContext(ctx)
		child opentracing.Span
	)
	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("Ladon permission check", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "ladon")
		child.SetTag("ladon.action", "read")
		child.SetTag("ladon.resource", "metadata:metadata")
		ctx = opentracing.ContextWithSpan(ctx, child)
	} else {
		glog.V(1).Infoln(span)
		glog.V(1).Infoln(ctx)
	}

	if err := runecms.Access(ident, w, "read", "metadata:metadata"); err != nil {
		finishError(child, err)
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	if child != nil {
		child.Finish()
	}

	if IsValidKey(key) != true {
		glog.V(1).Infof("Received invalid key %s", key)
		finishError(child, fmt.Errorf("Received invalid key %s", key))
		return nil, runecms.Errorf(runecms.InvalidArgument, fmt.Sprintf("Received invalid key %s", key))
	}

	glog.V(2).Infof("Retreiving metdata for key %s...", key)

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("GET", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "redis")
		child.SetTag("key", key)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	var (
		getcmd = redis.Get(key)
	)
	lKey, err := getcmd.Result()
	if err != nil {
		finishError(child, err)
		return nil, runecms.Errorf(runecms.NotFound, err.Error())
	}
	if child != nil {
		child.Finish()
	}

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("LRANGE", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "redis")
		child.SetTag("key", lKey)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}

	lgetCmd := redis.LRange(lKey, 0, -1)
	keys, err := lgetCmd.Result()
	if err != nil {
		finishError(child, err)
		return nil, runecms.Errorf(runecms.Internal, err.Error())
	}
	if child != nil {
		child.SetTag("results", len(keys))
		child.Finish()
	}

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("GET", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "redis")
		child.SetTag("key", lKey+"_data")
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	getdatacmd := redis.Get(lKey + "_data")
	data, err := getdatacmd.Result()
	if child != nil {
		child.Finish()
	}
	if err != nil {
		finishError(child, err)
		return nil, runecms.Errorf(runecms.Internal, err.Error())
	}
	return &models.MetadataSet{
		Keys:     keys,
		Data:     data,
		ObjectId: lKey,
	}, nil
}

func createMetadataObject(ctx context.Context, redis *redis.Client, data []byte) (string, error) {
	var (
		span  = opentracing.SpanFromContext(ctx)
		child opentracing.Span
	)

	id := uuid.New().String()
	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("SET", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "redis")
		child.SetTag("key", id)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}

	setcmd := redis.Set(id+"_data", string(data), 0)
	if child != nil {
		child.Finish()
	}

	if err := setcmd.Err(); err != nil {
		finishError(child, err)
		return "", err
	}

	return id, nil
}

func deleteMetadataKey(ctx context.Context, redis *redis.Client, key string) error {
	var (
		span  = opentracing.SpanFromContext(ctx)
		child opentracing.Span
	)

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("GET", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "redis")
		child.SetTag("key", key)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}

	// Get the objid for this key
	getcmd := redis.Get(key)
	if err := getcmd.Err(); err != nil {
		finishError(child, err)
		return err
	}
	if child != nil {
		child.Finish()
	}
	objectId := getcmd.String()

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("DEL", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "redis")
		child.SetTag("key", key)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	delcmd := redis.Del(key)
	if err := delcmd.Err(); err != nil {
		finishError(child, err)
		return err
	}
	if child != nil {
		child.Finish()
	}
	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("LREM", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "redis")
		child.SetTag("key", objectId)
		child.SetTag("value", key)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	lremcmd := redis.LRem(objectId, 1, key)
	if err := lremcmd.Err(); err != nil {
		finishError(child, err)
		return err
	}
	if child != nil {
		child.Finish()
	}

	return nil
}

func createMetadataKey(ctx context.Context, redis *redis.Client, key string, objectId string) error {
	var (
		span  = opentracing.SpanFromContext(ctx)
		child opentracing.Span
	)

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("SET", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "redis")
		child.SetTag("key", key)
		child.SetTag("value", objectId)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	setcmd := redis.Set(key, objectId, 0)
	if err := setcmd.Err(); err != nil {
		finishError(child, err)
		return err
	}
	if child != nil {
		child.Finish()
	}

	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("LPUSH", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "redis")
		child.SetTag("key", objectId)
		child.SetTag("value", key)
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	lpushcmd := redis.LPush(objectId, key)
	if child != nil {
		child.Finish()
	}
	if err := lpushcmd.Err(); err != nil {
		if span != nil {
			child = opentracing.GlobalTracer().StartSpan("LSET", opentracing.ChildOf(span.Context()))
			tags.PeerService.Set(child, "redis")
			child.SetTag("key", objectId)
			child.SetTag("value", key)
			ctx = opentracing.ContextWithSpan(ctx, child)
		}
		lsetcmd := redis.LSet(objectId, 0, key)
		if err := lsetcmd.Err(); err != nil {
			finishError(child, err)
			return err
		}
		if child != nil {
			child.Finish()
		}
	}

	return nil
}

// Set can be given a new, unique key (like rune:1234),
// and the metadata will be associated with that unique key.
//  If provided a meta object where ObjectId == 0, then that metadata_object will be created.
//  If provided a meta.ObjectId, then the meta.Data is ignored
func Set(ctx context.Context, redis *redis.Client, w ladon.Warden, ident runecms.Identity, meta *models.MetadataItem) (*models.MetadataSet, error) {
	var (
		span  = opentracing.SpanFromContext(ctx)
		child opentracing.Span
	)
	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("Ladon permission check", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "ladon")
		child.SetTag("ladon.action", "create")
		child.SetTag("ladon.resource", "metadata:metadata")
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	if err := runecms.Access(ident, w, "create", "metadata:metadata"); err != nil {
		finishError(child, err)
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	if child != nil {
		child.Finish()
	}

	if IsValidKey(meta.Key) != true {
		finishError(child, fmt.Errorf("Received invalid key %s", meta.Key))
		glog.V(1).Infof("Received invalid key %s", meta.Key)
		return nil, runecms.Errorf(runecms.InvalidArgument, fmt.Sprintf("Received invalid key %s", meta.Key))
	}

	if meta.ObjectId == "" {
		objid, err := createMetadataObject(ctx, redis, []byte(meta.Data))
		if err != nil {
			finishError(child, err)
			return nil, err
		}
		meta.ObjectId = objid
	}
	glog.V(2).Infof("Creating key %s with metadata_object id %s...", meta.Key, meta.ObjectId)
	err := createMetadataKey(ctx, redis, meta.Key, meta.ObjectId)
	if err != nil {
		finishError(child, err)
		return nil, err
	}

	return Get(ctx, redis, w, ident, meta.Key)
}

func Delete(ctx context.Context, redis *redis.Client, w ladon.Warden, ident runecms.Identity, key string) error {
	var (
		span  = opentracing.SpanFromContext(ctx)
		child opentracing.Span
	)
	if span != nil {
		child = opentracing.GlobalTracer().StartSpan("Ladon permission check", opentracing.ChildOf(span.Context()))
		tags.PeerService.Set(child, "ladon")
		child.SetTag("ladon.action", "delete")
		child.SetTag("ladon.resource", "metadata:metadata")
		ctx = opentracing.ContextWithSpan(ctx, child)
	}
	if err := runecms.Access(ident, w, "delete", "metadata:metadata"); err != nil {
		finishError(child, err)
		return runecms.Error(runecms.PermissionDenied, err.Error())
	}
	if child != nil {
		child.Finish()
	}

	if IsValidKey(key) != true {
		finishError(child, fmt.Errorf("Received invalid key %s", key))
		glog.V(1).Infof("Received invalid key %s", key)
		return runecms.Errorf(runecms.InvalidArgument, fmt.Sprintf("Received invalid key %s", key))
	}

	return deleteMetadataKey(ctx, redis, key)
}
