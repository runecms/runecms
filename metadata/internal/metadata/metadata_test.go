// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package metadata

import (
	"context"
	"fmt"
	"testing"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/internal/testutil"
	models "git.runecms.io/rune/runecms/metadata/rpc"

	"github.com/google/go-cmp/cmp"
	"github.com/ory/ladon"
)

var (
	valid_id   runecms.Identity
	invalid_id runecms.Identity
	warden     ladon.Warden
)

func init() {
	valid_id = runecms.Be("valid", "admin")
	invalid_id = runecms.Be("invalid", "user")

	warden = testutil.GetTestWarden()
}

func TestMetadata(t *testing.T) {
	var (
		redis = testutil.GetTestRedis(t)
		key1  = fmt.Sprintf("test:%s", testutil.GetUUID())
		key2  = fmt.Sprintf("test:%s", testutil.GetUUID())
		key3  = fmt.Sprintf("test:%s", testutil.GetUUID())
	)

	t.Logf("key1: %s\nkey2: %s\nkey3: %s", key1, key2, key3)

	t.Run("Test single valid keys", func(t *testing.T) {
		meta, err := Set(context.Background(), redis, warden, valid_id, &models.MetadataItem{
			Key:  key1,
			Data: "example data?",
		})

		if err != nil {
			t.Fatal(err.Error())
		}
		expected := &models.MetadataSet{
			Keys:     []string{key1},
			Data:     "example data?",
			ObjectId: meta.ObjectId,
		}

		if cmp.Equal(expected, meta) != true {
			t.Errorf("Unexpected result. %s", cmp.Diff(expected, meta))
		}

		t.Run("Test multi-key metadata object", func(t *testing.T) {
			t.Run("With 2 keys...", func(t *testing.T) {
				meta2, err := Set(context.Background(), redis, warden, valid_id, &models.MetadataItem{
					Key:      key2,
					ObjectId: meta.ObjectId,
				})
				if err != nil {
					t.Fatal(err.Error())
				}
				expected.Keys = []string{key2, key1}

				if cmp.Equal(expected, meta2) != true {
					t.Errorf("Unexpected result. %s", cmp.Diff(expected, meta2))
				}
			})
			t.Run("With 3 keys...", func(t *testing.T) {
				meta3, err := Set(context.Background(), redis, warden, valid_id, &models.MetadataItem{
					Key:      key3,
					ObjectId: meta.ObjectId,
				})
				if err != nil {
					t.Fatal(err.Error())
				}
				expected.Keys = []string{key3, key2, key1}

				if cmp.Equal(expected, meta3) != true {
					t.Errorf("Unexpected result. %s", cmp.Diff(expected, meta3))
				}
			})
		})
	})

	t.Run("With an invalid id", func(t *testing.T) {
		_, err := Get(context.Background(), redis, warden, invalid_id, "not important")
		if err == nil {
			t.Error("Calling Get with an invalid ID should result in an error")
		}

		_, err = Set(context.Background(), redis, warden, invalid_id, nil)
		if err == nil {
			t.Error("Calling Set with an invalid ID should result in an error")
		}
	})

	t.Run("Test delete key", func(t *testing.T) {
		err := Delete(context.Background(), redis, warden, valid_id, key1)
		if err != nil {
			t.Fatal(err.Error())
		}
		_, err = Get(context.Background(), redis, warden, valid_id, key1)
		if err == nil {
			t.Fatal("Calling Get on a key that has been deleted should return an error")
		}
		_, err = Get(context.Background(), redis, warden, valid_id, key2)
		if err != nil {
			t.Fatal(err.Error())
		}
	})
}
