// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package metadata

import (
	"fmt"
	"strings"
)

// FindKey returns a list of keys found in the 'keys' argument that match 'key'
// If an error is returned, then no keys were found
func FindKey(key string, keys []string) ([]string, error) {
	found := []string{}
	for _, v := range keys {
		kv := strings.Split(v, ":")
		if kv[0] == key && len(kv) > 1 {
			found = append(found, kv[1])
		}
	}
	if len(found) == 0 {
		return nil, fmt.Errorf("The key %s was not found in the list", key)

	}

	return found, nil
}
