// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/ory/ladon"
	manager "github.com/ory/ladon/manager/sql"
	yaml "gopkg.in/yaml.v2"
)

var (
	ladonHost   = flag.String("ladon-host", "database.runecms.svc.cluster.local", "ladon database host")
	ladonUser   = flag.String("ladon-user", "root", "ladon database user")
	ladonPort   = flag.String("ladon-port", "26257", "ladon database port")
	ladonPass   = flag.String("ladon-pass", "", "ladon database password")
	ladonSchema = flag.String("ladon-schema", "ladon", "ladon database schema")

	yamlDir = flag.String("policy-dir", "./permissions", "directory containing permissions to load")
)

type Policy struct {
	ID          string   `yaml:"id"`
	Description string   `yaml:"description"`
	Subjects    []string `yaml:"subjects"`
	Actions     []string `yaml:"actions"`
	Resources   []string `yaml:"resources"`
	Effect      string
}

func main() {
	flag.Parse()
	ladonDb, err := sqlx.Open(
		"postgres",
		fmt.Sprintf(
			"postgres://%s:%s@%s:%s/%s?sslmode=disable",
			*ladonUser,
			*ladonPass,
			*ladonHost,
			*ladonPort,
			*ladonSchema,
		),
	)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer ladonDb.Close()

	sqlManager := manager.NewSQLManager(ladonDb, nil)

	warden := &ladon.Ladon{
		Manager: sqlManager,
	}

	if _, err := sqlManager.CreateSchemas("ladon", "ladon_migrations"); err != nil {
		log.Fatal(err.Error())
	}

	canonicalDir, err := filepath.Abs(*yamlDir)
	if err != nil {
		log.Fatalf("Unable to canonicalize directory path: %v", err)
	}
	files, err := ioutil.ReadDir(canonicalDir)
	if err != nil {
		log.Fatalf("Unable to read directory %s: %v", canonicalDir, err)
	}

	files = Filter(files, func(s os.FileInfo) bool {
		return strings.HasSuffix(s.Name(), ".yaml")
	})

	sort.Slice(files, func(i, j int) bool {
		num_i, err := strconv.Atoi(strings.Split(files[i].Name(), "_")[0])
		if err != nil {
			log.Fatalf("Unable to determine order of files: %v", err)
		}
		num_j, err := strconv.Atoi(strings.Split(files[j].Name(), "_")[1])
		if err != nil {
			log.Fatalf("Unable to determine order of files: %v", err)
		}
		return num_i < num_j
	})

	policies := []Policy{}

	for _, file := range files {
		canonicalPath, err := filepath.Abs(filepath.Join(canonicalDir, file.Name()))
		if err != nil {
			log.Fatalf("Unable to canonicalize filepath: %v", err)
		}
		content, err := ioutil.ReadFile(canonicalPath)
		if err != nil {
			log.Fatalf("Unable to read file %s: %v", file, err)
		}
		yaml.Unmarshal(content, &policies)
	}

	for _, policy := range policies {
		log.Printf("Creating policy: %#v\n", policy)
		err := warden.Manager.Create(&ladon.DefaultPolicy{
			ID:          policy.ID,
			Description: policy.Description,
			Subjects:    policy.Subjects,
			Actions:     policy.Actions,
			Resources:   policy.Resources,
			Effect:      policy.Effect,
		})
		if err != nil {
			log.Fatalf("Error creating policy ID %s: %v", policy.ID, err)
		}
	}
}

func Filter(vs []os.FileInfo, f func(os.FileInfo) bool) []os.FileInfo {
	vsf := make([]os.FileInfo, 0)
	for _, v := range vs {
		if f(v) {
			vsf = append(vsf, v)
		}
	}
	return vsf
}
