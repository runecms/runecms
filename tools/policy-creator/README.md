# policy-creator

Given a directory of `.yaml` files, will parse the YAML into Ladon policies
and apply them.

Files should be in the format `[N]_anything.yaml` where `[N]` is a number
designating the order to apply the policies. Multiple policies may (and
probably should) be included in a single YAML file, however the multi-file
format will allow for permissions migrations systems to be built and applied
later.

## Usage

```sh
go run create-policy.go \
  --ladon-host="database-host-name" \
  --ladon-user="database-user" \
  --ladon-pass="database-pass" \
  --ladon-port="database-port" \
  --ladon-schema="database-schema" \
  --policy-dir="yaml directory"
```

## YAML Format

The YAML file is expected to contain an array of policies. An example:

```yaml
---
- id: some-id
  description: Allow Consent app to read user information.
  subjects:
  - services:consent
  - groups:rune-internal
  actions:
  - read
  resources:
  - "users:<.*>"
  effect: allow
- id: some-other-id
  description: Allow Consent app to read & write session information.
  subjects:
  - services:consent
  - groups:rune-internal
  actions:
  - create
  - read
  - update
  - delete
  resources:
  - "sessions:<.*>"
  effect: allow
```

## Ladon Policy Format

Ladon policies are entirely string-based, making them very flexible.

`ID`s are strings, and should only be randomized when created dynamically. When we
pre-define a permissions policy, we should create an identifier for that policy
up front, to facilitate migrations and policy management. This can be a
descriptive identifier, e.g. `policy-consent-user-access` or a UUID. The
content doesn't matter, as long as it's pre-defined so that the policy can't
be mistakenly applied multiple times. This will also make it easier to mark
policies for deletion later, if necessary.

`Description` is any human-friendly string describing the policy and its effects.

`Subjects` is an array of strings describing the subject attempting to access
the resources. Again, being strings this is extremely flexible, and Ladon leaves
it to us to decide on our own standard naming schemes. Right now we've settled
on a hierarchical format with colon (`:`) separators. Hierarchical groupings
should be pluralized, i.e. `users` not `user`, etc.

Current values for the top level of the hierarchy:

* **users**: When the subject is a user of the application, e.g. `users:jhutchinson@wehco.com` or `users:1`.
* **groups**: For organizational purposes, all policies should include a group describing what category the policy falls in, e.g. `groups:admin`, `groups:subscriber`, or `groups:rune-internal`
* **services**: For service-to-service communication, e.g. `services:consent`. Should be accompanied by `groups:rune-internal` for internal services.

`Actions` are the actions to be performed by the `Subject` on the `Resources`.
Being strings they're fairly arbitrarily named, current standards are `create`,
`read`, `update`, `delete`.

`Resources` are the content/resources which the `Subject` is trying to perform
an `Action` upon. These can be a specific resource with an identifier, e.g.
`users:1`, or a grouping of resources with a regular expression denoting many
or all child resources, e.g. `users:<.*>`.

`Effect` **must** be either `allow` or `deny`, specifying whether the policy is
allowing or denying access. Overlapping policies will always prioritize deny,
i.e. if one policy says to allow access and another says to deny, then Ladon
will deny access.

