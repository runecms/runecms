// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package runecms

import (
	"sync"

	"github.com/ory/ladon"
	"github.com/pkg/errors"
)

// Multicheck will accept multiple Ladon requests and return true if one of them is allowed to continue
func Multicheck(warden ladon.Warden, requests ...*ladon.Request) bool {
	forcefullyDenied := false
	var mu sync.Mutex
	var mu2 sync.Mutex

	checks := []func() bool{}
	for _, req := range requests {
		req := req
		checks = append(checks, func() bool {
			mu2.Lock()
			defer mu2.Unlock()
			if err := warden.IsAllowed(req); err != nil {
				if errors.Cause(err).Error() == errors.Cause(ladon.ErrRequestForcefullyDenied).Error() {
					mu.Lock()
					forcefullyDenied = true
					mu.Unlock()
				}
				return false
			}
			return true
		})
	}

	var wg sync.WaitGroup
	var mu3 sync.Mutex
	result := false
	for _, fn := range checks {
		wg.Add(1)
		go func(fn func() bool) {
			res := fn()
			mu3.Lock()
			result = result || res
			mu3.Unlock()
			wg.Done()
		}(fn)
	}
	wg.Wait()
	return result && !forcefullyDenied
}
