// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package imagetypes

import (
	"database/sql"
	"time"

	"git.runecms.io/rune/runecms"

	"github.com/ory/ladon"
	"github.com/pkg/errors"
)

var ErrNotFound error = errors.New("Image Type not found")

// ImageType represents an image type
type ImageType struct {
	ID        int64
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}

// ImageTypes represents the storage and access controls for image types
type ImageTypes struct {
	read   *sql.Stmt
	create *sql.Stmt
	update *sql.Stmt
	delete *sql.Stmt
	Warden ladon.Warden
}

const (
	// SQLRead is the SQL to be executed when a read operation is performed
	SQLRead = `
		SELECT
			id,
			name,
			created_at,
			updated_at,
			deleted_at
		FROM
			image_types
		WHERE id = ?`

	// SQLUpdate is the SQL to be executed when an update operation is performed
	SQLUpdate = `
		UPDATE image_types SET
			name = ?,
			updated_at = now() at time zone 'utc'
		WHERE id = ?
	`

	// SQLCreate is the SQL to be executed when an insert operation is performed
	SQLCreate = `
		INSERT INTO image_types(name) VALUES(?) RETURNING id
	`

	// SQLDelete is the SQL to be executed when a delete operation is performed
	SQLDelete = `
		UPDATE image_types SET
			deleted_at = now() at time zone 'utc'
		WHERE id = ?
	`
)

// NewImageTypes is the constructor for ImageTypes
func NewImageTypes(db *sql.DB, w ladon.Warden) (*ImageTypes, error) {
	read, err := db.Prepare(SQLRead)
	if err != nil {
		return nil, err
	}

	create, err := db.Prepare(SQLCreate)
	if err != nil {
		return nil, err
	}

	update, err := db.Prepare(SQLUpdate)
	if err != nil {
		return nil, err
	}

	delete, err := db.Prepare(SQLDelete)
	if err != nil {
		return nil, err
	}

	return &ImageTypes{
		read:   read,
		create: create,
		update: update,
		delete: delete,
		Warden: w,
	}, nil
}

// Read will return an *ImageType if the provided identity has access to read
// image types and a valid ImageType ID is provided.
func (i *ImageTypes) Read(ident runecms.Identity, id int64) (*ImageType, error) {
	if err := runecms.Access(ident, i.Warden, "read", "content:image_types"); err != nil {
		return nil, err
	}

	out := ImageType{}
	err := i.read.QueryRow(id).Scan(
		&out.ID,
		&out.Name,
		&out.CreatedAt,
		&out.UpdatedAt,
		&out.DeletedAt,
	)
	if err == sql.ErrNoRows {
		err = errors.Wrapf(ErrNotFound, "Error reading image type ID %v", id)
	}
	return &out, err
}

// Create will create a new ImageType if the provided identity has access to
// create new image types.
func (i *ImageTypes) Create(ident runecms.Identity, typ *ImageType) (*ImageType, error) {
	if err := runecms.Access(ident, i.Warden, "create", "content:image_types"); err != nil {
		return nil, err
	}

	out := ImageType{}
	err := i.create.QueryRow(typ.Name).Scan(
		&out.ID,
	)
	if err != nil {
		return nil, errors.Wrap(err, "Error creating image type in database")
	}
	out.Name = typ.Name

	return &out, nil
}

// Update will update an ImageType if the provided identity has access to
// update image types.
func (i *ImageTypes) Update(ident runecms.Identity, typ *ImageType) error {
	if err := runecms.Access(ident, i.Warden, "update", "content:image_types"); err != nil {
		return err
	}

	res, err := i.update.Exec(typ.Name, typ.ID)
	if err != nil {
		return err
	}
	if rows, _ := res.RowsAffected(); rows == 0 {
		return errors.Wrapf(ErrNotFound, "Error updating image type ID %v", typ.ID)
	}

	return nil
}

// Delete will delete an ImageType if the provided identity has access to
// delete image types.
func (i *ImageTypes) Delete(ident runecms.Identity, typ *ImageType) error {
	if err := runecms.Access(ident, i.Warden, "delete", "content:image_types"); err != nil {
		return err
	}

	res, err := i.delete.Exec(typ.ID)
	if err != nil {
		return err
	}
	if rows, _ := res.RowsAffected(); rows == 0 {
		return errors.Wrapf(ErrNotFound, "Error deleting image type ID %v", typ.ID)
	}
	return nil
}
