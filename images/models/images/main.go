// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package images

import (
	"bytes"
	"database/sql"
	"fmt"
	"io"
	"strconv"
	"time"

	"git.runecms.io/rune/runecms"

	minio "github.com/minio/minio-go"
	"github.com/ory/ladon"
	"github.com/pkg/errors"
)

var ErrNotFound error = errors.New("Image not found")

// Image represents an image
type Image struct {
	ID                int64
	Width             int64
	Height            int64
	Format            string
	Active            bool
	PublishedAt       time.Time
	Caption           string
	Credit            string
	AllowReproduction bool
	TypeID            int64
	Data              []byte
	Sites             []int64
	CreatedAt         time.Time
	UpdatedAt         time.Time
	DeletedAt         time.Time
}

// Images represents the storage and access controls for images
type Images struct {
	read          *sql.Stmt
	create        *sql.Stmt
	update        *sql.Stmt
	delete        *sql.Stmt
	addSite       *sql.Stmt
	removeSite    *sql.Stmt
	sitesForImage *sql.Stmt
	imagesForSite *sql.Stmt
	Warden        ladon.Warden
	S3            *minio.Client
	Bucket        string
	Region        string
}

const (
	// SQLRead is the SQL to be executed when a read operation is performed
	SQLRead = `
		SELECT
			id,
			width,
			height,
			format,
			active,
			published_at,
			caption,
			credit,
			allow_reproduction,
			type_id,
			created_at,
			updated_at,
			deleted_at
		FROM
			images
		WHERE
			id = ?
	`

	// SQLCreate is the SQL to be executed when an insert operation is performed
	SQLCreate = `
		INSERT INTO images(
			format,
			width,
			height,
			active,
			published_at,
			caption,
			credit,
			allow_reproduction,
			type_id
		)
		VALUES(?,?,?,?,?,?,?,?,?)
		RETURNING id
	`

	// SQLUpdate is the SQL to be executed when an update operation is performed
	SQLUpdate = `
		UPDATE images SET
			format = ?,
			width = ?,
			height = ?,
			active = ?,
			published_at = ?,
			caption = ?,
			credit = ?,
			allow_reproduction = ?,
			type_id = ?,
			updated_at = now() at time zone 'utc'
		WHERE id = ?
	`

	// SQLDelete is the SQL to be executed when a delete operation is performed
	SQLDelete = `
		UPDATE images SET
			deleted_at = now() at time zone 'utc'
		WHERE id = ?
	`

	// SQLAddSite is the SQL to be executed to add an image to a site
	SQLAddSite = `INSERT INTO image_sites(id, site_id) VALUES(?,?)`

	// SQLRemoveSite is the SQL to be executed to remove an image from a site
	SQLRemoveSite = `DELETE FROM image_sites WHERE id = ? AND site_id = ?`

	// SQLGetSitesForImage is the SQL to be executed to list the sites associated
	// with an image
	SQLGetSitesForImage = `SELECT site_id FROM image_sites WHERE id = ?`
)

// NewImages is the constructor for Images
func NewImages(
	db *sql.DB,
	w ladon.Warden,
	s3 *minio.Client,
	bucket string,
	region string,
) (*Images, error) {
	read, err := db.Prepare(SQLRead)
	if err != nil {
		return nil, err
	}

	create, err := db.Prepare(SQLCreate)
	if err != nil {
		return nil, err
	}

	update, err := db.Prepare(SQLUpdate)
	if err != nil {
		return nil, err
	}

	delete, err := db.Prepare(SQLDelete)
	if err != nil {
		return nil, err
	}

	addSite, err := db.Prepare(SQLAddSite)
	if err != nil {
		return nil, err
	}

	removeSite, err := db.Prepare(SQLRemoveSite)
	if err != nil {
		return nil, err
	}

	sitesForImage, err := db.Prepare(SQLGetSitesForImage)
	if err != nil {
		return nil, err
	}

	// Ensure S3 bucket exists
	exists, err := s3.BucketExists(bucket)
	if err != nil {
		return nil, err
	}
	if !exists {
		err = s3.MakeBucket(bucket, region)
		if err != nil {
			return nil, err
		}
	}

	return &Images{
		read:          read,
		create:        create,
		update:        update,
		delete:        delete,
		addSite:       addSite,
		removeSite:    removeSite,
		sitesForImage: sitesForImage,
		Warden:        w,
		S3:            s3,
		Bucket:        bucket,
		Region:        region,
	}, nil
}

// Read will return an *Image if the provided identity has access to read
// images and a valid Image ID is provided.
// Read does not return the binary data for the image, see Fetch for that.
func (i *Images) Read(ident runecms.Identity, id int64) (*Image, error) {
	if err := runecms.Access(ident, i.Warden, "read", "content:images"); err != nil {
		return nil, err
	}

	out := Image{}
	err := i.read.QueryRow(id).Scan(
		&out.ID,
		&out.Width,
		&out.Height,
		&out.Format,
		&out.Active,
		&out.PublishedAt,
		&out.Caption,
		&out.Credit,
		&out.AllowReproduction,
		&out.TypeID,
		&out.CreatedAt,
		&out.UpdatedAt,
		&out.DeletedAt,
	)
	if err == sql.ErrNoRows {
		return nil, fmt.Errorf("No images with ID %v", id)
	}

	sites := []int64{}
	rows, err := i.sitesForImage.Query(id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var site int64
		if err := rows.Scan(&site); err != nil {
			return nil, err
		}
		sites = append(sites, site)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	out.Sites = sites

	return &out, err
}

// Create will create a new Image if the provided identity has access to create
// new images.
func (i *Images) Create(ident runecms.Identity, img *Image) (*Image, error) {
	if err := runecms.Access(ident, i.Warden, "create", "content:images"); err != nil {
		return nil, err
	}

	out := Image{}
	err := i.create.QueryRow(
		img.Format,
		img.Width,
		img.Height,
		img.Active,
		img.PublishedAt,
		img.Caption,
		img.Credit,
		img.AllowReproduction,
		img.TypeID,
	).Scan(&out.ID)
	if err != nil {
		return nil, errors.Wrap(err, "Error creating image in database")
	}

	_, err = i.S3.PutObject(i.Bucket, strconv.FormatInt(out.ID, 10), bytes.NewReader(img.Data), img.Format)
	if err != nil {
		return &out, err
	}

	return i.Read(ident, out.ID)
}

// Update will update an Image if the provided identity has access to update
// images.
func (i *Images) Update(ident runecms.Identity, img *Image) error {
	if err := runecms.Access(ident, i.Warden, "update", "content:images"); err != nil {
		return err
	}

	res, err := i.update.Exec(
		img.Format,
		img.Width,
		img.Height,
		img.Active,
		img.PublishedAt,
		img.Caption,
		img.Credit,
		img.AllowReproduction,
		img.TypeID,
		img.ID,
	)
	if err != nil {
		return err
	}
	if rows, _ := res.RowsAffected(); rows == 0 {
		return errors.Wrapf(ErrNotFound, "Error updating image ID %d", img.ID)
	}

	return nil
}

// Delete will delete an image if the provided identity has access to delete
// images.
func (i *Images) Delete(ident runecms.Identity, img *Image) error {
	if err := runecms.Access(ident, i.Warden, "delete", "content:images"); err != nil {
		return err
	}

	res, err := i.delete.Exec(img.ID)
	if err != nil {
		return err
	}
	if rows, _ := res.RowsAffected(); rows == 0 {
		return errors.Wrapf(ErrNotFound, "Error deleting image ID %d", img.ID)
	}
	return nil
}

// Fetch will fetch the binary data for the image from object storage and
// include it in the response.
func (i *Images) Fetch(ident runecms.Identity, id int64) (*Image, error) {
	img, err := i.Read(ident, id)
	if err != nil {
		return nil, err
	}

	obj, err := i.S3.GetObject(i.Bucket, strconv.FormatInt(id, 10))
	if err != nil {
		return nil, err
	}
	buf := bytes.NewBuffer(img.Data)
	if _, err = io.Copy(buf, obj); err != nil {
		return nil, err
	}
	return img, err
}

func (i *Images) AddToSite(ident runecms.Identity, id, siteID int64) error {
	if err := runecms.Access(ident, i.Warden, "add_site", "content:images"); err != nil {
		return err
	}

	_, err := i.addSite.Exec(id, siteID)
	if err != nil {
		return err
	}

	return nil
}

func (i *Images) RemoveFromSite(ident runecms.Identity, id, siteID int64) error {
	if err := runecms.Access(ident, i.Warden, "remove_site", "content:images"); err != nil {
		return err
	}

	res, err := i.removeSite.Exec(id, siteID)
	if err != nil {
		return err
	}
	if rows, _ := res.RowsAffected(); rows == 0 {
		return errors.Errorf("No image/site associations with image ID %d and site ID %d", id, siteID)
	}

	return nil
}
