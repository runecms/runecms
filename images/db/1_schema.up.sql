CREATE TABLE images(
  id                 serial NOT NULL,
  format             TEXT,
  width              int,
  height             int,
  active             bool,
  published_at       timestamp with time zone,
  caption            TEXT,
  credit             TEXT,
  allow_reproduction bool,
  type_id            int,

  created_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,
  deleted_at timestamp with time zone
);

CREATE TABLE image_types(
  id   serial NOT NULL,
  name TEXT,

  created_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,
  deleted_at timestamp with time zone
);

CREATE TABLE image_sites(
  id         serial NOT NULL,
  site_id    int
);
