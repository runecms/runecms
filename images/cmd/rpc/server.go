// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"log"
	"net"
	"time"

	"git.runecms.io/rune/runecms/images/models/images"
	"git.runecms.io/rune/runecms/images/models/imagetypes"
	"git.runecms.io/rune/runecms"

	"github.com/golang/protobuf/ptypes"
	ts "github.com/golang/protobuf/ptypes/timestamp"
	minio "github.com/minio/minio-go"
	"github.com/ory/ladon"
	manager "github.com/ory/ladon/manager/sql"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	rpc "git.runecms.io/rune/runecms/images/rpc"
)

var (
	port = flag.Int("port", 3000, "server port")

	dbhost   = flag.String("dbhost", "database-cockroachdb-public.default.svc.cluster.local", "content database host")
	dbuser   = flag.String("dbuser", "user", "content database username")
	dbpass   = flag.String("dbpass", "password", "content database password")
	dbschema = flag.String("dbschema", "runecms", "content database schema")

	ladonHost   = flag.String("ladon-host", "database-cockroachdb-public.default.svc.cluster.local", "ladon database host")
	ladonUser   = flag.String("ladon-user", "user", "ladon database user")
	ladonPass   = flag.String("ladon-pass", "password", "ladon database password")
	ladonSchema = flag.String("ladon-schema", "ladon", "ladon database schema")

	s3Endpoint  = flag.String("s3-endpoint", "storage-minio.runecms.svc.cluster.local", "s3 service endpoint")
	s3AccessKey = flag.String("s3-access-key", "access-key", "access key for the object storage")
	s3Secret    = flag.String("s3-secret", "password", "secret key for the object storage")
	s3Bucket    = flag.String("s3-bucket", "images", "the bucket name to store images in")
	s3Region    = flag.String("s3-region", "us-east-1", "the region to create the bucket in, if necessary")
)

func main() {
	flag.Parse()

	// Listen on port
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// Connect to content database
	db, err := sql.Open(
		"postgres",
		fmt.Sprintf(
			"postgres://%s:%s@%s/%s?sslmode=disable",
			*dbuser,
			*dbpass,
			*dbhost,
			*dbschema,
		),
	)
	if err != nil {
		log.Fatalf("Unable to connect to content database: %v", err)
	}

	// Connect to access control database
	ladonDb, err := sqlx.Open(
		"postgres",
		fmt.Sprintf(
			"postgres://%s:%s@%s/%s?sslmode=disable",
			*ladonUser,
			*ladonPass,
			*ladonHost,
			*ladonSchema,
		),
	)
	if err != nil {
		log.Fatalf("Unable to connect to ladon database: %v", err)
	}

	warden := &ladon.Ladon{
		Manager: manager.NewSQLManager(ladonDb, nil),
	}

	// Connect to object storage server
	s3Client, err := minio.New(*s3Endpoint, *s3AccessKey, *s3Secret, true)
	if err != nil {
		log.Fatalf("Unable to connect to s3 storage: %v", err)
	}

	// Initialize server
	imagesRPCServer, err := NewImagesRPCServer(db, warden, s3Client, *s3Bucket, *s3Region)
	if err != nil {
		log.Fatalf("Unable to initialize server: %v", err)
	}

	// Serve gRPC
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	rpc.RegisterImagesServer(grpcServer, imagesRPCServer)
	reflection.Register(grpcServer)
	log.Fatal(grpcServer.Serve(lis))
}

// ImagesRPCServer collates the RPC Server implementations for images and image types
type ImagesRPCServer struct {
	images *images.Images
	types  *imagetypes.ImageTypes
}

// NewImagesRPCServer is the constructor for ImagesRPCServer
func NewImagesRPCServer(
	db *sql.DB,
	w ladon.Warden,
	s3 *minio.Client,
	s3Bucket, s3Region string,
) (*ImagesRPCServer, error) {
	imgs, err := images.NewImages(db, w, s3, s3Bucket, s3Region)
	if err != nil {
		return nil, err
	}

	types, err := imagetypes.NewImageTypes(db, w)
	if err != nil {
		return nil, err
	}

	return &ImagesRPCServer{
		images: imgs,
		types:  types,
	}, nil
}

// ReadImage implements the RPC method for reading an image
func (i *ImagesRPCServer) ReadImage(ctx context.Context, readRequest *rpc.ImageReadRequest) (*rpc.ImageReadResponse, error) {
	response := &rpc.ImageReadResponse{
		PublishedAt: &ts.Timestamp{},
		CreatedAt:   &ts.Timestamp{},
		UpdatedAt:   &ts.Timestamp{},
		DeletedAt:   &ts.Timestamp{},
	}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}

	image, err := i.images.Read(user, readRequest.GetId())
	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}

	response.Id = image.ID
	response.Width = image.Width
	response.Height = image.Height
	response.Format = image.Format
	response.Active = image.Active
	response.Caption = image.Caption
	response.Credit = image.Credit
	response.AllowReproduction = image.AllowReproduction
	response.TypeId = image.TypeID
	response.Sites = image.Sites

	if response.CreatedAt, err = ptypes.TimestampProto(image.CreatedAt); err != nil {
		return response, status.Errorf(codes.DataLoss, "%v", err)
	}

	if response.UpdatedAt, err = ptypes.TimestampProto(image.UpdatedAt); err != nil {
		return response, status.Errorf(codes.DataLoss, "%v", err)
	}

	if response.DeletedAt, err = ptypes.TimestampProto(image.DeletedAt); err != nil {
		return response, status.Errorf(codes.DataLoss, "%v", err)
	}

	if response.PublishedAt, err = ptypes.TimestampProto(image.PublishedAt); err != nil {
		return response, status.Errorf(codes.DataLoss, "%v", err)
	}

	return response, status.Error(codes.OK, "")
}

// FetchImage implements the RPC method for fetching an image's data
func (i *ImagesRPCServer) FetchImage(ctx context.Context, fetchRequest *rpc.ImageFetchRequest) (*rpc.ImageFetchResponse, error) {
	response := &rpc.ImageFetchResponse{}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}

	image, err := i.images.Fetch(user, fetchRequest.GetId())
	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}

	response.Data = image.Data

	return response, status.Error(codes.OK, "")
}

// CreateImage implements the RPC method for creating a new image
func (i *ImagesRPCServer) CreateImage(ctx context.Context, createRequest *rpc.ImageCreateRequest) (*rpc.ImageCreateResponse, error) {
	response := &rpc.ImageCreateResponse{}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}

	var publishedAt time.Time

	if createRequest.GetPublishedAt() == nil {
		publishedAt = time.Now()
	} else {
		publishedAt, err = ptypes.Timestamp(createRequest.GetPublishedAt())
		if err != nil {
			return response, status.Errorf(codes.DataLoss, "%v", err)
		}
	}
	image, err := i.images.Create(user, &images.Image{
		Width:             createRequest.GetWidth(),
		Height:            createRequest.GetHeight(),
		Format:            createRequest.GetFormat(),
		Active:            createRequest.GetActive(),
		PublishedAt:       publishedAt,
		Caption:           createRequest.GetCaption(),
		Credit:            createRequest.GetCredit(),
		AllowReproduction: createRequest.GetAllowReproduction(),
		TypeID:            createRequest.GetTypeId(),
	})
	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}

	response.Id = image.ID

	return response, status.Error(codes.OK, "")
}

// UpdateImage implements the RPC method for updating an image's properties
func (i *ImagesRPCServer) UpdateImage(ctx context.Context, updateRequest *rpc.ImageUpdateRequest) (*rpc.ImageUpdateResponse, error) {
	response := &rpc.ImageUpdateResponse{}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}

	publishedAt, err := ptypes.Timestamp(updateRequest.GetPublishedAt())
	if err != nil {
		return response, status.Errorf(codes.DataLoss, "%v", err)
	}
	err = i.images.Update(user, &images.Image{
		Width:             updateRequest.GetWidth(),
		Height:            updateRequest.GetHeight(),
		Format:            updateRequest.GetFormat(),
		Active:            updateRequest.GetActive(),
		PublishedAt:       publishedAt,
		Caption:           updateRequest.GetCaption(),
		Credit:            updateRequest.GetCredit(),
		AllowReproduction: updateRequest.GetAllowReproduction(),
		TypeID:            updateRequest.GetTypeId(),
		ID:                updateRequest.GetId(),
	})
	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		case images.ErrNotFound:
			return response, status.Errorf(codes.NotFound, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}
	return response, status.Error(codes.OK, "")
}

// DeleteImage implements the RPC method for deleting an image
func (i *ImagesRPCServer) DeleteImage(ctx context.Context, deleteRequest *rpc.ImageDeleteRequest) (*rpc.ImageDeleteResponse, error) {
	response := &rpc.ImageDeleteResponse{}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}

	err = i.images.Delete(user, &images.Image{ID: deleteRequest.GetId()})
	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}

	return response, status.Error(codes.OK, "")
}

// AddToSite implements the RPC method for adding an image to a site
func (i *ImagesRPCServer) AddToSite(ctx context.Context, addRequest *rpc.AddToSiteRequest) (*rpc.AddToSiteResponse, error) {
	response := &rpc.AddToSiteResponse{}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}

	err = i.images.AddToSite(user, addRequest.GetId(), addRequest.GetSiteId())
	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}

	return response, status.Error(codes.OK, "")
}

// RemoveFromSite implements the RPC method for removing an image from a site
func (i *ImagesRPCServer) RemoveFromSite(ctx context.Context, removeRequest *rpc.RemoveFromSiteRequest) (*rpc.RemoveFromSiteResponse, error) {
	response := &rpc.RemoveFromSiteResponse{}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}

	err = i.images.RemoveFromSite(user, removeRequest.GetId(), removeRequest.GetSiteId())
	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}
	return response, status.Error(codes.OK, "")
}

// CreateImageType implements the RPC method for creating a new image type
func (i *ImagesRPCServer) CreateImageType(ctx context.Context, createRequest *rpc.TypeCreateRequest) (*rpc.TypeCreateResponse, error) {
	response := &rpc.TypeCreateResponse{}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}

	typ, err := i.types.Create(user, &imagetypes.ImageType{Name: createRequest.GetName()})
	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}

	response.Id = typ.ID

	return response, status.Error(codes.OK, "")
}

// ReadImageType implements the RPC method for reading an image type
func (i *ImagesRPCServer) ReadImageType(ctx context.Context, readRequest *rpc.TypeReadRequest) (*rpc.TypeReadResponse, error) {
	response := &rpc.TypeReadResponse{}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}

	typ, err := i.types.Read(user, readRequest.GetId())
	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		case imagetypes.ErrNotFound:
			return response, status.Errorf(codes.NotFound, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}

	response.Id = typ.ID
	response.Name = typ.Name

	if response.CreatedAt, err = ptypes.TimestampProto(typ.CreatedAt); err != nil {
		return response, status.Errorf(codes.DataLoss, "%v", err)
	}

	if response.UpdatedAt, err = ptypes.TimestampProto(typ.UpdatedAt); err != nil {
		return response, status.Errorf(codes.DataLoss, "%v", err)
	}

	if response.DeletedAt, err = ptypes.TimestampProto(typ.DeletedAt); err != nil {
		return response, status.Errorf(codes.DataLoss, "%v", err)
	}

	return response, status.Error(codes.OK, "")
}

// UpdateImageType implements the RPC method for updating an image type
func (i *ImagesRPCServer) UpdateImageType(ctx context.Context, updateRequest *rpc.TypeUpdateRequest) (*rpc.TypeUpdateResponse, error) {
	response := &rpc.TypeUpdateResponse{}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}

	err = i.types.Update(user, &imagetypes.ImageType{
		ID:   updateRequest.GetId(),
		Name: updateRequest.GetName(),
	})
	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		case imagetypes.ErrNotFound:
			return response, status.Errorf(codes.NotFound, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}

	return response, status.Error(codes.OK, "")
}

// DeleteImageType implements the RPC method for deleting an image type
func (i *ImagesRPCServer) DeleteImageType(ctx context.Context, deleteRequest *rpc.TypeDeleteRequest) (*rpc.TypeDeleteResponse, error) {
	response := &rpc.TypeDeleteResponse{}

	user, err := runecms.IdentityFromRPC(ctx)
	if err != nil {
		return response, status.Errorf(codes.Unauthenticated, "%v", err)
	}

	err = i.types.Delete(user, &imagetypes.ImageType{
		ID: deleteRequest.GetId(),
	})
	if err != nil {
		switch errors.Cause(err) {
		case runecms.ErrUnauthorized:
			return response, status.Errorf(codes.PermissionDenied, "%v", err)
		case imagetypes.ErrNotFound:
			return response, status.Errorf(codes.NotFound, "%v", err)
		default:
			return response, status.Errorf(codes.Unknown, "%v", err)
		}
	}

	return response, status.Error(codes.OK, "")
}
