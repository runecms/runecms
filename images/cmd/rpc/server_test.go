// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"context"
	"strings"
	"testing"
	"time"

	"git.runecms.io/rune/runecms/images/models/images"
	"git.runecms.io/rune/runecms/images/models/imagetypes"
	rpc "git.runecms.io/rune/runecms/images/rpc"
	"google.golang.org/grpc/metadata"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"

	"github.com/golang/protobuf/ptypes"
	"github.com/google/go-cmp/cmp"
	minio "github.com/minio/minio-go"
	"github.com/ory/ladon"
	manager "github.com/ory/ladon/manager/memory"
	"github.com/pborman/uuid"
)

func newWarden() ladon.Warden {
	warden := &ladon.Ladon{
		Manager: manager.NewMemoryManager(),
	}

	// Images
	_ = warden.Manager.Create(&ladon.DefaultPolicy{
		ID:          uuid.New(),
		Description: "Allow user:jason and group:admin to read, create, update, delete content:images.",
		Subjects:    []string{"user:jason", "group:admin"},
		Effect:      ladon.AllowAccess,
		Resources:   []string{"content:images"},
		Actions:     []string{"read", "create", "update", "delete", "add_site", "remove_site"},
		Conditions:  ladon.Conditions{},
	})
	_ = warden.Manager.Create(&ladon.DefaultPolicy{
		ID:          uuid.New(),
		Description: "Deny user:bob and group:editor from read, create, update, delete on content:images.",
		Subjects:    []string{"user:bob", "group:editor"},
		Effect:      ladon.DenyAccess,
		Resources:   []string{"content:images"},
		Actions:     []string{"read", "create", "update", "delete", "add_site", "remove_site"},
		Conditions:  ladon.Conditions{},
	})

	// Image Types
	_ = warden.Manager.Create(&ladon.DefaultPolicy{
		ID:          uuid.New(),
		Description: "Allow user:jason and group:admin to read, create, update, delete content:image_types.",
		Subjects:    []string{"user:jason", "group:admin"},
		Effect:      ladon.AllowAccess,
		Resources:   []string{"content:image_types"},
		Actions:     []string{"read", "create", "update", "delete"},
		Conditions:  ladon.Conditions{},
	})
	_ = warden.Manager.Create(&ladon.DefaultPolicy{
		ID:          uuid.New(),
		Description: "Deny user:bob and group:editor from read, create, update, delete on content:image_types.",
		Subjects:    []string{"user:bob", "group:editor"},
		Effect:      ladon.DenyAccess,
		Resources:   []string{"content:image_types"},
		Actions:     []string{"read", "create", "update", "delete"},
		Conditions:  ladon.Conditions{},
	})

	return warden
}

func incomingContextMd(md map[string][]string) context.Context {
	return metadata.NewIncomingContext(context.Background(), metadata.MD(md))
}
func regexEscape(in string) string {
	r := strings.NewReplacer(
		"?", `\?`,
		")", `\)`,
		"(", `\(`,
	)
	return r.Replace(in)
}

func TestImagesRPCServer(t *testing.T) {
	t.Logf("Testing ImagesRPCServer")
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Failed to open mock database: %v", err)
	}
	defer func() {
		_ = mock.ExpectClose()
		err := db.Close()
		if err != nil {
			t.Fatalf("Failed to close mock database: %v", err)
		}
	}()
	warden := newWarden()

	// Prepare Images SQL Expectations
	expectedReadImage := mock.ExpectPrepare(regexEscape(images.SQLRead))
	expectedCreateImage := mock.ExpectPrepare(regexEscape(images.SQLCreate))
	expectedUpdateImage := mock.ExpectPrepare(regexEscape(images.SQLUpdate))
	expectedDeleteImage := mock.ExpectPrepare(regexEscape(images.SQLDelete))
	expectedImageAddSite := mock.ExpectPrepare(regexEscape(images.SQLAddSite))
	expectedImageRemoveSite := mock.ExpectPrepare(regexEscape(images.SQLRemoveSite))
	expectedGetSitesForImage := mock.ExpectPrepare(regexEscape(images.SQLGetSitesForImage))

	// Prepare Image Types SQL Expectations
	expectedReadImageType := mock.ExpectPrepare(regexEscape(imagetypes.SQLRead))
	expectedCreateImageType := mock.ExpectPrepare(regexEscape(imagetypes.SQLCreate))
	expectedUpdateImageType := mock.ExpectPrepare(regexEscape(imagetypes.SQLUpdate))
	expectedDeleteImageType := mock.ExpectPrepare(regexEscape(imagetypes.SQLDelete))

	// Connect to Minio test server
	s3Client, err := minio.New(
		"play.minio.io:9000",
		"Q3AM3UQ867SPQQA43P2F",
		"zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG",
		true,
	)
	if err != nil {
		t.Fatalf("Failed to connect to Minio test server: %v", err)
	}

	server, err := NewImagesRPCServer(db, warden, s3Client, "testbucket", "us-east-1")
	if err != nil {
		t.Fatalf("Failed to initialize new images RPC server: %v", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Fatalf("Unfulfilled database expectations: %v", err)
	}

	// ======Test Image Read Operations======
	t.Run("Image Read Operations", func(t *testing.T) {
		t.Run("Operation allowed, image found", func(t *testing.T) {
			now := time.Now().UTC()
			readRows := sqlmock.NewRows([]string{
				"id",
				"width",
				"height",
				"format",
				"active",
				"published_at",
				"caption",
				"credit",
				"allow_reproduction",
				"type_id",
				"created_at",
				"updated_at",
				"deleted_at",
			}).AddRow(
				1,           // id
				100,         // width
				100,         // height
				"image/jpg", // format
				true,        // active
				now,         // published_at
				"caption",   // caption
				"credit",    // credit
				false,       // allow_reproduction
				1,           // type_id
				now,         // created_at
				now,         // updated_at
				time.Time{}, // deleted_at
			)
			expectedReadImage.ExpectQuery().WithArgs(1).WillReturnRows(readRows)

			siteRows := sqlmock.NewRows([]string{"id"}).AddRow(1).AddRow(2).AddRow(3)
			expectedGetSitesForImage.ExpectQuery().WithArgs(1).WillReturnRows(siteRows)

			readResponse, err := server.ReadImage(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin", "author"},
				}),
				&rpc.ImageReadRequest{Id: 1},
			)
			if err != nil {
				t.Fatalf("Error in ImagesRPCServer.ReadImage: %v", err)
			}
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Fatalf("Unfulfilled database expectations: %v", err)
			}
			tsPublishedAt, err := ptypes.TimestampProto(now)
			if err != nil {
				t.Fatalf("Error converting time to proto: %v", err)
			}
			tsCreatedAt, err := ptypes.TimestampProto(now)
			if err != nil {
				t.Fatalf("Error converting time to proto: %v", err)
			}
			tsUpdatedAt, err := ptypes.TimestampProto(now)
			if err != nil {
				t.Fatalf("Error converting time to proto: %v", err)
			}
			tsDeletedAt, err := ptypes.TimestampProto(time.Time{})
			if err != nil {
				t.Fatalf("Error converting time to proto: %v", err)
			}

			expect := &rpc.ImageReadResponse{
				Id:                1,
				Width:             100,
				Height:            100,
				Format:            "image/jpg",
				Active:            true,
				PublishedAt:       tsPublishedAt,
				Caption:           "caption",
				Credit:            "credit",
				AllowReproduction: false,
				TypeId:            1,
				Sites:             []int64{1, 2, 3},
				CreatedAt:         tsCreatedAt,
				UpdatedAt:         tsUpdatedAt,
				DeletedAt:         tsDeletedAt,
			}
			if !cmp.Equal(readResponse, expect) {
				t.Errorf("Response mismatch:\n%v", cmp.Diff(readResponse, expect))
			}
		})

		t.Run("Operation allowed, image not found", func(t *testing.T) {
			expectedReadImage.ExpectQuery().WithArgs(2).WillReturnRows(
				sqlmock.NewRows([]string{
					"id",
					"width",
					"height",
					"format",
					"active",
					"published_at",
					"caption",
					"credit",
					"allow_reproduction",
					"type_id",
					"created_at",
					"updated_at",
					"deleted_at",
				}),
			)

			_, err := server.ReadImage(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin", "author"},
				}),
				&rpc.ImageReadRequest{Id: 2},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			}
			if err.Error() != "rpc error: code = Unknown desc = No images with ID 2" {
				t.Errorf(`Expected error "rpc error: code = Unknown desc = No images with ID 2" got "%v"`, err)
			}
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
		})

		t.Run("Operation not allowed", func(t *testing.T) {
			_, err := server.ReadImage(
				incomingContextMd(map[string][]string{
					"user":   []string{"bob"},
					"groups": []string{"admin"},
				}),
				&rpc.ImageReadRequest{Id: 1},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			} else if err.Error() != "rpc error: code = PermissionDenied desc = The user is not authorized to access this content." {
				t.Errorf("Unexpected error: %v", err)
			}
		})
	})

	t.Run("Image Create Operations", func(t *testing.T) {
		t.Run("Operation allowed, image created", func(t *testing.T) {
			now := time.Now().UTC()
			expectedCreateImage.ExpectQuery().WithArgs(
				"image/jpg", // format
				100,         // width
				100,         // height
				true,        // active
				now,         // published_at
				"caption",   // caption
				"credit",    // credit
				false,       // allow_reproduction
				1,           // type_id
			).WillReturnRows(
				sqlmock.NewRows([]string{"id"}).AddRow(1),
			)
			readRows := sqlmock.NewRows([]string{
				"id",
				"width",
				"height",
				"format",
				"active",
				"published_at",
				"caption",
				"credit",
				"allow_reproduction",
				"type_id",
				"created_at",
				"updated_at",
				"deleted_at",
			}).AddRow(
				1,           // id
				100,         // width
				100,         // height
				"image/jpg", // format
				true,        // active
				now,         // published_at
				"caption",   // caption
				"credit",    // credit
				false,       // allow_reproduction
				1,           // type_id
				now,         // created_at
				now,         // updated_at
				time.Time{}, // deleted_at
			)
			expectedReadImage.ExpectQuery().WithArgs(1).WillReturnRows(readRows)

			siteRows := sqlmock.NewRows([]string{"id"})
			expectedGetSitesForImage.ExpectQuery().WithArgs(1).WillReturnRows(siteRows)

			tsPublishedAt, err := ptypes.TimestampProto(now)
			if err != nil {
				t.Fatalf("Error converting time to proto: %v", err)
			}
			createResponse, err := server.CreateImage(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.ImageCreateRequest{
					Format:            "image/jpg",
					Width:             100,
					Height:            100,
					Active:            true,
					PublishedAt:       tsPublishedAt,
					Caption:           "caption",
					Credit:            "credit",
					AllowReproduction: false,
					TypeId:            1,
				},
			)
			if err != nil {
				t.Errorf("Unexpected error: %v", err)
			}
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
			expectedResponse := &rpc.ImageCreateResponse{
				Id: 1,
			}
			if !cmp.Equal(createResponse, expectedResponse) {
				t.Errorf("Response mismatch:\n%v", cmp.Diff(createResponse, expectedResponse))
			}
		})
		t.Run("Operation not allowed", func(t *testing.T) {
			_, err := server.CreateImage(
				incomingContextMd(map[string][]string{
					"user":   []string{"bob"},
					"groups": []string{"admin"},
				}),
				&rpc.ImageCreateRequest{},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			} else if err.Error() != "rpc error: code = PermissionDenied desc = The user is not authorized to access this content." {
				t.Errorf("Unexpected error: %v", err)
			}
		})
	})

	t.Run("Image Update Operations", func(t *testing.T) {
		t.Run("Operation allowed, image updated", func(t *testing.T) {
			now := time.Now().UTC()
			expectedUpdateImage.ExpectExec().WithArgs(
				"image/jpg", // format
				100,         // width
				100,         // height
				true,        // active
				now,         // published_at
				"caption",   // caption
				"credit",    // credit
				false,       // allow_reproduction
				1,           // type_id
				1,           // id
			).WillReturnResult(sqlmock.NewResult(0, 1))

			tsPublishedAt, err := ptypes.TimestampProto(now)
			if err != nil {
				t.Fatalf("Error converting time to proto: %v", err)
			}
			updateResponse, err := server.UpdateImage(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.ImageUpdateRequest{
					Format:            "image/jpg",
					Width:             100,
					Height:            100,
					Active:            true,
					PublishedAt:       tsPublishedAt,
					Caption:           "caption",
					Credit:            "credit",
					AllowReproduction: false,
					TypeId:            1,
					Id:                1,
				},
			)
			if err != nil {
				t.Errorf("Unexpected error: %v", err)
			}
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
			expectedResponse := &rpc.ImageUpdateResponse{}
			if !cmp.Equal(updateResponse, expectedResponse) {
				t.Errorf("Response mismatch:\n%v", cmp.Diff(updateResponse, expectedResponse))
			}
		})
		t.Run("Operation allowed, image not found", func(t *testing.T) {
			now := time.Now().UTC()
			expectedUpdateImage.ExpectExec().WithArgs(
				"image/jpg", // format
				100,         // width
				100,         // height
				true,        // active
				now,         // published_at
				"caption",   // caption
				"credit",    // credit
				false,       // allow_reproduction
				1,           // type_id
				1,           // id
			).WillReturnResult(sqlmock.NewResult(0, 0))

			tsPublishedAt, err := ptypes.TimestampProto(now)
			if err != nil {
				t.Fatalf("Error converting time to proto: %v", err)
			}
			_, err = server.UpdateImage(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.ImageUpdateRequest{
					Format:            "image/jpg",
					Width:             100,
					Height:            100,
					Active:            true,
					PublishedAt:       tsPublishedAt,
					Caption:           "caption",
					Credit:            "credit",
					AllowReproduction: false,
					TypeId:            1,
					Id:                1,
				},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			} else if err.Error() != "rpc error: code = NotFound desc = Error updating image ID 1: Image not found" {
				t.Errorf("Unexpected error: %v", err)
			}
		})
		t.Run("Operation not allowed", func(t *testing.T) {
			_, err = server.UpdateImage(
				incomingContextMd(map[string][]string{
					"user":   []string{"bob"},
					"groups": []string{"admin"},
				}),
				&rpc.ImageUpdateRequest{
					Format:            "image/jpg",
					Width:             100,
					Height:            100,
					Active:            true,
					PublishedAt:       ptypes.TimestampNow(),
					Caption:           "caption",
					Credit:            "credit",
					AllowReproduction: false,
					TypeId:            1,
					Id:                1,
				},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			} else if err.Error() != "rpc error: code = PermissionDenied desc = The user is not authorized to access this content." {
				t.Errorf("Unexpected error: %v", err)
			}
		})
	})

	t.Run("Image Delete Operations", func(t *testing.T) {
		t.Run("Operation allowed, image deleted", func(t *testing.T) {
			expectedDeleteImage.ExpectExec().WithArgs(1).WillReturnResult(sqlmock.NewResult(0, 1))

			deleteResponse, err := server.DeleteImage(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.ImageDeleteRequest{Id: 1},
			)
			if err != nil {
				t.Errorf("Unexpected error: %v", err)
			}
			expectedResponse := &rpc.ImageDeleteResponse{}
			if !cmp.Equal(deleteResponse, expectedResponse) {
				t.Errorf("Response mismatch\n%v", cmp.Diff(deleteResponse, expectedResponse))
			}
		})
		t.Run("Operation allowed, image not found", func(t *testing.T) {
			expectedDeleteImage.ExpectExec().WithArgs(1).WillReturnResult(sqlmock.NewResult(0, 0))

			_, err := server.DeleteImage(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.ImageDeleteRequest{Id: 1},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			} else if err.Error() != "rpc error: code = Unknown desc = Error deleting image ID 1: Image not found" {
				t.Errorf("Unexpected error: %v", err)
			}
		})
		t.Run("Operation not allowed", func(t *testing.T) {
			_, err := server.DeleteImage(
				incomingContextMd(map[string][]string{
					"user":   []string{"bob"},
					"groups": []string{"admin"},
				}),
				&rpc.ImageDeleteRequest{Id: 1},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			} else if err.Error() != "rpc error: code = PermissionDenied desc = The user is not authorized to access this content." {
				t.Errorf("Unexpected error: %v", err)
			}

		})
	})

	t.Run("Image Add Site Operations", func(t *testing.T) {
		t.Run("Operation allowed, image added to site", func(t *testing.T) {
			expectedImageAddSite.ExpectExec().WithArgs(1, 1).WillReturnResult(sqlmock.NewResult(0, 1))
			addResponse, err := server.AddToSite(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.AddToSiteRequest{
					Id:     1,
					SiteId: 1,
				},
			)
			if err != nil {
				t.Errorf("Unexpected error: %v", err)
			}
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
			expectedResponse := &rpc.AddToSiteResponse{}
			if !cmp.Equal(addResponse, expectedResponse) {
				t.Errorf("Response mismatch:\n%v", cmp.Diff(addResponse, expectedResponse))
			}
		})
		t.Run("Operation not allowed", func(t *testing.T) {
			_, err := server.AddToSite(
				incomingContextMd(map[string][]string{
					"user":   []string{"bob"},
					"groups": []string{"admin"},
				}),
				&rpc.AddToSiteRequest{
					Id:     1,
					SiteId: 1,
				},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			} else if err.Error() != "rpc error: code = PermissionDenied desc = The user is not authorized to access this content." {
				t.Errorf("Unexpected error: %v", err)
			}
		})
	})

	t.Run("Image Remove Site Operations", func(t *testing.T) {
		t.Run("Operation allowed, image removed from site", func(t *testing.T) {
			expectedImageRemoveSite.ExpectExec().WithArgs(1, 1).WillReturnResult(sqlmock.NewResult(0, 1))
			removeResponse, err := server.RemoveFromSite(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.RemoveFromSiteRequest{
					Id:     1,
					SiteId: 1,
				},
			)
			if err != nil {
				t.Errorf("Unexpected error: %v", err)
			}
			expectedResponse := &rpc.RemoveFromSiteResponse{}
			if !cmp.Equal(removeResponse, expectedResponse) {
				t.Errorf("Response mismatch:\n%v", cmp.Diff(removeResponse, expectedResponse))
			}
		})
		t.Run("Operation not allowed", func(t *testing.T) {
			_, err := server.RemoveFromSite(
				incomingContextMd(map[string][]string{
					"user":   []string{"bob"},
					"groups": []string{"admin"},
				}),
				&rpc.RemoveFromSiteRequest{
					Id:     1,
					SiteId: 1,
				},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			} else if err.Error() != "rpc error: code = PermissionDenied desc = The user is not authorized to access this content." {
				t.Errorf("Unexpected error: %v", err)
			}
		})
	})

	t.Run("Image Type Read Operations", func(t *testing.T) {
		t.Run("Operation allowed, image type read", func(t *testing.T) {
			now := time.Now().UTC()

			expectedReadImageType.ExpectQuery().WithArgs(1).WillReturnRows(
				sqlmock.NewRows([]string{"id", "name", "created_at", "updated_at", "deleted_at"}).
					AddRow(1, "Some Type", now, now, time.Time{}),
			)

			readResponse, err := server.ReadImageType(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.TypeReadRequest{Id: 1},
			)
			if err != nil {
				t.Fatalf("Unexpected error: %v", err)
			}
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Fatalf("Unfulfilled database expectations: %v", err)
			}

			tsCreatedAt, err := ptypes.TimestampProto(now)
			if err != nil {
				t.Fatalf("Error converting time to proto: %v", err)
			}
			tsUpdatedAt, err := ptypes.TimestampProto(now)
			if err != nil {
				t.Fatalf("Error converting time to proto: %v", err)
			}
			tsDeletedAt, err := ptypes.TimestampProto(time.Time{})
			if err != nil {
				t.Fatalf("Error converting time to proto: %v", err)
			}

			expected := &rpc.TypeReadResponse{
				Id:        1,
				Name:      "Some Type",
				CreatedAt: tsCreatedAt,
				UpdatedAt: tsUpdatedAt,
				DeletedAt: tsDeletedAt,
			}
			if !cmp.Equal(readResponse, expected) {
				t.Errorf("Response mismatch:\n%v", cmp.Diff(readResponse, expected))
			}
		})
		t.Run("Operation allowed, image type not found", func(t *testing.T) {
			expectedReadImageType.ExpectQuery().WithArgs(2).WillReturnRows(
				sqlmock.NewRows([]string{
					"id", "name", "created_at", "updated_at", "deleted_at",
				}),
			)

			_, err := server.ReadImageType(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.TypeReadRequest{Id: 2},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			}
			if err.Error() != "rpc error: code = NotFound desc = Error reading image type ID 2: Image Type not found" {
				t.Errorf("Unexpected error: %v", err)
			}
		})
		t.Run("Operation not allowed", func(t *testing.T) {
			_, err := server.ReadImageType(
				incomingContextMd(map[string][]string{
					"user":   []string{"bob"},
					"groups": []string{"admin"},
				}),
				&rpc.TypeReadRequest{Id: 1},
			)

			if err == nil {
				t.Fatalf("Expected error, got nil")
			} else if err.Error() != "rpc error: code = PermissionDenied desc = The user is not authorized to access this content." {
				t.Errorf("Unexpected error: %v", err)
			}
		})
	})

	t.Run("Image Type Create Operations", func(t *testing.T) {
		t.Run("Operation allowed, image type created", func(t *testing.T) {
			expectedCreateImageType.ExpectQuery().WithArgs("Some Type").WillReturnRows(
				sqlmock.NewRows([]string{"id"}).AddRow(1),
			)
			createResponse, err := server.CreateImageType(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.TypeCreateRequest{Name: "Some Type"},
			)
			if err != nil {
				t.Errorf("Unexpected error: %v", err)
			}
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
			expectedResponse := &rpc.TypeCreateResponse{Id: 1}
			if !cmp.Equal(createResponse, expectedResponse) {
				t.Errorf("Response mismatch:\n%v", cmp.Diff(createResponse, expectedResponse))
			}
		})
		t.Run("Operation not allowed", func(t *testing.T) {
			_, err := server.CreateImageType(
				incomingContextMd(map[string][]string{
					"user":   []string{"bob"},
					"groups": []string{"admin"},
				}),
				&rpc.TypeCreateRequest{Name: "Some Type"},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			} else if err.Error() != "rpc error: code = PermissionDenied desc = The user is not authorized to access this content." {
				t.Errorf("Unexpected error: %v", err)
			}
		})
	})

	t.Run("Image Type Update Operations", func(t *testing.T) {
		t.Run("Operation allowed, image type updated", func(t *testing.T) {
			expectedUpdateImageType.ExpectExec().WithArgs(
				"Some Type", 1,
			).WillReturnResult(sqlmock.NewResult(0, 1))
			updateResponse, err := server.UpdateImageType(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.TypeUpdateRequest{Id: 1, Name: "Some Type"},
			)
			if err != nil {
				t.Errorf("Unexpected error: %v", err)
			}
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
			expectedResponse := &rpc.TypeUpdateResponse{}
			if !cmp.Equal(updateResponse, expectedResponse) {
				t.Errorf("Response mismatch:\n%v", cmp.Diff(updateResponse, expectedResponse))
			}
		})
		t.Run("Operation allowed, image type not found", func(t *testing.T) {
			expectedUpdateImageType.ExpectExec().WithArgs(
				"Some Type", 1,
			).WillReturnResult(sqlmock.NewResult(0, 0))
			_, err := server.UpdateImageType(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.TypeUpdateRequest{Id: 1, Name: "Some Type"},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			}
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
			if err.Error() != "rpc error: code = NotFound desc = Error updating image type ID 1: Image Type not found" {
				t.Errorf("Unexpected error: %v", err)
			}
		})
		t.Run("Operation not allowed", func(t *testing.T) {
			_, err := server.UpdateImageType(
				incomingContextMd(map[string][]string{
					"user":   []string{"bob"},
					"groups": []string{"admin"},
				}),
				&rpc.TypeUpdateRequest{Id: 1, Name: "Some Type"},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			} else if err.Error() != "rpc error: code = PermissionDenied desc = The user is not authorized to access this content." {
				t.Errorf("Unexpected error: %v", err)
			}
		})
	})

	t.Run("Image Type Delete Operations", func(t *testing.T) {
		t.Run("Operation allowed, image type deleted", func(t *testing.T) {
			expectedDeleteImageType.ExpectExec().WithArgs(1).WillReturnResult(sqlmock.NewResult(0, 1))

			deleteResponse, err := server.DeleteImageType(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.TypeDeleteRequest{Id: 1},
			)
			if err != nil {
				t.Fatalf("Unexpected error: %v", err)
			}
			expectedResponse := &rpc.TypeDeleteResponse{}
			if !cmp.Equal(deleteResponse, expectedResponse) {
				t.Errorf("Response mismatch\n%v", cmp.Diff(deleteResponse, expectedResponse))
			}
		})
		t.Run("Operation allowed, image type not found", func(t *testing.T) {
			expectedDeleteImageType.ExpectExec().WithArgs(2).WillReturnResult(sqlmock.NewResult(0, 0))

			_, err := server.DeleteImageType(
				incomingContextMd(map[string][]string{
					"user":   []string{"jason"},
					"groups": []string{"admin"},
				}),
				&rpc.TypeDeleteRequest{Id: 2},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			}
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("Unfulfilled database expectations: %v", err)
			}
			if err.Error() != "rpc error: code = NotFound desc = Error deleting image type ID 2: Image Type not found" {
				t.Errorf("Unexpected error: %v", err)
			}

		})
		t.Run("Operation not allowed", func(t *testing.T) {
			_, err := server.DeleteImageType(
				incomingContextMd(map[string][]string{
					"user":   []string{"bob"},
					"groups": []string{"admin"},
				}),
				&rpc.TypeDeleteRequest{Id: 1},
			)
			if err == nil {
				t.Fatalf("Expected error, got nil")
			} else if err.Error() != "rpc error: code = PermissionDenied desc = The user is not authorized to access this content." {
				t.Errorf("Unexpected error: %v", err)
			}
		})
	})
}
