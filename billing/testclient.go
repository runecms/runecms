// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package billing

import (
	"context"
	"strings"
	"testing"
	"time"

	"git.runecms.io/rune/runecms/billing/internal/billing"
	pb "git.runecms.io/rune/runecms/billing/rpc"
	"git.runecms.io/rune/runecms/internal/testutil"
)

type TestClient struct {
	T *testing.T
}

var (
	token = strings.ToUpper(testutil.GetUUID())
)

func (c *TestClient) Create(ctx context.Context, req *pb.CreateBillingAccountRequest) (*pb.CreateBillingAccountResponse, error) {
	return &pb.CreateBillingAccountResponse{
		BillingAccount: &pb.BillingAccount{
			Id:         strings.ToUpper(testutil.GetUUID()),
			LastFour:   "4321",
			NameOnCard: "Test User",
			Token:      token,
			CreatedAt:  time.Now().Format(time.RFC3339),
		},
	}, nil
}

func (c *TestClient) Delete(ctx context.Context, req *pb.DeleteBillingAccountRequest) (*pb.DeleteBillingAccountResponse, error) {
	return &pb.DeleteBillingAccountResponse{
		BillingAccount: &pb.BillingAccount{
			Id:         req.GetId(),
			LastFour:   "4321",
			NameOnCard: "Test User",
			Token:      token,
			CreatedAt:  time.Now().Format(time.RFC3339),
			DeletedAt:  time.Now().Format(time.RFC3339),
		},
	}, nil
}

func (c *TestClient) Get(ctx context.Context, req *pb.GetBillingAccountRequest) (*pb.GetBillingAccountResponse, error) {
	return &pb.GetBillingAccountResponse{
		BillingAccount: &pb.BillingAccount{
			Id:         req.GetId(),
			LastFour:   "4321",
			NameOnCard: "Test User",
			Token:      token,
			CreatedAt:  time.Now().Format(time.RFC3339),
		},
	}, nil
}

func (c *TestClient) Charge(ctx context.Context, req *pb.ChargeBillingAccountRequest) (*pb.ChargeBillingAccountResponse, error) {
	return &pb.ChargeBillingAccountResponse{
		Transaction: &pb.BillingTransaction{
			Id:               strings.ToUpper(testutil.GetUUID()),
			BillingAccountId: req.GetId(),
			Amount:           req.GetAmount(),
			Status:           billing.StatusSuccess,
			Response:         "",
			Metadata:         []byte{},
			CreatedAt:        time.Now().Format(time.RFC3339),
		},
	}, nil
}

func NewTestClient(t *testing.T) *TestClient {
	return &TestClient{
		T: t,
	}
}
