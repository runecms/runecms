+++
title = "general"
weight = 1
+++

## The Billing Service

The Billing service is responsible for:

* securely storing tokenized payment methods, and allowing internal services to charge a token
* processing recurring subscription payments

This service only supports BrainTree.  For Stripe integration, we would recommend cloning this service and replacing Braintree-specific functions with Stripe equivelants.
