+++
title = "api"
weight = 2
+++
## Using the Billing API

When using the Billing API there's 3 main actions you can take.

### Create a new [Billing account](https://godocs.gorune.io/pkg/git.runecms.io/rune/runecms/billing/rpc/#BillingAccount)

This is done by sending a [CreateBillingAccountRequest](https://godocs.gorune.io/pkg/git.runecms.io/rune/runecms/billing/rpc/#CreateBillingAccountRequest) to `Create`.

### Delete a Billing Account

Deleting a billing account sets the `DeletedAt` date to `now`.

When an account is deleted, you will be unable to retrieve or charge the account.

This is done by sending a [DeleteBillingAccountRequest](https://godocs.gorune.io/pkg/git.runecms.io/rune/runecms/billing/rpc/#DeleteBillingAccountRequest) to `Delete`.

### Charge a Billing Account

You can charge an account a specified amount of money.

This charge is logged in the rune database in the `billing_transactions` table.

This is done by sending a [ChargeBillingAccountRequest](https://godocs.gorune.io/pkg/git.runecms.io/rune/runecms/billing/rpc/#ChargeBillingAccountRequest)
