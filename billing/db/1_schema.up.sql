CREATE TABLE billing_accounts(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  token TEXT NOT NULL,
  name_on_card TEXT NOT NULL,
  last_four TEXT NOT NULL,
  expiration_date DATE NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE billing_transaction_statuses(
  name TEXT PRIMARY KEY,
  description TEXT
);

INSERT INTO
  billing_transaction_statuses (
    name,
    description
  )
VALUES (
  'failure',
  'The transaction was processed but failed'
), (
  'success',
  'The transaction was successfully processed'
), (
  'pending',
  'The transaction is being processed and is awaiting an update'
), (
  'error',
  'The transaction failed before it could be submitted for processing because of an internal server error'
);

-- billing_transactions contains a log of all transactions
CREATE TABLE billing_transactions(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  billing_account_id UUID NOT NULL REFERENCES billing_accounts(id),
  amount INTEGER NOT NULL,
  status TEXT NOT NULL REFERENCES billing_transaction_statuses(name),
  response TEXT,
  metadata BYTEA,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp
);
