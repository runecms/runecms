// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package billingtwirp

import (
	"context"
	"math/rand"
	"os"
	"testing"
	"time"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/billing/internal/billing"
	pb "git.runecms.io/rune/runecms/billing/rpc"
	"git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/webhooks"

	"github.com/google/go-cmp/cmp"
)

var (
	valid_context   context.Context
	invalid_context context.Context
	valid_id        runecms.Identity
	webhooksClient  = &webhooks.TestClient{}

	braintreePublicKey  string
	braintreePrivateKey string
	braintreeMerchantId string
	braintreeToken      string
)

func init() {
	valid_context = testutil.CreateContextWithMeta(map[string][]string{
		"user":   []string{"valid"},
		"groups": []string{"admin"},
	})

	invalid_context = testutil.CreateContextWithMeta(map[string][]string{
		"user":   []string{"invalid"},
		"groups": []string{},
	})

	valid_id = runecms.Be("valid", "admin")

	braintreePublicKey = os.Getenv("BRAINTREE_PUBLIC_KEY")
	braintreePrivateKey = os.Getenv("BRAINTREE_PRIVATE_KEY")
	braintreeMerchantId = os.Getenv("BRAINTREE_MERCHANT_ID")
	braintreeToken = os.Getenv("BRAINTREE_TOKEN")
}

func TestDeleteAccount(t *testing.T) {
	// Create a new billing item to call Get on
	var (
		token  = testutil.GetUUID()
		db     = testutil.GetTestDb(t)
		warden = testutil.GetTestLadon()
		s      = &Server{
			Db:       db,
			Warden:   warden,
			Webhooks: webhooksClient,
			BraintreeInfo: &pb.BraintreeInfo{
				Environment: "sandbox",
				MerchantId:  braintreeMerchantId,
				PublicKey:   braintreePublicKey,
				PrivateKey:  braintreePrivateKey,
			},
		}
	)

	account, err := billing.CreateBillingAccount(db, warden, webhooksClient, valid_id, token, "4321", "Test User", time.Now().AddDate(1, 0, 0).UTC())
	if err != nil {
		t.Fatalf("Unable to set up test for delete. Error: %s", err.Error())
	}

	t.Run("Valid request", func(t *testing.T) {
		res, err := s.Delete(valid_context, &pb.DeleteBillingAccountRequest{
			Id: account.Id,
		})

		if err != nil {
			t.Error(err.Error())
		}

		if res.GetBillingAccount().GetDeletedAt() == "" {
			t.Fatal("Valid delete returned nil DeletedAt")
		}
	})

	t.Run("Invalid request", func(t *testing.T) {
		_, err := s.Delete(valid_context, &pb.DeleteBillingAccountRequest{
			Id: "idontexist",
		})

		if err == nil {
			t.Error("s.Get should return an error when given an invalid key")
		}
	})

	t.Run("Invalid context", func(t *testing.T) {
		_, err := s.Delete(invalid_context, &pb.DeleteBillingAccountRequest{
			Id: account.Id,
		})

		if err == nil {
			t.Error("s.Delete should return an error when given an invalid key")
		}
	})
}

func TestGetAccount(t *testing.T) {
	// Create a new billing item to call Get on
	var (
		token  = testutil.GetUUID()
		db     = testutil.GetTestDb(t)
		warden = testutil.GetTestLadon()
		s      = &Server{
			Db:       db,
			Warden:   warden,
			Webhooks: webhooksClient,
			BraintreeInfo: &pb.BraintreeInfo{
				Environment: "sandbox",
				MerchantId:  braintreeMerchantId,
				PublicKey:   braintreePublicKey,
				PrivateKey:  braintreePrivateKey,
			},
		}
	)

	account, err := billing.CreateBillingAccount(db, warden, webhooksClient, valid_id, token, "4321", "Test User", time.Now().AddDate(1, 0, 0).UTC())
	if err != nil {
		t.Fatalf("Unable to set up test for grpc.Get. Error: %s", err.Error())
	}

	t.Run("Valid request", func(t *testing.T) {
		res, err := s.Get(valid_context, &pb.GetBillingAccountRequest{
			Id: account.Id,
		})

		if err != nil {
			t.Error(err.Error())
		}

		expected := &pb.GetBillingAccountResponse{
			BillingAccount: &pb.BillingAccount{
				Id:             account.Id,
				CreatedAt:      account.CreatedAt,
				Token:          account.Token,
				LastFour:       "4321",
				NameOnCard:     "Test User",
				ExpirationDate: account.ExpirationDate,
			},
		}
		if cmp.Equal(res, expected) != true {
			t.Fatalf("Unexpected response.%s\n", cmp.Diff(res, expected))
		}
	})

	t.Run("Invalid Get", func(t *testing.T) {
		_, err := s.Get(valid_context, &pb.GetBillingAccountRequest{
			Id: "idontexist",
		})

		if err == nil {
			t.Error("s.Get should return an error when given an invalid key")
		}
	})

	t.Run("Invalid context", func(t *testing.T) {
		_, err := s.Get(invalid_context, &pb.GetBillingAccountRequest{
			Id: account.Id,
		})

		if err == nil {
			t.Error("s.Get should return an error when given an invalid key")
		}
	})
}

func TestCreateAccount(t *testing.T) {
	// Create a new billing item to call Get on
	var (
		token  = testutil.GetUUID()
		db     = testutil.GetTestDb(t)
		warden = testutil.GetTestLadon()
		s      = &Server{
			Db:       db,
			Warden:   warden,
			Webhooks: webhooksClient,
			BraintreeInfo: &pb.BraintreeInfo{
				Environment: "sandbox",
				MerchantId:  braintreeMerchantId,
				PublicKey:   braintreePublicKey,
				PrivateKey:  braintreePrivateKey,
			},
		}
	)

	t.Run("Valid Request", func(t *testing.T) {
		exp := time.Now().AddDate(1, 1, 1).Format(time.RFC3339)
		res, err := s.Create(valid_context, &pb.CreateBillingAccountRequest{
			Token:          token,
			LastFour:       "4321",
			NameOnCard:     "Test User",
			ExpirationDate: exp,
		})

		if err != nil {
			t.Error(err.Error())
		}

		expected := &pb.CreateBillingAccountResponse{
			BillingAccount: &pb.BillingAccount{
				Id:             res.GetBillingAccount().GetId(),
				Token:          token,
				CreatedAt:      res.GetBillingAccount().GetCreatedAt(),
				LastFour:       "4321",
				NameOnCard:     "Test User",
				ExpirationDate: res.GetBillingAccount().GetExpirationDate(),
			},
		}

		if expected.GetBillingAccount().GetId() == "" {
			t.Fatal("Valid call to Create gave an empty Id")
		}

		if cmp.Equal(res, expected) != true {
			t.Fatalf("Unexpected response.%s\n", cmp.Diff(res, expected))
		}
	})

	t.Run("Invalid Request", func(t *testing.T) {
		_, err := s.Create(valid_context, &pb.CreateBillingAccountRequest{
			Token:          "",
			LastFour:       "4321",
			NameOnCard:     "Test User",
			ExpirationDate: time.Now().AddDate(1, 0, 0).UTC().Format(time.RFC3339),
		})
		if err == nil {
			t.Fatal("When given an empty token, Create should return an error")
		}
	})

	t.Run("Invalid context", func(t *testing.T) {
		_, err := s.Create(invalid_context, &pb.CreateBillingAccountRequest{
			Token:          token,
			LastFour:       "4321",
			NameOnCard:     "Test User",
			ExpirationDate: time.Now().AddDate(1, 0, 0).UTC().Format(time.RFC3339),
		})

		if err == nil {
			t.Error("Create should return an error when given an invalid context")
		}
	})
}

func TestCharge(t *testing.T) { // Create a new billing item to call Get on
	var (
		token  = braintreeToken
		db     = testutil.GetTestDb(t)
		warden = testutil.GetTestLadon()
		s      = &Server{
			Db:       db,
			Warden:   warden,
			Webhooks: webhooksClient,
			BraintreeInfo: &pb.BraintreeInfo{
				Environment: "sandbox",
				MerchantId:  braintreeMerchantId,
				PublicKey:   braintreePublicKey,
				PrivateKey:  braintreePrivateKey,
			},
		}
	)
	account, err := billing.CreateBillingAccount(db, warden, webhooksClient, valid_id, token, "4321", "Test User", time.Now().AddDate(1, 0, 0).UTC())

	if err != nil {
		t.Fatalf("Unable to set up test for testcredit. Error: %s", err.Error())
	}

	t.Run("Valid request", func(t *testing.T) {
		res, err := s.Charge(valid_context, &pb.ChargeBillingAccountRequest{
			Id:                  account.Id,
			Amount:              rand.Int63() % 1000,
			SubmitForSettlement: true,
		})

		if err != nil {
			t.Error(err.Error())
		}

		if res.GetTransaction().GetId() == "" {
			t.Fatal("Valid credit returned empty transaction Id")
		}
	})
}
