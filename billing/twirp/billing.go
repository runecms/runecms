// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package billingtwirp

import (
	"context"
	"time"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/billing/internal/billing"
	pb "git.runecms.io/rune/runecms/billing/rpc"

	"github.com/lionelbarrow/braintree-go"
	"github.com/twitchtv/twirp"
)

func (s *Server) Get(c context.Context, req *pb.GetBillingAccountRequest) (*pb.GetBillingAccountResponse, error) {
	user, err := runecms.IdentityFromRPC(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := billing.GetBillingAccountById(s.Db, s.Warden, user, req.GetId())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.GetBillingAccountResponse{
		BillingAccount: model,
	}, nil
}

func (s *Server) Create(c context.Context, req *pb.CreateBillingAccountRequest) (*pb.CreateBillingAccountResponse, error) {
	user, err := runecms.IdentityFromRPC(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	exp, err := time.Parse(time.RFC3339, req.GetExpirationDate())
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	model, err := billing.CreateBillingAccount(s.Db, s.Warden, s.Webhooks, user, req.GetToken(), req.GetLastFour(), req.GetNameOnCard(), exp)

	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.CreateBillingAccountResponse{
		BillingAccount: model,
	}, nil
}

func (s *Server) Delete(c context.Context, req *pb.DeleteBillingAccountRequest) (*pb.DeleteBillingAccountResponse, error) {
	user, err := runecms.IdentityFromRPC(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	model, err := billing.DeleteBillingAccount(s.Db, s.Warden, s.Webhooks, user, req.GetId())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}
	return &pb.DeleteBillingAccountResponse{
		BillingAccount: model,
	}, nil
}

func (s *Server) Charge(c context.Context, req *pb.ChargeBillingAccountRequest) (*pb.ChargeBillingAccountResponse, error) {
	user, err := runecms.IdentityFromRPC(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	var env braintree.Environment
	switch s.BraintreeInfo.GetEnvironment() {
	case billing.EnvironmentSandbox:
		env = braintree.Sandbox
	case billing.EnvironmentProduction:
		env = braintree.Production
	default:
		return nil, twirp.InternalError("The environment provided was unrecognized")
	}

	bt := braintree.New(
		env,
		s.BraintreeInfo.MerchantId,
		s.BraintreeInfo.PublicKey,
		s.BraintreeInfo.PrivateKey,
	)

	transaction, err := billing.ChargeBillingAccount(s.Db, s.Warden, s.Webhooks, user, req.GetId(), req.GetAmount(), req.GetSubmitForSettlement(), bt, req.GetMetadata())
	if err != nil {
		return nil, err.(runecms.RuneError).GetTwirpError()
	}

	return &pb.ChargeBillingAccountResponse{
		Transaction: transaction,
	}, nil
}
