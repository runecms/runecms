// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package billingtwirp

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"github.com/ory/ladon"

	"git.runecms.io/rune/runecms/billing/rpc"
	"git.runecms.io/rune/runecms/internal/twirputil"
	"git.runecms.io/rune/runecms/webhooks/rpc"
)

type Server struct {
	Db            *sql.DB
	Warden        *ladon.Ladon
	BraintreeInfo *billingpb.BraintreeInfo
	Webhooks      webhookspb.Webhooks
}

func NewServer(db *sql.DB, w *ladon.Ladon, bt *billingpb.BraintreeInfo, webhooks webhookspb.Webhooks) *Server {
	return &Server{
		Db:            db,
		Warden:        w,
		BraintreeInfo: bt,
		Webhooks:      webhooks,
	}
}

func (s *Server) Start(port int) error {
	log.Printf("Starting the billing service on 0.0.0.0:%d", port)
	var (
		billingHandler = billingpb.NewBillingAccountsServer(s, nil)
	)
	r := http.NewServeMux()
	r.Handle(billingpb.BillingAccountsPathPrefix, twirputil.AddMiddleware(billingHandler))
	return http.ListenAndServe(fmt.Sprintf(":%d", port), r)
}
