// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package billing

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"git.runecms.io/rune/runecms"
	pb "git.runecms.io/rune/runecms/billing/rpc"
	"git.runecms.io/rune/runecms/webhooks"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/lionelbarrow/braintree-go"
	"github.com/ory/ladon"
)

const (
	EnvironmentSandbox    = "sandbox"
	EnvironmentProduction = "production"
)

const (
	StatusPending = "pending"
	StatusFailure = "failure"
	StatusSuccess = "success"
	StatusError   = "error"
)

var (
	SqlSelectBillingAccountById = `
	SELECT
		token,
		last_four,
		name_on_card,
		expiration_date,
		created_at,
		deleted_at
	FROM
		billing_accounts
	WHERE
		id=$1
	`

	SqlInsertBillingAccount = `
	INSERT INTO
		billing_accounts (
			token,
			last_four,
			name_on_card,
			expiration_date
		)
	VALUES (
		$1,
		$2,
		$3,
		$4
	)
	RETURNING id
	`

	SqlDeleteBillingAccountById = `
	UPDATE
		billing_accounts
	SET
		deleted_at=now()
	WHERE
		id=$1
	AND
		deleted_at IS NULL
	RETURNING
		deleted_at
	`
)

var (
	InternalIdent = runecms.Be("interal:billing", "system")
)

func GetBillingAccountById(db *sql.DB, w *ladon.Ladon, ident runecms.Identity, id string) (*pb.BillingAccount, error) {
	if err := runecms.Access(ident, w, "read", fmt.Sprintf("billing:billing_accounts:%s", id)); err != nil {
		log.Printf("User %s tried (and failed) to access billing account %s", ident.User, id)
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	if id == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "The ID provided is empty")
	}

	row := db.QueryRow(SqlSelectBillingAccountById, id)

	model := &pb.BillingAccount{
		Id: id,
	}

	var (
		deletedAt sql.NullString
	)

	if err := row.Scan(
		&model.Token,
		&model.LastFour,
		&model.NameOnCard,
		&model.ExpirationDate,
		&model.CreatedAt,
		&deletedAt,
	); err != nil {
		log.Printf("Error when trying to scan SQL result for %s. Error: %s", id, err.Error())
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, fmt.Sprintf("Error when trying to scan SQL result for %s. Error: %s", id, err.Error()))
		}
		return nil, runecms.Error(runecms.Internal, fmt.Sprintf("Error when trying to scan SQL result for %s. Error: %s", id, err.Error()))
	}

	if deletedAt.Valid {
		model.DeletedAt = deletedAt.String
	}

	return model, nil
}

// CreateBillingAccount creates a new billing account in the provided database
func CreateBillingAccount(
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	token string,
	lastFour string,
	nameOnCard string,
	expirationDate time.Time,
) (*pb.BillingAccount, error) {
	if err := runecms.Access(ident, w, "create", "billing:billing_accounts"); err != nil {
		log.Printf("User %s tried (and failed) to create a billing account", ident.User)
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}
	if token == "" {
		return nil, runecms.Error(runecms.InvalidArgument, "The token provided is empty")
	}

	row := db.QueryRow(
		SqlInsertBillingAccount,
		token,
		lastFour,
		nameOnCard,
		expirationDate,
	)

	var id string
	if err := row.Scan(&id); err != nil {
		log.Printf("Error when trying to scan SQL result when creating a new BillingAccount. Error: %s", err.Error())
		return nil, runecms.Error(runecms.Internal, "The token provided is empty")
	}

	log.Printf("User %s successfully created billing account %s", ident.User, id)

	model, err := GetBillingAccountById(db, w, ident, id)
	if err != nil {
		return nil, err
	}

	w.Manager.Create(&ladon.DefaultPolicy{
		ID:          fmt.Sprintf("%s-billing-account-%s", ident.User, model.GetId()),
		Description: fmt.Sprintf("Give user %s full access to the billing account %s", ident.User, model.GetId()),
		Subjects:    []string{runecms.UserSubject(ident.User)},
		Effect:      ladon.AllowAccess,
		Resources:   []string{fmt.Sprintf("billing:billing_accounts:%s", model.GetId())},
		Actions:     []string{"read", "update", "delete"},
	})

	go webhooks.Distribute(webhooksClient, ident, "billing:accounts", "create", model)
	return model, nil
}

func DeleteBillingAccount(
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	id string,
) (*pb.BillingAccount, error) {
	if err := runecms.Access(ident, w, "delete", fmt.Sprintf("billing:billing_accounts:%s", id)); err != nil {
		log.Printf("User %s tried (and failed) to delete billing account %s", ident.User, id)
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	row := db.QueryRow(SqlDeleteBillingAccountById, id)
	var deletedAt string

	if err := row.Scan(&deletedAt); err != nil {
		log.Printf("Error when trying to scan SQL result when deleting billing account %s. Error: %s", id, err.Error())
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	log.Printf("User %s successfully deleted billing account %s", ident.User, id)
	go webhooks.Distribute(webhooksClient, ident, "billing:accounts", "delete", map[string]interface{}{
		"id":         id,
		"deleted_at": deletedAt,
	})

	return GetBillingAccountById(db, w, ident, id)
}

func ChargeBillingAccount(
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	id string,
	amount int64,
	submitForSettlement bool,
	bt *braintree.Braintree,
	meta json.RawMessage,
) (*pb.BillingTransaction, error) {
	if err := runecms.Access(ident, w, "update", fmt.Sprintf("billing:billing_accounts:%s", id)); err != nil {
		log.Printf("User %s tried (and failed) to credit billing account %s", ident.User, id)
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	// Start a new transaction
	billingTransaction, err := CreateTransaction(db, w, webhooksClient, ident, id, amount, StatusPending, meta)
	if err != nil {
		return nil, err
	}

	ba, err := GetBillingAccountById(db, w, ident, id)
	if err != nil {
		return nil, err
	}

	braintreeTransaction, err := bt.Transaction().Create(context.Background(), &braintree.TransactionRequest{
		Amount:             braintree.NewDecimal(amount, 2),
		Type:               "sale",
		PaymentMethodToken: ba.Token,
		Options: &braintree.TransactionOptions{
			SubmitForSettlement: submitForSettlement,
		},
	})

	if err != nil {
		billingTransaction.Status = StatusError
		billingTransaction.Metadata = meta
		billingTransaction.Response = err.Error()
		if _, err := UpdateTransaction(db, w, webhooksClient, ident, billingTransaction); err != nil {
			log.Printf("Failed to update transaction to error status. This should never happen; there must be a connection issue. Error: %s", err.Error())
		}
		return nil, err
	}

	response, err := json.Marshal(braintreeTransaction)
	if err != nil {
		log.Printf("Failed to marshal braintree transaction for response before updating billing transaction. Error: %s", err.Error())
	}

	billingTransaction.Status = StatusSuccess
	billingTransaction.Response = string(response)

	transaction, err := UpdateTransaction(db, w, webhooksClient, ident, billingTransaction)
	if err != nil {
		return nil, runecms.Error(runecms.PermissionDenied, fmt.Sprintf("Failed to update transaction to success status. This should never happen; there must be a connection issue. Error: %s", err.Error()))
	}

	webhookMeta := map[string]interface{}{
		"braintree_transaction": transaction,
		"transaction":           billingTransaction,
		"meta":                  meta,
	}

	go webhooks.Distribute(webhooksClient, ident, "billing:charge", "create", webhookMeta)
	return transaction, nil
}
