// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package billing

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"

	"git.runecms.io/rune/runecms"
	pb "git.runecms.io/rune/runecms/billing/rpc"
	"git.runecms.io/rune/runecms/webhooks"
	"git.runecms.io/rune/runecms/webhooks/rpc"

	"github.com/ory/ladon"
)

var (
	SqlSelectBillingTransactionById = `
	SELECT
		billing_account_id,
		amount,
		status,
		response,
		metadata,
		created_at,
		updated_at
	FROM
		billing_transactions
	WHERE
		id=$1
	`
	SqlInsertBillingTransaction = `
	INSERT INTO
		billing_transactions (
			billing_account_id,
			amount,
			status,
			metadata
		)
	VALUES (
		$1,
		$2,
		$3,
		$4
	)
	RETURNING id, created_at
	`

	SqlUpdateBillingTransactionStatus = `
	UPDATE
		billing_transactions
	SET
		status=$1,
		response=$2,
		updated_at=now()
	WHERE
		id=$3
	RETURNING
		updated_at
	`
)

func CreateTransaction(
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	billingAccountId string,
	amount int64,
	status string,
	meta json.RawMessage,
) (*pb.BillingTransaction, error) {
	if err := runecms.Access(ident, w, "create", "billing:transactions"); err != nil {
		log.Printf("User %s tried (and failed) to create a new billing transaction record for %s", ident.User, billingAccountId)
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	var (
		id        string
		createdAt string
	)

	log.Printf("Creating billing transaction (%d) record for billing account %s...", amount, billingAccountId)

	// Convert meta from bytes to a string just because
	if meta == nil {
		meta = []byte(`{}`)
	}
	if err := db.QueryRow(
		SqlInsertBillingTransaction,
		billingAccountId,
		amount,
		status,
		string(meta),
	).Scan(
		&id,
		&createdAt,
	); err != nil {
		log.Printf("Error when trying to scan SQL result when creating a new BillingTransaction. Error: %s", err.Error())
		return nil, runecms.Error(runecms.Internal, err.Error())
	}
	log.Printf("Successfully created billing transaction %s for billing account %s", id, billingAccountId)

	model, err := GetTransaction(db, w, ident, id)
	if err != nil {
		return nil, err
	}

	go webhooks.Distribute(webhooksClient, ident, "billing:transactions", "create", model)
	return model, nil
}

func UpdateTransaction(
	db *sql.DB,
	w *ladon.Ladon,
	webhooksClient webhookspb.Webhooks,
	ident runecms.Identity,
	model *pb.BillingTransaction,
) (*pb.BillingTransaction, error) {
	if err := runecms.Access(ident, w, "update", fmt.Sprintf("billing:transactions:%s", model.Id)); err != nil {
		log.Printf("User %s tried (and failed) to update a new billing transaction record for %s", ident.User, model.Id)
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	var (
		updatedAt string
	)

	if err := db.QueryRow(SqlUpdateBillingTransactionStatus, model.Status, model.Response, model.Id).Scan(&updatedAt); err != nil {
		log.Printf("Error when trying to scan SQL result when updating a new BillingTransaction. Error: %s", err.Error())
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	go webhooks.Distribute(webhooksClient, ident, "billing:transactions", "update", model)
	return GetTransaction(db, w, ident, model.Id)
}

func GetTransaction(db *sql.DB, w *ladon.Ladon, ident runecms.Identity, id string) (*pb.BillingTransaction, error) {
	if err := runecms.Access(ident, w, "read", "billing:transactions"); err != nil {
		log.Printf("User %s tried (and failed) to update a new billing transaction record for %s", ident.User, id)
		return nil, runecms.Error(runecms.PermissionDenied, err.Error())
	}

	model := &pb.BillingTransaction{}

	var (
		response = sql.NullString{}
	)
	log.Printf("Selecting BillingTransaction %s from database...", id)
	if err := db.QueryRow(
		SqlSelectBillingTransactionById,
		id,
	).Scan(
		&model.BillingAccountId,
		&model.Amount,
		&model.Status,
		&response,
		&model.Metadata,
		&model.CreatedAt,
		&model.UpdatedAt,
	); err != nil {
		log.Printf("Error when trying to scan SQL result when selecting a BillingTransaction. Error: %s", err.Error())
		if err == sql.ErrNoRows {
			return nil, runecms.Error(runecms.NotFound, err.Error())
		}
		return nil, runecms.Error(runecms.Internal, err.Error())
	}

	if response.Valid {
		model.Response = response.String
	}

	model.Id = id

	log.Printf("Found BillingTransaction %s", id)
	log.Printf("BillingAccount %s:\n%+v\n", id, model)
	return model, nil
}
