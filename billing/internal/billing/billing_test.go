// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package billing

import (
	"database/sql"
	"os"
	"testing"
	"time"

	"git.runecms.io/rune/runecms"
	pb "git.runecms.io/rune/runecms/billing/rpc"
	"git.runecms.io/rune/runecms/internal/testutil"
	"git.runecms.io/rune/runecms/webhooks"

	"github.com/google/go-cmp/cmp"
	"github.com/lionelbarrow/braintree-go"
	"github.com/ory/ladon"
)

var (
	valid_id       runecms.Identity
	invalid_id     runecms.Identity
	warden         *ladon.Ladon
	webhooksClient = &webhooks.TestClient{}

	braintreePublicKey  string
	braintreePrivateKey string
	braintreeMerchantId string
	braintreeToken      string
)

func init() {
	valid_id = runecms.Be("valid", "admin")
	invalid_id = runecms.Be("invalid", "user")

	warden = testutil.GetTestLadon()

	braintreePublicKey = os.Getenv("BRAINTREE_PUBLIC_KEY")
	braintreePrivateKey = os.Getenv("BRAINTREE_PRIVATE_KEY")
	braintreeMerchantId = os.Getenv("BRAINTREE_MERCHANT_ID")
	braintreeToken = os.Getenv("BRAINTREE_TOKEN")
}

func createBillingAccount(model *pb.BillingAccount, db *sql.DB, t *testing.T) *pb.BillingAccount {
	ba, err := CreateBillingAccount(db, warden, webhooksClient, valid_id, model.Token, model.LastFour, model.NameOnCard, time.Now())
	if err != nil {
		t.Fatal(err.Error())
	}

	return ba
}

func TestCreate(t *testing.T) {
	db := testutil.GetTestDb(t)
	exp := time.Now().UTC().Add(time.Hour * 24 * 30 * 2)
	t.Run("Valid request", func(t *testing.T) {
		model, err := CreateBillingAccount(db, warden, webhooksClient, valid_id, "example-token", "5432", "Test User", exp)
		if err != nil {
			t.Fatal(err)
		}
		if model.Id == "" {
			t.Fatal("Creating a valid billing account should return a model with an ID")
		}
		if cmp.Equal(model.CreatedAt, time.Time{}) {
			t.Fatal("Creating a valid billing account should return a valid CreatedAt datetime")
		}
	})
	t.Run("Providing an empty token should return an error", func(t *testing.T) {
		_, err := CreateBillingAccount(db, warden, webhooksClient, valid_id, "", "5432", "Test User", exp)
		if err == nil {
			t.Fatal("Providing an empty token should return an error")
		}
	})
	t.Run("Using an unauthorized identity should return an error", func(t *testing.T) {
		_, err := CreateBillingAccount(db, warden, webhooksClient, invalid_id, "example-token", "5432", "Test User", exp)
		if err == nil {
			t.Fatal("Using an unauthorized identity should return an error")
		}
	})
}

func TestGet(t *testing.T) {
	db := testutil.GetTestDb(t)
	m := createBillingAccount(&pb.BillingAccount{
		Token:      "example-token",
		LastFour:   "4321",
		NameOnCard: "Test User",
	}, db, t)

	t.Run("Valid request", func(t *testing.T) {
		ba, err := GetBillingAccountById(db, warden, valid_id, m.Id)
		if err != nil {
			t.Fatalf("Sending a valid Id to GetBillingAccountById returned an error. Error: %s", err.Error())
		}
		if cmp.Equal(ba, m) != true {
			t.Fatalf("Returned BillingAccount from Get does not match the created BillingAccount\n%s", cmp.Diff(ba, m))
		}
	})
	t.Run("Providing an empty id should return an error", func(t *testing.T) {
		_, err := GetBillingAccountById(db, warden, valid_id, "")
		if err == nil {
			t.Fatal("Providing an empty token should return an error")
		}
	})
	t.Run("Using an unauthorized identity should return an error", func(t *testing.T) {
		_, err := GetBillingAccountById(db, warden, invalid_id, m.Id)
		if err == nil {
			t.Fatal("Using an unauthorized identity should return an error")
		}
	})
}

func TestDelete(t *testing.T) {
	db := testutil.GetTestDb(t)
	m := createBillingAccount(&pb.BillingAccount{
		Token:      "example-token",
		LastFour:   "4321",
		NameOnCard: "Test User",
	}, db, t)

	t.Run("Valid request", func(t *testing.T) {
		deletedAt, err := DeleteBillingAccount(db, warden, webhooksClient, valid_id, m.Id)
		if err != nil {
			t.Fatalf("Sending a valid Id to DeleteBillingAccount returned an error. Error: %s", err.Error())
		}

		if deletedAt == nil {
			t.Fatal("Sending a valid request to DeleteBillingAccount returned a nil deletedAt time")
		}
	})
	t.Run("Providing an empty id should return an error", func(t *testing.T) {
		_, err := DeleteBillingAccount(db, warden, webhooksClient, valid_id, "")
		if err == nil {
			t.Fatal("Providing an empty token should return an error")
		}
	})
	t.Run("Using an unauthorized identity should return an error", func(t *testing.T) {
		_, err := DeleteBillingAccount(db, warden, webhooksClient, invalid_id, m.Id)
		if err == nil {
			t.Fatal("Using an unauthorized identity should return an error")
		}
	})
}

func TestCharge(t *testing.T) {
	db := testutil.GetTestDb(t)
	m := createBillingAccount(&pb.BillingAccount{
		Token:      braintreeToken,
		LastFour:   "4321",
		NameOnCard: "Test User",
	}, db, t)

	info := braintree.New(
		braintree.Sandbox,
		braintreeMerchantId,
		braintreePublicKey,
		braintreePrivateKey,
	)
	t.Run("Valid request", func(t *testing.T) {
		transaction, err := ChargeBillingAccount(db, warden, webhooksClient, valid_id, m.Id, 1000, true, info, nil)
		if err != nil {
			t.Fatalf("Sending a valid Id to ChargeBillingAccount returned an error. Error: %s", err.Error())
		}

		if transaction.Id == "" {
			t.Fatal("Sending a valid Id to ChargeBillingAccount returned an empty transaction id")
		}
	})
	t.Run("Providing an empty id should return an error", func(t *testing.T) {
		_, err := ChargeBillingAccount(db, warden, webhooksClient, valid_id, "", 100, true, info, nil)
		if err == nil {
			t.Fatal("Providing an empty token should return an error")
		}
	})
	t.Run("Using an unauthorized identity should return an error", func(t *testing.T) {
		_, err := ChargeBillingAccount(db, warden, webhooksClient, invalid_id, m.Id, 100, true, info, nil)
		if err == nil {
			t.Fatal("Using an unauthorized identity should return an error")
		}
	})
}
