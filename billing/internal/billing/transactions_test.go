// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package billing

import (
	"encoding/json"
	"testing"
	"time"

	"git.runecms.io/rune/runecms"
	pb "git.runecms.io/rune/runecms/billing/rpc"
	"git.runecms.io/rune/runecms/internal/testutil"

	"github.com/google/go-cmp/cmp"
)

func init() {
	valid_id = runecms.Be("valid", "admin")
	invalid_id = runecms.Be("invalid", "user")

	warden = testutil.GetTestLadon()
}

func TestCreateTransaction(t *testing.T) {
	db := testutil.GetTestDb(t)
	m := createBillingAccount(&pb.BillingAccount{
		Token: braintreeToken,
	}, db, t)

	t.Run("Valid request", func(t *testing.T) {
		var (
			amount int64 = 1000
			status       = "success"
			meta         = json.RawMessage("{\"example\":\"json\"}")
		)

		model, err := CreateTransaction(db, warden, webhooksClient, valid_id, m.Id, amount, status, meta)
		if err != nil {
			t.Fatal("A valid request to create a new Billing Transaction should not return an error")
		}

		if cmp.Equal(model.CreatedAt, time.Time{}) {
			t.Error("CreatedAt is empty")
		}

		if cmp.Equal(model.UpdatedAt, time.Time{}) {
			t.Error("UpdatedAt is empty")
		}

		if model.Id == "" {
			t.Error("Id is empty")
		}
		if model.Amount != amount {
			t.Error("The amounts do not match")
		}
		if model.Status != status {
			t.Errorf("The statuses do not match.\n%s", cmp.Diff(model.Status, status))
		}
		if cmp.Equal(json.RawMessage(model.Metadata), meta) != true {
			t.Errorf("The metadata does not match.\n%s", cmp.Diff(model.Metadata, meta))
		}
	})

	t.Run("Invalid status", func(t *testing.T) {
		var (
			amount int64 = 1000
			status       = "non-existent status"
			meta         = json.RawMessage("{\"example\":\"json\"}")
		)

		_, err := CreateTransaction(db, warden, webhooksClient, valid_id, m.Id, amount, status, meta)
		if err == nil {
			t.Fatal("Providing an invalid status should return an error. Statuses can be found in the db schema")
		}
	})

	t.Run("Invalid identity", func(t *testing.T) {
		var (
			amount int64 = 1000
			status       = "success"
			meta         = json.RawMessage("{\"example\":\"json\"}")
		)

		_, err := CreateTransaction(db, warden, webhooksClient, invalid_id, m.Id, amount, status, meta)
		if err == nil {
			t.Fatal("Providing an invalid identity should return an error")
		}
	})
}

func TestGetTransaction(t *testing.T) {
	db := testutil.GetTestDb(t)
	m := createBillingAccount(&pb.BillingAccount{
		Token: braintreeToken,
	}, db, t)

	var (
		amount int64 = 1000
		status       = "success"
		meta         = json.RawMessage("{\"example\":\"json\"}")
	)

	trans, err := CreateTransaction(db, warden, webhooksClient, valid_id, m.Id, amount, status, meta)
	if err != nil {
		t.Fatalf("Unable to setup test for GetTransaction. Error: %s", err.Error())
	}
	t.Run("Valid request", func(t *testing.T) {
		model, err := GetTransaction(db, warden, valid_id, trans.Id)
		if err != nil {
			t.Fatal("A valid request to GetTransaction should not return an error")
		}

		if model.Id == "" {
			t.Error("Id returned by GetTransaction is empty")
		}
		if model.Id != trans.Id {
			t.Errorf("Id returned by GetTransaction does not match %s", trans.Id)
		}
		if model.Status == "" {
			t.Error("Status returned by GetTransaction is empty")
		}
		if model.Status != status {
			t.Errorf("Status returned by GetTransaction does not match %s", status)
		}
		if cmp.Equal(model.CreatedAt, time.Time{}) {
			t.Error("CreatedAt returned by GetTransaction is empty")
		}
		if cmp.Equal(model.UpdatedAt, time.Time{}) {
			t.Error("UpdatedAt returned by GetTransaction is empty")
		}
		if model.Amount != amount {
			t.Errorf("Amount returned by GetTransaction does not match %d", amount)
		}
	})
	t.Run("Invalid request", func(t *testing.T) {
		_, err := GetTransaction(db, warden, valid_id, "bad uuid")
		if err == nil {
			t.Fatal("Providing an invalid UUID to GetTransaction should return an error")
		}
	})

	t.Run("Invalid identity", func(t *testing.T) {
		_, err := GetTransaction(db, warden, invalid_id, trans.Id)
		if err == nil {
			t.Fatal("Providing an invalid identity to GetTransaction should return an error")
		}
	})
}

func TestUpdateTransaction(t *testing.T) {
	db := testutil.GetTestDb(t)
	m := createBillingAccount(&pb.BillingAccount{
		Token: braintreeToken,
	}, db, t)

	var (
		amount      int64 = 1000
		status            = "success"
		newStatus         = "failure"
		newResponse       = "example response"
		meta              = json.RawMessage("{\"example\":\"json\"}")
	)

	t.Run("Valid request", func(t *testing.T) {
		model, err := CreateTransaction(db, warden, webhooksClient, valid_id, m.Id, amount, status, meta)
		if err != nil {
			t.Fatalf("Unable to setup test for UpdateTransaction. Error: %s", err.Error())
		}
		expected := &pb.BillingTransaction{
			Id:               model.Id,
			BillingAccountId: model.BillingAccountId,
			Metadata:         meta,
			Status:           newStatus,
			Amount:           amount,
			Response:         newResponse,
			CreatedAt:        model.CreatedAt,
		}

		model.Status = newStatus
		model.Response = newResponse

		updated, err := UpdateTransaction(db, warden, webhooksClient, valid_id, model)
		if err != nil {
			t.Fatal("Providing a valid request to UpdateTransaction should not return an error")
		}

		// impossible to set these value up initially
		expected.UpdatedAt = updated.UpdatedAt

		if cmp.Equal(updated, expected) != true {
			t.Errorf("Unexpected response.\n%s", cmp.Diff(updated, expected))
		}

		if updated.UpdatedAt == model.UpdatedAt {
			t.Errorf("The updated transaction should not have the same UpdatedAt value as the original")
		}
	})

	t.Run("Invalid status", func(t *testing.T) {
		model, err := CreateTransaction(db, warden, webhooksClient, valid_id, m.Id, amount, status, meta)
		if err != nil {
			t.Fatalf("Unable to setup test for UpdateTransaction. Error: %s", err.Error())
		}
		model.Status = "invalid status"
		_, err = UpdateTransaction(db, warden, webhooksClient, valid_id, model)
		if err == nil {
			t.Fatal("Providing an invalid status should return an error. Statuses can be found in the db schema")
		}
	})

	t.Run("Updating amount is not allowed", func(t *testing.T) {
		model, err := CreateTransaction(db, warden, webhooksClient, valid_id, m.Id, amount, status, meta)
		if err != nil {
			t.Fatalf("Unable to setup test for UpdateTransaction. Error: %s", err.Error())
		}
		model.Amount = 1200
		updated, err := UpdateTransaction(db, warden, webhooksClient, valid_id, model)
		if err != nil {
			t.Fatal("Providing a valid request to UpdateTransaction should not return an error")
		}
		if updated.Amount == model.Amount {
			t.Fatal("Changing the model.Amount should not actually update the transaction")
		}
	})
	t.Run("Updating metadata is not allowed", func(t *testing.T) {
		model, err := CreateTransaction(db, warden, webhooksClient, valid_id, m.Id, amount, status, meta)
		if err != nil {
			t.Fatalf("Unable to setup test for UpdateTransaction. Error: %s", err.Error())
		}
		model.Metadata = json.RawMessage(`{"changethe":"json"}`)
		updated, err := UpdateTransaction(db, warden, webhooksClient, valid_id, model)
		if err != nil {
			t.Fatal("Providing a valid request to UpdateTransaction should not return an error")
		}
		if cmp.Equal(updated.Metadata, model.Metadata) {
			t.Fatal("Changing the model.Metadata should not actually update the transaction")
		}
	})

	t.Run("Invalid identity", func(t *testing.T) {
		model, err := CreateTransaction(db, warden, webhooksClient, valid_id, m.Id, amount, status, meta)
		if err != nil {
			t.Fatalf("Unable to setup test for UpdateTransaction. Error: %s", err.Error())
		}
		model.Status = "failure"
		_, err = UpdateTransaction(db, warden, webhooksClient, invalid_id, model)
		if err == nil {
			t.Fatal("Providing an invalid identity should return an error")
		}
	})
}
