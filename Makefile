JULES_BIN := $(GOPATH)/bin/jules
$(JULES_BIN):
	go get github.com/kminehart/jules/cmd/jules

.PHONY: test
test:
	go test \
	. \
	./internal/... \
	./metadata/... \
	./promotions/... \
	./subscribers/... \
	./users/... \
	./webhooks/...

.PHONY: deploy
deploy: $(JULES_BIN)
	export $$(cat ./internal/rune.config | xargs) && $(JULES_BIN) -stage deploy
