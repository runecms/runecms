### Microservice Architecture

Rather than take the monolithic approach of building one large application, Rune is built in small individual components that work together.

While this methodology may produce a fair amount more overhead, we feel that the individual components themselves are usable in more applications than just the Rune software suite.

### Monolithic Repository

If you ever need to browse the [Rune codebse](https://git.runecms.io/rune/runecms), you'll notice that, while it's built in small components, all of the components live in the same git repository.

This approach makes it significantly easier for us to keep track of version changes and to fix software that communicates together.

### About these docs

These documentation pages are built from the individual `.md` files in the `docs` folder from each individual service.

The only requiremnt is that the `.md` files have at least these information at the top in this format (frontmatter):

```
+++
title = "title"
weight = 1
+++
```

If it's a `.md` file, and it's in the `docs` folder in a service that is being tested by `jules`, then it will be copied over to the docs service.

### Technology

### Authentication

### Errors
