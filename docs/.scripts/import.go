// Copyright (C) 2018 WEHCO Media, Inc.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"

	yaml "gopkg.in/yaml.v2"
)

func (p ProjectList) Contains(path string) (bool, error) {
	fp, err := filepath.Abs(path)
	if err != nil {
		return false, err
	}

	for _, v := range p {
		subdir, err := filepath.Abs(v.Path)
		if err != nil {
			continue
		}
		if strings.Contains(fp, subdir) {
			return true, nil
		}
	}

	return false, nil
}

// ProjectList is a list of projects pulled from the config.
type ProjectList map[string]Project

// StageList is a list of stages pulled from the config.
type StageList map[string]Stage

// Stage is a single stage defined in the config.  A stage basically runs a command on your projects.
type Stage struct {
	Command []string `yaml:"command"`
}

// Project is essentially a filepath where a stage command is run.
type Project struct {
	Path   string    `yaml:"path"`
	Stages StageList `yaml:"stages"`
	Env    []string  `yaml:"env"`
}

// The Config type defines the structure of the yaml configuration file.
type Config struct {
	Stages   StageList   `yaml:"stages"`
	Projects ProjectList `yaml:"projects"`
}

// ReadConfig will open a filepath and return a Config object.
func ReadConfig(path string) (c *Config, err error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("Invalid config file.  Could not open \"%s\"\n", path)
	}

	config := &Config{}
	if err = yaml.Unmarshal(data, config); err != nil {
		return nil, fmt.Errorf("There was an error reading your config file.  Please run \"jules lint\" to find possible problems with your configuration.\n Error: %s", err.Error())
	}
	return config, nil
}

// Copy the src file to dst. Any existing file will be overwritten and will not
// copy file attributes.
func Copy(src, dst string) error {
	in, err := os.Open(src)
	if err != nil {
		return err
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, in)
	if err != nil {
		return err
	}
	return out.Close()
}

func main() {
	var (
		err       error
		julesPath string
		hugoPath  string
		ok        bool
	)
	julesPath, ok = os.LookupEnv("JULES_PATH")
	if !ok {
		julesPath, err = os.Getwd()
		if err != nil {
			log.Panic(err)
		}
	}

	julesPath = path.Clean(julesPath)

	hugoPath, ok = os.LookupEnv("HUGO_PATH")
	if !ok {
		hugoPath = path.Join(julesPath, "docs")
	}

	c, err := ReadConfig(path.Join(julesPath, "jules.yaml"))
	if err != nil {
		log.Panic(err)
	}
	err = Copy(path.Join(julesPath, "README.md"), path.Join(hugoPath, "content", "_index.md"))
	if err != nil {
		log.Printf("%s was not found. %s", path.Join(julesPath, "README.md"))
	}
	log.Println("Jules:", julesPath)
	log.Println("Hugo:", hugoPath)

	for name, v := range c.Projects {
		sectionPath := path.Join(hugoPath, "content", name)
		docsPath := path.Join(julesPath, v.Path, "docs")
		log.Printf("Processing project %s", name)
		if _, err := os.Stat(path.Join(julesPath, v.Path, "docs")); os.IsNotExist(err) {
			log.Printf("%s does not exist. Ignoring", path.Join(julesPath, v.Path, "docs"))
			continue
		}
		log.Printf("Using section %s", sectionPath)
		if err := os.MkdirAll(sectionPath, 0755); err != nil {
			log.Println(err.Error())
			continue
		}
		wd, err := os.Getwd()
		if err == nil {
			log.Printf("----\n%s\n----\n%s\n", docsPath, wd)
			if filepath.Clean(docsPath) == filepath.Clean(wd) {
				log.Printf("%s is pwd, skipping", docsPath)
				continue
			}
		}
		log.Printf("walking %s", docsPath)
		filepath.Walk(docsPath, func(filePath string, info os.FileInfo, err error) error {
			// skip, err := c.Projects.Contains(filePath)
			// if err != nil {
			// 	continue
			// }
			// if skip == true {
			// 	fmt.Printf("skipping a dir without errors: %+v \n", info.Name())
			// 	return filepath.SkipDir
			// }
			log.Printf("processing file %s", filePath)
			if info.IsDir() && info.Name() != "docs" {
				log.Printf("%s is a directory. Ignoring", filePath)
				return filepath.SkipDir
			}
			if filepath.Ext(filePath) != ".md" {
				log.Printf("%s is not markdown. Ignoring", filePath)
				return nil
			}
			destination := path.Join(sectionPath, info.Name())
			log.Printf("Copying %s to %s", filePath, destination)
			return Copy(filePath, destination)
		})
		if err != nil {
			fmt.Printf("walk error [%v]\n", err)
		}
	}
}
