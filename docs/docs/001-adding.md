+++
title="adding docs"
weight=1
+++

## Documentation

The documentation service automatically pulls in all markdown files from the `docs` folders from each service being tested/built by the CI runner. Once it obtains all of the markdown files, it then uses Hugo to build the static content.

To see a list of services being tested, check the `jules.yaml` file in the root of the repository

### Adding Documentation

1. Make sure the service is listed in `jules.yaml`
2. Create a `docs` folder in the path used by Jules
3. Each markdown file with a `.md` extension will be processed by the docs service.

**Ensure each markdown file has the following frontmatter at the top**

```
+++
title="title"
weight=1
+++
```

The weight defines the order in which this markdown file is shown on the page
