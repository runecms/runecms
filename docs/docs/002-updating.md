+++
title="updating the docs"
weight=2
+++

### Updating the docs

Here's a list of useful directories if you want to update the documentation's look and feel.

| Description | Path |
|-------------|------|
| CSS & JS    | `rune/docs/themes/rune-docs/static` |
| Partials (html / markdown snippets) | `rune/docs/themes/rune-docs/layouts/partials` |
| Page templates | `rune/docs/themes/rune-docs/layouts/_default` |

After updating these, run `make build` to build the static content.

If you want hugo to watch for changes and host a server for you for development, run `make serve`, and navigate to [http://localhost:1313](http://localhost:1313).
