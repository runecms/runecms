// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"net/http"
	// "encoding/json"
	"log"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/integration/pkg/integration"
	"git.runecms.io/rune/runecms/integration/twirp"
	metadatapb "git.runecms.io/rune/runecms/metadata/rpc"
	subscriberspb "git.runecms.io/rune/runecms/subscribers/rpc"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/ory/ladon"
	manager "github.com/ory/ladon/manager/sql"
	"github.com/spf13/viper"
)

func init() {
	// Viper config
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.AddConfigPath("$HOME/.integration")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err.Error())
	}

	err = viper.UnmarshalKey("complaint_statuses", &integration.ComplaintStatusMap)
	if err != nil {
		log.Fatal(err.Error())
	}
}

func main() {
	config, err := runecms.GetConfig()
	if err != nil {
		log.Panicln("Unable to get config. Error:", err.Error())
	}
	ladonDB, err := sqlx.Open("postgres", config.LadonDBURL)
	if err != nil {
		log.Panicln(err.Error())
	}

	defer ladonDB.Close()
	err = ladonDB.Ping()
	if err != nil {
		log.Panicln(err.Error())

	}
	ladonDB.SetMaxOpenConns(config.DatabaseConnectionLimit)
	warden := &ladon.Ladon{
		Manager: manager.NewSQLManager(ladonDB, nil),
	}

	// Connect to metadata service...
	mdClient := metadatapb.NewMetadataProtobufClient(config.RuneMetadataAddress, &http.Client{})
	cmpClient := subscriberspb.NewComplaintsProtobufClient(config.RuneSubscribersAddress, &http.Client{})
	integration.ComplaintsClient = cmpClient
	server := integrationtwirp.NewServer(mdClient, warden)
	log.Println(server.Start(config.ListenPort))
}
