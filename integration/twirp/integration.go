// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package integrationtwirp

import (
	"context"

	"git.runecms.io/rune/runecms"
	"git.runecms.io/rune/runecms/integration/pkg/integration"
	pb "git.runecms.io/rune/runecms/integration/rpc"

	"github.com/twitchtv/twirp"
)

func (s *Server) Create(c context.Context, req *pb.CreateIntegrationRequest) (*pb.CreateIntegrationResponse, error) {
	user, err := runecms.IdentityFromRPC(c)
	if err != nil {
		return nil, twirp.NewError(twirp.Unauthenticated, err.Error())
	}

	if err := integration.CreateIntegrationStatus(s.MetadataClient, s.Warden, user, req.GetWebhookTransactionId(), req.GetStatus()); err != nil {
		return nil, err
	}

	return nil, nil
}
