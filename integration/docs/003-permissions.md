+++
title = "permissions"
weight = 3
+++

## Permissions

| Action | Permission | Description |
|--------|------------|-------------|
| create | `integration:status` | This permission will allow the user to create a new status entry |
