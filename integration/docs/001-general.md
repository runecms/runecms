+++
title = "general"
weight = 1
+++

## The Integration Service

The integration service is responsible for maintaining integration statuses from external services.

This service is necessary for maintaining the status of asynchronous transactions to and from external services.

Because external service integration relies heavily on webhooks, this service does not maintain it's own data. Instead, it relies on [webhook]('/webhooks') transactions.

This service also uses the [metadata](/metadata) service to determine what type of resource is being updated.
