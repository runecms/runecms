// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package integration

import (
	"fmt"
	"log"

	"git.runecms.io/rune/runecms"
	models "git.runecms.io/rune/runecms/integration/rpc"
	subscriberspb "git.runecms.io/rune/runecms/subscribers/rpc"
)

// ComplaintStatusMap is expected to be initialized by the program using this library
var ComplaintStatusMap = map[string]string{}

var ComplaintsClient subscriberspb.Complaints

func IntegrationComplaint(ident runecms.Identity, value string, s *models.Status) error {
	log.Printf("Received status %s (%s) for complaint '%s'", s.Code, s.Description, value)

	complaintId := value

	// complaintStatusId to set the complaint to upon integration status update
	// If there is no provided code, or the code does not have a corresponding complaint_status id, then:
	//    if it was an error, try to use the "error" key
	//    if it was not an error, then try to use the "success" key
	var complaintStatusId string
	if val, ok := ComplaintStatusMap[s.Code]; ok {
		complaintStatusId = val
	} else {
		if s.Error {
			if val2, ok2 := ComplaintStatusMap["error"]; ok2 {
				complaintStatusId = val2
			} else {
				log.Printf("Unable to find complaint type id corresponding to %s or \"error\"", s.Code)
				return fmt.Errorf("Unable to find complaint type id corresponding to %s or \"success\"", s.Code)
			}
		} else {
			if val2, ok2 := ComplaintStatusMap["success"]; ok2 {
				complaintStatusId = val2
			} else {
				log.Printf("Unable to find complaint type id corresponding to %s or \"success\"", s.Code)
				return fmt.Errorf("Unable to find complaint type id corresponding to %s or \"success\"", s.Code)
			}
		}
	}

	// now update the complaint
	log.Printf("Retrieving complaint %s...", complaintStatusId)
	res, err := ComplaintsClient.GetComplaint(ident.ToContext(), &subscriberspb.GetComplaintRequest{
		Id: complaintId,
	})
	if err != nil {
		log.Printf("Failed to get complaint %s. Error: %s", complaintId, err.Error())
		return err
	}
	complaint := res.GetComplaint()
	log.Printf("Got complaint %s...", complaintStatusId)
	log.Printf("Updating Complaint %s's status to reflect ComplaintStatusId %s...", complaintId, complaintStatusId)
	complaint.ComplaintStatusId = complaintStatusId
	_, err = ComplaintsClient.UpdateComplaint(ident.ToContext(), &subscriberspb.UpdateComplaintRequest{
		Complaint: complaint,
	})

	if err != nil {
		log.Printf("Failed to update complaint %s. Error: %s", complaintId, err.Error())
		return err
	}
	log.Printf("Updated Complaint %s's status to reflect ComplaintStatusId %s...", complaintId, complaintStatusId)

	return nil
}
