// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package integration

import (
	"fmt"
	"log"

	"git.runecms.io/rune/runecms"
	models "git.runecms.io/rune/runecms/integration/rpc"
	"git.runecms.io/rune/runecms/metadata"
	metadatapb "git.runecms.io/rune/runecms/metadata/rpc"

	"github.com/ory/ladon"
)

const (
	webhookTransactionPrefix = "runecms.webhooks.transaction"
	integrationStatusPrefix  = "runecms.integration.status"
)

var MetadataActions = map[string]func(runecms.Identity, string, *models.Status) error{
	"runecms.subscribers.subscription_hold": IntegrationSubscriptionHold,
	"runecms.subscribers.subscription":      IntegrationSubscription,
	"runecms.subscribers.complaint":         IntegrationComplaint,
	"runecms.subscribers.subscriber":        IntegrationSubscriber,
}

// CreateIntegrationStatus uses the connection to the Metadata service and creates an entry associated with the key `runecms.webhook_transaction:<id>`
func CreateIntegrationStatus(mdClient metadatapb.Metadata, w ladon.Warden, ident runecms.Identity, webhookTransactionId string, s *models.Status) error {
	if err := runecms.Access(ident, w, "create", "integration:status"); err != nil {
		return runecms.NewError(runecms.PermissionDenied, err.Error())
	}

	// Step 1: Make sure that the metadata entry exists
	res, err := mdClient.Get(ident.ToContext(), &metadatapb.GetMetadataRequest{
		Key: fmt.Sprintf("%s:%s", webhookTransactionPrefix, webhookTransactionId),
	})

	if err != nil {
		return err
	}

	integrationKey := fmt.Sprintf("%s:%s", integrationStatusPrefix, s.Code)
	// Step 2: Add the metadata entry for "runecms.integration.status" if it doesn't already existA
	log.Printf("[%s] Adding metadata entry %s", webhookTransactionId, integrationKey)
	mdClient.Set(ident.ToContext(), &metadatapb.SetMetadataRequest{
		Key:      integrationKey,
		ObjectId: res.GetMetadata().GetObjectId(),
		Data:     res.GetMetadata().GetData(),
	})

	// Step 3: If the entry does exist, find the associated resource in the MetadataActions map, and call the function.
	// Presumably to set the appropriate status...
	keys := res.GetMetadata().GetKeys()
	for k, v := range MetadataActions {
		val, err := metadata.FindKey(k, keys)
		if err != nil {
			log.Printf("[%s] Error when finding key %s. Error: %s", webhookTransactionId, k, err.Error())
			continue
		}
		if err := v(ident, val[0], s); err != nil {
			log.Printf("[%s] Error when executing action for %s. Error: %s", webhookTransactionId, k, err.Error())
			return err
		}
	}

	return nil
}
