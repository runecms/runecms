# Integration

The integration service is essentially a wrapper around the metadata service for managing asyncronous external integrations.

Because some external services have an import process which is done periodically, rather than instantly, returned values and errors need to be pushed to Rune independently.

## Configuration

See the [example](config.example.json)

## Requirements

#### Only one transaction per HTTP request

While a little inconvenient, this gives us an opportunity to send an accurate response code for each transaction.

#### All requests must include the `Authorization` header with a valid Bearer token

#### All requests must follow the correct format

## Formats

### Pushing a new status

`POST api.runecms.io/integration/v1/{rune_trans_id}/status`

**Example**

`POST api.runecms.io/integration/v1/102930219/status`

**Format**

| Field | Description | Type |
|-------|-------------|------|
| `error`  | If the status is an error, then set this to true. | bool (optional, default false) |
| `code`  | Error or success Code (if any). | string (optional) |
| `description` | Description of status or error | string |

**notes**

**Remember, in JSON, bool values are not quoted**

Successful import without code:

```json
{
    "description": "Import of complaint 1234 successful"
}
```

```json
{
    "error": true,
    "code": "SOME_CODE",
    "description": "Unable to create complaint: Subscription has status CANCELED"
}
```


**curl commands**

```
curl -X POST -d '{"error": true,"code":"SOME_STATUS","description":"Unable to create complaint: Subscription has status CANCELED"}' api.runecms.io/integration/v1/102930219/status
```

```
curl -X POST -d '{"error": false,"description":"Import of complaint 102930219 successful"}' api.runecms.io/integration/v1/102930219/status
```


**Response**

`200 OK`: no body

`400 Bad Request`, `500 Internal Server Error`:

```
{
    "error": "'error'field can not be empty or nil"
}
````


### Pushing a new related ID

In the event that the import was successful, Rune needs to know about what the Id of that item is so that it can keep track for future updates

`POST api.runecms.io/integration/v1/{rune_trans_id}/{publication_code}/{unique_id}`

**Example**

`POST api.runecms.io/integration/v1/102930219/CTFP/102938219`

The integration service will check what is associated with that transaction, and add the "unique_id" provided to the metadata service, so that it's associated for future updates.

**curl command**

```
curl -X POST api.runecms.io/integration/v1/102930219/CTFP/102938219
```

**Response**

`200 OK`: no body

`400 Bad Request`, `500 Internal Server Error`:

```
{
    "error": "'error'field can not be empty or nil"
}
````
