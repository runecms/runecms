// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Code generated by protoc-gen-go. DO NOT EDIT.
// source: rpc.proto

/*
Package integrationpb is a generated protocol buffer package.

It is generated from these files:
	rpc.proto

It has these top-level messages:
	Status
	CreateIntegrationRequest
	CreateIntegrationResponse
	GetIntegrationRequest
	GetIntegrationResponse
*/
package integrationpb

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Status struct {
	Error       bool   `protobuf:"varint,1,opt,name=error" json:"error,omitempty"`
	Description string `protobuf:"bytes,2,opt,name=description" json:"description,omitempty"`
	Code        string `protobuf:"bytes,3,opt,name=code" json:"code,omitempty"`
}

func (m *Status) Reset()                    { *m = Status{} }
func (m *Status) String() string            { return proto.CompactTextString(m) }
func (*Status) ProtoMessage()               {}
func (*Status) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *Status) GetError() bool {
	if m != nil {
		return m.Error
	}
	return false
}

func (m *Status) GetDescription() string {
	if m != nil {
		return m.Description
	}
	return ""
}

func (m *Status) GetCode() string {
	if m != nil {
		return m.Code
	}
	return ""
}

type CreateIntegrationRequest struct {
	WebhookTransactionId string  `protobuf:"bytes,1,opt,name=webhook_transaction_id,json=webhookTransactionId" json:"webhook_transaction_id,omitempty"`
	Status               *Status `protobuf:"bytes,2,opt,name=status" json:"status,omitempty"`
}

func (m *CreateIntegrationRequest) Reset()                    { *m = CreateIntegrationRequest{} }
func (m *CreateIntegrationRequest) String() string            { return proto.CompactTextString(m) }
func (*CreateIntegrationRequest) ProtoMessage()               {}
func (*CreateIntegrationRequest) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *CreateIntegrationRequest) GetWebhookTransactionId() string {
	if m != nil {
		return m.WebhookTransactionId
	}
	return ""
}

func (m *CreateIntegrationRequest) GetStatus() *Status {
	if m != nil {
		return m.Status
	}
	return nil
}

type CreateIntegrationResponse struct {
}

func (m *CreateIntegrationResponse) Reset()                    { *m = CreateIntegrationResponse{} }
func (m *CreateIntegrationResponse) String() string            { return proto.CompactTextString(m) }
func (*CreateIntegrationResponse) ProtoMessage()               {}
func (*CreateIntegrationResponse) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

type GetIntegrationRequest struct {
	WebhookTransactionId string `protobuf:"bytes,1,opt,name=webhook_transaction_id,json=webhookTransactionId" json:"webhook_transaction_id,omitempty"`
}

func (m *GetIntegrationRequest) Reset()                    { *m = GetIntegrationRequest{} }
func (m *GetIntegrationRequest) String() string            { return proto.CompactTextString(m) }
func (*GetIntegrationRequest) ProtoMessage()               {}
func (*GetIntegrationRequest) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{3} }

func (m *GetIntegrationRequest) GetWebhookTransactionId() string {
	if m != nil {
		return m.WebhookTransactionId
	}
	return ""
}

type GetIntegrationResponse struct {
}

func (m *GetIntegrationResponse) Reset()                    { *m = GetIntegrationResponse{} }
func (m *GetIntegrationResponse) String() string            { return proto.CompactTextString(m) }
func (*GetIntegrationResponse) ProtoMessage()               {}
func (*GetIntegrationResponse) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{4} }

func init() {
	proto.RegisterType((*Status)(nil), "runecms.integration.Status")
	proto.RegisterType((*CreateIntegrationRequest)(nil), "runecms.integration.CreateIntegrationRequest")
	proto.RegisterType((*CreateIntegrationResponse)(nil), "runecms.integration.CreateIntegrationResponse")
	proto.RegisterType((*GetIntegrationRequest)(nil), "runecms.integration.GetIntegrationRequest")
	proto.RegisterType((*GetIntegrationResponse)(nil), "runecms.integration.GetIntegrationResponse")
}

func init() { proto.RegisterFile("rpc.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 265 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xac, 0x92, 0x4f, 0x4b, 0x03, 0x31,
	0x10, 0xc5, 0x59, 0xff, 0x2c, 0xee, 0x54, 0x11, 0x62, 0x2d, 0xab, 0xbd, 0x2c, 0x39, 0xf5, 0x62,
	0x0e, 0xad, 0x9f, 0x40, 0x0f, 0xd2, 0x83, 0x97, 0xd8, 0x93, 0x97, 0x92, 0xcd, 0x0e, 0x75, 0x11,
	0x93, 0x38, 0xc9, 0xd2, 0x4f, 0xe0, 0xf7, 0x96, 0x26, 0x85, 0x2e, 0xb2, 0x82, 0x07, 0x6f, 0xc9,
	0xbc, 0x47, 0xde, 0xef, 0x85, 0x81, 0x82, 0x9c, 0x16, 0x8e, 0x6c, 0xb0, 0xec, 0x8a, 0x3a, 0x83,
	0xfa, 0xc3, 0x8b, 0xd6, 0x04, 0xdc, 0x90, 0x0a, 0xad, 0x35, 0x7c, 0x05, 0xf9, 0x4b, 0x50, 0xa1,
	0xf3, 0x6c, 0x0c, 0xa7, 0x48, 0x64, 0xa9, 0xcc, 0xaa, 0x6c, 0x76, 0x26, 0xd3, 0x85, 0x55, 0x30,
	0x6a, 0xd0, 0x6b, 0x6a, 0xdd, 0xce, 0x5e, 0x1e, 0x55, 0xd9, 0xac, 0x90, 0xfd, 0x11, 0x63, 0x70,
	0xa2, 0x6d, 0x83, 0xe5, 0x71, 0x94, 0xe2, 0x99, 0x7f, 0x65, 0x50, 0x3e, 0x12, 0xaa, 0x80, 0xcb,
	0x43, 0x96, 0xc4, 0xcf, 0x0e, 0x7d, 0x60, 0xf7, 0x30, 0xd9, 0x62, 0xfd, 0x66, 0xed, 0xfb, 0x3a,
	0x90, 0x32, 0x5e, 0xe9, 0x9d, 0xba, 0x6e, 0x9b, 0x98, 0x5c, 0xc8, 0xf1, 0x5e, 0x5d, 0x1d, 0xc4,
	0x65, 0xc3, 0x16, 0x90, 0xfb, 0x08, 0x1a, 0x19, 0x46, 0xf3, 0xa9, 0x18, 0xa8, 0x23, 0x52, 0x17,
	0xb9, 0xb7, 0xf2, 0x29, 0xdc, 0x0c, 0x60, 0x78, 0x67, 0x8d, 0x47, 0xfe, 0x0c, 0xd7, 0x4f, 0x18,
	0xfe, 0x0b, 0x90, 0x97, 0x30, 0xf9, 0xf9, 0x5c, 0x0a, 0x9a, 0x6f, 0xe1, 0xbc, 0x37, 0xf6, 0x6c,
	0x03, 0x79, 0xa2, 0x62, 0x77, 0x83, 0x25, 0x7e, 0xfb, 0xb9, 0x5b, 0xf1, 0x57, 0x7b, 0x0a, 0x7e,
	0xb8, 0x7c, 0xbd, 0xe8, 0x19, 0x5d, 0x5d, 0xe7, 0x71, 0x13, 0x16, 0xdf, 0x01, 0x00, 0x00, 0xff,
	0xff, 0x9f, 0x08, 0x87, 0x9e, 0x16, 0x02, 0x00, 0x00,
}
