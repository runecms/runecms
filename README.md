Rune
====

You can find this, and the generated documentation [here](https://docs.gorune.io)

Installation
------------

Rune heavily relies on `kubernetes`. Kubernetes is an orchestration platform for container applications. Read more about kubernetes at [kubernetes.io](kubernetes.io).

An effort is being made to automate this install process, however it is not a priority until the infrastructure has reached a high level of stability.

* Ensure that `kubectl` is pointed to a Kubernetes cluster.
  * Don't have `kubectl` or a `kubernetes` cluster? Try setting up [minikube](https://kubernetes.io/docs/getting-started-guides/minikube/)

```
runecms/runecms ‹master*› » kubectl cluster-info
Kubernetes master is running at https://123.123.123.123
GLBCDefaultBackend is running at https://123.123.123.123/api/v1/namespaces/kube-system/services/default-http-backend/proxy
Heapster is running at https://123.123.123.123/api/v1/namespaces/kube-system/services/heapster/proxy
KubeDNS is running at https://123.123.123.123/api/v1/namespaces/kube-system/services/kube-dns/proxy
kubernetes-dashboard is running at https://123.123.123.123/api/v1/namespaces/kube-system/services/kubernetes-dashboard/proxy
Metrics-server is running at https://123.123.123.123/api/v1/namespaces/kube-system/services/metrics-server/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

#### Install CockroachDB

Follow the install guide here: https://www.cockroachlabs.com/docs/stable/orchestrate-cockroachdb-with-kubernetes.html

Rune configuration settings by default prefer the "Secure" mode with certificate authentication.

#### Install Redis

Rune uses Redis, as does our authorization / permission service "Ladon". We opt to use 2 separate Redis instances.

**TODO: Add basic Redis yaml files in the install folder**

* Configure the Rune environment variables at `internal/rune.config`. The defaults should be fine for a secure environment.

```
runecms/runecms ‹master*› » vim internal/kubernetes/rune-config.yaml
```

#### Install Rune

In the repository root, edit `internal/rune.confg`.

For information about these variables, see [this section]().


Configuration
------------

| Variable                         | Description                                              |
|----------------------------------|----------------------------------------------------------|
| `LADON_DATABASE_USER`            | sets the username for the Ladon database. (To be unused) |
| `LADON_DATABASE_PASSWORD`        | sets the password for the Ladon database. (To be unused) |
| `LADON_REDIS_PASSWORD`           | sets the password for the Ladon redis instance.          |
| `RUNE_DATABASE_CONNECTION_LIMIT` | defines the database connection limit PER SERVICE. Setting this to 10 will limit each running pod in the cluster to 10 |
| `RUNE_SMTP_HOST`                 |                                                          |
| `RUNE_SMTP_PORT`                 |                                                          |
| `RUNE_SMTP_USER`                 |                                                          |
| `RUNE_SMTP_PASS`                 |                                                          |
| `RUNE_BRAINTREE_PUBLIC_KEY`      |                                                          |
| `RUNE_BRAINTREE_PRIVATE_KEY`     |                                                          |
| `RUNE_BRAINTREE_MERCHANT_ID`     |                                                          |
| `RUNE_BRAINTREE_ENVIRONMENT`     |                                                          |


Repository
------------

Rune is built using microservices. Each folder represents a separately-deployed, individually-compiled service.

There are a few exceptions though:

| Path        | Description                                                                                                  |
|-------------|--------------------------------------------------------------------------------------------------------------|
| `/`         | These are Go packages which are intended to be used internally and externally                                |
| `/internal` | These are Go packages which are only used internally                                                         |
| `/cmd`      | These are command line utilities for use with Rune, either from within a service or from a user's terminal   |
| `/vendor`   | These are vendored packages shared across all of our individual services                                     |
| `/toools`   | These are basic scripts which we use to maintain a deployed Rune cluster (**TODO: Consider moving to /cmd**) |
| `/build`    | This is where Makefile pieces reside for our centralized Makefile system                                     |

Services
------------

Individual services also follow a structure. Ensure that the following requirements have been met when creating a new service:

| Path                     | Contents                                                                                                                     | Description                                                                                                                |
|--------------------------|------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------|
| `/{service}`             | This is where importable files in the service reside, as well as the folders for the service listed below                    |                                                                                                                            |
| `/{service}/Makefile`    | see [Makefile](#Makefile)                                                                                                    | The Makefile defines how to build, deploy, and test your service                                                           |
| `/{service}/Dockerfile`  | The Dockerfile for your service                                                                                              | This is required because all services are deployed via. docker                                                             |
| `/{service}/docs/`       | see [docs](https://docs.gorune.io/documentation/)                                                                            | The docs folder contains a list of markdown files; those files are then copied over to Hugo and deployed to docs.gorune.io |
| `/{service}/internal/`   | The `internal` folder contains the non-importable packages that your service uses; these are usually database functions      | Expect other applications to possibly import your packages                                                                 |
| `/{service}/cmd/`        | The `cmd` folder contains the code for the runnable applications of your service                                             | You will probably only have one of these for your twirp server                                                             |
| `/{service}/rpc/`        | The `rpc` folder contains the protobuf and the code generated by [Twirp](https://github.com/twitchtv/twirp) for your service |                                                                                                                            |
| `/{service}/kubernetes/` | The `kubernetes` folder contains the kubernetes yaml files for deploying your service                                        | You can optionally use basic templating (see `make deploy` in your Makefile)                                               |
| `/{service}/build_dir/`  |                                                                                                                              | This is where `make build`'s output is set; these files are then to be used by the Docker image                            |
| `/{service}/rootfs/`     |                                                                                                                              | These are (presumably static) files that are copied into your Docker image                                                 |

Documentation
------------

You can find Rune documentation [here](https://docs.gorune.io).

Infrastructure
------------

The foundational systems within which the application is to be built.

| Role                    | Software                                                                | Version  |
|-------------------------|-------------------------------------------------------------------------|----------|
| Containers              | [Docker](https://docker.com)                                            | N/A      |
| Container Orchestration | [Kubernetes](https://kubernetes.io)                                     | N/A      |
| Content Database        | [CockroachDB](https://www.cockroachlabs.com/)                           | N/A      |
| Monitoring              | [Prometheus](https://prometheus.io/)                                    | N/A      |
| Tracing                 | [Zipkin](https://github.com/openzipkin/zipkin)                          | N/A      |
| Image/Object Storage    | [Minio](https://minio.io/)                                              | N/A      |
| Service Mesh            | [Istio](https://istio.io)                                               | N/A      |

Architecture
------------

The components used in the design of the application.

| Role                    | Software                                                                |
|-------------------------|-------------------------------------------------------------------------|
| Data Queries            | [GraphQL](http://graphql.org)                                           |
| Transport               | [twirp](http://https://github.com/twitchtv/twirp)                       |


Makefile
--------

Each service uses the same Makefile. Whenever a change to the Makefile is made the change *must* be
propagated out to the other services' Makefiles to keep them in sync.

### Usage

<dl>
    <dt><code>make proto</code></dt>
    <dd>Compiles protocol buffer files located in <code>RPC_DIR</code>.</dd>
    <br>
    <dt><code>make build</code></dt>
    <dd>Compiles the Go binaries found in <code>CMD_DIR</code> and outputs them to <code>BUILD_DIR</code>.</dd>
    <br>
    <dt><code>make clean</code></dt>
    <dd>Deletes the <code>$BUILD_DIR</code> and its contents.</dd>
    <br>
    <dt><code>make docker</code></dt>
    <dd>Runs all build and deploy operations to create a Docker container and push it to the registry.</dd>
    <br>
    <dt><code>make test</code></dt>
    <dd>Runs <code>go test</code> on the service and its packages.</dd>
    <br>
    <dt><code>make lint</code></dt>
    <dd>Performs multiple code linting operations and shows the results.</dd>
    <br>
    <dt><code>make migrate</code></dt>
    <dd>Performs the database migrations for this service.</dd>
    <br>
    <dt><code>make migrate-to TARGET=[n]</code></dt>
    <dd>Performs the database migrations necessary to move the database to version <code>[n]</code>.</dd>
</dl>

### Variables

<dl>
    <dt><strong>PACKAGE</strong></dt>
    <dd>Contains the package name, which is derived from the path, including everything beyond <code>$GOPATH/src/git.runecms.io/</code>.</dd>
    <br>
    <dt><strong>SERVICE</strong></dt>
    <dd>Contains the service name, which is derived from the path, including everything beyond <code>$GOPATH/src/git.runecms.io/rune/runecms/</code>.</dd>
    <br>
    <dt><strong>GOOS</strong></dt>
    <dd>The target operating system to use when building the Go binary. Default <code>linux</code>.</dd>
    <br>
    <dt><strong>GOARCH</strong></dt>
    <dd>The target architecture to use when building the Go binary. Default <code>amd64</code>.</dd>
    <br>
    <dt><strong>DATE</strong></dt>
    <dd>The current date, e.g. <code>2017-11-21T11:59:11-0600</code>.</dd>
    <br>
    <dt><strong>VERSION</strong></dt>
    <dd>The current version, derived from the most recent git tag and commit hash, e.g. <code>v0.0.4-31-g4c75e10</code>. The commit hash is only included if there have been additional commits since the most recent tag.</dd>
    <br>
    <dt><strong>COMMIT</strong></dt>
    <dd>The current commit hash in short form, e.g. <code>4c75e10</code>.</dd>
    <dt><strong>RPC_DIR</strong></dt>
    <dd>The directory - relative to the Makefile - containing the <code>.proto</code> files.</dd>
    <br>
    <dt><strong>PROTOS</strong></dt>
    <dd>An array of the <code>.proto</code> files which were found in the <code>RPC_DIR</code> directory.</dd>
    <br>
    <dt><strong>ROOTFS</strong></dt>
    <dd>Default <code>./rootfs</code>. The directory - relative to the Makefile - containing static files and folders to be added to the docker container. These files are copied to <code>BUILD_DIR</code> with each build.</dd>
    <br>
    <dt><strong>BUILD_DIR</strong></dt>
    <dd>Default <code>./build_dir</code>. The directory - relative to the Makefile - to which binaries will be compiled and <code>ROOTFS</code> files and folders will be copied. Afterwards, the contents of <code>BUILD_DIR</code> will be copied to the Docker container.</dd>
    <br>
    <dt><strong>CMD_DIR</strong></dt>
    <dd>Default <code>./cmd</code>. The directory - relative to the Makefile - containing Go commands to be compiled to binaries.</dd>
    <br>
    <dt><strong>CMDS</strong></dt>
    <dd>An array of commands found in <code>CMD_DIR</code>.</dd>
    <br>
    <dt><strong>DOCKERFILE</strong></dt>
    <dd>Default <code>./Dockerfile</code>. The file - relative to the Makefile - to be used when building the docker container.</dd>
    <br>
    <dt><strong>DOCKER_REGISTRY_DOMAIN</strong></dt>
    <dd>Default <code>registry.runecms.io</code>. The Docker registry to push built Docker containers to.</dd>
    <br>
    <dt><strong>DOCKER_REGISTRY_USER</strong></dt>
    <dd>Default empty, automatically populated by CI.</dd>
    <br>
    <dt><strong>DOCKER_REGISTRY_PASS</strong></dt>
    <dd>Default empty, automatically populated by CI.</dd>
    <br>
    <dt><strong>DATABASE_HOST</strong></dt>
    <dd>Default <code>database-cockroachdb-public.default.svc.cluster.local</code>. The database host to use for migrations.</dd>
    <br>
    <dt><strong>DATABASE_PORT</strong></dt>
    <dd>Default <code>26257</code>. The port to use for database migrations.</dd>
    <br>
    <dt><strong>DATABASE_USER</strong></dt>
    <dd>Default <code>rune</code>. The username to use for database migrations.</dd>
    <br>
    <dt><strong>DATABASE_PASS</strong></dt>
    <dd>Default empty. The password to use for database migrations.</dd>
    <br>
    <dt><strong>DATABASE_SCHEMA</strong></dt>
    <dd>Default <code>runecms</code>. The database/schema to use for databse migrations.</dd>
    <br>
    <dt><strong>DATABASE_MIGRATIONS_TABLE</strong></dt>
    <dd>Default <code>schema_$(SERVICE)_migrations</code>. The name of the table to use to track migrations for this service.</dd>
    <br>
    <dt><strong>DATABASE_MIGRATIONS_LOCK_TABLE</strong></dt>
    <dd>Default <code>schema_$(SERVICE)_lock</code>. The name of the table to use for migration locks, to prevent simultaneous migration operations.</dd>
    <br>
    <dt><strong>DATABASE_SSL_MODE</strong></dt>
    <dd>Default <code>disable</code>. The <code>sslmode</code> to use when establishing a connection to the migrations database.</dd>
    <br>
    <dt><strong>MIGRATIONS_DIR</strong></dt>
    <dd>Default <code>./db</code>. The directory - relative to the Makefile - containing the migration steps for this service.</dd>
    <br>
    <dt><strong>MIGRATE_DATABASE</strong></dt>
    <dd>The migration database connection string, default <code>cockroachdb://$(DATABASE_USER):$(DATABASE_PASS)@$(DATABASE_HOST):$(DATABASE_PORT)/$(DATABASE_SCHEMA)?sslmode=$(DATABASE_SSLMODE)&x-migrations-table=$(DATABASE_MIGRATIONS_TABLE)&x-lock-table=$(DATABASE_MIGRATIONS_LOCK_TABLE)</code></dd>
</dl>

