// Copyright (C) 2018 WEHCO Media, Inc.
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Package rune is used to create consistent errors across Rune services
package runecms

import (
	"net/http"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/twitchtv/twirp"
)

var (
	AlreadyExists    = "The infomration provided already exists"
	Unauthorized     = "The provided user is not allowed to access this resource"
	PermissionDenied = "The provided user is not allowed to access this resource"
	InvalidArgument  = "The information provided is invalid"
	NotFound         = "The requested resource was not found or has been deleted"
	Internal         = "Our service encountered an error unrelated to the input provided"
	Unknown          = Internal
)

// GetGRPCStatusCode returns the grpc status code defined in https://github.com/grpc/grpc/blob/master/doc/statuscodes.md based on the or
func (err RuneError) GetGRPCStatusCode() codes.Code {
	switch err.Err {
	case PermissionDenied:
		return codes.PermissionDenied
	case InvalidArgument:
		return codes.InvalidArgument
	case NotFound:
		return codes.NotFound
	default:
		return codes.Internal
	}
}

// GetHTTPStatusCode returns the http status code based on the or
func (err RuneError) GetHTTPStatusCode() int {
	switch err.Err {
	case PermissionDenied:
		return http.StatusUnauthorized
	case InvalidArgument:
		return http.StatusBadRequest
	case NotFound:
		return http.StatusNotFound
	default:
		return http.StatusInternalServerError
	}
}

// GetTwirporCode returns the twirp error code defined in https://github.com/twitchtv/twirp/wiki/ors based on the or
func (err RuneError) GetTwirpErrorCode() twirp.ErrorCode {
	switch err.Err {
	case PermissionDenied:
		return twirp.PermissionDenied
	case InvalidArgument:
		return twirp.InvalidArgument
	case NotFound:
		return twirp.NotFound
	case AlreadyExists:
		return twirp.NotFound
	default:
		return twirp.Internal
	}
}

// GetGRPCor returns a gRPC error using the `status` package
func (err RuneError) GetGRPCError() error {
	return status.Error(err.GetGRPCStatusCode(), err.Error())
}

// GetTwirpor returns a Twirp error using the `twirp` package
func (err RuneError) GetTwirpError() error {
	return twirp.NewError(err.GetTwirpErrorCode(), err.Error())
}
